/*************Show Asset List Based on Asset Status*****************/
function report_store_asset() {
    var office_id = '';
    if ($("#office_id").length != 0) {
        office_id = document.getElementById("office_id").value;
    }
    var filter = {
        'asset_name': document.getElementById("asset_name").value,
        'category_id': document.getElementById("category_id").value,
        'from': document.getElementById("from").value,
        'to': document.getElementById("to").value,
        'lifetime': document.getElementById("lifetime").value,
        'office_id': office_id,
        'funded_by': document.getElementById("funded_by").value,
    }
    //alert(asset_status);

    $.ajax({
        type: "POST",
        data: {filter: filter},
        url: globalserver + 'ajax2/report_store_asset',
        success: function (msg) {
            //alert(msg);
            $("#abc").html(msg);
        }
    });

    return false;
}

/*************Show Allocation List *****************/
function report_allocation_asset() {
    //alert("Hi");  
    var office_id = '';
    if ($("#office_id").length != 0) {
        office_id = document.getElementById("office_id").value;
    }
    var filter = {
        'asset_name': document.getElementById("asset_name").value,
        'category_id': document.getElementById("category_id").value,
        'from': document.getElementById("from").value,
        'to': document.getElementById("to").value,
        'office_id': office_id,
        'asset_label': document.getElementById("asset_label").value,
        'user_name': document.getElementById("user_name").value
    }
    //alert("asset_status");

    $.ajax({
        type: "POST",
        data: {filter: filter},
        url: globalserver + 'ajax3/report_allocation_asset',
        success: function (msg) {
            //alert(msg);
            $("#abc").html(msg);

        }
    });

    return false;
}

/********* Asset Information based on different filtering **************/

function report_asset() {
    //alert("Hi");  
    var office_id = '';
    if ($("#office_id").length != 0) {
        office_id = document.getElementById("office_id").value;
        //alert(office_id);
    }
    var filter = {
        'asset_name': document.getElementById("asset_name").value,
        'category_id': document.getElementById("category_id").value,
        'status_id': document.getElementById("status_id").value,
        'from': document.getElementById("from").value,
        'to': document.getElementById("to").value,
        'lifetime': document.getElementById("lifetime").value,
        'office_id': office_id,
        'asset_label': document.getElementById("asset_label").value,
        'user_name': document.getElementById("user_name").value,
        'funded_by': document.getElementById("funded_by").value,
    }
    //alert("asset_status");

    $.ajax({
        type: "POST",
        data: {filter: filter},
        url: globalserver + 'ajax3/report_asset_info',
        success: function (msg) {
            //alert(msg);
            $("#abc").html(msg);

        }
    });

    return false;

}

