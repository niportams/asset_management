//var globalserver="http://localhost/asset_management/";
//var globalserver =  document.getElementById("globalserver").value;

/*
 $(document).ready(function() {
 $("#office_add").submit(function(e) {
 event.preventDefault();
 alert("Form submitted");
 });
 });
 */

/*
 function getData(obj) 
 {
 var myData = obj.data, i, output = '';
 
 for (i = 0; i < myData.length; i += 1) {
 for (key in myData[i]) {
 output += key + " : " + myData[i][key];
 }       
 }
 return output;
 }
 */
/**********Office Validation**************/
function office_validation() {
    var item = "{";
    for (var i = 0; i < office.length; i++) {
        //var obj = json[i];
        //console.log(obj.id);
        //var key_name = office[i].id;
        //var key_required = office[i].required;
        //alert(key_required);
        //var message = office[i].message;
        // alert(key_name);

        office[i].value = document.getElementById(office[i].id).value;

        if ((validation_json(office[i].id, office[i].type, office[i].required, office[i].message)) === 0)
            return false;
        else
            item += '"' + office[i].field_name + '":"' + office[i].value + '",';
    }
    item = item.slice(0, -1);
    item += "}";

    //console.log(item); 
    var jsonString = JSON.parse(item);
    jsonString = JSON.stringify(jsonString);
    //console.log(jsonString);

    $.ajax({
        type: "POST",
        data: {jsondata: jsonString},
        url: globalserver + 'ajax/office_submit',
        success: function (msg) {
            sci_alert("", msg, "success");
            $("#office_form").load(" #office_form");
        }
    });

}

/************** Office edit***************/
function office_edit(office_id) {
    // alert("office_id");
    var postData =
        {
            "office_id": office_id
        };
    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax/office_update',
        success: function (msg) {
            //alert(msg);
            $("#office_edit").html(msg);
        }
    });
    return false;
}

/************** Room edit***************/
function room_edit(room_id) {
    // alert("office_id");
    var postData =
        {
            "room_id": room_id
        };
    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax3/room_update',
        success: function (msg) {
            //alert(msg);
            $("#room_edit").html(msg);
        }
    });
    return false;
}

/***********Update Office submit**************/
function update_office_submit(office_id) {
    //alert(office_id);
    var item = "{";
    item += '"office_id":"' + office_id + '",';
    //console.log(item);
    for (var i = 0; i < office.length; i++) {
        office[i].value = document.getElementById(office[i].id).value;
        if ((validation_json(office[i].id, office[i].type, office[i].required, office[i].message)) === 0)
            return false;
        else
            item += '"' + office[i].field_name + '":"' + office[i].value + '",';

        /*
         if((office[i].required==="true")&&(office[i].value==""))
         {
         validation_json(office[i].id,office[i].message);  
         return false; 
         }
         else
         {
         item += '"'+office[i].field_name+'":"'+office[i].value+'",'; 
         }
         */
    }
    item = item.slice(0, -1);
    item += "}";

    //console.log(item); 
    var jsonString = JSON.parse(item);
    jsonString = JSON.stringify(jsonString);
    //console.log(jsonString);
    $.ajax({
        type: "POST",
        data: {jsondata: jsonString},
        url: globalserver + 'office/office_edit',
        success: function (msg) {
            //alert(msg);
            sci_alert(msg, "msg", "success");
            $("#example1").load(" #example1");
        }
    });
}

/**************Office Delete***************/
function office_delete(office_id) {
    //alert(office_id); 

    var postData =
        {
            "office_id": office_id
        };

    var dataString = JSON.stringify(postData);
    sci_confirm("Are you sure to delete this office?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {jsondata: dataString},
                url: globalserver + 'office/office_delete',
                success: function (msg) {
                    //alert(msg);
                    sci_alert(msg, "", "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;

}

/**************Room Delete***************/
function room_delete(room_id) {
    //alert(office_id); 

    var postData =
        {
            "room_id": room_id
        };

    var dataString = JSON.stringify(postData);
    sci_confirm("Are you sure to delete this room?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {jsondata: dataString},
                url: globalserver + 'office/deleteroom',
                success: function (msg) {

                    sci_alert(msg, "", "success");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;

}

/**************Department Validation******************/
function department_validation() {
    var department_name = document.getElementById("department_name").value;

    if ((validation_rule(department_name, 'department_name', "Enter Department Name")) == 0) {
        return false;
    }
    var postData =
        {
            "department_name": department_name
        };

    var dataString = JSON.stringify(postData);
    //alert(dataString);

    $.ajax({
        type: "POST",
        //dataType: "json",
        data: {deptdata: dataString},
        url: globalserver + 'ajax/department_submit',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            $("#department_form").load(" #department_form ");
        }
    });
}

/**********Department Update***********/
function department_edit(dept_id) {
    //alert(dept_id); 
    var postData =
        {
            "department_id": dept_id
        };

    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax/department_update',
        success: function (msg) {
            //alert(msg);
            $("#department_edit").html(msg);
        }
    });
    return false;
}

/********Department update submit************/
function update_department_submit(dept_id) {
    var department_name = document.getElementById("department_name").value;
    var postData =
        {
            "department_id": dept_id,
            "department_name": department_name
        };
    var dataString = JSON.stringify(postData);

    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'department/department_edit',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            //$("#category_edit").html(msg);
            $("#example1").load(" #example1");
        }
    });
}

/**************Office Delete***************/
function department_delete(dept_id) {
    var postData =
        {
            "department_id": dept_id
        };
    var dataString = JSON.stringify(postData);
    sci_confirm("Are you sure to delete this department?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {jsondata: dataString},
                url: globalserver + 'department/department_delete',
                success: function (msg) {
                    //alert(msg);
                    sci_alert("", msg, "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;
}

/**************Supplier Create***************/
function supplier_validation() {

    var supplier_name = document.getElementById("supplier_name").value;
    var supplier_address = document.getElementById("supplier_address").value;
    var supplier_phone = document.getElementById("supplier_phone").value;
    var supplier_email = document.getElementById("supplier_email").value;

    if ((validation_rule(supplier_name, 'supplier_name', "Enter Supplier Name")) == 0) {
        return false;
    }
    if ((validation_rule(supplier_address, 'supplier_address', "Enter Supplier Address")) == 0) {
        return false;
    }
    if ((validation_rule(supplier_phone, 'supplier_phone', "Enter Supplier Phone")) == 0) {
        return false;
    }

    var postData =
        {
            "supplier_name": supplier_name,
            "supplier_address": supplier_address,
            "supplier_phone": supplier_phone,
            "supplier_email": supplier_email
        };

    var dataString = JSON.stringify(postData);

    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax/supplier_create',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            $("#supplier_form").load(" #supplier_form ");
        }
    });
}


/**************Supplier Edit****************/
function supplier_edit(supplier_id) {
    var postData =
        {
            "supplier_id": supplier_id
        };

    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax/supplier_update',
        success: function (msg) {
            //alert(msg);
            $("#supplier_edit").html(msg);
        }
    });
    return false;
}

/**************Manufacture Edit****************/
function manufacture_edit(manufacture_id) {
    var postData =
        {
            "manufacture_id": manufacture_id
        };

    var dataString = JSON.stringify(postData);

    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax/manufacture_update',
        success: function (msg) {
            //alert(msg);
            $("#manufacture_edit").html(msg);
        }
    });
    return false;
}

/************Supplier Edit Submit***************/
function update_supplier_submit(supplier_id) {
    var supplier_name = document.getElementById("supplier_name").value;
    var supplier_address = document.getElementById("supplier_address").value;
    var supplier_phone = document.getElementById("supplier_phone").value;
    var supplier_email = document.getElementById("supplier_email").value;

    if ((validation_rule(supplier_name, 'supplier_name', "Enter Supplier Name")) == 0) {
        return false;
    }
    if ((validation_rule(supplier_address, 'supplier_address', "Enter Supplier Address")) == 0) {
        return false;
    }
    if ((validation_rule(supplier_phone, 'supplier_phone', "Enter Supplier Phone")) == 0) {
        return false;
    }

    var postData =
        {
            "supplier_id": supplier_id,
            "supplier_name": supplier_name,
            "supplier_address": supplier_address,
            "supplier_phone": supplier_phone,
            "supplier_email": supplier_email
        };
    var dataString = JSON.stringify(postData);

    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'supplier/supplier_edit',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            $("#example1").load(" #example1");
        }
    });

}

/**********Supplier Delete*************/
function supplier_delete(supplier_id) {
    var postData =
        {
            "supplier_id": supplier_id
        };

    var dataString = JSON.stringify(postData);
    sci_confirm("Are you sure to delete this supplier?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {jsondata: dataString},
                url: globalserver + 'supplier/supplier_delete',
                success: function (msg) {
                    //alert(msg);
                    sci_alert("", msg, "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;
}

/**************Manufacture Create***************/
function manufacture_validation() {
    var manufacture_name = document.getElementById("manufacture_name").value;
    var manufacture_code = document.getElementById("manufacture_code").value;
    var manufacture_address = document.getElementById("manufacture_address").value;
    var manufacture_phone = document.getElementById("manufacture_phone").value;
    var manufacture_email = document.getElementById("manufacture_email").value;

    if ((validation_rule(manufacture_name, 'manufacture_name', "Enter Manufacture Name")) == 0) {
        return false;
    }
    if ((validation_rule(manufacture_phone, 'manufacture_phone', "Enter Manufacture Phone")) == 0) {
        return false;
    }

    var postData =
        {
            "manufacture_name": manufacture_name,
            "manufacture_code": manufacture_code,
            "manufacture_address": manufacture_address,
            "manufacture_phone": manufacture_phone,
            "manufacture_email": manufacture_email
        };
    var dataString = JSON.stringify(postData);

    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax/manufacture_create',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            $("#manufacture_form").load(" #manufacture_form ");
        }
    });

}

function update_manufacture_submit(manufacture_id) {

    var manufacture_name = document.getElementById("manufacture_name").value;
    var manufacture_code = document.getElementById("manufacture_code").value;
    var manufacture_address = document.getElementById("manufacture_address").value;
    var manufacture_phone = document.getElementById("manufacture_phone").value;
    var manufacture_email = document.getElementById("manufacture_email").value;

    if ((validation_rule(manufacture_name, 'manufacture_name', "Enter Manufacture Name")) == 0) {
        return false;
    }
    if ((validation_rule(manufacture_phone, 'manufacture_phone', "Enter Manufacture Phone")) == 0) {
        return false;
    }

    var postData =
        {
            "manufacture_id": manufacture_id,
            "manufacture_name": manufacture_name,
            "manufacture_code": manufacture_code,
            "manufacture_address": manufacture_address,
            "manufacture_phone": manufacture_phone,
            "manufacture_email": manufacture_email
        };
    var dataString = JSON.stringify(postData);

    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'manufacture/manufacture_edit',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            $("#example1").load(" #example1");
        }
    });
}

/**************Manufacture Delete***************/
function manufacture_delete(manufacture_id) {

    var postData =
        {
            "manufacture_id": manufacture_id
        };

    var dataString = JSON.stringify(postData);

    sci_confirm("Are you sure to delete this Manufacture?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {jsondata: dataString},
                url: globalserver + 'manufacture/manufacture_delete',
                success: function (msg) {
                    sci_alert("", msg, "success");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;
}

/**********Organization Update***********/
function organization_edit(org_id) {
    //alert(dept_id);
    var postData =
        {
            "organization_id": org_id
        };

    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax3/organization_update',
        success: function (msg) {
            //alert(msg);
            $("#organization_edit").html(msg);
        }
    });
    return false;
}


/**************organization Delete***************/
function organization_delete(org_id) {
    var postData =
        {
            "organization_id": org_id
        };
    var dataString = JSON.stringify(postData);
    sci_confirm("Are you sure to delete this department?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {jsondata: dataString},
                url: globalserver + 'organization/organization_delete',
                success: function (msg) {
                    //alert(msg);
                    sci_alert("", msg, "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;
}

//FORM VALIDATON updated by shahed 30/12/17
/*
function form_validation(url, json_obj, form_id) {
    
   // alert("user");
     
    var jsonData = {};

    for (var i = 0; i < json_obj.length; i++)
    {

        json_obj[i].value = document.getElementById(json_obj[i].id).value;
        if ((validation_json(json_obj[i].id, json_obj[i].type, json_obj[i].required, json_obj[i].message)) === 0)
        {
            return false;
        }
        else
        {
            $("#" + json_obj[i].id).removeClass('validation_error');
            //removeSpecialChars(id_value); 
            jsonData[office[i].field_name] = removeSpecialChars(office[i].value);
        }
    }

    jsonString = JSON.stringify(jsonData);
    $.ajax({
        type: "POST",
        data: {jsondata: jsonString},
        url: globalserver + url,
        success: function (msg)
        {
            msgOffset = msg.split(',');

            sci_alert(msgOffset[0], msgOffset[1], "info");

            form_id = "#" + form_id;
            $(form_id).load(" " + form_id);
        }
    });
   

}
 */
 