function reference_details(id)
{
 //alert(id);
  var postData =
            {
                "reference_no": id
            };
    var dataString = JSON.stringify(postData);
    $.ajax({
        type:"POST",
        data: {jsondata: dataString},
        url: globalserver+'ajax3/reference_details',    
        success: function(msg)
        {
          $("#reference_details").html(msg);
        }
      }); 
return false;
}

/************* Allocate available item ************/
function allocate_item(index,request_id,category_id,quantity)
{
//alert('#rstatus'+index);
var reference_no =  document.getElementById("reference_no").value;
var on_behalf =  document.getElementById("on_behalf").value;
var requested_user =  document.getElementById("requested_user").value;

//alert(reference_no_new);
//alert(on_behalf_new);
//exit();

var asset_location =  document.getElementById("asset_location").value;
var approved_by =  document.getElementById("approved_by").value;
var value_context =  document.getElementById("value_context").value;
var remarks =  document.getElementById("remarks").value;

  if(approved_by=="")
    {
    sci_alert("Please select approver from 'Allocation Approver'","","info");
    document.getElementById("approved_by").focus();
    document.getElementById("approved_by").style.background = "pink";
    return false;
    }

  $.ajax({
    type:"POST",
    data:{request_id:request_id,category_id:category_id,quantity:quantity,reference_no:reference_no,
    requested_user:requested_user,on_behalf:on_behalf,asset_location:asset_location,
    approved_by:approved_by,value_context:value_context,remarks:remarks}, 
    url: globalserver+'ajax3/singleallocate',

    success: function(msg)
    {
        sci_alert("",msg,"success");
        //alert('"'+'#rstatus'+index +'"');
        var abc = $('#rstatus'+index);
        abc.html("<font color='green'>Allocated</font>"); 
        var xyz = $('#rtest'+index);
        xyz.html("<a href='#' class='btn btn-success btn-xs'> <i class='fa fa-undo'>Revert</i> </a>"); 
    
    }
    }); 
return false;
}

/********* Asset Allocation from Multiple checkbox**************/
function new_allocate(approved_by,request_id,category_id,quantity,office_id,requested_by,
asset_location,on_behalf,requisition_pkid,index)
{

  var checkedValue = [];
  var check_string = "";

  var allocate_key = document.getElementsByName("asset_check[]");
  var l = allocate_key.length;
    
    //alert(on_behalf);
    //alert(requested_user);
    //alert(asset_location);
 
  var count = 0; var k = 0;
    for (var i = 0; i < l; i++) {
        if (allocate_key[i].checked) {
            check_string = check_string + allocate_key[i].value + ',';
            //alert(check_string);
            k = k + 1;
        }
        else
        {
         count = count + 1;
        }
            
    }
    //alert(count);
    if (count == l) {
        //alert('Check at least one checkbox to allocate.');
        sci_alert("Please check at least one checkbox to allocate.","", "info");
        return false;
    }
    //alert(k);
    if (k != quantity) {
        //alert('Check at least one checkbox to allocate.');
        sci_alert("Please check " +quantity+ " item to allocate.","", "info");
        return false;
    }

    $.ajax({
        type:"POST",
        data:{check_string:check_string,approved_by:approved_by,requisition_id:request_id,category_id:category_id,
        asset_qty:quantity,office_id:office_id,allocate_to:requested_by,asset_location:asset_location,
        on_behalf:on_behalf,requisition_pkid:requisition_pkid},
        url: globalserver+'allocation/choice_allocate',
        success: function(msg)
          {
            $("#alc_button").html("Item is allocating...");
            sci_alert(msg, "","success");  
            $("#alc_button").html("<font color='green'>Allocated</font>"); 
            $('#allocation_status'+index).html("<font color='blue'>Allocated</font>"); 
            $('#modal-itemlist').modal('hide');
          }
        });

return false;
}

/********* Asset Transfer from Multiple checkbox**************/
function new_transfer(request_id,category_id,quantity,office_id,requested_by,
requisition_pkid,index)
{

  var checkedValue = [];
  var check_string = "";

  var allocate_key = document.getElementsByName("asset_check[]");
  var l = allocate_key.length;
    
    //alert(on_behalf);
    //alert(requested_user);
    //alert(asset_location);
 
  var count = 0; var k = 0;
    for (var i = 0; i < l; i++) {
        if (allocate_key[i].checked) {
            check_string = check_string + allocate_key[i].value + ',';
            //alert(check_string);
            k = k + 1;
        }
        else
        {
         count = count + 1;
        }
            
    }
    //alert(count);
    if (count == l) {
        //alert('Check at least one checkbox to allocate.');
        sci_alert("Please check at least one checkbox to allocate.","", "info");
        return false;
    }
    //alert(k);
    if (k != quantity) {
        //alert('Check at least one checkbox to allocate.');
        sci_alert("Please check " +quantity+ " item to allocate.","", "info");
        return false;
    }

    $.ajax({
        type:"POST",
        data:{check_string:check_string,requisition_id:request_id,category_id:category_id,
        asset_qty:quantity,office_id:office_id,allocate_to:requested_by,
        requisition_pkid:requisition_pkid},
        url: globalserver+'allocation/choice_transfer',
        success: function(msg)
          {
            $("#alc_button").html("Item is transfering...");
            sci_alert(msg, "","success");  
            $("#alc_button").html("<font color='green'>Transferred</font>"); 
            $('#transfer_status'+index).html("<font color='blue'>Transferred</font>"); 
            $('#modal-itemlist').modal('hide');
          }
      });

return false;
}


/**************** Allocation Submit of all selected checkboxes *********************/

function allocation_submit()
{
    //var checkedValue = [];
    //var check_string = "";

    var reference_no =  document.getElementById("reference_no").value;
    var on_behalf =  document.getElementById("on_behalf").value;
    var requested_user =  document.getElementById("requested_user").value;
    //exit();

    var asset_location =  document.getElementById("asset_location").value;
    var approved_by =  document.getElementById("approved_by").value;
    var value_context =  document.getElementById("value_context").value;
    var remarks =  document.getElementById("remarks").value;

    //var allocate_key = document.getElementsByName("allocate[]");
    //var l = allocate_key.length;
    
    //alert(on_behalf);
    //alert(requested_user);
    //alert(asset_location);
 /*
    var count = 0;
    for (var i = 0; i < l; i++) {
        if (allocate_key[i].checked) {
            check_string = check_string + allocate_key[i].value + ',';
            //alert(check_string);
            //k++;
        }
        else
        {
         count = count + 1;
        }
            
    }
    //alert(count);
    if (count == l) {
        //alert('Check at least one checkbox to allocate.');
        sci_alert("Please check at least one checkbox to allocate.","", "info");
        return false;
    }
*/
    if(approved_by=="")
    {
        sci_alert("Please select approver from 'Allocation Approver'","","info");
        document.getElementById("approved_by").focus();
        document.getElementById("approved_by").style.background = "pink";
        return false;
    }

   // alert("hello");
     $.ajax({
      type: "POST",
      data: {reference_no: reference_no,approved_by:approved_by,on_behalf:on_behalf,requested_user:requested_user,
      asset_location:asset_location,value_context:value_context,remarks:remarks},
      url: globalserver + 'ajax3/allocation_submit_all',
      success: function (msg) {
        //alert(msg);
        if(msg == 0)
        {
          sci_alert("Item is not available to allocate","", "info"); 
          return false; 
        }
        else
        {
        sci_alert("","Asset is Allocated Successfully", "success");    
        }
        
        $("#item_list").html(msg);
        }
    });
    return false;
}

/*********Asset Reverting from Allocation Screen ************/
function revert_now(request_id)
{
    //alert("Hello!");
    alert(request_id);
    return false;
}

/*********** Asset Allocation Update  *************/
function allocation_update(reference_no,allocation_id)
{

  var postData =
            {
                "reference_no": reference_no,
                "allocate_id": allocation_id,
            };
            
  var dataString = JSON.stringify(postData);
  $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax3/allocation_edit',
        success: function (msg) {
            //alert(msg);
           $("#allocated_location_update").html(msg); 

        }
    });

    return false;
}

/*************** Asset Allocation Update Location Submit***************/

function allocation_update_submit(allocate_id)
{
  var location_id =  document.getElementById("asset_location").value;
  //alert(location_id);
  //alert(allocate_id);

  $.ajax({
        type: "POST",
        data: {allocate_id: allocate_id,location_id:location_id},
        url: globalserver + 'allocation/allocation_edit',
        success: function (msg) {

            sci_alert("",msg,"success");
            $("#example1").load(" #example1");

        }
    });

    return false;
}

/*********** Asset Revert Form *************/
function asset_revert_form(reference_no,allo_id,revert_id,revert_type)
{
    //alert("Hello!");
    $.ajax({
        type: "POST",
        data: {reference_no:reference_no,allo_id:allo_id,revert_id:revert_id,revert_type:revert_type},
        url: globalserver + 'ajax3/allocation_revert_form',
        success: function (msg) {
            //alert(msg);
           $("#allocation_revert").html(msg); 

        }
    });

    return false;
}

/************ Cancel Allocation within 1 day ****************/
function cancel_allocation(request_id,reference_no,category_id)
{
    //alert(request_id);
    //alert(reference_no);
  sci_confirm("Are you sure to cancel this allocation?", "Confirmation", function(res)
  {
    if (res)
    {
    $.ajax({
        type: "POST",
        data: {request_id: request_id,reference_no:reference_no,category_id:category_id},
        url: globalserver + 'ajax3/allocation_revert_all',
        success: function (msg) {
            //alert(msg);
            sci_alert("Asset Allocation is Canceled","", "info");
            $("#item_list").html(msg);
        }
    });
  }
  });

return false;
}

/**************Generate PR List*************/
function generate_pr_list(reference_no)
{
    //alert(reference_no);

    $.ajax({
    type:"POST",
    data: {ref_no: reference_no},
    url: globalserver+'ajax3/pr_list',    
    success: function(msg)
        {
         //$("#pr_list").html(msg);
         if(msg == 1) /***** Unavailable items found*****/
         {
           //alert(" pr request proceed"); 
           pr_request_all(reference_no);
           return false;
         }
         else /* Unavailable items not found  */
         {
            //alert("There is no un available items found in your selected list");
            return false;
         }
        }
    }); 
    //return false;          
}

/*********** Generate PR Request for all item****************/
function pr_request_all(reference_no)
{
//alert(reference_no);

sci_confirm("Do you want to raise PR for unavailable items?", "Confirmation", function(res){
    if (res)
            //sci_alert('message will go here');
          {
            $.ajax({
            type:"POST",
            data: {ref_no: reference_no},
            url: globalserver+'ajax3/pr_submit_all',    
            success: function(msg)
             {
                //$("#reference_details").html(msg);
                if(msg != 0)
                {
                  sci_alert(msg,"","success");  
                }
                else
                {
                  sci_alert("Please select item first to raise PR ","","info");    
                }
                // var abc = $('#prstatus'+index);
               // abc.html("<font color='red'>Not Available</font> (PR Generated)"); 
             }
            }); 
          }
          
      });
return false;
}


/************** Generate PR Request for new Item  ****************/
/*
function request_pr(category_name,quantity,index)
{

  var ref_no =  document.getElementById("ref_no").value;
  
  //alert(category_name);
  //alert(quantity);

  sci_confirm("Are you sure to raise this PR?", "Confirmation", function(res){
    if (res)
            //sci_alert('message will go here');
          {

            $.ajax({
            type:"POST",
            data: {ref_no: ref_no,category_name: category_name,quantity: quantity},
            url: globalserver+'ajax3/pr_submit',    
            success: function(msg)
            {
                //$("#reference_details").html(msg);
                sci_alert(msg,"","success");
                var abc = $('#prstatus'+index);
                abc.html("<font color='red'>Not Available</font> (PR Generated)"); 
            }
            }); 

          }
      });

  return false;  
}
*/

/*************** Show Attachment of this requisition  ********************/
function revert_attachment(allocate_id)
{

  //alert(allocate_id);

          $.ajax({
            type:"POST",
            data: {allocate_id: allocate_id},
            url: globalserver+'ajax3/get_revert_files',    
            success: function(msg)
            {
            //sci_alert("", msg,"info"); 

             //sci_alert("", "Item is deleted","success");
            //$("#selected_asset").refresh();
             $("#attached_files").html(msg);
            }
            }); 
          return false;
 }
 /************* Show attachment of a disposal *******************/
 function dispose_attachment(asset_id)
 {

          $.ajax({
            type:"POST",
            data: {asset_id: asset_id},
            url: globalserver+'ajax3/get_disposed_files',    
            success: function(msg)
            {
            //sci_alert("", msg,"info"); 

             //sci_alert("", "Item is deleted","success");
            //$("#selected_asset").refresh();
             $("#attached_files").html(msg);
            }
            }); 
          return false;
 }

/************* Check Availability of an allocation request ***************/
function check_available(index,req_id,category_id,quantity)
{

//alert(category_id);
//alert(quantity);
//alert(reference_no);

var reference_no =  document.getElementById("reference_no").value;
$.ajax({
    type:"POST",
    data: {reference_no: reference_no,category_id: category_id,quantity: quantity},
    url: globalserver+'ajax3/request_available_check',    
    success: function(msg)
    {
        //$("#reference_details").html(msg);
        if(msg == 0)
        {
        var abc = $('#rstatus'+index);
        abc.html("<font color='green'>Available</font>"); 

        var xyz = $('#rtest'+index);

        a ='<input type="checkbox" name="allocate[]"';
        a +=' value="'+req_id+'"';
        a +='> Check to allocate';
        /*
        a ='<a href="#" class="btn btn-success btn-xs allocatet" ';
        a +=' data-index="'+index+'"';
        a +=' data-request_id="'+req_id+'"';
        a +=' data-category_id="'+category_id+'"';
        a +=' data-quantity="'+quantity+'"><i class="fa fa-send">Allocate</i></a>';
        */
        xyz.html(a);    
        }
        else
        {
          sci_alert(msg,"","info");  
        }

    }
    }); 
return false;
}

/*************** Revert file deletion*****************/
function alloc_file_delete(file_id,file_name,reference_no)
{
  //alert(reference_number);
  sci_confirm("Are you sure to delete this file?", "Confirmation", function(res){
        if (res)
          {
            $.ajax({
            type:"POST",
            data: {file_id: file_id,file_name:file_name,reference_no:reference_no},
            url: globalserver+'ajax3/alloc_file_delete',    
            success: function(data)
            {
            //sci_alert("", msg,"success");
            $('#uploaded_image').html(data);
            }
            }); 

          }
      });
  return false;
}
/*************** Revert file deletion*****************/
function revert_file_delete(file_id,file_name,reference_number,allocate_id,purpose)
{
  //alert(reference_number);
  sci_confirm("Are you sure to delete this file?", "Confirmation", function(res){
        if (res)
          //sci_alert('message will go here');
          {

            $.ajax({
            type:"POST",
            data: {file_id: file_id,file_name:file_name,reference_number:reference_number,allocate_id:allocate_id,
            purpose:purpose},
            url: globalserver+'ajax3/revert_file_delete',    
            success: function(data)
            {
            //sci_alert("", msg,"success");
            $('#reverted_image').html(data);
            }
            }); 

          }
      });
}
/***********Allocation Item Search***************/

function allocation_asset_search()
{
  var asset_search =  document.getElementById("asset_search").value;

  if(asset_search=='')
    {
        sci_alert("Please Enter Asset ID","","warning");
        document.getElementById("asset_search").focus();
        document.getElementById("asset_search").style.background = "#e2eef5";
        return false;
    }

    $.ajax({
        type:"POST",
        data:{asset_search:asset_search},
        url: globalserver+'ajax2/allocation_asset_search',
        success: function(msg)
        {
            if(msg==1)
            {
              sci_alert("Sorry! Invalid Asset ID. Please Try Again!","","warning");
            }
            else
            {
              $("#asset_info_form").html(msg);
            }
        }
    });
    return false;      

}

/***********Single Item Allocation from Asset List************/
function single_asset_allocation(asset_encrypt,label_id,category_id)
{
//alert(category_id);
$.ajax({
        type:"POST",
        data:{asset_encrypt:asset_encrypt,label_id:label_id,category_id:category_id},
        url: globalserver+'ajax2/single_allocation',
        success: function(msg)
        {
           $("#asset_allocate").html(msg);
        }
    });
  return false;      

}
/*************  Single direct allocation submit ****************/
function direct_allocation_submit(form_id)
{

    var asset_id =  document.getElementById("asset_id").value;
    var asset_label_id =  document.getElementById("asset_label_id").value;
    var category_id =  document.getElementById("category_id").value;
    var reference_no =  document.getElementById("ref_no").value;
    var on_behalf =  document.getElementById("on_behalf").value;
    var requested_user =  document.getElementById("req_user_id").value;
    //exit();

    var asset_location =  document.getElementById("asset_location").value;
    var approved_by =  document.getElementById("approved_by").value;
    //var value_context =  document.getElementById("value_context").value;
    var remarks =  document.getElementById("remarks").value;

    //alert(asset_label_id);

    if(requested_user=="")
    {
        sci_alert("Please select allocate to","","info");
        document.getElementById("req_user_id").focus();
        document.getElementById("req_user_id").style.background = "pink";
        return false;
    }

    if(on_behalf=="")
    {
        sci_alert("Please select on behalf","","info");
        document.getElementById("on_behalf").focus();
        document.getElementById("on_behalf").style.background = "pink";
        return false;
    }
 
    if(approved_by=="")
    {
        sci_alert("Approver can not be empty","","info");
        document.getElementById("approved_by").focus();
        document.getElementById("approved_by").style.background = "pink";
        return false;
    }
    var value_context="";

   // alert("hello");
     $.ajax({
      type: "POST",
      data: {asset_id:asset_id,asset_label_id:asset_label_id,category_id:category_id,reference_no: reference_no,approved_by:approved_by,on_behalf:on_behalf,requested_user:requested_user,
      asset_location:asset_location,value_context:value_context,remarks:remarks},
      url: globalserver + 'ajax3/direct_allocation_submit',
      success: function (msg) {
        //alert(msg);
        if(msg == 0)
        {
          sci_alert("Sorry! failed to allocate","", "info"); 
          return false; 
        }
        else
        {

        if(form_id == "allocation_form")
          {
          $("#allocation_form").load(" #allocation_form");
          }
        else
            {
              window.location.reload(false); 
              //$("#submit_button").
            }  
        sci_alert("Asset is Allocated Successfully","", "success");    
        }

        
        }
    });
    return false;

}

$(document).ready(function(){  
$(document).on('click','.allocatet',function(){

 // alert($(this).data('index'));  
 var index =  $(this).data('index');
 var req_id =  $(this).data('request_id');
 var category_id =  $(this).data('category_id');
 var quantity =  $(this).data('quantity');

 //alert(index);
 //alert(req_id);
 //alert(category_id);
 //alert(quantity);

allocate_item(index,req_id,category_id,quantity);

})
})

/****************** Single direct allocation File Uploading class ******************/
 $(document).ready(function(){
 $(document).on('change', '#allo_file', function(){
  //event.preventDefault();
  var name = document.getElementById("allo_file").files[0].name;
  var reference_no = document.getElementById("ref_no").value;
  
  //alert(reference_no);
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','docx','pdf','doc']) == -1) 
  {
   sci_alert("Sorry! Invalid File Type.", "","info"); 
   //sci_alert("Invalid Image File");
   return false;
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("allo_file").files[0]);
  var f = document.getElementById("allo_file").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 2000000)
  {
   //alert("Image File Size is very big");
    sci_alert("Image file size must be less than 2 MB!", "","info"); 
    return false;
  }
  else
  {
   form_data.append("allo_file", document.getElementById('allo_file').files[0]);
   form_data.append("reference_no", document.getElementById('ref_no').value);
   
   $.ajax({
    url:globalserver+"ajax3/allocation_upload",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
     $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
    },   
    success:function(data)
    {
     //sci_alert("", "File Uploaded Successfully!","success");  
     $('#uploaded_image').html(data);
    }
   });
  }
 });
});

/****************** Reverting File Uploading class ******************/
 $(document).ready(function(){
 $(document).on('change', '#revert_file', function(){
  //event.preventDefault();
  var name = document.getElementById("revert_file").files[0].name;
  var reference_no = document.getElementById("reference_no").value;
  var revert_id = document.getElementById("revert_id").value;
  //alert(reference_no);
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','docx','pdf','doc']) == -1) 
  {
   sci_alert("Sorry! Invalid File Type.", "","info"); 
   //sci_alert("Invalid Image File");
   return false;
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("revert_file").files[0]);
  var f = document.getElementById("revert_file").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 2000000)
  {
   //alert("Image File Size is very big");
    sci_alert("Image file size must be less than 2 MB!", "","info"); 
    return false;
  }
  else
  {
   form_data.append("file", document.getElementById('revert_file').files[0]);
   form_data.append("reference_no", document.getElementById('reference_no').value);
   form_data.append("revert_id", document.getElementById('revert_id').value);
   form_data.append("revert_purpose", document.getElementById('revert_purpose').value);

   $.ajax({
    url:globalserver+"ajax3/revert_upload",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
     $('#reverted_image').html("<label class='text-success'>Image Uploading...</label>");
    },   
    success:function(data)
    {
     //sci_alert("", "File Uploaded Successfully!","success");  
     $('#reverted_image').html(data);
    }
   });
  }
 });
});



