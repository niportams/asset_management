function get_notification() {

    //notice_map.condition_value='1';

    notification_ajax_call('notifications/get_notification', "", function (resp) {

        //Some information found. Return 0 when get nothing
        if (resp == -100) {
            window.location.href = base_url;

        }
        else if (resp != 0) {
            //Update notification popup icon and menu
            $("#notification-popup-menu").html(resp);
            $('#background-audio-player').get(0).play();

            //console.log(resp);
        }
        else {
            resp = '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i><span class="label label-warning"></span>   </a>';
            $("#notification-popup-menu").html(resp);
        }


    });

    //clearInterval(get_notification());

}

window.setInterval(get_notification, 60000 * 2);

$(document).ready(function (e) {

    get_notification();

    $(document).on('click', '.notice-item', function (e) {

        e.preventDefault();

        url = $(this).data('notice_url');
        //Update notification status
        notification_ajax_call('notifications/update_status/' + $(this).data('notice_id'), "", function (resp) {
            //Some information found. Return 0 when get nothing
            window.location.href = url;
            //console.log(url);

        });

    });
});


