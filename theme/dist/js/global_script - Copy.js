/************Form Validation**************/
function validation_rule(id_value, id_name, message) {
    //alert(id_name);
    if (id_value == '') {
        //alert(message);
        sci_alert("Warning!", message);
        //document.getElementById(id_name).focus();
        $("#id_name").focus();
        document.getElementById(id_name).style.background = "#e2eef5";
        return 0;
    }
    else
        return 1;
}

/************Email Validation****************/
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/***********Special charecters checking function**************/
function removeSpecialChars(str) {
    /*
    return str.replace(/(?!\w|\s)./g, '')
      .replace(/\s+/g, ' ')
      .replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2');
      */
    return str.replace(/[!#$%^&*]/g, "");
}

/********Alert function***********/
function start_alert(id_name, message, title) {
    if (typeof title === "undefined") {
        title = "";
    }
    $("#" + id_name).addClass('validation_error');
    sci_alert(title, message, "", id_name); //Third parameter id_name is used for set focus after clicking ok button

}

/**********Json Validation********/
function validation_json(id_name, id_type, id_required, message) {
    //alert(id_name);

    switch (id_type) {
        case "textbox":
            var id_value = document.getElementById(id_name).value;
            //id_value = removeSpecialChars(id_value);
            //console.log(id_value);
            if (id_required === "true") {
                if (id_value == "") {
                    start_alert(id_name, message);
                    return 0;
                }
                else
                    return 1;
            }
            else
                return 1;

            break;
        case "radio":
            /*
            var rates = document.getElementsByName('gik_check');
            var rate_value;
            for(var i = 0; i < rates.length; i++)
            {
              if(rates[i].checked)
                {
                rate_value = rates[i].value;
                }
            }
            */
            //var check_status = document.getElementById('gik_check').checked;
            if (document.getElementById(id_name).checked) {
                var id_value = document.getElementById(id_name).value;
                console.log(id_value);

                return 1;
            }
            else
                return 0;

            break;
        case "checkbox":

            break;
        case "select":
            var id_value = document.getElementById(id_name).value;
            //console.log(id_value);
            if (id_required === "true") {
                if (id_value == "") {
                    start_alert(id_name, message);
                    return 0;
                }
                else
                    return 1;
            }
            else
                return 1;
            break;
        case "integer":
            var id_value = document.getElementById(id_name).value;
            if (id_required === "true") {
                if (id_value == "") {
                    start_alert(id_name, message);
                    return 0;
                }
                else {
                    if (isNaN(id_value)) {
                        start_alert(id_name, message);
                        return 0;
                    }
                    else
                        return 1;
                }
            }
            else {

                if (isNaN(id_value)) {
                    start_alert(id_name, message);
                    return 0;
                }
                else
                    return 1;

            }
            break;
        case "email":
            var id_value = document.getElementById(id_name).value;
            if (id_required === "true") {
                if (id_value == "") {
                    start_alert(id_name, message);
                    return 0;
                }
                else {
                    if (validateEmail(id_value)) {
                        return 1;
                    }
                    else {
                        start_alert(id_name, message);
                        return 0;
                    }
                }
            }
            else {
                //if ((validateEmail(id_value))&&(id_value<>""))
                if (id_value != "") {
                    if (validateEmail(id_value)) {
                        return 1;
                    }
                    else {
                        start_alert(id_name, message);
                        return 0;
                    }
                }

            }
            break;
        default:
        //text = "I have never heard of that...";
    }

}

//FORM VALIDATON //

function form_validation(url, json_obj, form_id, updateid) {


    var jsonData = {};

    if (updateid != 0) /*********For update Case************/
    {
        jsonData["updateid"] = updateid;
    }

    for (var i = 0; i < json_obj.length; i++) {

        json_obj[i].value = document.getElementById(json_obj[i].id).value;
        //alert(json_obj[i].value);

        //console.log(json_obj[i].value);
        if ((validation_json(json_obj[i].id, json_obj[i].type, json_obj[i].required, json_obj[i].message)) === 0) {
            return false;
        }
        else {
            $("#" + json_obj[i].id).removeClass('validation_error');

            jsonData[json_obj[i].field_name] = removeSpecialChars(document.getElementById(json_obj[i].id).value);
        }
    }

    jsonString = JSON.stringify(jsonData);
    console.log(jsonString);
    //alert(jsonString);

    $.ajax({
        type: "POST",
        data: {jsondata: jsonString},
        url: globalserver + url,
        success: function (msg) {
            msgOffset = msg.split(',');

            sci_alert(msgOffset[0], msgOffset[1], "info");

            form_id = "#" + form_id;
            $(form_id).load(" " + form_id);
        }
    });


}



