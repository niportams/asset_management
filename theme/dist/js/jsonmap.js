//var myJSON = "{'office':{'office_name':'office_name','display_name':'Office Name','format':'string','type':'textbox','required':'true','id':'office_name','value':''},{'allocated_area':'allocated_area','display_name':'AllocatedArea','format':'string','type':'textarea','required':'true','id':'allocated_area','value':''}}";
/****########## Office ###########****/

var office = [{
    "id": "office_name",
    "field_name": "office_name",
    "format": "string",
    "type": "textbox",
    "required": "true",
    "message": "Enter Office Name",
    "value": "",
    "rule_set": {"required": true,}
    },
    {
        "id": "office_area",
        "field_name": "allocated_area",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Allocated Area",
        "value": "",
        "rule_set": {"required": true,}
    },
    /*
    {
        "id": "geo_loc",
        "field_name": "geo_loc",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Allocated Area",
        "value": "",
        "rule_set": {"required": true,}
    },*/

    {
        "id": "latitude",
        "field_name": "latitude",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Latitude",
        "value": "",
        "rule_set": {"required": true,}
    },
     {
        "id": "longitude",
        "field_name": "longitude",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Longitude",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "office_prefix",
        "field_name": "office_prefix",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Allocated Area",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "office_address",
        "field_name": "office_address",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Allocated Area",
        "value": "",
        "rule_set": {"required": true,}
    }
];

/********### Office Room  ####***************/
var room = [{
    "id": "room_name",
    "field_name": "room_name",
    "format": "string",
    "type": "textbox",
    "required": "true",
    "message": "Enter Room Name",
    "value": "",
    "rule_set": {"required": true,}
},
    {
        "id": "office_id",
        "field_name": "office_id",
        "format": "integer",
        "type": "text",
        "required": "false",
        "message": "Select Office",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "room_location",
        "field_name": "room_location",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Room Location",
        "value": "",
        "rule_set": {"required": true,}
    }
];

/****########## Category ###########****/

var category = [{
    "id": "category_name",
    "field_name": "category_name",
    "format": "string",
    "type": "textbox",
    "required": "true",
    "message": "Enter Category",
    "value": "",
    "rule_set": {"required": true,}
},
    {
        "id": "parent_id",
        "field_name": "parent_id",
        "format": "string",
        "type": "select",
        "required": "false",
        "message": "Select Parrent Category",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "registration_type",
        "field_name": "registration_type",
        "format": "string",
        "type": "select",
        "required": "false",
        "message": "Select Category Registration Type",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "category_status",
        "field_name": "category_status",
        "format": "string",
        "type": "select",
        "required": "false",
        "message": "",
        "value": 1,
    },
    {
        "id": "lifetime",
        "field_name": "lifetime",
        "format": "integer",
        "type": "select",
        "required": "false",
        "message": "Select Category Lifetime",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "cat_prefix",
        "field_name": "cat_prefix",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Category Prefix",
        "value": "",
        "rule_set": {"required": true,}
    }
    ];

/****########## Department ###########****/
var department = [{
    "id": "department_name",
    "field_name": "department_name",
    "format": "string",
    "type": "textbox",
    "required": "true",
    "message": "Enter Department Name",
    "value": "",
    "rule_set": {"required": true}
}];

/****########## Organization ###########****/
var organization = [{
        "id": "organization_name",
        "field_name": "organization_name",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Department Name",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "organization_email",
        "field_name": "organization_email",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Department Name",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "organization_phone",
        "field_name": "organization_phone",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Department Name",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "organization_address",
        "field_name": "organization_address",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Department Name",
        "value": "",
        "rule_set": {"required": false}
    }
];

/****########## User Credential ###########****/
var user = [{
    "id": "employee_id",
    "field_name": "employee_id",
    "format": "integer",
    "type": "integer",
    "required": "false",
    "message": "Only numbers are allowed here.",
    "value": 0,
},
    {
        "id": "user_name",
        "field_name": "user_name",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter User Name",
        "value": "",
    },
    {
        "id": "nice_name",
        "field_name": "user_nicename",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter your nice name",
        "value": "",
    },
    {
        "id": "user_pass",
        "field_name": "user_pass",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter User Password",
        "value": "",
    },
    {
        "id": "user_email",
        "field_name": "user_email",
        "format": "string",
        "type": "email",
        "required": "false",
        "message": "Enter Valid Email Address",
        "value": "",
    },
    {
        "id": "posting_center",
        "field_name": "posting_center",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "Select Posting Center",
        "value": "",
    },
    {
        "id": "user_role",
        "field_name": "user_role",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "Select User Role",
        "value": "",
    },
    {
        "id": "fullname",
        "field_name": "fullname",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id": "designation",
        "field_name": "designation",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id": "department",
        "field_name": "department",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id": "phone",
        "field_name": "phone",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false, "maxlength": 11, "minlength": 9}
    },
    {
        "id": "mobile",
        "field_name": "mobile",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false, "maxlength": 11, "minlength": 11}
    },
    {
        "id": "district",
        "field_name": "posting_district",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id": "upozella",
        "field_name": "posting_upazella",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id": "village",
        "field_name": "posting_village",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id": "postcode",
        "field_name": "posting_postcode",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id": "hrid",
        "field_name": "hr_id",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id": "national_id",
        "field_name": "national_id",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
    },
    {
        "id" : "user_status",
        "field_name" : "user_status",
        "format" : "string",
        "type": "select",
        "required": "false",
        "message": "",
        "value":1,
    }
];
/*###############****Privilege****###################*/

var privilege_table = [{
    "id": "privilege_name",
    "field_name": "privilege",
    "format": "string",
    "type": "textbox",
    "required": "true",
    "message": "Enter Privilege Name",
    "value": "",
    "rule_set": {"required": true}
},
    {
        "id": "privilege_status",
        "field_name": "privilege_status",
        "format": "string",
        "type": "select",
        "required": "false",
        "message": "Select Privilege Status",
        "value": 1,
        "rule_set": {"required": true}
    }
];

/*###############****Asset Capitalization****###################*/

var capital = [{
    "id": "asset_name",
    "field_name": "asset_name",
    "format": "string",
    "type": "textbox",
    "required": "true",
    "message": "Enter Asset Title",
    "value": "",
},
    {
        "id": "category_id",
        "field_name": "category_id",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "Select Category",
        "value": 0,
    },
    {
        "id": "supplier_id",
        "field_name": "supplier_id",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "Select Supplier",
        "value": 0,
    },
    {
        "id": "office_id",
        "field_name": "receiving_office",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "Select Office",
        "value": 0,
    },
    {
        "id": "voucher_no",
        "field_name": "voucher_no",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter voucher Number",
        "value": "",
    },
    {
        "id": "total_received",
        "field_name": "received_item",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Total Item Received",
        "value": 0,
    },
    {
        "id": "registered_item",
        "field_name": "registered_item_no",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "Total Item Registered",
        "value": 0,
    },
    {
        "id": "received_by",
        "field_name": "received_by",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "Enter Received By",
        "value": "",
    },
    {
        "id": "grn_no",
        "field_name": "grn_no",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "GRN #",
        "value": "",
    },
    {
        "id": "gik",
        "field_name": "gik",
        "format": "integer",
        "type": "radio",
        "required": "true",
        "message": "GIK check",
        "value": 0,
    },
    {
        "id": "funded_by",
        "field_name": "funded_by",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "Enter Funded By",
        "value": "",
    },
    {
        "id": "own_funded",
        "field_name": "own_funded",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "Own Funded By",
        "value": "NIPORT",
    },
    {
        "id": "department_id",
        "field_name": "department_id",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "Received Unit",
        "value": "NULL",
    },
];


/* #############################  PR Request ################################### */

var pr_request = [{
    "id": "title",
    "field_name": "title",
    "format": "string",
    "type": "textbox",
    "value": "",
    "rule_set": {"required": true,}
},
    {
        "id": "asset_name",
        "field_name": "asset_name",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "asset_quantity",
        "field_name": "asset_quantity",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "remark",
        "field_name": "remark",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
];

/* #############################  Asset Registration ################################### */

var asset = [{
    "id": "cpid",
    "field_name": "capital_id",
    "format": "integer",
    "type": "string",
    "value": 0,
    "rule_set": {"required": false,}
},
    {
        "id": "received_item",
        "field_name": "received_item",
        "format": "string",
        "type": "string",
        "value": 0,
        "rule_set": {"required": true,}
    },

    {
        "id": "grn_no",
        "field_name": "grn_no",
        "format": "string",
        "type": "string",
        "value": 0,
        "rule_set": {"required": true,}
    },
    {
        "id": "category_id",
        "field_name": "category_id",
        "format": "string",
        "type": "select",
        "value": 0,
        "rule_set": {"required": true,}
    },
    {
        "id": "asset_name",
        "field_name": "asset_name",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "asset_description",
        "field_name": "asset_description",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "manufacture_id",
        "field_name": "manufacture_id",
        "format": "string",
        "type": "select",
        "value": 0,
        "rule_set": {"required": false,}
    },

    {
        "id": "warrenty_date",
        "field_name": "warrenty_date",
        "format": "string",
        "type": "date",
        "value": 0,
        "rule_set": {"required": false,}
    },
    {
        "id": "asset_lifetime",
        "field_name": "asset_lifetime",
        "format": "string",
        "type": "date",
        "value": 0,
        "rule_set": {"required": false,}
    },
    {
        "id": "sku",
        "field_name": "sku",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "model_name",
        "field_name": "model_name",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "model_no",
        "field_name": "model_no",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "serial_no",
        "field_name": "serial_no",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "reference_no",
        "field_name": "reference_no",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "purchase_date",
        "field_name": "purchase_date",
        "format": "date",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "purchase_no",
        "field_name": "purchase_no",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "purchase_price",
        "field_name": "purchase_price",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "purchase_location",
        "field_name": "purchase_location",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },

    {
        "id": "registration_type",
        "field_name": "registration_type",
        "format": "string",
        "type": "checkbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "registered_item_no",
        "field_name": "registered_item_no",
        "format": "string",
        "type": "integer",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "pending_item_no",
        "field_name": "pending_item_no",
        "format": "integer",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "sci_id",
        "field_name": "sci_id",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "office_id",
        "field_name": "office_id",
        "format": "integer",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "asset_remarks",
        "field_name": "asset_remarks",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
     "id" : "pr_reference_no",
     "field_name" : "pr_reference_no",
     "format" : "string",
     "type": "textbox",
     "value":"",
     "rule_set":{"required": false,} 
    },
];

/* #############################  Requisition Request ################################### */

var asset_requisition = [{
        "id": "title",
        "field_name": "title",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "asset_name",
        "field_name": "asset_name",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "asset_quantity",
        "field_name": "asset_quantity",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "remark",
        "field_name": "remark",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
    {
        "id": "on_behalf",
        "field_name": "on_behalf",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "process",
        "field_name": "process",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "type",
        "field_name": "type",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": true,}
    },
    {
        "id": "asset_location",
        "field_name": "asset_location",
        "format": "string",
        "type": "textbox",
        "value": "",
        "rule_set": {"required": false,}
    },
];

/*###############  Asset Physical Condition Checking ###################*/

var physical = [{
    "id": "asset_id",
    "field_name": "asset_id",
    "format": "integer",
    "type": "select",
    "required": "true",
    "message": "Give Asset Name",
    "value": "",
    "rule_set": {"required": true}
},
    {
        "id": "checked_by",
        "field_name": "checked_by",
        "format": "integer",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "condition",
        "field_name": "condition",
        "format": "string",
        "type": "select",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "comments",
        "field_name": "comments",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    }
];

/****########## User details edit ###########****/
var user_update_json = [
    {
        "id": "user_email",
        "field_name": "user_email",
        "format": "string",
        "type": "email",
        "required": "false",
        "message": "Enter Valid Email Address",
        "value": "",
        "rule_set": {"required": false, "email": true}
    },
    {
        "id": "fullname",
        "field_name": "fullname",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "phone",
        "field_name": "phone",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false, "maxlength": 11, "minlength": 9}
    },
    {
        "id": "mobile",
        "field_name": "mobile",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false, "maxlength": 11, "minlength": 11}
    },
    {
        "id": "national_id",
        "field_name": "national_id",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    }

];
/****##########  password change ###########****/
var change_password = [
    {
        "id": "current_pass",
        "field_name": "current_pass",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "Enter Valid password",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "new_pass",
        "field_name": "new_pass",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "confirm_pass",
        "field_name": "confirm_pass",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true, "equalTo": "#new_pass"}
    }
];

/****########## User password change ###########****/
var user_change_password = [
    {
        "id": "id",
        "field_name": "id",
        "format": "integer",
        "type": "textbox",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "new_pass",
        "field_name": "new_pass",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "confirm_pass",
        "field_name": "confirm_pass",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true, "equalTo": "#new_pass"}
    }
];

/***************** Asset Allocation to User *********************/
var asset_allocation = [
    {
        "id": "allocate_to",
        "field_name": "allocate_to",
        "format": "interger",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "approved_by",
        "field_name": "approved_by",
        "format": "interger",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "reference_no",
        "field_name": "reference_no",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "on_behalf",
        "field_name": "on_behalf",
        "format": "string",
        "type": "select",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },

    {
        "id": "asset_location",
        "field_name": "asset_location",
        "format": "string",
        "type": "select",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "value_context",
        "field_name": "value_context",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "remarks",
        "field_name": "remarks",
        "format": "string",
        "type": "textbox",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    }
];

/********* Request Allocation for User ***************/
var request_allocation = [
    {
        "id": "ref_no",
        "field_name": "ref_no",
        "format": "interger",
        "type": "text",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "req_user_id",
        "field_name": "req_user_id",
        "format": "interger",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "on_behalf",
        "field_name": "on_behalf",
        "format": "interger",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    }
        
];

/********* Requisition FLOW ***************/
var requisition_flow = [
    {
        "id": "process_name",
        "field_name": "process_name",
        "format": "text",
        "type": "text",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "department_id",
        "field_name": "department_id",
        "format": "interger",
        "type": "select",
        "required": "true",
        "message": "",
        "value": 0,
        "rule_set": {"required": true}
    },
    {
        "id": "next_flow_id",
        "field_name": "next_flow_id",
        "format": "interger",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "step",
        "field_name": "step",
        "format": "integer",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "access_type",
        "field_name": "access_type",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    }
        
];

/********* TRANSFER FLOW ***************/
var transfer_flow = [
    {
        "id": "user_id",
        "field_name": "user_id",
        "format": "interger",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "step",
        "field_name": "step",
        "format": "integer",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "access_type",
        "field_name": "access_type",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    }
        
];

/*********Data Entry Form***********/
var dataentry= [
     {
        "id": "grn_no",
        "field_name": "grn_no",
        "format": "interger",
        "type": "text",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "capital_id",
        "field_name": "capital_id",
        "format": "interger",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "category_id",
        "field_name": "category_id",
        "format": "integer",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "asset_name",
        "field_name": "asset_name",
        "format": "string",
        "type": "select",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": true}
    },
    {
        "id": "asset_description",
        "field_name": "asset_description",
        "format": "string",
        "type": "textbox",
        "required": "true",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "manufacture_id",
        "field_name": "manufacture_id",
        "format": "string",
        "type": "select",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "warrenty_date",
        "field_name": "warrenty_date",
        "format": "date",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "sku",
        "field_name": "sku",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "sci_id",
        "field_name": "sci_id",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "model_name",
        "field_name": "model_name",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "model_no",
        "field_name": "model_no",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "serial_no",
        "field_name": "serial_no",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "reference_no",
        "field_name": "reference_no",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "purchase_date",
        "field_name": "purchase_date",
        "format": "date",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "purchase_no",
        "field_name": "purchase_no",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "purchase_price",
        "field_name": "purchase_price",
        "format": "date",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "purchase_location",
        "field_name": "purchase_location",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "asset_remarks",
        "field_name": "asset_remarks",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    /*
    {
        "id": "gik",
        "field_name": "gik",
        "format": "integer",
        "type": "radio",
        "required": "true",
        "message": "GIK check",
        "value": 0,
        "rule_set": {"required": true}
    },*/
    {
        "id": "funded_by",
        "field_name": "funded_by",
        "format": "textbox",
        "type": "select",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "own_funded",
        "field_name": "own_funded",
        "format": "textbox",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "assigned_to",
        "field_name": "assigned_to",
        "format": "integer",
        "type": "select",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "asset_location",
        "field_name": "asset_location",
        "format": "text",
        "type": "select",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "allocation_date",
        "field_name": "allocation_date",
        "format": "text",
        "type": "date",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "asset_status_id",
        "field_name": "asset_status_id",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
     {
        "id": "pr_reference_no",
        "field_name": "pr_reference_no",
        "format": "text",
        "type": "text",
        "required": "false",
        "message": "",
        "value": "",
        "rule_set": {"required": false}
    },
    {
        "id": "asset_lifetime",
        "field_name": "asset_lifetime",
        "format": "string",
        "type": "date",
        "value": 0,
        "rule_set": {"required": false,}
    },


        
];