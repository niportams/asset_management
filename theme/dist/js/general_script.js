/************User Access Rights validation************/
function access_validation() {
    var checkedValue = [];
    var check_string = "";
    var role_name = document.getElementById("role_name").value;
    var role_rank = document.getElementById("role_rank").value;
    var selected_key = document.getElementsByName("access_right[]");

    if ((validation_rule(role_name, 'role_name', "Enter User Role")) == 0) {
        return false;
    }

    if ((validation_rule(role_rank, 'role_rank', "Enter User Role Rank")) == 0) {
        return false;
    }

    var l = selected_key.length;
    var count = 0;
    var k = 0;
    for (var i = 0; i < l; i++) {
        if (selected_key[i].checked) {
            check_string = check_string + selected_key[i].value + ',';
            //alert(check_string);
            //k++;
        }
        else
            count = count + 1;
    }
    if (count == l) {
        alert('Check at least one privilege.');
    }


    $.ajax({
        type: "POST",
        data: {role_name: role_name, role_rank: role_rank, check_string: check_string},
        url: globalserver + 'ajax/userrole_submit',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            $("#role_create").load(" #role_create");
            //$("#registration_form").html(msg);
            //$("#role_create_form").load(location.href + "#role_create_form");
        }
    });

}

/************Privilege validation************/
function privilege_validation() {
    //alert("Hello");
    var privilege_name = document.getElementById("privilege_name").value;
    var privilege_status = document.getElementById("privilege_status").value;

    if ((validation_rule(privilege_name, 'privilege_name', "Enter Privilege Name")) == 0) {
        return false;
    }
    //alert(privilege_status); 
    //alert(privilege_name);  

    $.ajax({
        type: "POST",
        data: {privilege_name: privilege_name, privilege_status: privilege_status},
        url: globalserver + 'ajax/privilege_submit',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            //$("#registration_form").html(msg);
            $("#privilege_form").load(" #privilege_form");
        }
    });

}

/*Display privilege details based on privilege_id*/
function show_privilege(privilege_id) {
    //alert(privilege_id);

    $.ajax({
        type: "POST",
        data: {privilege_id: privilege_id},
        url: globalserver + 'ajax/privilege_details',
        success: function (msg) {
            //alert(msg);
            $("#privilege_name").html(msg);
        }
    });

}

/*********Profile details**************/
function profile_details(userid) {
    //alert(userid);
    $.ajax({
        type: "POST",
        data: {userid: userid},
        url: globalserver + 'ajax/profile_details',
        success: function (msg) {
            //alert(msg);
            $("#profile_details").html(msg);
        }
    });
}

/********Profile Update**********/
function profile_edit(dbuserid) {
    //alert(userid);
    $.ajax({
        type: "POST",
        data: {userid: dbuserid},
        url: globalserver + 'ajax/profile_update',
        success: function (msg) {
            //alert(msg);
            $("#profile_details").html(msg);
        }
    });
}

/********user Update**********/
function user_edit(dbuserid) {
    //alert(userid);
    $.ajax({
        type: "POST",
        data: {userid: dbuserid},
        url: globalserver + 'ajax3/profile_update',
        success: function (msg) {
            //alert(msg);
            $("#user_edit").html(msg);
        }
    });
}

/******* change pass  ************/
function password_change(dbuserid) {
    //alert(userid);
    $.ajax({
        type: "POST",
        data: {userid: dbuserid},
        url: globalserver + 'ajax3/pass_change',
        success: function (msg) {
            //alert(msg);
            $("#change_pass").html(msg);
        }
    });
}

/********Profile Update**********/
function profile_delete(dbuserid) {
    //alert(userid);
    sci_confirm("Are you sure to delete this user?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {user_id: dbuserid},
                url: globalserver + 'user/delete',
                success: function (msg) {
                    //alert(msg);
                    sci_alert("", msg, "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });

    return false;
}

/******** Role Delete **********/
function role_delete(role_id) {
    //alert(role_id);
    sci_confirm("Are you sure to delete this role?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {role_id: role_id},
                url: globalserver + 'role/role_delete',
                success: function (msg) {
                    //alert(msg);
                    sci_alert("", msg, "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });

    return false;
}

/********Role Update**********/
function role_edit(role_id) {
    //alert(role_id);
    $.ajax({
        type: "POST",
        data: {role_id: role_id},
        url: globalserver + 'ajax/role_update',
        success: function (msg) {
            //alert(msg);
            $("#role_edit").html(msg);
        }
    });
}

/***********Update Role Submit************/
function update_role(role_id) {

    //alert(role_id);
    var role_name = document.getElementById("role_name").value;
    var role_rank = document.getElementById("role_rank").value;
    var role_status = document.getElementById("role_status").value;
    var selected_key = document.getElementsByName("access_right[]");

    var checkedValue = [];
    var check_string = "";


    if ((validation_rule(role_name, 'role_name', "Enter User Role")) == 0) {
        return false;
    }
    if ((validation_rule(role_rank, 'role_rank', "Enter User Role Rank")) == 0) {
        return false;
    }

    var l = selected_key.length;
    var count = 0;
    var k = 0;
    for (var i = 0; i < l; i++) {
        if (selected_key[i].checked) {
            check_string = check_string + selected_key[i].value + ',';
        }
        else
            count = count + 1;
    }
    if (count == l) {
        alert('Check at least one privilege.');
    }

    //alert(check_string);
    $.ajax({
        type: "POST",
        data: {role_id: role_id, role_name: role_name,role_rank: role_rank, role_status: role_status, check_string: check_string},
        url: globalserver + 'role/role_edit',
        success: function (msg) {
            // alert(msg);
            sci_alert("", msg, "success");
            //$("#role_edit").load(" #role_edit");
            $("#example1").load(" #example1");
        }
    });

}

/*************privilege Edit****************/
function privilege_edit(privilege_id) {
    //alert(privilege_id);
    $.ajax({
        type: "POST",
        data: {privilege_id: privilege_id},
        url: globalserver + 'ajax/privilege_update',
        success: function (msg) {
            //alert(msg);
            $("#privilege_edit").html(msg);
        }
    });
}

/************Update Privilege Submit************/
function update_privilege_submit(privilege_id) {
    var privilege_name = document.getElementById("privilege_name").value;
    var privilege_status = document.getElementById("privilege_status").value;

    $.ajax({
        type: "POST",
        data: {privilege_id: privilege_id, privilege_name: privilege_name, privilege_status: privilege_status},
        url: globalserver + 'privilege/privilege_edit',
        success: function (msg) {
            //alert(msg);
            sci_alert("", msg, "success");
            //$("#privilege_edit").html(msg);
            //window.location.reload();
            $("#example1").load(" #example1");
        }
    });
}

/*************privilege Delete****************/
function privilege_delete(privilege_id) {
    //alert(privilege_id);
    sci_confirm("Are you sure to delete this privilege?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {privilege_id: privilege_id},
                url: globalserver + 'privilege/privilege_delete',
                success: function (msg) {
                    //alert(msg);
                    sci_alert("", msg, "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });

    return false;
}

/***************  physical Asset Search  *******************/

function physical_asset_search() {
    var asset_search = document.getElementById("asset_search").value;

    //alert(asset_search);

    if (asset_search == '') {
        sci_alert("", "Please Enter Asset ID", "warning");
        document.getElementById("asset_id").focus();
        document.getElementById("asset_id").style.background = "#e2eef5";
        return false;
    }


    $.ajax({
        type: "POST",
        data: {asset_search: asset_search},
        url: globalserver + 'ajax2/asset_physical_search',
        success: function (msg) {
            if (msg == 1) {
                sci_alert("", "Sorry! Invalid Asset ID. Please Try Again!", "warning");
            }
            else {
                $("#asset_check_form").html(msg);
            }
        }
    });
    return false;

}

/********Profile Update**********/
function user_logout(key) {
    //alert(userid);
    sci_confirm("Are you sure to logout this user?", "Logout", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {key: key},
                url: globalserver + 'admin/loggedout',
                success: function (msg) {
                    //alert(msg);
                    sci_alert("", msg, "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);

                }
            });
        }
    });

    return false;
}
 
 