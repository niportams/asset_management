$(document).ready(function () {

    //  $(this).add('<div class="tblbg"></div>');


    $(document).on('mouseenter', '.sci-table td', function () {
        columnTh = $(this); // Find the heading with the text THEHEADING
        columnIndex = columnTh.index() + 1; // Get the index & increment by 1 to match nth-child indexing
        $('table tr td:nth-child(' + columnIndex + ')').addClass('tblbg');
    });
    
    $(document).on('mouseleave', '.sci-table td', function () {
        columnTh = $(this); // Find the heading with the text THEHEADING
        columnIndex = columnTh.index() + 1; // Get the index & increment by 1 to match nth-child indexing
        $('table tr td:nth-child(' + columnIndex + ')').removeClass('tblbg');
    });

});