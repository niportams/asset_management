//Load this file after jquery 
function test(form_id, rules, url, update_id, json_method='manual') {
    console.log(form_id);
    console.log(rules);
    console.log(url);
    console.log(update_id);
    console.log(json_method);
    form_data = $("form").serializeArray();
    console.log(form_data);
    return false;
}

// validation on click call
function handle_form(form_id, rules, url, update_id, json_method='manual', clear = false, clear_id = '') {

    //alert(update_id);

    validator_call(form_id, rules, function (resp) {

        if (resp) {
            json_data = {};
            if (update_id != 0) {
                json_data["update_id"] = update_id;
                //alert(json_data["update_id"]);
            }
            if (json_method == 'manual') {

                for (var i = 0; i < rules.length; i++) {
                    //rules[i].value = document.getElementById(rules[i].id).value;
                    //json_data[rules[i].field_name] = removeSpecialChars(rules[i].value);


                    if (rules[i].type === "checkbox") {

                        if (document.getElementById(rules[i].id).checked) {
                            //alert("checkbo button checked");
                            json_data[rules[i].field_name] = removeSpecialChars(document.getElementById(rules[i].id).value);
                        }
                        else {
                            json_data[rules[i].field_name] = "";
                        }
                    } else {
                        json_data[rules[i].field_name] = removeSpecialChars(document.getElementById(rules[i].id).value);
                    }

                }
                form_data = JSON.stringify(json_data);
            } else if (json_method == 'serialize') {
                form_data = $("form").serializeArray();
            }
            else if (json_method == 'serialize_object') {
                form_data = $("form").serializeArray();
                form_data = JSON.stringify(form_data);
            }
            // console.log(form_data);
            $("#modalLoading").modal({backdrop: 'static', keyboard: false});
            ajax_call(url, "POST", form_data, function (response) {
                if (response) {
                    result = response.trim();
                    console.log(result);
                    response = JSON.parse(result);

                    $("#modalLoading").modal('hide');

                    if (response.status == 'success') {


                        sci_alert(response.message, "", "success", "", function (res) {

                            if (res) 
                            {
                               // window.location.reload(false); 
                                // alert(res);
                                
                                if (update_id != 0) 
                                {  
                                  window.location.reload(false); 
                                }
                                if(form_id == "request_allocation")
                                {
                                   window.location.reload(false);  
                                }
                                
                                //self.location.reload();
                            }

                        })

                        //sci_alert("", response.message,"success");

                        /*
                        if(!sci_alert("", response.message,"success"))

                            {window.location.reload();}

                        */


                    }
                    else {
                        sci_alert("", response.message, "error");
                    }
                    if (update_id == 0) {
                        reset_form(form_id);
                        if (clear) {
                            $('#' + clear_id).html('');
                        }
                    }
                    else {
                        $("#example1").load(" " + "#example1");
                    }
                }
            })
        }
    });
}

// global ajax function
function ajax_call(url, method, map, callback) {

    var resp = null;
    $.ajax({
        method: method,
        url: globalserver + url,
        data: {jsondata: map},
        error: function (xhr, status, error) {
            alert(xhr.responseText);

        }
    })

        .done(function (response) {
            callback(response);

        });
}

// form validator. validate all
function validator_call(id, json_data, callback) {

    rule = rule_conversion(json_data);
    $("#" + id).validate({
        rules: rule,
        submitHandler: function (form) {

            callback(true);
            return false;  //This doesn't prevent the form from submitting.
        },
        invalidHandler: function (event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();

            if (errors) {
                callback(false);
            }
        }
    });
}

// collect the rules from the json data
function rule_conversion(data) {
    var jsondata = {}
    for (var i = 0; i < data.length; i++) {
        jsondata[data[i].field_name] = data[i].rule_set;
    }
    return jsondata;
}

// Notification Ajax call

function notification_ajax_call(url, map, callback) {
    var resp = null;
    $.ajax({
        method: "POST",
        url: base_url + url,
        data: {json_map: map}
    })
        .done(function (response) {
            callback(response);
        });

}

//Reset form data
function reset_form(form_id) {
    $("#" + form_id).trigger('reset');
}

function isNormalInteger(str) {
    var n = Math.floor(Number(str));
    return String(n) === str && n > 0;
}