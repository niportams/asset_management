$(document).ready(function () {
   $('.dataTable-full-functional').DataTable({
        dom: 'Blfrtip',
        responsive: true,
        pageLength: 25,
        buttons: [
            {
                //className:'btn-info',
                extend: 'copyHtml5',
                text: '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy'
            },
            {
                //className:'btn-info',
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel'
            },
            {
                // className:'btn-info',
                extend: 'csvHtml5',
                text: '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV'
            },
            {
                //className:'btn-info',
                extend: 'pdfHtml5',
                text: '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF'
            },
//            {
//                extend: 'print',
//                text: 'Print current page',
//                exportOptions: {
//                    modifier: {
//                        page: 'current'
//                    }
//                }
//            }
            {
                text: '<i class="fa fa-print"></i>',
                extend: 'print',
                className: 'btn-print',
                titleAttr: 'Print',
                
            }


        ],
        //lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
    });
});