$('body').on('focus', ".date-picker", function () {

    $(this).datepicker({
        autoclose: true,
        orientation: "bottom auto",
        format: 'dd/mm/yyyy',
        changeMonth: true,
        changeYear: true

    }).val();

    //var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
});