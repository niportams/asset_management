$(document).ready(function (e) {


    $('.parent').change(function () {
        $('.child').remove();
        $('.sub-cat').remove();
        
        if($(this).val()==-1){
            $(this).attr('id','item');
            $(this).attr('name','item');
            
            
        }
    });
    
    
    $(document).on('change', '.child', function (){
       $(this).next('.sub-cat').remove();
 
       $(this).attr('id','item');
    });
    

    $(document).on('change', '.select-chain', function () {
        current_element = $(this);
        parent_id = $(this).val();
        id = 'item';

        //remove old element
        $(this).next('.child').remove();

        $.ajax({
            type: "POST",
            data: {parent_id: parent_id},
            url: globalserver + 'ajax/get_category_by_parent_id/' + parent_id,
            success: function (result) {
                result = $.trim(result);

                if (result !== 'false')
                {
                    //remove item ID and name
                    $('.select-chain').removeAttr('id');
                    $('.select-chain').removeAttr('name');
                    
                    
                    optObj = JSON.parse(result);

                    sel='<div class="form-horizontal sub-cat"><label class="control-label ">Sub item</label>';
                    sel += '<select  class="select-chain child form-control" id="' + id + '" name="' + id + '">';

                    sel += '<option value="-1" selected="selected">Please select</option>';
                    $.each(optObj, function (key, value) {
                        sel += "<option value=" + optObj[key].category_id + ">" + optObj[key].category_name + "</option>";
                    });

                    sel += "</select></div>";
                    current_element.after(sel);

                }

            }

        });

    });

});