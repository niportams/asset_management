$(document).ready(function(e){
    
    //Add print area for iframe
    $('body').append('<iframe style="display:none" id="printf" name="printf"></iframe>');
    
    $(document).on('click','.asset-detail',function(){
        $('.single-asset-detail-print-modal').data('asset_id',$(this).data('asset_id'));
    });
    
    $(document).on('click','.single-asset-detail-print-modal',function(){
        sci_print('print_asset_detail/'+$(this).data('asset_id'));
    });
    
    $(document).on('click','.sci-print',function(){
        sci_print('print_allocated_items/'+$(this).data('req_no'));
    });
    
    $(document).on('click',".single-asset-detail-print", function(){
        // alert($(this).data('asset_id'));
        sci_print('print_asset_detail/'+$(this).data('asset_id'));
    });
});

function sci_print(form) {

   // alert(globalserver + 'sci_print/' + form);
    $.ajax({
        type: "POST",
        //data: {parent_id: parent_id},
        url: globalserver + 'sci_print/' + form,
        success: function (result) {
            result = $.trim(result);

            var newWin = window.frames["printf"];
            newWin.document.write(result);
            newWin.document.close();

        }

    });
   // window.print();

}