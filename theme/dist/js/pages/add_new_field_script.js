$(document).ready(function () {
    $(".add").click(function () {
        var intId = $("#buildyourform div").length + 1;
        var fieldWrapper = $("<div id=\"field" + intId + "\"/>");
        var row = $("<div class=\"row\"/>");
        var col6 = $("<div class=\"col-md-6\"/>");
        var col4 = $("<div class=\"col-md-4\"/>");
        var col2 = $("<div class=\"col-md-2\"/>");
        var fNameTitle = $("<label class=\"control-label\">Asset Name<span class=\"text-danger \">*</span></label>")
        var fName = $("<input type=\"text\" class=\"form-control \"  name=\"asset_name\" />");
        var fQuantityTitle = $("<label class=\"control-label\">Asset Quantity<span class=\"text-danger \">*</span></label>")
        var fQuantity = $("<input type=\"text\" class=\"form-control \" name=\"asset_quantity\"/>");
        var removeButton = $("<a class=\"remove btn btn-danger btn-xs \" style=\"margin-top:25px\"><i class=\"fa fa-trash-o\"> </i></a>");
        // var addButton = $(" <a class=\"btn btn-success add\"  style=\"margin-top:25px\" ><i class=\"fa fa-plus\"> </i></a>");
        var fcapital = $("<input class=\"form-control\" id=\"capital_id\" type=\"hidden\" name=\"capital_id\">")
        var fcategory = $("<input class=\"form-control\" id=\"category_id\" type=\"hidden\" name=\"category_id\">")

        removeButton.click(function () {
            $(this).parent().parent().remove();
        });
        fieldWrapper.append(row);
        row.append(col6);
        row.append(col4);
        row.append(col2);
        col6.append(fNameTitle);
        col6.append(fName);
        col4.append(fQuantityTitle);
        col4.append(fQuantity);
        col2.append(removeButton);
        //col2.append(addButton);
        $("#buildyourform").append(fieldWrapper);
        // $('.autocomplete').autocomplete({
        //     source:assets,
        //     focus: function (event, ui) {

        //     $(event.target).val( ui.item.label);
        //     // $(event.target).parent().find("input[name='capital_id']").val( ui.item.id);
        //     // $(event.target).parent().find("input[name='category_id']").val( ui.item.catagory);
        //     return false;
        //     },
        //     select: function( event, ui ) {
        //     $(event.target).val( ui.item.label);
        //     // $(event.target).parent().find("input[name='capital_id']").val( ui.item.id);
        //     // $(event.target).parent().find("input[name='category_id']").val( ui.item.catagory);
        //     return false;
        //     },
        //     change: function (event, ui) {
        //     if (ui.item) {
        //     } else {
        //        // $(event.target).parent().find("input[name='capital_id']").val( null);
        //        //  $(event.target).parent().find("input[name='category_id']").val( null);
        //     }
        // },
        // });
    });
    // $('.autocomplete').autocomplete({
    //     source:assets,
    //     focus: function (event, ui) {

    //         $(event.target).val( ui.item.label);
    //         //  $(event.target).parent().find("input[name='capital_id']").val( ui.item.id);
    //         // $(event.target).parent().find("input[name='category_id']").val( ui.item.catagory);
    //         return false;
    //     },
    //     select: function( event, ui ) {
    //         $(event.target).val( ui.item.label);
    //         // $(event.target).parent().find("input[name='capital_id']").val( ui.item.id);
    //         // $(event.target).parent().find("input[name='category_id']").val( ui.item.catagory);
    //         return false;
    //     },
    //     change: function (event, ui) {
    //         if (ui.item) {
    //         } else {
    //            // $(event.target).parent().find("input[name='capital_id']").val( null);
    //            //  $(event.target).parent().find("input[name='category_id']").val( null);
    //         }
    //     },
    // });
    // }
    var item_count = 0;
    $(".add_item").click(function () {

        item_count++;
        var item_name = $("#item").find('option:selected').text();
        var item_id = $("#item").val();
        var item_qty = $("#qty").val();
        var intId = $("#buildyourform tbody tr").length + 1;
        var row = $("<tr id=\"field" + intId + "\"/>");
        var col6 = $("<td/>");
        var col4 = $("<td />");
        var col2 = $("<td />");
        var item_name_show = $("<b>" + item_name + "</b>");
        var item_qty_show = $("<b>" + item_qty + "</b>");
        var fName = $("<input type=\"hidden\" class=\"form-control \" value=\"" + item_name + "\"  name=\"asset_name\" />");
        var fQuantity = $("<input type=\"hidden\" class=\"form-control \" value=\"" + item_qty + "\" name=\"asset_quantity\"/>");
        var removeButton = $("<a class=\"remove btn btn-danger btn-xs \"><i class=\"fa fa-trash-o\"> </i></a>");
        // var addButton = $(" <a class=\"btn btn-success add\"  style=\"margin-top:25px\" ><i class=\"fa fa-plus\"> </i></a>");
        var fcategory = $("<input class=\"form-control\" value=\"" + item_id + "\" type=\"hidden\" name=\"category_id\">")

        removeButton.click(function () {
            var item = $(this).parent().parent();
            sci_confirm("Are you sure to delete this Item?", "Confirmation", function (res) {
                if (res) {
                    item.remove();
                    item_count--;
                    if (item_count == 0) {
                        $('#requisition_submit').attr("disabled", "disabled");
                    }
                }
            });
        });
        if (item_id != '' && item_qty != '' && isNormalInteger(item_qty)) {
            // fieldWrapper.append(row);
            row.append(col6);
            row.append(col4);
            row.append(col2);
            col6.append(item_name_show);
            col6.append(fName);
            col6.append(fcategory);
            col4.append(item_qty_show);
            col4.append(fQuantity);
            col2.append(removeButton);
            $("#buildyourform").append(row);
            $("#qty").val('');
            $("#item").val('');
        } else {
            sci_alert("", 'Please Select an Item', "warning");
        }

        if (!isNormalInteger(item_qty)) {
            sci_alert("", 'Please provide quantity', "warning");
        }
        if (item_count > 0) {
            $('#requisition_submit').removeAttr("disabled");
        }
        //col2.append(addButton);

    });
});