$(function() {
    $('input[name="gik"]').on('click', function() {
        if ($(this).val() == '1') {
            $('#textboxes').show();
            $('#textbox').hide();
        }
        else {
            $('#textboxes').hide();
            $('#textbox').show();
        }
    });
});


/*************Show Asset List Based on Asset Status*****************/
function show_asset_list()
{
    var asset_status =  document.getElementById("asset_status").value;
    //alert(asset_status);

    $.ajax({
        type:"POST",
        data:{asset_status:asset_status},
        url: globalserver+'ajax2/status_wise_asset',
        success: function(msg)
        {
            //alert(msg);

            $("#abc").html(msg);

        }
    });

    return false;
}
/**********Category Validation**************/
function category_validation()
{
    var category_name =  document.getElementById("category_name").value;
    var parrent_id = document.getElementById("parrent_id").value;

    if((validation_rule(category_name,'category_name',"Enter Category Name"))==0)
    {
        return false;
    }

    $.ajax({
        type:"POST",
        data:{category_name:category_name,parrent_id:parrent_id},
        url: globalserver+'ajax/category_submit',
        success: function(msg)
        {
            //alert(msg);
            sci_alert("", msg,"success");
            //$("#registration_form").html(msg);
            $("#category_form").load(" #category_form");
        }
    });
}
/************** category edit***************/
function category_edit(category_id)
{
    //alert(category_id);

    $.ajax({
        type:"POST",
        data:{category_id:category_id},
        url: globalserver+'ajax/category_update',
        success: function(msg)
        {
            //alert(msg);
            $("#category_edit").html(msg);
        }
    });
    return false;
}
/***********Update Category submit**************/
function update_category_submit(category_id)
{
    //alert(category_id);

    var category_name =  document.getElementById("category_name").value;
    var parrent_id =  document.getElementById("parrent_id").value;
    var category_status = document.getElementById("category_status").value;

    //alert(category_name);
    //alert(parrent_id);
    //alert(category_status);

    //var key_name='category_name';
    if((validation_rule(category_name,'category_name',"Enter Category Name"))==0)
    {
        return false;
    }


    $.ajax({
        type:"POST",
        data:{category_id:category_id,category_name:category_name,parrent_id:parrent_id,category_status:category_status},
        url: globalserver+'category/category_edit',
        success: function(msg)
        {
            //alert(msg);
            sci_alert(msg, "","success");
            //$("#category_edit").html(msg);
            $("#example1").load(" #example1");
        }
    });
}

/**************Category Delete***************/
function category_delete(category_id)
{
    sci_confirm("Are you sure to delete this category?", "Delete", function(res){
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: { category_id: category_id},
                url: globalserver + 'category/category_delete',
                success: function (msg)
                {
                    //alert(msg);
                    sci_alert("", msg,"success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;
}

/*************Asset Capitalization Validation***************/
function capital_validation()
{

    var asset_name =  document.getElementById("asset_name").value;
    var category_id =  document.getElementById("category_id").value;
    var office_id = document.getElementById("office_id").value;
    var supplier_id = document.getElementById("supplier_id").value;
    var voucher_no = document.getElementById("voucher_no").value;
    var total_received = document.getElementById("total_received").value;
    var grn_no = document.getElementById("grn_no").value;
    var funded_by = document.getElementById("funded_by").value;
    var own_funded = document.getElementById("own_funded").value;
    var received_by = document.getElementById("received_by").value;

    if((validation_rule(asset_name,'asset_name',"Enter Title"))==0)
    {return false;}
    if((validation_rule(category_id,'category_id',"Select a category"))==0)
    {return false;}
    if((validation_rule(office_id,'office_id',"Select an Office"))==0)
    {return false;}
    if((validation_rule(supplier_id,'supplier_id',"Select a Supplier."))==0)
    {return false;}
    if((validation_rule(voucher_no,'voucher_no',"Voucher # is a required field."))==0)
    {return false;}
    if((validation_rule(total_received,'total_received',"Received item can not be empty."))==0)
    {return false;}

    var own_fund_check = 0;
    if(document.getElementById('gik_check').checked)
    {
        var gik_value = document.getElementById('gik_check').value;

        if(gik_value==1) //funded by
        {
            //alert(gik_value);

            if((validation_rule(funded_by,'funded_by',"Please Enter Funded By."))==0)
            {return false;}

        }
        else
        {
            gik_value = 0;

            /*
              if(own_funded=='')
                {
                  alert('Please Enter Own Funded Name!');
                  document.getElementById("own_funded").focus();
                  document.getElementById("own_funded").style.background = "#e2eef5";
                  return false;
                }
            */
        }
    }


    $.ajax({
        type:"POST",
        data:{asset_name:asset_name,category_id:category_id,office_id:office_id,supplier_id:supplier_id,voucher_no:voucher_no,total_received:total_received,
            grn_no:grn_no,gik_check:gik_value,funded_by:funded_by,received_by:received_by,own_funded:own_funded},
        url: globalserver+'ajax/capital_submit',
        success: function(msg)
        {
            //alert(msg);
            sci_alert(msg, "","success");
            window.location.reload();
            //$("#capital_form").load(" #capital_form");
            // $("#capital_form").load(" #capital_form");
        }
    });

}
/***********Asset capital edit**************/
function capital_edit(capital_id)
{
    $.ajax({
        type:"POST",
        data:{capital_id:capital_id},
        url: globalserver+'ajax/capital_update',
        success: function(msg)
        {
            //alert(msg);
            $("#capital_edit").html(msg);
        }
    });
    return false;
}



/******Capital Update Submit********/
function update_capital_submit(capital_id)
{
    var asset_name =  document.getElementById("asset_name").value;
    var category_id =  document.getElementById("category_id").value;
    var office_id = document.getElementById("office_id").value;
    var voucher_no = document.getElementById("voucher_no").value;
    var supplier_id = document.getElementById("supplier_id").value;
    var total_received = document.getElementById("total_received").value;
    //var grn_no = document.getElementById("grn_no").value;
    var funded_by = document.getElementById("funded_by").value;
    var own_funded = document.getElementById("own_funded").value;
    var received_by = document.getElementById("received_by").value;
    var department_id = document.getElementById("department_id").value;

    //alert(department_id);

    if((validation_rule(asset_name,'asset_name',"Enter Title"))==0)
    {return false;}
    if((validation_rule(category_id,'category_id',"Select a category"))==0)
    {return false;}
    if((validation_rule(office_id,'office_id',"Select an Office"))==0)
    {return false;}
    if((validation_rule(supplier_id,'supplier_id',"Select a Supplier."))==0)
    {return false;}
    if((validation_rule(voucher_no,'voucher_no',"Voucher # is a required field."))==0)
    {return false;}
    if((validation_rule(total_received,'total_received',"Received item can not be empty."))==0)
    {return false;}


    var own_fund_check = 0;
    if(document.getElementById('gik_check').checked)
    {
        var gik_value = document.getElementById('gik_check').value;
        if(gik_value==1) //funded by
        {
            //alert(gik_value);
            if((validation_rule(funded_by,'funded_by',"Please Enter Funded By."))==0)
            {return false;}
            /*
            if(funded_by=='')
             {

             alert('Please Enter Funded By.');
             document.getElementById("funded_by").focus();
             document.getElementById("funded_by").style.background = "#e2eef5";
             return false;
             }
             */
        }
        else
        {
            gik_value = 0;
        }
    }

    $.ajax({
        type:"POST",
        data:{capital_id:capital_id,asset_name:asset_name,category_id:category_id,office_id:office_id,supplier_id:supplier_id,
            total_received:total_received,voucher_no:voucher_no,
            gik_check:gik_value,funded_by:funded_by,received_by:received_by,
            own_funded:own_funded,department_id:department_id},
        url: globalserver+'asset/capital_update',
        success: function(msg)
        {
            //alert(msg);
            sci_alert(msg, "","success");
            $("#example1").load(" #example1");
        }
    });

}

/*********Capital Delete***********/
function capital_delete(capital_id)
{
    sci_confirm("Are you sure to delete this record?", "Delete", function(res){
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: { capital_id: capital_id},
                url: globalserver + 'asset/capital_delete',
                success: function (msg)
                {
                    //alert(msg);
                    sci_alert(msg, "","success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;
}

/************ Show requisition list *******/
function show_requisition_list(requisition_id)
{
    //alert(category_id);

    $.ajax({
        type:"POST",
        data:{requisition_id:requisition_id},
        url: globalserver+'ajax/requisition_list',
        success: function(msg)
        {
            //alert(msg);
            $("#requisition_list").html(msg);
        }
    });
    return false;
}

function show_requisition_list_only(requisition_id)
{
    $.ajax({
        type:"POST",
        data:{requisition_id:requisition_id},
        url: globalserver+'ajax/requisition_list_only',
        success: function(msg)
        {
            $("#requisition_list").html(msg);
        }

    });
    return false;
}

/********** Requisition Flow Update****************/
function flow_edit(flow_id)
{
    var postData =
        {
            "id": flow_id
        };
    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax/flow_update',
        success: function (msg) {
            //alert(msg);
            $("#flow_details").html(msg);
        }
    });
    return false;
}
/********** Transfer Flow Update****************/
function transfer_flow_edit(flow_id)
{
    var postData =
        {
            "id": flow_id
        };
    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax/transfer_flow_update',
        success: function (msg) {
            //alert(msg);
            $("#transfer_flow_details").html(msg);
        }
    });
    return false;
}
/************ Transfer Flow Delete**************/
function transfer_flow_delete(flow_id)
{

    sci_confirm("Are you sure to delete this record?", "Delete", function(res){
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: { flow_id: flow_id},
                url: globalserver + 'requisition/transfer_flow_delete',
                success: function (msg)
                {
                    //alert(msg);
                    sci_alert(msg, "","success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;

}
/************ Requisition Flow Delete**************/
function flow_delete(flow_id)
{

    sci_confirm("Are you sure to delete this record?", "Delete", function(res){
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: { flow_id: flow_id},
                url: globalserver + 'requisition/flow_delete',
                success: function (msg)
                {
                    //alert(msg);
                    sci_alert(msg, "","success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;

}
/****************** Requisition status change **********/
function update_requisition_status(requisition_id,status){
    
    var remarks = $('#remarks').val();
    if(status=="declined" && remarks==""){
        sci_alert("","Please Provide Remarks.","error");
    }else{
        $.ajax({
            type:"POST",
            data:{'requisition_id':requisition_id,'status': status,'remarks':remarks},
            url: globalserver+'ajax/update_requisition_status',
            success: function(msg)
            {
                sci_alert("", msg,"success");
                $("#example1").load(" #example1");
                $('#remarks').val('');
                $('.change_status').hide();
                $("#button_panel").hide();

            }
        });
    }
    return false;
}

/****************** Requisition  transfer status change **********/
function update_transfer_status(requisition_id,status){
    var remarks = $('#remarks').val();
    if(status=="declined" && remarks==""){
        sci_alert("","Please Provide Remarks.","error");
    }else {
        $.ajax({
            type: "POST",
            data: {'requisition_id': requisition_id, 'status': status, 'remarks': remarks},
            url: globalserver + 'ajax/update_transfer_status',
            success: function (msg) {
                sci_alert(msg, "", "success");
                $("#example1").load(" #example1");
                $('#remarks').val('');
                $('.change_status').hide();
                $("#button_panel").hide();
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);

            }
        });
    }
    return false;
}

/*************** Requisition approve decline asset status change ****************/
$(document).ready(function () {


    $(document).on('click','.change_status',function () {
        var id = $(this);
        var button_show = '';
        var status =''
        if($(this).data('status') == 'RECOMMANDED'){
            button_show = $(this).parent().find('.btn-danger');
            status = $(this).parent().parent().find('.status').html('RECOMMANDED');
        }else if($(this).data('status') == 'DECLINED'){
            button_show = $(this).parent().find('.btn-success');
            status = $(this).parent().parent().find('.status').html('DECLINED');
        }
        $.ajax({
            type:"POST",
            data:{'asset_id':$(this).data('asset_id'),'status': $(this).data('status')},
            url: globalserver+'ajax2/update_requisition_asset_status',
            success: function(msg)
            {
                //sci_alert("", msg,"success");
                id.hide();
                button_show.show();
            },
            error: function(xhr, status, error) {
                //alert(xhr.responseText);
            }
        });

    })
});


/************** Regsitered Asset Record Edit***************/
function asset_edit(asset_id)
{
    // alert("asset_id");
    var postData =
        {
            "asset_id": asset_id
        };
    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: globalserver + 'ajax2/asset_edit_form',
        success: function (msg)
        {
            //alert(msg);
            $("#asset_edit").html(msg);
        }
    });
    return false;
}

/***********Asset Delete**************/
function asset_delete(delete_id)
{
    //alert(office_id); 

    var postData =
        {
            "delete_id": delete_id
        };

    var dataString = JSON.stringify(postData);

    sci_confirm("Are you sure to delete this Asset?", "Confirmation", function(res){
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {jsondata: dataString},
                url: globalserver + 'asset/delete',
                success: function (msg)
                {
                    sci_alert(msg, "Deleted!","success");
                    //$("#example1 thead tbody").after().load(" #abc");
                    $("#example1").load(" #abc");
                }
            });

        }
    });
    return false;
}

/****************Asset Registration form call*****************/
function registration_form_call(registration_way)
{
    //var registration_way =  document.getElementById("registration_way").value;
    if(registration_way!=0){
        $.ajax({
            type:"POST",
            data:{'registration_way':registration_way},
            url: globalserver+'ajax2/registration_way',
            success: function(msg)
            {

                $("#asset_registration_form_type").html(msg);

            }
        });
        $('#registration_btn').hide();
        $('#registration_details').show();
        $('#back_btn').show();
    }else{
        $('#asset_register_form').html('');
        $('#registration_btn').show();
        $('#registration_details').hide();
        $('#back_btn').hide();
    }


    return false;
}

/***************Search Asset***************/
function searching_asset()
{
    var asset_search =  document.getElementById("asset_no").value;
    var chosen_state_name =  document.getElementById("chosen_state_name").value;
    //alert(state_id);

    if(asset_search=='')
    {
        sci_alert("Please Enter Asset ID","","warning");
        document.getElementById("asset_no").focus();
        document.getElementById("asset_no").style.background = "#e2eef5";
        return false;
    }

    $.ajax({
        type:"POST",
        data:{asset_search:asset_search,chosen_state_name:chosen_state_name},
        url: globalserver+'ajax2/asset_search',
        success: function(msg)
        {
            if(msg==1)
            {
                sci_alert("Sorry! Invalid Asset ID. Please Try Again!","","warning");
            }
            else
            {
                $("#asset_update_form").html(msg);
            }
        }
    });
    return false;

}

function searching_grn()
{
    //alert("hello");
    var grn_no_search =  document.getElementById("grn_no").value;
    //alert(grn_no_search);
    if(grn_no_search=="")
    {
        sci_alert("","Please Enter GRN# to search!","info");
        return false;
    }

    $.ajax({
        type:"POST",
        data:{grn_no_search:grn_no_search},
        url: globalserver+'ajax2/grn_search',
        success: function(msg)
        {
            //alert(msg);
            $("#new_asset_registration").html(msg);
        }
    });

    //return false;
}

/*********** Asset Allocate form ************/
function asset_allocate(requested_id)
{
    $.ajax({
        type:"POST",
        data:{requested_id:requested_id},
        url: globalserver+'ajax2/asset_allocate',
        success: function(msg)
        {
            //alert(msg);
            $("#asset_allocation").html(msg);
        }
    });
}

/********** Asset List for allocation process*************/
function asset_list(requisition_id,category_id,quantity,requisition_pkid,index,process)
{
  $.ajax({
        type:"POST",
        data:{requisition_id:requisition_id,category_id:category_id,quantity:quantity,
            requisition_pkid:requisition_pkid,index:index,process:process},
        url: globalserver+'ajax2/category_assetlist',
        success: function(msg)
        {
            //alert(msg);
            $("#item_list").html(msg);
        }
    });  
}


/*********** Asset Transfer form ************/
function asset_transfer(requested_id)
{
    //alert(requested_id);
    $.ajax({
        type:"POST",
        data:{requested_id:requested_id},
        url: globalserver+'ajax2/asset_transfer',
        success: function(msg)
        {
            //alert(msg);
            $("#asset_transfer").html(msg);
        }
    });
}

/************* Category Wise Asset Get **************/
function category_wise_asset()
{

    var cat_id =  document.getElementById("category_id").value;
    //alert(cat_id);
    $.ajax({
        type:"POST",
        data:{cat_id:cat_id},
        url: globalserver+'ajax2/cat_wise_asset',
        success: function(msg)
        {
            //alert(msg);
            $("#cat_asset").html(msg);
        }
    });
}

/************** Check Item Available *************/
$(document).ready(function(){
    // event.preventDefault();
    $(document).on("change","#cat_asset_id",function(event){

        var cat_asset_id =  document.getElementById("cat_asset_id").value;
        event.preventDefault();

        //alert(cat_asset_id);
        $.ajax({
            type:"POST",
            data:{cat_asset_id:cat_asset_id},
            url: globalserver+'ajax2/cat_asset_count',
            success: function(msg)
            {
                //alert(msg);
                $("#cat_asset_count").html(msg);
            }
        });

    });

})


/************** choose Item Add *************/

function add_allocate_item()
{
    //alert("hello");
    //var cat_id =  document.getElementById("category_id").value;
    var cat_asset_id =  document.getElementById("cat_asset_id").value;
    var total_qty =  document.getElementById("qty_no").value;
    var request_id =  document.getElementById("request_id").value;

    if(cat_asset_id=="")
    {
        start_alert("Please select your item first!","","info");
        return false;
    }
    if(total_qty=="")
    {
        start_alert("Please add quantity","","info");
        return false;
    }

    $.ajax({
        type:"POST",
        data:{cat_asset_id:cat_asset_id,total_qty:total_qty,request_id:request_id},
        url: globalserver+'ajax2/addto_allocation_list',
        success: function(msg)
        {

            if(msg==1)
            {
                sci_alert("Your expected quantity exceeds the limit. Please re-type quantity!", "","info");
                //return false;
            }
            else
            {
                // alert(msg);
                $("#allocation_list").html(msg);
            }

        }
    });

}

/**********temporary choice delete*************/
function choice_delete(temp_id)
{

    var agree = confirm("Are you sure to delete this Record?");
    if (agree == true)
    {

        $.ajax({
            type: "POST",
            data: {temp_id: temp_id},
            url: globalserver + 'ajax2/temp_allocation_delete',
            success: function (msg)
            {
                // $("#example3").load(" #example3");
                $("#allocation_list").html(msg);
            }
        });

    }
    return false;

}
/**************Asset Deliver finally *****************/
function asset_allocation_deliver(requisition_id,requested_by)
{
//alert(requisition_id);
//alert(requested_by);

    $.ajax({
        type:"POST",
        data:{requisition_id:requisition_id,requested_by:requested_by},
        url: globalserver+'asset/allocate',
        success: function(msg)
        {
            //alert(msg);
            sci_alert("", msg,"success");
            $("#temp_list").load(" #temp_list");

        }
    });

    return false;
}
/*************** Single Asset Allocate   ********************/
function single_allocate_item(index,approved_by,item_id,request_id,category_id,asset_qty,office_id,
    allocate_to,allocate_type,event, encription_id,asset_location,on_behlf)
{
    //alert(index);
    //alert(allocate_to);
    event.preventDefault();

    $.ajax({
        type:"POST",
        data:{approved_by:approved_by,item_id:item_id,requisition_id:request_id,category_id:category_id,asset_qty:asset_qty,office_id:office_id,
            allocate_to:allocate_to,allocate_type:allocate_type,asset_location:asset_location,on_behlf:on_behlf},
        url: globalserver+'asset/allocate',
        success: function(msg)
        {
            $("#allocate_item"+index).html("Item is allocating...");
            sci_alert(msg, "","success");
            asset_allocate(encription_id);
        }
    });

    return false;
}

/*************** Single Asset Transfer   ********************/
function transfer_item(index,item_id,request_id,category_id,asset_qty,office_id,allocate_to,allocate_type,event, encription_id)
{
    //alert(item_id);
    //alert(allocate_to);
    event.preventDefault();

    $.ajax({
        type:"POST",
        data:{item_id:item_id,requisition_id:request_id,category_id:category_id,asset_qty:asset_qty,office_id:office_id,allocate_to:allocate_to,allocate_type:allocate_type},
        url: globalserver+'asset/transfer',
        success: function(msg)
        {
            $("#transfer_item"+index).html("Item is transfering...");
            sci_alert(msg, "","success");
            asset_transfer(encription_id);
        }
    });

    return false;
}

/************* Revert Asset ******************/
function asset_revert(from_office,allo_id,revert_id,revert_type,reference_no)
{
    //alert(from_office);
    sci_confirm("Are you sure to revert this asset?", "Confirmation", function(res){
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type:"POST",
                data:{from_office:from_office,allo_id:allo_id,revert_id:revert_id,revert_type:revert_type,reference_no:reference_no},
                url: globalserver+'asset/revert',
                success: function(msg)
                {
                    if(msg == 0)
                    {
                        sci_alert("Please upload acknowledgement paper before reverting.", "","warning");
                        return false;
                    }
                    else
                    {
                        sci_alert(msg, "","success");
                        window.location.reload(false); 
                        $("#example1").load(" #example1");
                    }

                }
            });
        }
    });

    return false;
}
/*********** Asset State Revert  ******************/
function state_revert(state_id,asset_id,state_type)
{
    //alert(from_office);
    sci_confirm("Are you sure to revert this asset?", "Confirmation", function(res){
        if (res)

        {
            $.ajax({
                type:"POST",
                data:{state_id:state_id,asset_id:asset_id,state_type:state_type},
                url: globalserver+'asset/state_revert',
                success: function(msg)
                {
                    sci_alert(msg,"","success");
                    $("#example1").load(" #example1");
                }
            });
        }
    });

    return false;
}
/************** Asset Details Information  *****************/
function asset_details(id)
{
    //alert(id);

    var postData =
        {
            "asset_id": id
        };
    var dataString = JSON.stringify(postData);
    // //alert(dataString);
    $.ajax({
        type:"POST",
        data: {jsondata: dataString},
        url: globalserver+'ajax2/asset_details',
        success: function(msg)
        {
            $("#asset_details").html(msg);
        }
    });
    return false;
}

/****************** Asset Status Update ***********************/
function asset_status_update(id,status_state_name)
{
    var asset_status =  document.getElementById("asset_status").value;

    //alert(status_state_name);  
    var postData =
        {
            "asset_id": id,
            "status_id": asset_status,
            "state_name": status_state_name,
        };
        
    var dataString = JSON.stringify(postData);
    //alert(dataString);
    $.ajax({
        type:"POST",
        data: {jsondata: dataString},
        url: globalserver+'ajax2/asset_status_update',
        error: function (xhr, status, error) {
            alert(xhr.responseText);

        },
        success: function(msg)
        {
            //alert(msg);
            if(msg == 0)
            {
                sci_alert("Please upload acknowledgement paper before disposed.","","info");
            }
            else
            {
                sci_alert(msg, "","success");
            }

        }
    });
    return false;
}

/***************** Allocation Script *******************/
function add_to_list()
{
    //$("#next").hide();
    var item =  document.getElementById("item").value;
    var qty =  document.getElementById("qty").value;

    //alert(item);
    //alert(qty);


    if(item =='-1'){
        sci_alert("Please Select Category","","warning");
        return;
    }

    if(qty=='')
    {
        sci_alert("Please Add a Quantity","","warning");

        document.getElementById("qty").focus();
        document.getElementById("qty").style.background = "#ffe6e6";
        return false;
    }


    $.ajax({
        type:"POST",
        data: {category_id: item,quantity:qty},
        url: globalserver+'ajax3/request_availability',
        success: function(msg)
        {
            //sci_alert("", msg,"info");

            if(msg==0)
            {
                sci_alert("Sorry! This asset is not available now.", "","info");
            }
            else
            {
                $("#add_product").refresh;
                $("#selected_asset").html(msg);
                $("#request_next").show();
                //$(this).find("#next").hide();
            }
        }
    });

    return false;
}

/************** Temporary allocation Delete  *******************/
function tmp_delete(item_id)
{

    sci_confirm("Are you sure to delete this item?", "Confirmation", function(res){
        if (res)
        //sci_alert('message will go here');
        {

            $.ajax({
                type:"POST",
                data: {item_id: item_id},
                url: globalserver+'ajax3/temp_delete',
                success: function(msg)
                {
                    //sci_alert("", msg,"info");
                    //sci_alert("", "Item is deleted","success");
                    $("#selected_asset").html(msg);
                }
            });
        }
    });

    return false;

}

/****************** File Uploading class ******************/
$(document).ready(function(){
    $(document).on('change', '#file', function(event){
        event.preventDefault();
        var name = document.getElementById("file").files[0].name;
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','docx','pdf','doc']) == -1)
        {
            sci_alert("Sorry! Invalid File Type.", "","info");
            //sci_alert("Invalid Image File");
            return false;
        }
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("file").files[0]);
        var f = document.getElementById("file").files[0];
        var fsize = f.size||f.fileSize;
        if(fsize > 2000000)
        {
            //alert("Image File Size is very big");
            sci_alert("Image File Size is Very Big!", "","info");
            return false;
        }
        else
        {
            form_data.append("file", document.getElementById('file').files[0]);
            $.ajax({
                url:globalserver+"ajax3/upload",
                method:"POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend:function(){
                    $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
                },
                success:function(data)
                {
                    //sci_alert("", "File Uploaded Successfully!","success");
                    $('#uploaded_image').html(data);
                }
            });
        }
    });
});


/***************  Attachment Delete ******************/
function file_delete(file_id,file_name)
{
    sci_confirm("Are you sure to delete this file?", "Confirmation", function(res){
        if (res)
        //sci_alert('message will go here');
        {

            $.ajax({
                type:"POST",
                data: {file_id: file_id,file_name:file_name},
                url: globalserver+'ajax3/file_delete',
                success: function(data)
                {

                    //sci_alert("", msg,"success");
                    $('#uploaded_image').html(data);
                }
            });

        }
    });

    return false;

}

/***************  Disposed Attachment Delete ******************/
function dispose_file_delete(file_id,file_name,asset_id)
{
    sci_confirm("Are you sure to delete this file?", "Confirmation", function(res){
        if (res)
        //sci_alert('message will go here');
        {

            $.ajax({
                type:"POST",
                data: {file_id: file_id,file_name:file_name,asset_id:asset_id},
                url: globalserver+'ajax3/disposed_file_delete',
                success: function(data)
                {

                    //sci_alert("", msg,"success");
                    $('#dispose_image').html(data);
                }
            });

        }
    });

    return false;

}
/*************** Show Attachment of this requisition  ********************/
function show_attachment(reference_no,requisition_no)
{
    //alert(reference_no);
    //alert(requisition_no);
    $.ajax({
        type:"POST",
        data: {reference_no:reference_no,requisition_no: requisition_no},
        url: globalserver+'ajax3/get_files',
        success: function(msg)
        {
            //sci_alert("", msg,"info"); 

            //sci_alert("", "Item is deleted","success");
            //$("#selected_asset").refresh();
            $("#attached_files").html(msg);
        }
    });
}
/***************  Allocation Attachment Add after allocation*******************/
function add_attachment(requisition_no,url,div_id)
{
    //alert(url);
    //alert(div_id);

    $.ajax({
        type:"POST",
        data: {requisition_no: requisition_no},
        url: globalserver+url,
        success: function(msg)
        {
            //sci_alert("", msg,"info");
            //alert(msg);
            //sci_alert("", "Item is deleted","success");
            //$("#selected_asset").refresh();
            $('#'+div_id).html(msg);
        }
    });
}
/******** Add to Previous allocation*********/
function old_allocation_attachment(requisition_no)
{
    //alert(requisition_no);
    $.ajax({
        type:"POST",
        data: {requisition_no: requisition_no},
        url: globalserver+'ajax3/add_to_allocation',
        success: function(data)
        {
            //alert(msg);
            if(data==3)
            {
                sci_alert("Please upload files before add.", "","info");
            }
            else
            {
                //sci_alert("File is attached", "","success");
                $("#uploaded_files").html(data);
                $("#uploaded_image").load(" #uploaded_image");
            }

        }
    });
    return false;
}
/************** Allocation Details of a requisition or reference *************/

function allocation_details(id,reference_no)
{
    //alert(id);
    var postData =
        {
            "requisition_no": id,
            "reference_no":reference_no
        };
    var dataString = JSON.stringify(postData);
    $.ajax({
        type:"POST",
        data: {jsondata: dataString},
        url: globalserver+'ajax3/allocation_details',
        success: function(msg)
        {
            $("#allocation_details").html(msg);
        }
    });
    return false;
}

/************ Auto Fill of Asset Title ******************/

$(document).ready(function(){
    $('#asset_name').keyup(function(){
        var query = $(this).val();
        //alert(query);
        if(query != '')
        {
            $.ajax({
                url:globalserver+"ajax3/assetsearch",
                method:"POST",
                data:{query:query},
                success:function(data)
                {
                    $('#assetList').fadeIn();
                    $('#assetList').html(data);
                }
            });
        }
    });
    $(document).on('click', '#assetList li', function(){
        $('#asset_name').val($(this).text());
        $('#assetList').fadeOut();
    });
});

/*
 function input_box()
 {
  alert("hello");
  return false;
 } 
 */

 /***** Edit capitalization Registration Limit Edit *******/
 function capital_limit_edit(capital_id)
 {
    //alert(capital_encrypt);

    var postData =
        {
            "capital_id": capital_id
        };

    var dataString = JSON.stringify(postData);
    $.ajax({
            type:"POST",
            data: {jsondata: dataString},
            url: globalserver+'ajax/registration_limit',    
            success: function(msg)
             {
                $("#capital_limit").html(msg);
              }
            }); 
    return false;
 }

 /*********** Capital registration submit  **************/
 function update_capital_limit_submit(capital_id)
 {
    //alert(capital_id);
    var capital_limit =  document.getElementById("entry_limit").value;
    //alert(capital_limit);

    if(capital_limit < 10)
    {
       sci_alert("Limit can not be less than 10", "","warning");
       document.getElementById("entry_limit").focus();
       document.getElementById("entry_limit").style.background = "#ffe6e6";
       return false; 
    }

    if(capital_limit > 15)
    {
       sci_alert("Limit can not be greater than 15", "","warning");
       document.getElementById("entry_limit").focus();
       document.getElementById("entry_limit").style.background = "#ffe6e6";
       return false; 
    }

    var postData =
        {
            "updateid": capital_id,
            "entry_limit": capital_limit,
        }
        
    var dataString = JSON.stringify(postData);
    $.ajax({
        type:"POST",
        data: {jsondata: dataString},
        url: globalserver+'asset/capital_limit_update',
        success: function(msg)
        {
           sci_alert(msg, "","success");
           $("#example1").load(" #example1");
        }
    });

    return false;
 }

/**********  Generate Procurement Request ****************/
function generate_pr_request(reference_no,request_id)
{
  //alert(reference_no);
  //alert(request_id);
  //alert(pr_list);
 var pr_list =  document.getElementById("pr_list").value;
 //alert(pr_list);
  sci_confirm("Do you want to raise PR for unavailable items?", "Confirmation", function(res){
    if (res)
            //sci_alert('message will go here');
          {
            $.ajax({
            type:"POST",
            data: {ref_no: reference_no,request_id:request_id,pr_list:pr_list},
            url: globalserver+'ajax/requested_pr_submit',    
            success: function(msg)
             {
                //$("#reference_details").html(msg);

                if(msg != 0)
                {
                  sci_alert(msg,"","success"); 
                  $("#pr_button").hide(); 
                }
                else
                {
                  sci_alert("Please select item first to raise PR ","","info");    
                }
                
               
             }
            }); 
          }
          
      });

  return false;
}

/*************  From Asset List*************/
function call_permission(asset_id,index)
{
    //alert(asset_id);
    //alert(index);
    $.ajax({
            type:"POST",
            data: {asset_id: asset_id},
            url: globalserver+'asset/permission_button',    
            success: function(msg)
             {
                $("#abc"+index).html(msg);
              }
            }); 

return false;
}

/*************  From Asset List*************/
function free_permission(asset_id,index)
{
    $(document).ready(function(){
               
   $("#abc"+index).toggle();
});
    
return false;
}

/********Database Backup********/

function db_export()
{

    sci_confirm("Do you really want to backup your database?", "Confirmation", function(res){
    if (res)
            //sci_alert('message will go here');
          {
              $("#modalLoading").modal({backdrop: 'static', keyboard: false});
            $.ajax({
            type:"POST",
            //data: {ref_no: reference_no,request_id:request_id,pr_list:pr_list},
            url: globalserver+'ajax4/startbackup',    
            success: function(msg)
             {

                if(msg != 0)
                {                  

                  //$("#backup_button").hide(); 
                  
                  sci_alert(msg,"","success"); 
                  //document.getElementById("backup_button").disabled = false;
                  //$("#backup_button").show();
                }
                else
                {
                  sci_alert("Sorry! Failed to export database.","","warning");    
                }
                 $("#modalLoading").modal('hide');
               
             }

            }); 
          }
          
      });


    return false;
}

/*****  Database backup delete*******/
function backup_delete(file_name)
{

    sci_confirm("Do you really want to delete your file?", "Confirmation", function(res){
    if (res)
           {
            $.ajax({
            type:"POST",
            data: {file_name: file_name},
            url: globalserver+'ajax4/db_delete',    
            success: function(msg)
             {
                sci_alert(msg,"","success"); 
                $("#example1").load(" #example1");
             }

            }); 
          }
          
      });

    return false;
}

/*
$(document).ready(function(){
$("#permission_details").on("mouseover mouseout",function()
{                  
   $("#my-box").toggle();
});

});
*/

/****************** Disposed File Uploading Class ******************/
$(document).ready(function(){
    $(document).on('change', '#dispose_file', function(event){
        event.preventDefault();

        var name = document.getElementById("dispose_file").files[0].name;
        var asset_id = document.getElementById("asset_id").value;

        //alert(reference_no);
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','docx','pdf','doc']) == -1)
        {
            sci_alert("Sorry! Invalid File Type.", "","info");
            //sci_alert("Invalid Image File");
            return false;
        }
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("dispose_file").files[0]);
        var f = document.getElementById("dispose_file").files[0];
        var fsize = f.size||f.fileSize;
        if(fsize > 2000000)
        {
            //alert("Image File Size is very big");
            sci_alert("Image file size must be less than 2 MB!", "","info");
            return false;
        }
        else
        {
            form_data.append("file", document.getElementById('dispose_file').files[0]);
            form_data.append("asset_id", document.getElementById('asset_id').value);

            $.ajax({
                url:globalserver+"ajax3/dispose_upload",
                method:"POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend:function(){
                    $('#dispose_image').html("<label class='text-success'>Image Uploading...</label>");
                },
                success:function(data)
                {
                    //sci_alert("", "File Uploaded Successfully!","success");
                    $('#dispose_image').html(data);
                }
            });
        }
    });
});
