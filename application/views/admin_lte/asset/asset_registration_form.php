<form id="asset_register_form">
    <?php
    if ($registration_way == 1) /*******New Asset Registration***********/ {
        ?>
        <div class="form-group col-sm-offset-1">
            <label for="exampleInputName" class="col-sm-1 control-label"><?php echo load_message('GRN'); ?><span
                        class="text-danger">*</span></label>
            <div class="col-sm-8">
            <select name="grn_no" id="grn_no" class="form-control">
            <option value="" selected="selected">Select GRN #</option>
            <?php 
			foreach($grn_list as $glist)
			{
			 echo $asst_title_name = $glist->asset_name;
			 echo $asst_grn = $glist->grn_no;
			 ?>
             <option value="<?php echo $asst_grn;?>"><?php echo $asst_title_name;?>&nbsp;(<?php echo $asst_grn;?>)</option>
             <?php
			}
			?>
            </select>
              <!--  <input type="text" class="form-control" id="grn_no" name="grn_no"
                       placeholder="<?php //echo load_message('FIND_ASSET_BY_GRN'); ?>">-->
            </div>
                
            <div class="col-sm-1">
                <button name="search_grn" type="button" class="btn btn-info"
                        title="<?php echo load_message('FIND_TITLE'); ?>" onclick="searching_grn();">
                    <i class="fa fa-binoculars"></i> &nbsp; <?php echo load_message('FIND_TITLE'); ?>
                </button>
            </div>
        </div>
        <br>
        <br>
        <?php
    } else {
        ?>
        <div id="old_asset_registration"> <!--New Asset Registration Starts here-->
            <?php
            $this->load->view('admin_lte/asset/old_asset_registration');
            ?>
        </div> <!--New Asset Registration div end here -->
        <?php
    }
    ?>

    <?php
    if ($registration_way == 1) /*******New Asset Registration***********/ {
        ?>
        <div id="new_asset_registration"> <!--New Asset Registration Starts here-->

            <?php
            //    $data['read_mode'] = 1;
            //    $this->load->view('admin_lte/asset/new_asset_registration', $data);
            ?>
        </div> <!-- New Asset Registration div end here  -->
        <?php
    } else {
        ?>
        <div id="old_asset_registration"> <!--New Asset Registration Starts here-->
            <?php
            //$this->load->view('admin_lte/old_asset_registration');
            ?>
        </div> <!-- New Asset Registration div end here  -->
        <?php
    }
    ?>

    <!-- /.box-body -->

    <!-- /.box-footer-->

    <div class="box-footer">
        <div align="right" hidden id="submit_btn">
            <!-- <button type="submit" class="btn btn-primary" onClick="return asset_register();">Submit</button>-->
            <button type="submit" class="btn btn-success btn-lg"
                    onClick="handle_form('asset_register_form', asset, 'ajax/asset_register',0,'manual')">
                <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_ASSET'); ?>
            </button>
        </div>
    </div>
</form>
