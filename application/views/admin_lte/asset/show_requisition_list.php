<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- <div class="box-header">
            <h3 class="box-title">Panding Requisition List</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <th width="10">#</th>
                <th width="120"><?php echo load_message('REQUISITION_TITLE'); ?></th>
                <th width="130"><?php echo load_message('REMARKS'); ?></th>
                <th width="80"><?php echo load_message('REQUISITION_FOR'); ?></th>
                <th width="80"><?php echo load_message('REQUISITION_REQUEST_INITIATED_BY'); ?></th>
                <th width="80"><?php echo load_message('ALLOCATION_LOCATION'); ?></th>
                <th width="80"><?php echo load_message('STATUS'); ?></th>
                <th width="80"><?php echo load_message('REQUISITION_REQUESTED_DATE'); ?></th>
                <th width="80"><?php echo load_message('REQUISITION_LAST_REVIEWED_DATE'); ?></th>
                <th width="20"></th>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($requisition_list) {
                    foreach ($requisition_list as $rlist) {
                        $title = $rlist->title;
                        $remark = $rlist->remark;
                        $on_behalf = $rlist->on_behalf;
                        if ($on_behalf == null) {
                            $on_behalf = $rlist->userid;
                        }
                        $allocation_location = $rlist->asset_location;
                        $created_date = $rlist->created_date;
                        $update_date = $rlist->update_date;
                        $requisition_id = $rlist->id;
                        $user_id = $rlist->userid;
                        $status = $rlist->status;

                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $title; ?></td>
                            <td><?php echo $remark; ?></td>
                            <td><?php get_user_fullname($user_id); ?></td>
                            <td><?php echo get_user_fullname($on_behalf); ?></td>
                            <td><?php echo get_office_room($allocation_location); ?></td>
                            <td><?php echo $status; ?></td>
                            <td><?php echo get_format_date($created_date); ?></td>
                            <td><?php if($update_date == NULL){ echo ""; }else{ echo get_format_date($update_date); }  ?></td>
                            <td>
                                <button title="Show" class="btn btn-primary btn-xs" data-toggle="modal"
                                   data-target="#modal-requisition"
                                   onclick="show_requisition_list_only('<?php echo $requisition_id; ?>');"><i
                                            class="fa fa-folder-open fa-x"></i></button>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->


        <!-- ------------Modal for Cpitalization edit Start--------------- -->
        <div class="modal fade" id="modal-requisition">
            <div class="modal-dialog" style="width:660px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('REQUISITION_DETAILS'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="requisition_list"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal for Capital Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->