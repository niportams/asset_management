<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<style>
    .modal-header {
        background-color: #28A0C8;
        color: #FFF9F9;
    }

    .modal-body {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: none;
        border-right-style: solid;
        border-bottom-style: none;
        border-left-style: solid;
        border-top-color: #28A0C8;
        border-right-color: #28A0C8;
        border-bottom-color: #28A0C8;
        border-left-color: #28A0C8;
    }

    .modal-footer {

        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;

        border-right-style: solid;
        border-bottom-style: none;
        border-left-style: solid;

        border-right-color: #28A0C8;
        border-bottom-color: #28A0C8;
        border-left-color: #28A0C8;

        background-color: #F0F0F0;
    }

</style>

<?php
if ($asset_details <> "") {
    foreach ($asset_details as $adetails) {
        $asset_id = $adetails->asset_id;
        $encrypt_asset = base64_encode($asset_id);
        $capital_id = $adetails->capital_id;
        $category_id = $adetails->category_id;
        $manufacture_id = $adetails->manufacture_id;
        $asset_name = $adetails->asset_name;
        $asset_description = $adetails->asset_description;
        $office_id = $adetails->office_id;
        $model_name = $adetails->model_name;
        $model_no = $adetails->model_no;
        $grn_no = $adetails->grn_no;
        $sku = $adetails->sku;
        $serial_no = $adetails->serial_no;
        $reference_no = $adetails->reference_no;
        $purchase_order_no = $adetails->purchase_no;
        $purchase_date = $adetails->purchase_date;
        $purchase_price = $adetails->purchase_price;
        $purchase_location = $adetails->purchase_location;
        $status_id = $adetails->asset_status_id;
        $warrenty_date = $adetails->warrenty_date;
        $lifetime = $adetails->asset_lifetime;
        $assigned_to = $adetails->assigned_to;
        $label_id = $adetails->label_id;
        $read_id = $adetails->asset_readable_id;
        $sci_id = $adetails->sci_id;
        $db_status_id = $adetails->asset_status_id;
        $asset_remarks = $adetails->asset_remarks;
    }
}
?>
<!-- Main content -->
<div class="box-body form-horizontal">

    <div class="col-sm-6">

        <table class="table table-bordered-bottom">

            <tr>
                <td><strong>
                        <?= load_message('OFFICE'); ?>:
                    </strong></td>
                <td><?= get_office_name($office_id); ?></td>
            </tr>

            <tr>
                <td><strong>
                        <?= load_message('ASSET_TITLE'); ?>:
                    </strong></td>
                <td><?= $asset_name; ?></td>
            </tr>

            <?php //if ($assigned_to != "" && $assigned_to != NULL && $assigned_to != 0): ?>
            <tr>
                <td><strong>
                        <?= load_message('ASSIGNED_TO'); ?>:
                    </strong></td>
                <td><?= get_user_fullname($assigned_to); ?></td>
            </tr>
            <?php //endif; ?>

            <tr>
                <td><strong>
                        <?= load_message('ASSET_ID_L'); ?>:
                    </strong></td>
                <td><?php echo $read_id; ?></td>
            </tr>

            <?php //if ($sci_id != "" && $sci_id != NULL && $sci_id != 0): ?>
            <tr>
                <td><strong>
                        <?= load_message('ASSET_LABEL_ID'); ?>:
                    </strong></td>
                <td><?= $sci_id; ?></td>
            </tr>
            <?php //endif; ?>

            <?php //if ($category_id != "" && $category_id != NULL && $category_id != 0): ?>
            <tr>
                <td><strong>
                        <?= load_message('CATEGORY'); ?>:
                    </strong></td>
                <td>
                    <?php echo get_category_name($category_id); ?>
                    <div class="small"><?= get_parent_category_name($category_id);
                    echo ' > ';
                    get_category_name($category_id); ?></div>
                </td>
            </tr>
            <?php // endif;  ?>


                        <?php //if ($asset_description != "" && $asset_description != NULL && $asset_description != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('DESCRIPTION'); ?>:
                    </strong></td>
                <td><textarea style="width:100%; border:none; background: transparent;"><?= $asset_description; ?></textarea></td>
            </tr>
            <?php //endif; ?>

                        <?php //if ($model_name != "" && $model_name != NULL && $model_name != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('MODEL_NAME'); ?>:
                    </strong></td>
                <td><?= $model_name; ?></td>
            </tr>
            <?php //endif; ?>

                        <?php //if ($model_no != "" && $model_no != NULL && $model_no != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('MODEL_NO'); ?>:
                    </strong></td>
                <td><?= $model_no; ?></td>
            </tr>
            <?php //endif; ?>

                        <?php //if ($serial_no != "" && $serial_no != NULL && $serial_no != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('SERIAL_NO'); ?>:
                    </strong></td>
                <td><?= $serial_no; ?></td>
            </tr>
            <?php //endif; ?>

                        <?php //if ($reference_no != "" && $reference_no != NULL && $reference_no != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('REF'); ?>:
                    </strong></td>
                <td><?= $reference_no; ?></td>
            </tr>
            <?php //endif;  ?>


                        <?php //if ($manufacture_id != "" && $manufacture_id != NULL && $manufacture_id != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('MANUFACTURER'); ?>:
                    </strong></td>
                <td><?= get_manufacture_name($manufacture_id); ?></td>
            </tr>
            <?php //endif;  ?>


                        <?php //if ($warrenty_date != "" && $warrenty_date != NULL && $warrenty_date != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('WARRENTY_EXP_DATE'); ?>:
                    </strong></td>
                <td><?php echo ($warrenty_date!="")?get_format_date($warrenty_date):""; ?></td>
            </tr>
<?php //endif;  ?>
            <tr>
                <td><strong>
                        <?= load_message('ASSET_LIFETIME'); ?>:
                    </strong></td>
                <td><?php echo ($lifetime!="")?get_format_date($lifetime):""; ?></td>
            </tr>


        </table>

    </div>
    <div class="col-sm-6">




        <table class="table table-bordered-bottom">
                        <?php //if ($sku != "" && $sku != NULL && $sku != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('SKU'); ?>:
                    </strong></td>
                <td><?= $sku; ?></td>
            </tr>
            <?php //endif; ?>

                        <?php //if ($purchase_date != "" && $purchase_date != NULL && $purchase_date != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('PURCHASE_DATE'); ?>:
                    </strong></td>
                <td><?php echo get_format_date($purchase_date); ?></td>
            </tr>
            <?php //endif; ?>

                        <?php //if ($purchase_price != "" && $purchase_price != NULL && $purchase_price != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('PURCHASE_PRICE'); ?>:
                    </strong></td>
                <td><?= $purchase_price; ?></td>
            </tr>
            <?php //endif; ?>

                        <?php //if ($purchase_order_no != "" && $purchase_order_no != NULL && $purchase_order_no != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('PURCHASE_ORDER_NO'); ?>:
                    </strong></td>
                <td><?= $purchase_order_no; ?></td>
            </tr>
            <?php //endif; ?>

                        <?php //if ($purchase_location != "" && $purchase_location != NULL && $purchase_location != 0):  ?>
            <tr>
                <td><strong>
<?= load_message('PURCHASE_LOC'); ?>:
                    </strong></td>
                <td><?= $purchase_location; ?>    </td>
            </tr>
<?php //endif;  ?>

            <tr>
                <td><strong>
<?= load_message('ASSET_REMARKS'); ?>:
                    </strong></td>
                <td><?= $asset_remarks; ?></td>
            </tr>

            <tr>
                <td><strong>
<?= load_message('ASSET_STATE'); ?>:
                    </strong></td>
                <td><?= get_asset_status($status_id); ?></td>
            </tr>

        </table>

        <div class="panel panel-info" style="cursor:pointer">
            <div class="panel-heading">
                <div class="panel-title" id="open-map">Asset Location <span class="small">(Click to open)</span></div>

            </div>
           
            <div class="panel-body" style="display:none"  id="map-body">
                <iframe id="map-frame" style="width:100%; height:350px" frameBorder="0" style="border:0" src=""
                    allowfullscreen></iframe>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $(document).on('click', '#open-map', function () {
                    $("#map-body").show();
                    
                    <?php
                        $loc =get_geo_loc($office_id);
                        $lat = substr($loc, 0, strpos($loc, ","));
                        $lng = substr($loc, strpos($loc, ",")+1 );
                    ?>
                     u=globalserver+"ajax/google_map/<?= $lat; ?>/<?= $lng; ?>";   
                     $("#map-frame").attr('src',u);                  
                })
            })
        </script>

    </div>

</div><!-- /.box-body -->