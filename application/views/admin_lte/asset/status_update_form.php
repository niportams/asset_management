<?php
foreach ($asset_details as $adetails) {
    $asset_id = $adetails->asset_id;
    $encrypt_asset = base64_encode($asset_id);
    $capital_id = $adetails->capital_id;
    $category_id = $adetails->category_id;
    $manufacture_id_edit = $adetails->manufacture_id;
    $asset_name = $adetails->asset_name;
    $asset_description = $adetails->asset_description;
    $office_id = $adetails->office_id;

    $model_name = $adetails->model_name;

    $model_no = $adetails->model_no;
    $grn_no = $adetails->grn_no;
    $sku = $adetails->sku;
    $serial_no = $adetails->serial_no;
    $reference_no = $adetails->reference_no;
    $purchase_order_no = $adetails->purchase_no;
    $purchase_date = $adetails->purchase_date;
    $purchase_price = $adetails->purchase_price;
    $asset_readable_id = $adetails->asset_readable_id;

    $warrenty_date = $adetails->warrenty_date;
    $warrenty_date = date("d/m/Y", strtotime($warrenty_date));
    $purchase_date = date("d/m/Y", strtotime($purchase_date));
    //$purchase_date=date("Y-m-d", strtotime($purchase_date));

    //$date = strtotime($date);
    //$date = date('d-m-Y', $date);

    $label_id = $adetails->label_id;
    $sci_id = $adetails->sci_id;
    $db_status_id = $adetails->asset_status_id;
    $asset_remarks = $adetails->asset_remarks;
}
//echo $status_desired_state;
?>

<div class="box-body">
    <!-- <div id="asset_status_change" style="background-color:#FFFFFF; border-style:dotted; border-width:1px;"  align="center">-->
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('ASSET_TITLE'); ?></label>

                <input type="text" class="form-control" value="<?php echo $asset_name; ?>"
                       readonly="readonly" style="background-color:#FFF;">

            </div>
            <div class="form-group">
                <label for="exampleInputName"
                       class="control-label"><?php echo load_message('ASSET_LABEL_ID'); ?></label>

                <input type="text" class="form-control" value="<?php echo $sci_id; ?>"
                       readonly="readonly" style="background-color:#FFF;">

            </div>


            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('CATEGORY'); ?></label>

                <input type="text" class="form-control" value="<?php echo get_category_name($category_id); ?>"
                       readonly="readonly" style="background-color:#FFF;">

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('MODEL_NAME'); ?></label>

                <input type="text" class="form-control" value="<?php echo $model_name; ?>"
                       readonly="readonly" style="background-color:#FFF;">

            </div>
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('ASSET_ID_L'); ?></label>

                <input type="text" class="form-control" value="<?php echo $asset_readable_id; ?>"
                       readonly="readonly" style="background-color:#FFF;">

            </div>

            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('STATUS'); ?><span
                            class="text-danger">*</span></label>

                <select name="asset_status_id" id="asset_status" class="form-control">

                    <?php
                    foreach ($status_list as $stlist) {
                        $status_id = $stlist->status_id;
                        $status_name = $stlist->status_name;
                        if ($status_id == $db_status_id) {
                            ?>
                            <option value="<?php echo $status_id; ?>"
                                    selected="selected"><?php echo $status_name; ?></option>
                            <?php
                        } else {
                            if ($status_desired_state == $status_name) {
                                ?>
                                <option value="<?php echo $status_id; ?>"><?php echo $status_name; ?></option>
                                <?php
                            }
                            ?>

                            <?php
                        }
                    }
                    ?>

                </select>
                <div class="form-group">&nbsp;</div>
            </div>
        </div>
    </div>

   
    <!--<div class="form-group">&nbsp;</div>  -->
    <!--</div> -->
    <?php 
	if($status_desired_state == "Disposed") /************ Asset Disposal Purpose*************/
	{
	?>
     <div class="box box-info">
                <div class="box-header with-border">
                    <h4><?php echo load_message('UPLOAD_ASSOCIATE_FILES'); ?></h4>
                </div>
                <div class="box-body form-horizontal">
                 <br/>
                    <a href="#" onclick="return clickAnother();">
                        <div class="upload">
                            <i class="fa fa-upload" aria-hidden="true" style="font-size: 5em;"></i>
                            <h4><?php echo load_message('CLICK_TO_UPLOAD'); ?></h4>
                            <label class="control-label"><?php echo load_message('SELECT_FILE_JPG_PNG_PDF_DOCX'); ?></label>
                        </div>
                    </a>
                    <input type="hidden" name="asset_id" id="asset_id" value="<?php echo $asset_id;?>"/>
                    <input type="file" name="file" id="dispose_file" style="display: none;"/>
                    <hr/>
                    <table id="buildyourform" style="width:350px;" class="table table-striped">
                        <thead>
                        <tr>
                            <th width="12px;"><?php echo load_message('FILE_NAME'); ?></th>
                            <th width="12px;"></th>
                        </tr>
                        </thead>
                        <!-- <span id="uploaded_image"></span>-->
                        <tbody id="dispose_image">
							<?php
                            if ($file_list <> "") {
                            foreach ($file_list as $flist) {
                            $file_id = $flist->file_id;
                            $file_name = $flist->file_name;
                            $asset_id = $flist->asset_id;
                            ?>
                            
                            <tr>
                            <td>
                            <!-- <img src="<?php //echo base_url();
                            ?>uploads/<?php //echo $file_name; ?>" height="50px;" width="50px;" border="0"/>-->
                            <?php echo $file_name; ?>
                            </td>
                            
                            <td>
                            <a href="<?php echo base_url(); ?>uploads/<?php echo $file_name; ?>" class="btn btn-success btn-xs"
                            title="Show File" target="_blank"><i class="fa fa-external-link " aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-danger btn-xs" title="Delete File"
                            onclick="return dispose_file_delete('<?php echo $file_id; ?>','<?php echo $file_name; ?>','<?php echo $asset_id;?>');">
                            <i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                            </tr>
                            <?php
                            }
                            }
                            ?>
                        
						
                        </tbody>
                    </table>

                    <div class="form-group" align="justify">&nbsp;</div>
                </div>
            </div>
       <?php 
		}
	   ?>
       <div class="form-group">
        <div align="right">
            <!-- <button type="submit" class="btn btn-primary" onClick="return asset_register();">Submit</button>-->
            <button type="button" class="btn btn-success btn-lg"
                    onClick="asset_status_update('<?php echo $encrypt_asset; ?>','<?php echo $status_desired_state;?>')"><i class="fa fa-save"></i> &nbsp;
                Change State
            </button>
            <br>
        </div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--<script src="dist/jquery.imageuploader.js"></script>-->
<script>
    (function () {
        var options = {};
        $('.js-uploader__box').uploader(options);
    }());

    function clickAnother() {
        $("#dispose_file").click();
		return false;
    }
</script>
                    
                      