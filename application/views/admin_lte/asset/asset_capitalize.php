<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->

  
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header"></div>
        <!-- <form class="form-horizontal"  id="capital_form"> -->
        <div class="box-body" id="capital_form">
            <div class="row">
                <!-- <div class="box-body">-->
                <!-- <form class="form-horizontal">-->
                <div class="col-sm-12"><?php echo load_message('MANDATORY_TEXT'); ?></div>
                <div class="col-sm-12">&nbsp;</div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="exampleInputName" class=" control-label"><?php echo load_message('ASSET_TITLE'); ?>
                   <span class="text-danger">*</span></label>
                   <input type="text" class="form-control" id="asset_name" placeholder="<?php echo load_message('ASSET_TITLE'); ?>" autocomplete="off">
                  <div id="assetList" class="suggestion-list"></div>  
                  </div>

                    <div class="form-group" align="justify">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('CATEGORY'); ?><span
                         class="text-danger">*</span></label>
                        <?php get_category_list("category_id", "category_id", "form-control select2", load_message('SELECT_CATEGORY')); ?>
                    </div>
                    <div class="form-group" align="justify">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('SUPPLIER'); ?><span
                                    class="text-danger">*</span></label>
                        <select name="supplier_id" id="supplier_id" class="form-control select2">
                            <option value="" selected="selected"><?php echo load_message('SELECT_SUPPLIER'); ?></option>
                            <?php
                            foreach ($supplier_list as $slist) {
                                $supplier_id = $slist->supplier_id;
                                $supplier_name = $slist->supplier_name;
                                ?>
                                <option value="<?php echo $supplier_id; ?>"><?php echo $supplier_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    
                     <div class="form-group" align="justify">
                      <label for="exampleInputName" class="control-label"><?php echo load_message('DEPARTMENT');?></label>
                        <select name="department_id" id="department_id" class="form-control select2">
                            <option value="0" selected="selected"><?php echo load_message('SELECT_DEPARTMENT'); ?></option>
                            <?php
                            foreach ($dept_list as $dlist) {
                                $department_id = $dlist->department_id;
                                $department_name = $dlist->department_name;
                                ?>
                                <option value="<?php echo $department_id; ?>"><?php echo $department_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                  
				  <input type="text" class="form-control" id="office_id" value="<?php echo $user_center; ?>" style="display:none;">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('VOUCHER'); ?><span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="voucher_no"
                               placeholder="<?php echo load_message('VOUCHER'); ?>">
                    </div>
                    <div class="form-group">

                        <label><?php echo load_message('GRN'); ?><span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="grn_no" value="<?php echo $grn; ?>" readonly="readonly" 
                        title="<?php echo load_message('GRN_TOOLTIP'); ?>">

                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label><?php echo load_message('NO_ITEM_RECEIVED'); ?><span
                                        class="text-danger">*</span></label>
                            <input type="text" class="form-control number-only" id="total_received" placeholder="#">
                            <input type="text" class="form-control" id="registered_item" placeholder="#"
                                   style="display:none;" value="0">
                        </div>

                        <div class="col-md-6 ">
                            <label><?php echo load_message('RECEIVED_BY'); ?></label>
                             <select name="received_by" id="received_by" class="form-control select2">
                            <option value="" selected="selected">Select User</option>
                            <?php
                            foreach ($user_list as $uslist) {
                                $user_id = $uslist->userid;
                                $user_name = $uslist->fullname;
                                ?>
                                <option value="<?php echo $user_id; ?>"><?php echo $user_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                            
                          <!--  <input type="text" class="form-control" id="received_by"
                                   placeholder="<?php //echo load_message('RECEIVED_BY'); ?>">-->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <br/>
                            <label><?php echo load_message('GIK'); ?> &nbsp; <input type="radio" name="gik" value="1"
                                                                                    checked="checked"></label>
                            <!-- <input type="text" class="form-control" id="funded_by" placeholder="Funded By" hidden="true"/>-->
                            <div id="textboxes">
                                <select name="funded_by" id="funded_by" class="form-control select2">
                                    <option value="" selected="selected"><?php echo load_message('SELECT_FUNDED_BY'); ?></option>
                                    <?php
                                    foreach ($org_list as $org_list) {
                                        $organization_name = $org_list->organization_name;
                                        ?>
                                        <option value="<?php echo $organization_name; ?>"><?php echo $organization_name; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <br/>
                            <label><?php echo load_message('OWN_FUNDED'); ?> &nbsp; <input type="radio" name="gik"
                                                                                         value="0"></label>
                            <!-- <input type="text" class="form-control" id="own_funded" placeholder="NIPORT" value="NIPORT"  />-->
                            <div id="textbox" style="display: none">
                                <input type="text" class="form-control" id="own_funded"
                                       value="<?php echo load_message('NIPORT_SHORT'); ?>"/>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- </form>-->
            </div>
        </div>

        <!-- /.box-body -->
        <div class="box-footer">
            <!-- <div align="center"><button type="submit" class="btn btn-primary" onClick="return capital_validation();">Submit</button></div>-->
            <div align="right">
                <button type="submit" class="btn btn-success btn-lg"
                        onClick="return form_validation('ajax/asset_capital',capital,'capital_form',0);"><i
                            class="fa fa-save"></i> &nbsp; <?php echo load_message('CAPITAL_NEW_ASSET'); ?>
                </button>
            </div>
            <br>
        </div>
    </div>
    <!-- /.box-footer-->
    <!-- </div>-->
    <!-- /.box -->

</section>
<!-- /.content -->

<script type="text/javascript">
    $(document).ready(function(){
        $(document).click(function(){
            $(".suggestion-list").hide();
        });
    })
</script>
