<div class="box-body form-horizontal">
    <h4><?php echo load_message('UPLOAD_ASSOCIATE_FILES'); ?></h4>
    <p><b>Requisition No:</b> &nbsp;<?php echo $requisition_no; ?></p>
    <br/>
    <a href="#" onclick="return clickAnother();">
        <div class="upload">
            <i class="fa fa-upload" aria-hidden="true" style="font-size: 5em;"></i>
            <h4><?php echo load_message('CLICK_TO_UPLOAD'); ?></h4>
            <label class="control-label"><?php echo load_message('SELECT_FILE_JPG_PNG_PDF_DOCX'); ?></label>
        </div>
    </a>
    <input type="file" name="file" id="file" style="display: none;"/>
    <hr/>
    <div class="box box-primary">
        <table id="buildyourform"
               class="table table-striped" align="center">
            <thead>
            <tr>
                <th width="12px;"><?php echo load_message('FILE_NAME'); ?></th>
                <th width="12px;"></th>
            </tr>
            </thead>
            <!-- <span id="uploaded_image"></span>-->
            <tbody id="uploaded_image">

            </tbody>
        </table>
    </div>

    <div class="form-group" align="center"><input type="button" name="button_upload" value="Add Attachment"
                                                  onClick="return old_allocation_attachment('<?php echo $requisition_no; ?>');"
                                                  class="btn btn-primary"
                                                  title="Please Upload files before click 'Add Attachment'"></div>

    <div class="box box-success" align="center" id="uploaded_files">

        <?php
        if ($file_list <> "")
        {
        ?>
        <table id="example2" class="table table-striped">
            <thead>
            <tr>
                <th width="12px;"><?php echo load_message('FILE_NAME') ?></th>
                <th width="12px;"><?php echo load_message('UPLOAD_DATE') ?></th>
                <th width="12px;"><?php echo load_message('UPLOAD_BY') ?></th>
                <th width="12px;"></th>
            </tr>
            </thead>
            <!-- <span id="uploaded_image"></span>-->
            <tbody>
            <?php
            foreach ($file_list as $flist) {
                $file_id = $flist->file_id;
                $file_name = $flist->file_name;
                $created_by = $flist->created_by;
                $create_date = $flist->create_date;
                ?>

                <tr>
                    <td>
                        <a href="<?php echo base_url(); ?>uploads/<?php echo $file_name; ?>" title="Show File"
                         target="_blank">
                       <?php echo $file_name; ?></a>
                    </td>
                    <td><?php echo get_format_date($create_date); ?></td>
                    <td><?php echo get_user_fullname($created_by); ?></td>
                    <td>
                    </td>
                </tr>
                <?php
            }
            }
            ?>
            </tbody>
        </table>
    </div>

</div>

<script type="text/javascript">
    var assets =
        <?php echo json_encode($assets_name);?>;
</script>
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--<script src="dist/jquery.imageuploader.js"></script>-->
<script>
    (function () {
        var options = {};
        $('.js-uploader__box').uploader(options);
    }());

    function clickAnother() {
        $("#file").click();
    }
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>