<?php
$i = 1;
if ($asset_list) {
    foreach ($asset_list as $aslist) {
        $asset_id = $aslist->asset_id;
        $encrypt_asset = base64_encode($asset_id);
        $category_id = $aslist->category_id;
        $category_name = $aslist->category_name;
        $asset_name = $aslist->asset_name;
        $office_id = $aslist->office_id;

        $model_name = $aslist->model_name;

        $model_no = $aslist->model_no;
        $grn_no = $aslist->grn_no;
        $sku = $aslist->sku;
        $purchase_no = $aslist->purchase_no;
        $purchase_date = $aslist->purchase_date;
        $status_id = $aslist->asset_status_id;
        $status_name = $aslist->status_name;
        $purchase_date = date("Y-m-d", strtotime($purchase_date));
        $label_id = $aslist->label_id;
        $create_date = $aslist->create_date;
        $create_date = date("Y-m-d", strtotime($create_date));
        ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $asset_name; ?></td>
            <td><?php echo $category_name; ?></td>
            <td><?php echo $model_name; ?></td>
            <td><?php echo $model_no; ?></td>
            <td><?php echo $sku; ?></td>
            <td><?php echo $label_id; ?></td>
            <td><?php echo $purchase_no; ?></td>
            <td><?php echo $status_name; ?></td>
            <td><?php echo $create_date; ?></td>
            <td>
                <a href="#" title="Edit Asset" class="btn btn-default" data-toggle="modal" data-target="#modal-asset"
                   onclick="asset_edit('<?php echo $encrypt_asset; ?>');"><i class="fa fa-pencil fa-x"></i></a>&nbsp;
                <a href="#" title="Delete Asset" class="btn btn-default"
                   onclick="return asset_delete('<?php echo $encrypt_asset; ?>');"><i class="fa fa-trash fa-x"
                                                                                      aria-hidden="true"
                                                                                      style="color: red;"></i></a>
            </td>
        </tr>
        <?php
        $i++;
    }
}
?>
               
              