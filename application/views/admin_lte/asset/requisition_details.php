<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->


<!-- Main content -->
<div class="box-body">
    <div class="col-md-12">
        <div class="row">
            <?php if ($requisition_details) { ?>
                <div class="col-md-6">
                    <?php echo "<b>".load_message('REQUISITION_FOR').": </b> "; get_user_fullname($requisition_details[0]->userid)?>
                </div>
                <div class="col-md-6">
                    <?php echo "<b>".load_message('REQUISITION_REQUEST_INITIATED_BY').": </b> "; get_user_fullname($requisition_details[0]->on_behalf)?>
                </div>
                <div class="col-md-6">
                    <b><?php echo load_message('REQUISITION_REQUESTED_DATE'); ?> </b> <?php get_format_date($requisition_details[0]->created_date);?>
                </div>
            <?php
            }
            if ($approval_list) {
                foreach ($approval_list as $approver) {?>
                    <div class="col-md-6">
                        <?php if ($approver->authorization == "RECOMMANDED") {
                            echo "<b>Recommended By: </b> ". $approver->fullname;
                        }elseif ($approver->authorization == "APPROVED"){
                            echo "<b>Approved By: </b> ". $approver->fullname;
                        }elseif ($approver->authorization == "PROCEED"){
                            echo "<b>Acknowledged By: </b> ". $approver->fullname;
                        }
                        ?>
                    </div>
                <?php  }
            }
            ?>
        </div>
        <br>

        <div class="box box-success modal-table">
            <table class="table table-bordered table-striped">
                <thead>
                <th width="10">#</th>
                <th width="140"><?php echo load_message('ASSET_TITLE'); ?></th>
                <th width="50"><?php echo load_message('QUANTITY'); ?></th>
                <th width="50"><?php echo load_message('STATUS'); ?></th>
                <th width="50"></th>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($requisition_details) {


                    foreach ($requisition_details as $rdetails) { ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rdetails->name; ?></td>
                            <td><?php echo $rdetails->quantity; ?></td>
                            <td class="status">
                                <?php echo $rdetails->requisition_state; ?>
                            </td>
                            <td>
                                <?php
                                $next_flow = 0;
                                if($rdetails->office_id ==1 ){
                                    $next_flow = $user_details[0]->next_flow_id ;
                                }else{
                                    $next_flow = $user_details[0]->id;
                                }
                                if ($requisition_details[0]->type == "INTERNAL" && $next_flow== $this->session->userdata('user_db_id')) {
                                    if ($rdetails->requisition_state == "DECLINED") {
                                        ?>
<!--                                        <button type="submit" class="btn btn-success btn-xs change_status"-->
<!--                                                data-asset_id="--><?php //echo $rdetails->asset_id; ?><!--" data-status="Recommended">-->
<!--                                            <i class='fa fa-check'></i></button>-->
<!--                                        <button type="submit" class="btn btn-danger btn-xs change_status"-->
<!--                                                data-asset_id="--><?php //echo $rdetails->asset_id; ?><!--" data-status="Declined"-->
<!--                                                style="display: none;"><i class='fa fa-close'></i></button>-->
                                    <?php }
                                    if ($rdetails->requisition_state == "RECOMMANDED" || $rdetails->requisition_state = "REQUESTED") {
                                        ?>
                                        <button type="submit" class="btn btn-success btn-xs  change_status"
                                                data-asset_id="<?php echo $rdetails->asset_id; ?>" data-status="RECOMMANDED"
                                                style="display: none"><i class='fa fa-check'></i></button>
                                        <button type="submit" class="btn btn-danger btn-xs  change_status"
                                                data-asset_id="<?php echo $rdetails->asset_id; ?>" data-status="DECLINED"><i
                                                    class='fa fa-close'></i></button>
                                    <?php }
                                    if ($rdetails->requisition_state != "RECOMMANDED" && $rdetails->requisition_state != "DECLINED" && $rdetails->requisition_state != "REQUESTED" ) {
                                        ?>
                                        <button type="submit" class="btn btn-success btn-xs  change_status"
                                                data-asset_id="<?php echo $rdetails->asset_id; ?>" data-status="RECOMMANDED">
                                            <i class='fa fa-check'></i></button>
                                        <button type="submit" class="btn btn-danger btn-xs  change_status"
                                                data-asset_id="<?php echo $rdetails->asset_id; ?>" data-status="DECLINED"><i
                                                    class='fa fa-close'></i></button>
                                    <?php }
                                } else if ($requisition_details[0]->type == "RTC") {
                                    for ($i = 0; $i < sizeof($transfer_flow_next); $i++) {
                                        if ($transfer_flow_next[$i]->user_id == $this->session->userdata('user_db_id')) {
                                            if ($rdetails->requisition_state == "DECLINED") {
                                                ?>
<!--                                                <button type="submit" class="btn btn-success btn-xs  change_status"-->
<!--                                                        data-asset_id="--><?php //echo $rdetails->asset_id; ?><!--"-->
<!--                                                        data-status="Recommended"><i class='fa fa-check'></i></button>-->
<!--                                                <button type="submit" class="btn btn-danger change_status"-->
<!--                                                        data-asset_id="--><?php //echo $rdetails->asset_id; ?><!--"-->
<!--                                                        data-status="Declined" style="display: none;"><i-->
<!--                                                            class='fa fa-close'></i></button>-->
                                            <?php }
                                            if ($rdetails->requisition_state == "RECOMMANDED" || $rdetails->requisition_state = "REQUESTED") {
                                                ?>
                                                <button type="submit" class="btn btn-success btn-xs  change_status"
                                                        data-asset_id="<?php echo $rdetails->asset_id; ?>"
                                                        data-status="RECOMMANDED" style="display: none"><i
                                                            class='fa fa-check'></i></button>
                                                <button type="submit" class="btn btn-danger btn-xs  change_status"
                                                        data-asset_id="<?php echo $rdetails->asset_id; ?>"
                                                        data-status="DECLINED"><i class='fa fa-close'></i></button>
                                            <?php }
                                            if ($rdetails->requisition_state != "RECOMMANDED" && $rdetails->requisition_state != "DECLINED"&& $rdetails->requisition_state != "REQUESTED" ) {
                                                ?>
                                                <button type="submit" class="btn btn-success btn-xs  change_status"
                                                        data-asset_id="<?php echo $rdetails->asset_id; ?>"
                                                        data-status="RECOMMANDED"><i class='fa fa-check'></i></button>
                                                <button type="submit" class="btn btn-danger btn-xs  change_status"
                                                        data-asset_id="<?php echo $rdetails->asset_id; ?>"
                                                        data-status="DECLINED"><i class='fa fa-close'></i></button>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }

                }

                ?>
                </tbody>
            </table>
        </div>

        <font><b><?php echo load_message('SPECIAL_NOTE'); ?> </b> <?php echo $requisition_details[0]->remark; ?></font>
        <br>
        <label><?php echo load_message('REMARKS'); ?></label>
           <?php if ($approval_list) {
            foreach ($approval_list as $approver) {?>
            <div class="col-md-12">
                <?php
                    echo "<b>".$approver->fullname.": </b> ". $approver->remarks;
            ?>
            </div>
        <?php  }
            }
            ?>
        <br>
        <label><?php echo load_message('REMARKS'); ?></label>
        <textarea id="remarks" style="width:100%;min-height: 100px;"></textarea>
        <br> <br>
        <?php if ($requisition_details) {
            $next_flow_1 = 0;
            if($office_id_temp ==1 ){
                $next_flow_1 = $user_details[0]->next_flow_id ;
            }else{
                $next_flow_1 = $user_details[0]->id;
            }
            if ($requisition_details[0]->type == "INTERNAL") {
                if ($next_flow_1 == $this->session->userdata('user_db_id')) {
                    ?>
                    <div class="form-group" id="button_panel">
                        <div align="center">
                            <button type="submit" class="btn btn-primary"
                                    onclick="return update_requisition_status(<?php echo $requisition_details[0]->requisition_id; ?>,'proceed');">
                               <i class="fa fa-check-circle "></i> &nbsp; Proceed
                            </button>
                            <button type="submit" class="btn btn-danger"
                                    onclick="return update_requisition_status(<?php echo $requisition_details[0]->requisition_id; ?>,'declined');">
                                <i class="fa fa-times-circle "></i> &nbsp; Decline
                            </button>
                        </div>
                    </div>
                <?php }
            } else if ($requisition_details[0]->type == "RTC") {
                for ($i = 0; $i < sizeof($transfer_flow_next); $i++) {
                    if ($transfer_flow_next[$i]->user_id == $this->session->userdata('user_db_id')) {
                        ?>
                        <div class="form-group" id="button_panel">
                            <div align="center">
                                <button type="submit" class="btn btn-primary"
                                        onclick="return update_transfer_status(<?php echo $requisition_details[0]->requisition_id; ?>,'proceed');">
                                    <i class="fa fa-check-circle "></i> &nbsp; Proceed
                                </button>
                                <button type="submit" class="btn btn-danger"
                                        onclick="return update_transfer_status(<?php echo $requisition_details[0]->requisition_id; ?>,'declined');">
                                    <i class="fa fa-times-circle "></i> &nbsp; Decline
                                </button>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
        }
        ?>
    </div>

    <!-- /.box-body -->
     