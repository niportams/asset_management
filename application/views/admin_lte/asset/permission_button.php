<?php 
$encrypt_asset = base64_encode($asset_id);
?>
									<button title="Details" data-asset_id="<?=$asset_id;?>" class="btn btn-success btn-xs asset-detail" data-toggle="modal"
                                   data-target="#modal-asset-details"
                                   onclick="return asset_details('<?php echo $encrypt_asset; ?>');"><i
                                            class="fa fa-list-alt"></i></button>
                   
                                <?php if (permission_check('asset/update')) { ?>
                                    <button title="Edit Asset" class="btn btn-primary btn-xs" data-toggle="modal"
                                    data-target="#modal-asset" onclick="asset_edit('<?php echo $encrypt_asset; ?>');">
                                    <i class="fa fa-pencil fa-x"></i></button>
                                <?php }
                                if (permission_check('asset/delete')) { ?>
                                   
                                    <button title="Delete Asset" class="btn btn-danger btn-xs"
                                       onclick="return asset_delete('<?php echo $encrypt_asset; ?>');"><i
                                                class="fa fa-trash fa-x" aria-hidden="true"></i></button>
                                <?php } ?>
                              
                                <?php 
                             
    permitted_button('sci_print/print_asset_detail', 'fa fa-print fa-x', 'btn btn-primary btn-xs single-asset-detail-print', '', 'asset_id="'.$asset_id.'"', '', 'Print');
                                
                                ?>