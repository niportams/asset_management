<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
foreach ($requisition_details as $cdetails) 
{
    $request_id = $cdetails->requisition_id;
    $request_title = $cdetails->title;
    $office_id = $cdetails->office_id;
    $office_name = $cdetails->office_name;
    $requested_for = $cdetails->userid;
    $requested_by = $cdetails->on_behalf;
    $approved_by = $cdetails->approved_by;
    $requested_date = $cdetails->created_date;
    $requested_date = date("d-m-Y", strtotime($requested_date));
    $approved_date = $cdetails->approved_date;
    $approved_date = date("d-m-Y", strtotime($approved_date));
    $remark = $cdetails->remark;
    $approval_remark = $cdetails->remarks;
	$asset_location = $cdetails->asset_location;
	$on_behalf = $cdetails->on_behalf;
}

?>
<!-- Main content -->
<div class=" box-body" id="allocation_form">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div align="left"><strong><?php echo load_message('REQUISITION_FROM'); ?>:</strong>
                     <?php echo $office_name; ?></div>
            </div>
            <div class="col-md-6">
<!--                <div align="right">-->
<!--            <button id="export" name="Export" class="btn btn-success btn-xs" title="Click to Export" value="Export"-->
<!--             onclick="ExportToExcel('allocation_form');"><i class="fa fa-file-excel-o fa-x"></i></button>-->
<!--                </div>-->
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <strong><?php echo load_message('REQUISITION_FOR'); ?>
                    : </strong>&nbsp;<?php echo get_user_fullname($requested_for); ?>
            </div>
            <div class="col-md-6">
                <strong><?php echo load_message('REQUISITION_REQUEST_INITIATED_BY'); ?>
                    : </strong>&nbsp;<?php echo get_user_fullname($requested_by); ?>
            </div>
            <div class="col-md-6">
                <strong><?php echo load_message('REQUISITION_REQUESTED_DATE'); ?>
                    : </strong>&nbsp; <?php echo $requested_date; ?>
            </div>
        </div>
        <br>
        <div class="row">
            <?php
            if ($approval_list) {
                foreach ($approval_list as $approver) {?>
                    <div class="col-md-6">
                        <?php if ($approver->authorization == "RECOMMANDED") {
                            echo "<b>Recommended By: </b> ". $approver->fullname;
                        }elseif ($approver->authorization == "APPROVED"){
                            echo "<b>Approved By: </b> ". $approver->fullname;
                        }elseif ($approver->authorization == "PROCEED"){
                            echo "<b>Acknowledged By: </b> ". $approver->fullname;
                        }
                        ?>
                    </div>
                <?php  }
            }
            ?>
            <div class="col-md-6">
                <strong><?php echo load_message('APPROVED_DATE'); ?>: </strong>&nbsp<?php echo $approved_date; ?>

            </div>
        </div>

        <?php
        if ($approval_remark <> "") {
            ?>
            <div class="form-group">
                <strong><?php echo load_message('REMARKS'); ?>: </strong>&nbsp; <?php echo $approval_remark; ?>
                <br/> By &nbsp;<?php echo get_user_fullname($approved_by); ?>
            </div>
            <?php
        }
        ?>
        <br>
        <!--<div class="form-group" id="requisition_list" style="overflow-y:scroll; height:250px;" >-->
        <div class="box box-success modal-table">
            <b><?php echo load_message('REQUESTED_ITEM_LIST'); ?>:</b>
            <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr style="background-color:#FFFFFF;">
                <th>#</th>
                <th><?php echo load_message('ASSET_TITLE'); ?></th>
                <th><?php echo load_message('REQUESTED_QTY'); ?></th>
                <th><?php echo load_message('AVAILABLE_QTY'); ?></th>
                <th><?php echo load_message('STATUS'); ?></th>
                <th></th>
                </tr>
                </thead>
                <tbody id="requisition_list">
                <?php
                $i = 1; $flag = 0; $j=0; $pr_list= array();
				$info = array();
                if ($req_list <> "") {

                    foreach ($req_list as $rqlist) {
                        $available = 0;
                        $asset_item_id = $rqlist->id;
                        $rq_id = $rqlist->requisition_id;
                        $encrypt_request = base64_encode($rq_id);
                        $category_id = $rqlist->category_id;
                        $ast_name = $rqlist->name;
                        $ast_qty = $rqlist->quantity;
                        $requisition_state = $rqlist->requisition_state;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $ast_name; ?></td>
                            <td><?php echo $ast_qty; ?></td>
                            <td><?php echo $available = get_category_available($category_id); ?> </td>
                            <td>
                                <div align="center">
                                    <?php if ($available >= $ast_qty) { ?>
                                    <font color="green">
									<?php if($requisition_state == "Delivered") { echo $requisition_state; }else{ echo"Available";  } ?></font>
                                    <?php } 
									else 
									{ 
									if($requisition_state == "Delivered") 
										{ 
										echo $requisition_state; 
										}
									else
										{
											$flag = 1; //Item not available
											$info[]=  array(
											"category_id" =>$category_id,
											//"asset_name" =>explode('','',$ast_name),
											"asset_name" =>$ast_name,
											"quantity" =>$ast_qty,
											) ;
											?>
                                             <font color="#FF0000">Not Available</font>
                                            <?php
										}		
									
									$j++;
									} 
									?>
                                </div>
                            </td>
                            <td>
                            <div id="allocation_status<?php echo $i;?>">
							<?php if ($available >= $ast_qty) {
                                    if ($requisition_state == "Delivered") /*This item is already delivered*/ {
                                     echo "<font color='#0000FF'>Allocated.</font>";
									 
									 ?>
                                     
                                     <?php
                                    } elseif ($available >= $ast_qty) {
                                  ?>
                                  <a href="#" data-toggle="modal"
                            data-target="#modal-itemlist" onclick="asset_list('<?php echo $request_id;?>','<?php echo $category_id; ?>','<?php echo $ast_qty; ?>','<?php echo $asset_item_id ?>','<?php echo $i;?>','allocate');">Item List</a>
                                     <?php
                                    }
								?>

                                <?php }
								elseif(($available < $ast_qty)and($requisition_state == "Delivered"))
								{
									echo "<font color='#0000FF'>Allocated.</font>";
								} 
								else {
                                    echo "-";
                                } ?>
                                </div>
                                </td>
                        </tr>
                        <?php
                        $i++;
                    }

                }
				?>
             	</tbody>
            </table>
            <!-- <div class="form-group" align="justify">&nbsp;</div>  -->
        </div>
        <div class="col-md-12">
        <label><?php echo load_message('SPECIAL_NOTE'); ?></label>
        <p><?php echo $remark; ?></p>
            <br>
            <label><?php echo load_message('REMARKS'); ?></label>
        </div>

        <?php if ($approval_list) {
            foreach ($approval_list as $approver) {?>
                <div class="col-md-12">
                    <?php
                    echo "<b>".$approver->fullname.": </b> ". $approver->remarks;
                    ?>
                </div>
            <?php  }
        }
        ?>
        <br>
        <?php 
		if(($flag == 1) and ($pr_status == 0))
		{
			$reference_no = date("Y-m-d H:i:s");
			$reference_no = str_replace('-', '', $reference_no);
			$reference_no = str_replace(':', '', $reference_no);
			$reference_no = str_replace(' ', '', $reference_no);
			$reference_no = substr($reference_no, 6);
	
		   //var_dump($pr_list);
			 $pr_list =  json_encode($info,JSON_FORCE_OBJECT);
			//var_dump($jsonarray);
			//echo $jsonarray = json_decode($pr_ist,true);
			//echo $jsonarray[1];
			?>
			<div class="col-md-12">
			 <p align="center">  
			  <input type="hidden" name="pr_list" id="pr_list" value='<?php echo $pr_list;?>' /> 
			  <div id="pr_button" align="center">          
				  <a class="btn btn-warning btn-flat" aria-expanded="true" 
				  onclick="return generate_pr_request('<?php echo $reference_no;?>','<?php echo $request_id;?>');" id="request_next">
				  <strong>Generate Procurement Request</strong>
				  </a> 
			  </div>
			  </p>
			</div>
        <?php 
		}
		?>
        <!--<div class="form-group" align="justify">&nbsp;</div> -->

    </div>
    <!-- /.box-body -->
   
    </div>
       