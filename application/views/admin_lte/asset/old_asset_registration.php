<div class="row">
<div class="col-md-12"><?php echo load_message('MANDATORY_TEXT'); ?></div>
<div class="col-md-12">&nbsp;</div>
    <div class="col-md-6">
        <div class="form-group">
            <input type="hidden" name="cpid" id="cpid" value="99999"/>
            <input type="hidden" name="received_item" id="received_item" value="0"/>
            <input type="hidden" name="office_id" id="office_id" value="<?php echo $office_id; ?>"/>
            <input type="hidden" name="pr_no" id="pr_no" value=""/>
            <input type="hidden" name="pr_reference_no" id="pr_reference_no" value=""/>
            <label for="exampleInputName" class="control-label"><?php echo load_message('GRN'); ?><span
                        class="text-danger">*</span></label>

            <input type="text" class="form-control" id="grn_no" name="grn_no" value="<?php echo $grn_no; ?>"
                   readonly="readonly"  title="<?php echo load_message('GRN_TOOLTIP'); ?>">

        </div>
        <div class="form-group">
            <label for="exampleInputName" class="control-label"><?php echo load_message('ASSET_TITLE'); ?><span
                        class="text-danger">*</span></label>

            <input type="text" class="form-control" id="asset_name" name="asset_name"
                   placeholder="<?php echo load_message('ASSET_TITLE'); ?>" value="">

        </div>
        <div class="form-group">
            <label><?php echo load_message('CATEGORY'); ?><span class="text-danger">*</span></label>
            <?php get_category_list("category_id", "category_id", "form-control select2", load_message('SELECT_CATEGORY')); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputName" class="control-label"><?php echo load_message('DESCRIPTION'); ?></label>
            <textarea name="asset_description" rows="8" id="asset_description" class="form-control"></textarea>

        </div>
    </div>
</div>


<div class=" row form-group" align="justify">
    <div class="col-md-3">
        <label><?php echo load_message('MANUFACTURER'); ?></label>

        <select name="manufacture_id" id="manufacture_id" class="form-control select2">
            <option value="" selected="selected"><?php echo load_message('SELECT_MANUFACTURER'); ?></option>
            <?php
            foreach ($manufacture_list as $mlist) {
                $manufacture_id = $mlist->manufacture_id;
                $manufacture_name = $mlist->manufacture_name;
                ?>
                <option value="<?php echo $manufacture_id; ?>"><?php echo $manufacture_name; ?></option>
                <?php
            }
            ?>
        </select>
    </div>

    <div class="col-md-3">
        <label><?php echo load_message('WARRENTY_EXP_DATE'); ?></label>
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control date-picker" id="warrenty_date" name="warrenty_date"
                   placeholder="dd/mm/yyyy">
        </div>
    </div>

    <div class="col-md-3">
        <label><?php echo load_message('SKU'); ?></label>
        <input type="text" class="form-control" id="sku" name="sku" placeholder="<?php echo load_message('SKU'); ?>"  title="<?php echo load_message('SKU_TOOLTIP'); ?>">
    </div>
    <div class="col-md-3">
        <label><?php echo load_message('ASSET_LABEL_ID'); ?></label>
        <input type="text" class="form-control" id="sci_id" name="sci_id"
               placeholder="<?php echo load_message('ASSET_LABEL_ID'); ?>">
    </div>

</div>
<!-- <div class="form-group" align="justify">&nbsp;</div>-->
<div class="row form-group" align="justify">

    <div class="col-md-3">
        <label><?php echo load_message('MODEL_NAME'); ?></label>
        <input type="text" class="form-control" id="model_name" name="model_name"
               placeholder="<?php echo load_message('MODEL_NAME'); ?>">
    </div>

    <div class="col-md-3">
        <label><?php echo load_message('MODEL_NO'); ?></label>
        <input type="text" class="form-control" id="model_no" name="model_no"
               placeholder="<?php echo load_message('MODEL_NO'); ?>">
    </div>

    <div class="col-md-3">
        <label><?php echo load_message('SERIAL_NO'); ?></label>
        <input type="text" class="form-control" id="serial_no" name="serial_no"
               placeholder="<?php echo load_message('SERIAL_NO'); ?>">
    </div>


    <div class="col-md-3">
        <label><?php echo load_message('REF'); ?></label>
        <input type="text" class="form-control" id="reference_no" name="reference_no"
               placeholder="<?php echo load_message('REF'); ?>">
    </div>
</div>

<div class="row form-group" align="justify">

    <div class="col-md-3">
        <label><?php echo load_message('PURCHASE_DATE'); ?> <span class="text-danger">*</span></label>
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control date-picker" id="purchase_date" name="purchase_date"
                   placeholder="dd/mm/yyyy">
        </div>
    </div>

    <div class="col-md-3">
        <label><?php echo load_message('PURCHASE_ORDER_NO'); ?></label>
        <input type="text" class="form-control" id="purchase_no" name="purchase_no"
               placeholder="<?php echo load_message('PURCHASE_ORDER_NO'); ?>">
    </div>

    <div class="col-md-3">
        <label><?php echo load_message('PURCHASE_PRICE'); ?></label>
        <input type="text" class="form-control number-only" id="purchase_price" name="purchase_price"
               placeholder="<?php echo load_message('PURCHASE_PRICE'); ?>">
    </div>

    <div class="col-md-3">
        <label><?php echo load_message('PURCHASE_LOC'); ?></label>
        <input type="text" class="form-control" id="purchase_location" name="purchase_location"
               placeholder="<?php echo load_message('PURCHASE_LOC'); ?>">
    </div>
</div>
<div class="row form-group" align="justify">
    <div class="col-md-3">
        <label><?php echo load_message('ASSET_LIFETIME'); ?></label>
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control date-picker" id="asset_lifetime"
                   name="asset_lifetime" placeholder="dd/mm/yyyy">
        </div>
    </div>
    
     <div class="col-md-7">
        <div class="form-group">
            <label for="exampleInputName" class="control-label"><?php echo load_message('REMARKS'); ?></label>

            <textarea name="asset_remarks" id="asset_remarks" name="asset_remarks" class="form-control" 
                      placeholder="<?php echo load_message('REMARKS'); ?>"></textarea>

        </div>
    </div>
</div>

<div class="row form-group" align="justify" style="display:none;">

    <div class="col-md-4" style="display:none;">
        <label><?php echo load_message('BULK_REGISTRATION'); ?></label> &nbsp;
        <!-- <input type="radio" id="registration_type" name="registration_type" value="bulk">-->
        <input type="checkbox" id="registration_type" name="registration_type" value="bulk">
    </div>

    <div class="col-md-4">
        <label><?php echo load_message('TOTAL_NUMBER_REGISTERED'); ?></label>
        <input type="text" class="form-control" id="registered_item_no" name="registered_item_no" readonly="readonly"
               value="0">
    </div>

    <div class="col-md-4">
        <label><?php echo load_message('PENDING_NUMBER_REGISTRATION'); ?></label>
        <input type="text" class="form-control" id="pending_item_no" name="pending_item_no" readonly="readonly"
               value="0">
    </div>

</div>
<!--<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleInputName" class="control-label"><?php echo load_message('REMARKS'); ?></label>

            <textarea name="asset_remarks" id="asset_remarks" name="asset_remarks" class="form-control"
                      placeholder="<?php //echo load_message('REMARKS'); ?>"></textarea>

        </div>
    </div>
</div>-->

<script>
    $('#submit_btn').show();
    $(function () {
        $('.select2').select2()

    });
</script>