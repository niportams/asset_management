<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->

<section class="content">
    <div class="row" id="asset_requsition_form">
        <div class="col-md-4">
            <div class="box  box-danger">
                <div class="box-header with-border">
                    <h4><?php echo load_message('ADD_ITEM_TO_REQUISITION_LIST'); ?></h4>
                </div>
                <div class="box-body form-horizontal">
                    <div id="add_product">
                        <div>
                            <label class="control-label"><?php echo load_message('SELECT_ITEM'); ?><span
                                        class="text-danger">*</span></label>
                            <?php get_category_list("category_id", "item", "form-control select2", load_message('SELECT_CATEGORY')); ?>

                        </div>
                        <div>
                            <label class="control-label"><?php echo load_message('REQUIRED_QTY'); ?> <span
                                        class="text-danger">*</span></label>
                            <input class="form-control number-only" type="text" id="qty" name="qty">
                        </div>
                        <br>
                        <div class="box-footer">
                            <div align="center">
                                <button class="btn btn-success add_item">
                                    <i class="fa fa-plus"></i> &nbsp;
                                    <?php echo load_message('ADD_TO_LIST'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 clear-style">
            <form id="asset_requisition">
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h4><?php echo load_message('ITEM_LIST'); ?> </h4>
                    </div>
                    <div class="box-body ">

                        <table id="buildyourform" class="table table-striped">
                            <thead>
                            <tr id="field1">
                                <th>
                                    <label class="control-label"><?php echo load_message('ASSET_TITLE'); ?></label>
                                </th>
                                <th>
                                    <label class="control-label"><?php echo load_message('REQUIRED_QTY'); ?></label>
                                </th>
                                <th>
                                    <label class="control-label"></label>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="clear-all">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box  box-primary">
                    <div class="box-header with-border">
                        <h4><?php echo load_message('REQUISITION_DETAILS'); ?></h4>
                    </div>
                    <div class="box-body form-horizontal">


                        <div>
                            <label class="control-label"><?php echo load_message('TITLE'); ?><span
                                        class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="title">
                        </div>
                        <div>
                            <label class="control-label"><?php echo load_message('REQUISITION_FOR'); ?><span
                                        class="text-danger">*</span></label>
                            <select class="form-control select2" id="on_behalf" name="on_behalf">
                                <option value=""><?php echo load_message('SELECT_USER'); ?>
                                </option>
                                <?php
                                foreach ($user_list as $uslist) {
                                    $allocate_userid = $uslist->userid;
                                    $allocate_name = $uslist->fullname;
                                    $allocate_designation = $uslist->designation;
                                    if ($this->session->userdata('user_db_id') != $allocate_userid) {
                                        ?>
                                        <option value="<?php echo $allocate_userid; ?>"><?php echo $allocate_name; ?>
                                            -<?php echo $allocate_designation; ?></option>
                                    <?php } else {
                                        ?>
                                        <option value="<?php echo $allocate_userid; ?>"
                                                selected="selected"><?php echo $allocate_name; ?>
                                            -<?php echo $allocate_designation; ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </div>
                        <div>
                            <label class="control-label"><?php echo load_message('ALLOCATION_LOCATION'); ?></label>
                            <select class="form-control select2" id="asset_location" name="asset_location">
                                <option value=""><?php echo load_message('SELECT_LOCATION'); ?></option>
                                <?php
                                foreach ($room_list as $rmlist) {
                                    $room_id = $rmlist->room_id;
                                    $room_name = $rmlist->room_name;
                                    $room_location = $rmlist->room_location;
                                    ?>
                                    <option value="<?php echo $room_id; ?>"><?php echo $room_name; ?>
                                        -<?php echo $room_location; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div>
                            <?php if (get_user_role_name($this->session->userdata('user_db_id')) != "RTC_ADMIN") { ?>
                                <input class="form-control" type="hidden" id="type" name="type" value="INTERNAL">
                            <?php } else { ?>
                                <label class="control-label"><?php echo load_message('REQUISITION_TYPE'); ?><span
                                            class="text-danger">*</span></label>
                                <select class="form-control" id="type" name="type">
                                    <option value=""><?php echo load_message('SELECT_REQUISITION_FOR'); ?></option>
                                    <option value="INTERNAL">Personal</option>
                                    <option value="RTC">RTC</option>
                                </select>
                            <?php } ?>
                        </div>
                        <div>
                            <label class="control-label"><?php echo load_message('REQUISITION_PROCESS'); ?><span
                                        class="text-danger">*</span></label>
                            <select class="form-control select2" id="process" name="process">
                                <option value=""><?php echo load_message('SELECT_REQUISITION_PROCESS'); ?></option>
                                <?php if (get_user_role_name($this->session->userdata('user_db_id')) != "RTC_ADMIN") { ?>
                                    <option value="Allocation" selected="selected">Allocation</option>
                                <?php } else { ?>
                                    <option value="Allocation">Allocation</option>
                                <?php }
                                if (get_user_role_name($this->session->userdata('user_db_id')) == "RTC_ADMIN") { ?>
                                    <option value="Transfer">Transfer</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div>
                            <label class="control-label"><?php echo load_message('REMARKS'); ?></label>
                            <textarea class="form-control" type="textarea" id="remark" name="remark"></textarea>
                        </div>
                        <br>
                    </div>
                    <br>
                    <div class="box-footer">
                        <div align="center">
                            <button class="btn btn-primary" id="requisition_submit"
                                    onclick="handle_form('asset_requisition', asset_requisition, 'ajax/requisition_request_submit',0,'serialize', true,'clear-all')"
                                    disabled="true">
                                <i class="fa fa-send"></i> &nbsp;
                                <?php echo load_message('PLACE_A_REQUISITION_REQUEST'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- /.box-body -->

    <!-- /.box-footer-->

    <!-- /.box -->

</section>
<!-- /.content -->

<script type="text/javascript">
    var assets =
        <?php echo json_encode($assets_name);?>;

    $(function(){
        $("#type").on('change', function(){
            if($("#type").val()=="INTERNAL"){
                $('#process option[value="Allocation"]').remove();
                $('#process option[value="Transfer"]').remove();
                $('#process').append($("<option></option>")
                        .attr("value","Allocation")
                        .text("Allocation"));

            }else if($("#type").val()=="RTC"){
                $('#process option[value="Allocation"]').remove();
                $('#process option[value="Transfer"]').remove();
                $('#process').append($("<option></option>")
                    .attr("value","Transfer")
                    .text("Transfer"));
            }else{
                $('#process option[value="Allocation"]').remove();
                $('#process option[value="Transfer"]').remove();
            }
        })

    });

</script>