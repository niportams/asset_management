<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">
    
    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered sci-table sci-table-blue  dataTable-full-functional"> <!-- table-striped table-hover -->
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="110"><?php echo load_message('CATEGORY'); ?></th>
                    
                    <th width="70"><?php echo load_message('ASSET_LABEL_ID'); ?></th>
                    <th width="90"><?php echo load_message('ASSET_ID_L'); ?></th>
                    <th width="100"><?php echo load_message('WARRENTY_EXP_DATE'); ?></th>
                    <th width="70"><?php echo load_message('ASSET_LIFETIME'); ?></th>
                    <th width="70"><?php echo load_message('PURCHASE_DATE'); ?></th>
                    <th width="100"><?php echo load_message('REGISTRATION_DATE'); ?></th>
                    <th width="50"><?php echo load_message('STATUS'); ?></th>
                    
<!--                <th width="70"></th>-->
                </tr>
                </thead>
                <tbody id="abc">
                <?php
                $i = 1;
                if ($asset_list <> "") {
                    foreach ($asset_list as $aslist) {
                        $asset_id = $aslist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $category_id = $aslist->category_id;
                        $category_name = $aslist->category_name;
                        $asset_name = $aslist->asset_name;
                        $office_id = $aslist->office_id;

                        $model_name = $aslist->model_name;
                        $model_no = $aslist->model_no;
                        $grn_no = $aslist->grn_no;
                        $sku = $aslist->sku;
                        $purchase_no = $aslist->purchase_no;
                        $purchase_date = $aslist->purchase_date;

                        $status_id = $aslist->asset_status_id;
                        $status_name = $aslist->status_name;
                        //$purchase_date = date("Y-m-d", strtotime($purchase_date));
                        $label_id = $aslist->asset_readable_id;
                        $sci_id = $aslist->sci_id;
						$warrenty_exp_date = $aslist->warrenty_date;
						$lifetime_date = $aslist->asset_lifetime;
                        $create_date = $aslist->create_date;

                        //$create_date=date("Y-m-d", strtotime($create_date));
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <!--<a href="#" onmouseover="call_permission('<?php //echo $asset_id;?>');"><?php //echo $asset_name; ?></a> -->
                                <div><?php echo $asset_name; ?></div>
                                <?php //echo $asset_name;
                                ?>
                                <div class="float-bar">
                                    <button title="Details" data-asset_id="<?=$asset_id;?>" class="btn btn-info btn-xs asset-detail" data-toggle="modal"
                                   data-target="#modal-asset-details"
                                   onclick="return asset_details('<?php echo $encrypt_asset; ?>');"><i
                                            class="fa fa-list-alt"></i></button>
                                <?php if (permission_check('asset/update')) { ?>
                                    <button title="Edit Asset" class="btn btn-success btn-xs" data-toggle="modal"
                                       data-target="#modal-asset"
                                       onclick="asset_edit('<?php echo $encrypt_asset; ?>');"><i
                                                class="fa fa-pencil fa-x"></i></button>
                                <?php }
                                if (permission_check('asset/delete')) { ?>
                                   
                                    <button title="Delete Asset" class="btn btn-danger btn-xs"
                                    onclick="return asset_delete('<?php echo $encrypt_asset; ?>');">
                                    <i class="fa fa-trash fa-x" aria-hidden="true"></i></button>
                                <?php 
								}
								 if (permission_check('allocation/newrequest') and ($status_id == 1))
								 {
								 ?>
                                 <button title="Allocate Asset" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-asset-allocation"
                                  onclick="return single_asset_allocation('<?php echo $encrypt_asset; ?>','<?php echo $label_id; ?>',
                                  '<?php echo $category_id;?>');">
                                  <i class="fa fa-send fa-x" aria-hidden="true"></i></button>
                                 <?php
								 }
								
								?>
                              
                                <?php 
                             
    permitted_button('sci_print/print_asset_detail', 'fa fa-print fa-x', 'btn btn-primary btn-xs single-asset-detail-print', '', 'asset_id="'.$asset_id.'"', '', 'Print');
                                
                               ?>
                                </div>
                            </td>
                            <td><?php echo $category_name; ?>
                             <div class="small"><?php get_parent_category_name($category_id);  echo ' > '.$category_name; ?></div>
                            </td>
                            
                            <td><?php echo $sci_id; ?></td>
                            <td><?php echo $label_id; ?></td>
                            <td><?php if($warrenty_exp_date <> NULL){echo get_format_date($warrenty_exp_date); } ?></td>
                            <td><?php if($lifetime_date <> NULL) {echo get_format_date($lifetime_date); } ?></td>
                            <td><?php if($purchase_date<> NULL) {echo get_format_date($purchase_date); } ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                            <td><?php label($status_name); ?></td>

                           

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>

        <!-- /.box-body -->

        <!-- ------------Modal for Asset edit Start--------------- -->
        <div class="modal fade" id="modal-asset">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_EDIT'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_edit"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="modal-asset-allocation">
            <div class="modal-dialog" style="width:700px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_ALLOCATION_FORM'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_allocate"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
             
    </div>
    <!--    Modal for Capital Edit end here -->
            <!-- /.modal -->
        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
            <!-- /.modal-dialog -->
             
    </div>

    <!-- /.box -->
</section>
<!-- /.content -->