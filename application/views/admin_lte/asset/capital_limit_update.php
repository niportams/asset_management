<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/jsonmap.js"); ?>
<?php
foreach ($capital_details as $cdetails) {
    $capital_id = $cdetails->capital_id;
    $asset_name = $cdetails->asset_name;
    $category_id_update = $cdetails->category_id;
    $receiving_office_id = $cdetails->receiving_office;
    $supplier_id = $cdetails->supplier_id;
    $voucher_no = $cdetails->voucher_no;
    $received_item = $cdetails->received_item;
    $registered_item = $cdetails->registered_item_no;
    $grn_no = $cdetails->grn_no;
    $gik = $cdetails->gik;
    $funded_by = $cdetails->funded_by;
    $own_funded = $cdetails->own_funded;
    $received_by = $cdetails->received_by;
	$entry_limit = $cdetails->entry_limit;
    $create_date = $cdetails->create_date;
}

?>
<!-- Main content -->
<div class="box-body">

    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleInputName" class="control-label"><?php echo load_message('ASSET_TITLE'); ?>
            <span class="text-danger">*</span></label>
            <input type="text" class="form-control"  value="<?php echo $asset_name; ?>" readonly="readonly">

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group" align="justify">
                    <label class="control-label"><?php echo load_message('CATEGORY'); ?></label>
                     <input type="text" class="form-control"  value="<?php echo get_category_name($category_id_update); ?>" 
                     readonly="readonly">    
                </div>
            </div>
           
       </div>
       <div class="row">
        <div class="col-md-12">
                <div class="form-group" align="justify">
                    <label class="control-label"><?php echo load_message('CAPITAL_LIMIT'); ?>
                    <span class="text-danger">*</span></label>
                    <input type="text" class="form-control number-only" id="entry_limit" value="<?php echo $entry_limit; ?>">
                </div>
            </div>
       </div>
       <div class="form-group" align="center">
            <div class="">
                <button type="submit" class="btn btn-primary" onclick="return update_capital_limit_submit('<?php echo $capital_id; ?>');">
                <i class="fa fa-pencil-square-o"></i> &nbsp;<?php echo load_message('UPDATE_REGISTRATION_LIMIT'); ?>
                </button>
             </div>

        </div>

    </div>

</div>

<!-- /.box-body -->
     