<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<div class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- <div class="box-header">
           <h3 class="box-title">Data Table of Approved Allocation Request</h3>
         </div>-->
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="120"><?php echo load_message('REQUISITION_TITLE'); ?></th>
                    <th width="80"><?php echo load_message('REQUISITION_FOR'); ?></th>
                    <th width="110"><?php echo load_message('REQUISITION_REQUEST_INITIATED_BY'); ?></th>
                    <th width="110"><?php echo load_message('REQUISITION_FROM'); ?></th>
                    <th width="100"><?php echo load_message('REQUISITION_REQUESTED_DATE'); ?></th>
                    <!--<th width="90"><?php //echo load_message('APPROVED_BY'); ?></th>-->
                    <th width="90"><?php echo load_message('APPROVED_DATE'); ?></th>
                    <th width="100"><?php echo load_message('REMARKS'); ?></th>
                    <th width="50"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($allocation_approved_list <> "") {
                    foreach ($allocation_approved_list as $alclist)
					 {
                        $requested_id = $alclist->requisition_id;
                        $encrypt_request = base64_encode($requested_id);
                        $requisition_title = $alclist->title;
                        $remark = $alclist->remark;
                        //$requisition_type = $clist->type;
                        $on_behalf = $alclist->on_behalf;
                        $office_id = $alclist->office_id;
                        $office_name = $alclist->office_name;
                        $requested_by = $alclist->userid;
                        $requested_date = $alclist->created_date;
                        $approve_of = $alclist->approve_of;
                        $approved_by = $alclist->approved_by;
                        //$approved_date = $alclist->create_date;
						$approved_date = $alclist->approved_date;
                        $requisition_process = $alclist->process;

                        //$approved_by_fullname = $alclist->fullname;
                        $approved_by_designation = $alclist->designation;
						$create_date = $alclist->created_date;
                        //$format_date=date_format($create_date, 'Y-m-d');
                        $format_date = date("Y-m-d", strtotime($create_date));
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $requisition_title; ?></td>
                            <td><?php echo get_user_fullname($requested_by); ?></td>
                            <td><?php echo get_user_fullname($on_behalf); ?></td>
                            <td><?php echo $office_name; ?></td>
                            <td><?php echo get_format_date($requested_date); ?></td>
                           <!-- <td><?php //echo get_user_fullname($approved_by); ?></td>-->
                            <td><?php echo get_format_date($approved_date); ?></td>
                            <td><?php echo $remark; ?></td>
                            <td>
                            <button title="Allocation Proceed" class="btn btn-primary btn-xs" data-toggle="modal"
                            data-target="#modal-allocate" onclick="asset_allocate('<?php echo $encrypt_request; ?>');">
                            <i class="fa fa-object-ungroup fa-x"></i></button>&nbsp;
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
            
        </div>
        <!-- /.box-body -->

        <!-- ------------Modal for Allocate Asset--------------- -->
        <div class="modal fade" id="modal-allocate">
            <div class="modal-dialog" style="width:800px;">
                <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">&times;</span></button>
                       <h4 class="modal-title"><?php echo load_message('ASSET_ALLOCATION_FORM') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_allocation"></div>
                    </div>
                    <div class="modal-footer">
                 <button type="button" class="btn btn-default pull-left"
                   data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal for Capital Edit end here -->
</div>
    <!-- /.box -->
    <!--    dfgdfg          -->
   		<div class="modal fade" id="modal-itemlist">
            <div class="modal-dialog" style="width:600px;">
                <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">&times;</span></button>
                       <h4 class="modal-title"><?php echo load_message('ASSET_LIST') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="item_list"></div>
                    </div>
                    <div class="modal-footer">
                 	<button type="button" class="btn btn-default pull-left" 
                   	data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
</section>
<!-- /.content -->