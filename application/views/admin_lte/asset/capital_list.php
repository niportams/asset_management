<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="110"><?php echo load_message('CATEGORY'); ?></th>
                    <th width="100"><?php echo load_message('VOUCHER'); ?></th>
                    <th width="50"><?php echo load_message('QUANTITY'); ?></th>
                    <th width="90"><?php echo load_message('RECEIVED_BY'); ?></th>
                    <th width="70"><?php echo load_message('DEPARTMENT'); ?></th>
                    <th width="70"><?php echo load_message('GRN'); ?></th>
                    <th width="70"><?php echo load_message('FUNDED_BY'); ?></th>
                    <th width="70"><?php echo load_message('CAPITAL_DATE'); ?></th>
                    <th width="70"><?php echo load_message('REGISTRATION_EXPIRE_DATE'); ?></th>
                    <th width="100"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($capital_list <> "") {
                    foreach ($capital_list as $clist) {
                        $capital_id = $clist->capital_id;
                        $encrypt_capital = base64_encode($capital_id);
                        $category_id = $clist->category_id;
                        $category_name = $clist->category_name;
                        $asset_name = $clist->asset_name;
                        $receiving_office_id = $clist->receiving_office;
						$dept_id = $clist->department_id;

                        $office_name = $clist->office_name;

                        $grn_no = $clist->grn_no;
                        $voucher_no = $clist->voucher_no;
                        $gik = $clist->gik;

                        $received_item = $clist->received_item;
                        $registered_item = $clist->registered_item_no;
                        $pending_item = $received_item - $registered_item;

                        //$funded_by = empty($clist->own_funded) ? $clist->funded_by : $clist->own_funded;
						$funded_by = $clist->funded_by;
						$own_funded = $clist->own_funded;
                        $received_by = $clist->received_by;
                        $entry_limit = $clist->entry_limit;
                        //$create_date = $clist->create_date;
                        $format_date = $clist->capitalize_date;

                        $create_m = date("M", strtotime($format_date));
                        $create_d = date("d", strtotime($format_date));
                        $create_y = date("Y", strtotime($format_date));

                        $dif_date = date("Y-m-d", strtotime($format_date));
                        $cur_date = date("Y-m-d");

                        $expire_date = date('Y-m-d', strtotime($dif_date . ' + ' . $entry_limit . 'days'));

                        $expire_m = date("M", strtotime($expire_date));
                        $expire_d = date("d", strtotime($expire_date));
                        $expire_y = date("Y", strtotime($expire_date));
						
						$curr_m = date("M", strtotime($expire_date));
                        $curr_d = date("d", strtotime($expire_date));
                        $curr_y = date("Y", strtotime($expire_date));


                        $diff = abs(strtotime($cur_date) - strtotime($dif_date));
						
				
                        $years = floor($diff / (365 * 60 * 60 * 24));
                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $asset_name; ?></td>
                            <td><?php get_parent_category_name($category_id); echo ' > '.$category_name; ?> </td>
                            <td><?php echo $voucher_no; ?></td>
                            <td><?php echo $received_item; ?></td>
                            <td><?php echo get_user_fullname($received_by); ?></td>
                            <td><?php echo get_department_name($dept_id);?></td>
                            <td><?php echo $grn_no; ?></td>
                            <td><?php if($gik == 1){ echo $funded_by;}else{ echo $own_funded; } ?></td>
                            <td><?php echo $create_d; ?>-<?php echo $create_m; ?>-<?php echo $create_y; ?></td>
                            <td>
                            <?php 
								if($pending_item <= 0)
								{
								?><div id="capital_dated" title="Registered">
								<?php echo $expire_d; ?>-<?php echo $expire_m; ?>-<?php echo $expire_y; ?>
                                </div>
								<?php
								}
								else
								{
									if(($expire_date > $cur_date)and($days<=3)) /* Newly Capitalized Asset */
									{
									 ?>
                                     <div id="capital_dated" title="Newly Capitalized Asset">
                                     <font color="blue"><?php echo $expire_d; ?>-<?php echo $expire_m; ?>-<?php echo $expire_y; ?></font>
                                     </div>
                                     <?php
									}
									elseif(($expire_date > $cur_date)and($days<=7 and $days>=4))
									{
										?>
                                         <div id="capital_dated" title="Going to expire capital date.">
                                         <font color="tomato"><?php echo $expire_d; ?>-<?php echo $expire_m; ?>
                                        -<?php echo $expire_y; ?></font></div>
                                        <?php	
									}
									elseif(($expire_date <= $cur_date))
									{
										?><div id="capital_dated" title="Expired capitalization date">
                                         <font color="red"><?php echo $expire_d; ?>-<?php echo $expire_m; ?>
                                        -<?php echo $expire_y; ?></font></div>
                                        <?php	
									}
									elseif(($expire_date > $cur_date))
									{
										?><div id="capital_dated" title="Expired capitalization date">
                                         <font color="blue"><?php echo $expire_d; ?>-<?php echo $expire_m; ?>
                                        -<?php echo $expire_y; ?></font></div>
                                        <?php	
									}
								
								}
							
							?>    
                            </td>
                            <td>
                                <?php if (permission_check('asset/capital_update')) { ?>
                                    <button title="<?php echo load_message('ASSET_CAPITAL_DETAIL') ?>"
                                       class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-capital"
                                       onclick="capital_edit('<?php echo $encrypt_capital; ?>');"><i
                                                class="fa fa-list-alt fa-x"></i></button>
                                <?php }
                                if (permission_check('asset/capital_delete')) { ?> &nbsp;
                                    <button title="<?php echo load_message('DELETE_CAPITAL') ?>"
                                       class="btn btn-danger btn-xs"
                                       onclick="return capital_delete('<?php echo $encrypt_capital; ?>');"><i
                                                class="fa fa-trash fa-x" aria-hidden="true"></i></button>
                                <?php }
                                if (permission_check('asset/capital_register')) 
								{
									if ($pending_item > 0) 
									{
									 if(($expire_date >= $cur_date))
									 	{
										?>
                                         	<a href="<?php echo site_url('asset/capital_register/' . $grn_no); ?>"
                                            title="Asset Register" class="btn btn-success btn-xs"><i
                                            class="fa fa-file-text fa-x" aria-hidden="true"></i></a>
                                        <?php
										}
									}	
                                  
                                }
								/*********** Capitalization Registration limit ************/
								if(permission_check('asset/capital_limit_update')) 
								{
								?>
                                                                     
                                    <button title="<?php echo load_message('EDIT_CAPITAL_LIMIT') ?>"
                                    class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-capital_limit"
                                    onclick="capital_limit_edit('<?php echo $capital_id;?>');">
                                    <i class="fa fa-pencil fa-x"></i></button>
                                <?php
								}
                                ?>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->


        <!-- ------------Modal for Cpitalization edit Start--------------- -->
        <div class="modal fade" id="modal-capital">
            <div class="modal-dialog" style="width:660px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_CAPITAL_DETAIL') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="capital_edit"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal for Capital Edit end here -->
    
    <!-- ------------Modal for Capitalization Registration limit edit Start--------------- -->
        <div class="modal fade" id="modal-capital_limit">
            <div class="modal-dialog" style="width:360px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('EDIT_CAPITAL_LIMIT'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="capital_limit"></div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                    data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--  Modal for Cpitalization Registration limit edit End Here -->


    <!-- /.box -->
</section>
<!-- /.content -->