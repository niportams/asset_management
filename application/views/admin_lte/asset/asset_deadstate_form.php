<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<!-- Main content -->

<section class="content">

    <!-- Default box -->


    <div class="box-body form-horizontal">

        <div class="col-md-12  form_shape">
            <div class="form-group" align="justify"></div>
            <div id="asset_dead_form">

                <div class="form-group">
                    <label for="exampleInputName" class="col-sm-3 control-label">Asset ID<span
                                class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="asset_no" name="asset_no"
                               placeholder="Asset ID/ Label ID">
                    </div>
                    <div class="col-sm-3">
                        <input type="button" name="search_asset" value="Search" class="btn btn-success"
                               onclick="searching_asset();"/>
                    </div>
                </div>

            </div>
            <div id="asset_update_form" align="center">

            </div>
        </div>

    </div>

    <!-- /.box-body -->

    <!-- /.box-footer-->

    <!-- /.box -->

</section>
<!-- /.content -->

   