<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->


<!-- Main content -->
<div class="box-body">
    <div class="col-md-12">
        <div class="row">
            <?php if ($requisition_details) { ?>
                <div class="col-md-6">
                    <?php echo "<b>".load_message('REQUISITION_FOR').": </b> "; get_user_fullname($requisition_details[0]->userid)?>
                </div>
                <div class="col-md-6">
                    <?php echo "<b>".load_message('REQUISITION_REQUEST_INITIATED_BY').": </b> "; get_user_fullname($requisition_details[0]->on_behalf)?>
                </div>
                <div class="col-md-6">
                    <b><?php echo load_message('REQUISITION_REQUESTED_DATE'); ?> </b> <?php get_format_date($requisition_details[0]->created_date);?>
                </div>
                <?php
            }
            if ($approval_list) {
                foreach ($approval_list as $approver) {?>
                    <div class="col-md-6">
                        <?php if ($approver->authorization == "RECOMMANDED") {
                            echo "<b>Recommended By: </b> ". $approver->fullname;
                        }elseif ($approver->authorization == "APPROVED"){
                            echo "<b>Approved By: </b> ". $approver->fullname;
                        }elseif ($approver->authorization == "PROCEED"){
                            echo "<b>Acknowledged By: </b> ". $approver->fullname;
                        }
                        ?>
                    </div>
                <?php  }
            }
            ?>
        </div>
        <br>
        <div class="box box-success modal-table">
            <table class="table table-bordered table-striped">
                <thead>
                <th width="10">#</th>
                <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                <th width="50"><?php echo load_message('QUANTITY'); ?></th>
                </thead>
                <tbody>
                <?php
                $i = 1;

                if ($requisition_details) {
                    foreach ($requisition_details as $rdetails) {
					echo "<tr>
							<td>" . $i . "</td>
							<td>" . $rdetails->name . "</td>
							<td>" . $rdetails->quantity . "</td>
						  </tr>";
                    }
                    $i++;
                }

                ?>
                </tbody>
            </table>
        </div>
        <font><b><?php echo load_message('SPECIAL_NOTE'); ?> </b> <?php echo $requisition_details[0]->remark; ?></font>
        <br>
        <label><?php echo load_message('REMARKS'); ?></label>
        <?php if ($approval_list) {
            foreach ($approval_list as $approver) {?>
                <div class="col-md-12">
                    <?php
                    echo "<b>".$approver->fullname.": </b> ". $approver->remarks;
                    ?>
                </div>
            <?php  }
        }
        ?>
        <br>
    </div>

    <!-- /.box-body -->
     