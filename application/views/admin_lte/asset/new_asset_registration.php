<?php
$message = "";
$received_item = "";
$registered_item = "";
$pending_item = "";
$get_registration_type = "";
if (isset($asset_capital)) {
    if ($asset_capital <> "") {
        //echo"Asset capital not empty!";
        foreach ($asset_capital as $capital_details) {
            $capital_id = $capital_details->capital_id;
            $asset_name = $capital_details->asset_name;
            $category_id = $capital_details->category_id;
            $receiving_office = $capital_details->receiving_office;
            $received_item = $capital_details->received_item;
            $registered_item = $capital_details->registered_item_no;
            $pending_item = $received_item - $registered_item;

            if ($pending_item < 0) {
                $pending_item = 0;
            }

            $format_date = $capital_details->create_date;
            //$format_date=date("Y-m-d", strtotime($create_date));

            $dif_date = date("Y-m-d", strtotime($format_date));
            $cur_date = date("Y-m-d");

            $diff = abs(strtotime($cur_date) - strtotime($dif_date));

            $years = floor($diff / (365 * 60 * 60 * 24));
            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
            $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
			
			$category_lifetime =  get_category_lifeime($category_id);
			if(($category_lifetime <> 0)and($category_lifetime<>NULL))
			{
			 //echo $category_lifetime;
			  $end_life = date('d/m/Y', strtotime('+'.$category_lifetime .'years'));
			}
			else
				{
					$end_life = "";
				}

        }
        $message = "";
        ?>
        <script>
            $('#new_registration_body').show();
            $('#submit_btn').show();
            $('#grn_no').css('background-color', '#eee');
        </script>
    <?php } else {
        $message = "Sorry! Invaild / Already Registered / Expired GRN Found. Please Try Again.";
    }
}
?>

<div class="form-group" align="center">
    <label for="exampleInputName"><span class="text-danger"><?php if ($message <> "") {
                echo $message;
            } ?></span></label>
</div>
<hr>
<div id="new_registration_body" hidden>

    <div class="row">
    <div class="col-md-12"><?php echo load_message('MANDATORY_TEXT'); ?></div>
	<div class="col-md-12">&nbsp;</div>
     <div class="col-md-12" >
          <div class="col-sm-6" style="background-color:#F5F9EC; border:dotted; border-width:1px;">
            <label><?php echo load_message('TOTAL_NUMBER_REGISTERED'); ?>:</label>&nbsp; <?php echo $registered_item; ?> <br />
            <label><?php echo load_message('PENDING_NUMBER_REGISTRATION'); ?>:</label>&nbsp; <?php echo $pending_item; ?>
            
        </div>
       </div>
       <div class="col-md-12">&nbsp;</div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('ASSET_TITLE'); ?><span
                            class="text-danger">*</span></label>

                <?php
                if ($read_mode == 1) {
                    ?>
                    <input type="text" class="form-control" id="asset_name" name="asset_name" placeholder="<?php echo load_message('ASSET_TITLE'); ?>"
                    value="" readonly="on">
                    <?php
                } else {

                    ?>
                    <input type="hidden" name="cpid" id="cpid" value="<?php echo $capital_id; ?>"/>
                    <input type="hidden" name="received_item" id="received_item" value="<?php echo $received_item; ?>"/>
                    <input type="hidden" name="office_id" id="office_id" value="<?php echo $receiving_office; ?>"/>
                    
                     <input type="hidden" class="form-control" id="registered_item_no" name="registered_item_no"
                                   readonly="readonly"
                                   value="<?php echo $registered_item; ?>">
                           <input type="hidden" class="form-control" id="pending_item_no" name="pending_item_no"
                                   readonly="readonly" value="<?php echo $pending_item; ?>">
                    <input type="text" class="form-control" id="asset_name" name="asset_name"
                           placeholder="<?php echo load_message('ASSET_TITLE'); ?>"
                           value="<?php echo $asset_name; ?>">
                    <?php
                }
                ?>
                <div class="form-group">
                    <label><?php echo load_message('CATEGORY'); ?><span class="text-danger">*</span></label>
                    <select name="category_id" id="category_id" class="form-control" disabled="disabled">
                        <!--<option value=""><?php //echo load_message('SELECT_CATEGORY'); ?></option>-->
                        <?php
                        foreach ($category_list as $clist) {
                            $db_category_id = $clist->category_id;
                            $category_name = $clist->category_name;
                            $parent_id = $clist->parent_id;
                            $registration_type = $clist->registration_type;
							//$category_lifetime = $clist->lifetime;
                            if ($category_id == $db_category_id) {
                                $get_registration_type = $registration_type;
                                ?>
                                <option value="<?php echo $db_category_id; ?>"
                                        selected="selected" > <?php if ($parent_id == NULL) {
                                        echo $category_name;
                                    } else {
                                        echo "&nbsp;-" . $category_name;
                                    } ?></option>
                                <?php
                            }

                        }
                        ?>

                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('DESCRIPTION'); ?></label>
                <textarea name="asset_description" rows="3" id="asset_description" class="form-control"></textarea>
            </div>
        </div>
    </div>

    <div class="row form-group" align="justify">

        <div class="col-md-3">
            <label><?php echo load_message('MANUFACTURER'); ?></label>
            <select name="manufacture_id" id="manufacture_id" class="form-control select2">
                <option value="" selected="selected"><?php echo load_message('SELECT_MANUFACTURER'); ?></option>
                <?php
                foreach ($manufacture_list as $mlist) {
                    $manufacture_id = $mlist->manufacture_id;
                    $manufacture_name = $mlist->manufacture_name;
                    ?>
                    <option value="<?php echo $manufacture_id; ?>"><?php echo $manufacture_name; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('WARRENTY_EXP_DATE'); ?></label>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control date-picker" id="warrenty_date" name="warrenty_date"
                       placeholder="dd/mm/yyyy">
            </div>
        </div>
        <div class="col-md-3">
            <label><?php echo load_message('SKU'); ?></label>
            <input type="text" class="form-control" id="sku" name="sku"
                   placeholder="<?php echo load_message('SKU'); ?>" title="<?php echo load_message('SKU_TOOLTIP'); ?>">
        </div>
        <div class="col-md-3">
            <label><?php echo load_message('ASSET_LABEL_ID'); ?></label>
            <input type="text" class="form-control" id="sci_id" name="sci_id"
                   placeholder="<?php echo load_message('ASSET_LABEL_ID'); ?>">
        </div>
	</div>
    
    <!-- <div class="form-group" align="justify">&nbsp;</div>-->
    <div class="row form-group" align="justify">
        <div class="col-md-3">
            <label><?php echo load_message('MODEL_NAME'); ?></label>
            <input type="text" class="form-control" id="model_name" name="model_name"
                   placeholder="<?php echo load_message('MODEL_NAME'); ?> ">
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('MODEL_NO'); ?></label>
            <input type="text" class="form-control" id="model_no" name="model_no"
                   placeholder="<?php echo load_message('MODEL_NO'); ?>">
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('SERIAL_NO'); ?></label>
            <input type="text" class="form-control" id="serial_no" name="serial_no"
                   placeholder="<?php echo load_message('SERIAL_NO'); ?>" title="Serial Number of the product.">
        </div>


        <div class="col-md-3">
            <label><?php echo load_message('REF'); ?></label>
            <input type="text" class="form-control" id="reference_no" name="reference_no"
                   placeholder="<?php echo load_message('REF'); ?>">
        </div>
    </div>

    <div class="row form-group" align="justify">

        <div class="col-md-3">

            <label><?php echo load_message('PURCHASE_DATE'); ?> <span class="text-danger">*</span></label>

            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right date-picker" id="purchase_date" name="purchase_date"
                       placeholder="dd/mm/yyyy">
            </div>
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('PURCHASE_ORDER_NO'); ?></label>
            <input type="text" class="form-control" id="purchase_no" name="purchase_no"
                   placeholder="<?php echo load_message('PURCHASE_ORDER_NO'); ?>">
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('PURCHASE_PRICE'); ?></label>
            <input type="text" class="form-control number-only" id="purchase_price" name="purchase_price"
                   placeholder="<?php echo load_message('PURCHASE_PRICE'); ?>">
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('PURCHASE_LOC'); ?></label>
            <input type="text" class="form-control" id="purchase_location" name="purchase_location"
                   placeholder="<?php echo load_message('PURCHASE_LOC'); ?>">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label><?php echo load_message('ASSET_LIFETIME'); ?></label>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control date-picker" id="asset_lifetime"
                       name="asset_lifetime" placeholder="dd/mm/yyyy" value="<?php echo $end_life;?>">
            </div>
        </div>
        <div class="col-md-6">
            <label for="exampleInputName" class="control-label"><?php echo load_message('PR_NO'); ?></label>

            <input type="text" class="form-control" id="pr_reference_no" name="pr_reference_no"
                   placeholder="<?php echo load_message('PR_NO'); ?>" title="Procurement Request Number">
        </div>
    </div>

    <div class="row form-group" align="justify">

        <div class="col-md-6">

            <?php
            if ($get_registration_type == 'single') {
                ?>
                <div class="col-md-6" style="display:none">
                    <label for="registration_type"><?php echo load_message('BULK_REGISTRATION'); ?></label> &nbsp;
                    <!-- <input type="radio" id="registration_type" name="registration_type" value="bulk">-->
                    <input type="checkbox" id="registration_type" name="registration_type" value="bulk">
                </div>
                <?php
            } else {
                ?>
                <div class="col-md-6">
                    <label for="registration_type"><?php echo load_message('BULK_REGISTRATION'); ?></label> &nbsp;
                    <!-- <input type="radio" id="registration_type" name="registration_type" value="bulk">-->
                    <input type="checkbox" id="registration_type" name="registration_type" value="bulk">
                </div>
                <?php
            }
            ?>

        </div>

       
    </div>

    <div class="row form-group">
        <div class="col-md-12">
            <label for="exampleInputName" class="control-label"><?php echo load_message('REMARKS'); ?></label>
				<textarea name="asset_remarks" id="asset_remarks" name="asset_remarks" class="form-control"
                      placeholder="<?php echo load_message('REMARKS'); ?>"></textarea>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.select2').select2()

    });
</script>