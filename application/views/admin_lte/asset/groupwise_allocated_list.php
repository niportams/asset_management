<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<div class="content">

    <!-- Default box -->
    <div class="box box-success">
        <!-- <div class="box-header">
           <h3 class="box-title">Data Table of Asset Allocation</h3>
         </div>-->
        <!-- /.box-header -->
        <div class="box-body table-responsive" id="allocate_list">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="204">Requested By</th>
                    <th width="204"><?php echo load_message('REQUISITION_NO'); ?></th>
                    <th width="100"><?php echo load_message('REF'); ?></th>
                    <th width="100"><?php echo load_message('ALLOCATED_DATE'); ?></th>
                    <th width="60"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($allocation_list <> "") {
                    foreach ($allocation_list as $aclist) 
					{
                        //$allocate_id = $aclist->allocate_id;
                        $requisition_no = $aclist->requisition_no;
                        //$allocated_by = $aclist->allocated_by;
                        $allocate_to = $aclist->allocate_to;
                        //$allocate_office = $aclist->allocate_office;
                        //$asset_location = $aclist->asset_location;
                        $reference_no = $aclist->reference_no;
                        $create_date = $aclist->create_date;

				?>
				<tr>
                  <td align="center"><?php echo $i;?></td>
                  <td><?php echo get_user_fullname($allocate_to);?></td>
        		  <td><?php echo $requisition_no;?></td>
                  <td><?php echo $reference_no;?></td>
                  <td><?php echo get_format_date($create_date);?></td>
                  <td>
                  <button class="btn btn-success btn-xs" title="Details" data-toggle="modal" data-target="#allocation-details"
                        onclick="return allocation_details('<?php echo $requisition_no;?>','<?php echo $reference_no;?>');">
                        <i class="fa fa-list-alt"></i>
                   </button>
                      <button onclick="return add_attachment('<?php echo $requisition_no;?>','ajax3/add_files','allocation_upload');" data-toggle="modal" data-target="#allocation-attachment"  title="Add Attachment" class="btn btn-primary btn-xs">
                       <i class="fa fa-paperclip"></i></button>&nbsp;<button data-req_no="<?php echo $requisition_no;?>" href="#" title="Print" class="sci-print btn btn-info btn-xs">
                  <i class="fa fa-print"></i></button></td>
               </tr>
                <?php 
				$i++;
				}
				}
				?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->

        <!--   Modal for Asset details-->
        <div class="modal fade" id="allocation-details">
            <div class="modal-dialog" style="width:750px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ALLOCATION_DETAIL') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="allocation_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!--  Allocation Attchment Modal   -->
        <div class="modal fade" id="allocation-attachment">
            <div class="modal-dialog" style="width:650px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ATTACHMENT') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="allocation_upload"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

     
      <!--    Modal end here -->
      		<div class="clearfix"></div>
      <!--    Modal for Role Edit end here -->
    </div>
</div>
      <!-- /.box -->
      </section>
    <!-- /.content -->
<!--    <iframe style="display:none" id="printf" name="printf"></iframe>-->