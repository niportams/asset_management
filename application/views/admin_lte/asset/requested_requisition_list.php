<?php

$i = 1;
if ($req_list <> "") {

    foreach ($req_list as $rqlist) {
        $available = 0;
        $asset_item_id = $rqlist->id;
        $rq_id = $rqlist->requisition_id;
        $category_id = $rqlist->category_id;
        $ast_name = $rqlist->name;
        $ast_qty = $rqlist->quantity;
        $requisition_state = $rqlist->requisition_state;
        ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $ast_name; ?></td>
            <td><?php echo $ast_qty; ?></td>
            <td><?php echo $available = get_category_available($category_id); ?> </td>
            <td>
                <div align="center">
                    <?php if ($available >= $ast_qty) { ?>
                        <font color="green">Available</font>
                    <?php } else { ?>
                        <font color="#FF0000">Not Available</font>
                    <?php } ?>
                </div>
            </td>
            <td><?php if ($available >= $ast_qty) {
                    if ($requisition_state == "Processed") /*This item is already delivered*/ {
                        ?>
                        <div id="allocate_status"><font color='#0000FF'>Allocated!</font></div>
                        <?php

                    } else {
                        ?>
                        <a href="#" title="Allocate Item"
                           onClick="allocate_item('<?php echo $asset_item_id ?>','<?php echo $rq_id; ?>','<?php echo $category_id; ?>','<?php echo $ast_qty; ?>','<?php echo $office_id; ?>',
                                   '<?php echo $requested_by; ?>','single');"><b>Allocate Item</b></a>
                        <?php
                    }

                    ?>

                <?php } else {
                    echo "-";
                } ?></td>
        </tr>
        <?php
        $i++;
    }

}


?>