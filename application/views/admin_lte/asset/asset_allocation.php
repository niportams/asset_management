<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
-->

<section class="content">
    <div class="row" id="asset_allocation_form">
     <div class="col-sm-12"><?php echo load_message('MANDATORY_TEXT'); ?></div>
                <div class="col-sm-12">&nbsp;</div>
        <div class="col-md-4">
            <div class="box  box-danger">
                <div class="box-header with-border">
                    <h4><?php echo load_message('ADD_ITEM_TO_REQUISITION_LIST'); ?></h4>
                </div>
                <div class="box-body form-horizontal">
                    <div id="add_product">

                        <div>
                            <label class="control-label"><?php echo load_message('SELECT_ITEM'); ?><span
                                        class="text-danger">*</span></label>
                                        <?php //var_dump(get_category_list("item","item","form-control",load_message('SELECT_ITEM'),"","value"));?>
                                        <?php select_chain();?>
<!--                           <select class="form-control" id="item" name="item">
                                <option value=""><?php //echo load_message('SELECT_ITEM'); ?></option>
                                <?php //foreach ($assets_name as $value) {
                                    //echo '<option value=""' . $value->category_id .' >' . $value->category_name . "</option>";
                               // } ?>
                            </select>-->
                        </div>
                        <div>
                            <label class="control-label"><?php echo load_message('REQUIRED_QTY'); ?> 
                            <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" id="qty" name="qty">
                        </div>
                        <br>
                        <div class="box-footer">
                            <div align="center">
                                <!-- <button class="btn btn-primary add_item">Add to List</button>-->
                                <button class="btn btn-success select_item"
                                        onclick="add_to_list();"><i class="fa fa-plus"></i> &nbsp;<?php echo load_message('ADD_TO_LIST'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header with-border">
                    <h4><?php echo load_message('UPLOAD_ASSOCIATE_FILES'); ?></h4>
                </div>
                <div class="box-body form-horizontal">


                    <br/>
                    <a href="#" onclick="return clickAnother();">
                        <div class="upload">
                            <i class="fa fa-upload" aria-hidden="true" style="font-size: 5em;"></i>
                            <h4><?php echo load_message('CLICK_TO_UPLOAD'); ?></h4>
                            <label class="control-label"><?php echo load_message('SELECT_FILE_JPG_PNG_PDF_DOCX'); ?></label>
                        </div>
                    </a>
                    <input type="file" name="file" id="file" style="display: none;"/>
                    <hr/>
                    <table id="buildyourform" style="width:350px;" class="table table-striped">
                        <thead>
                        <tr>
                            <th width="12px;"><?php echo load_message('FILE_NAME'); ?></th>
                            <th width="12px;"></th>
                        </tr>
                        </thead>
                        <!-- <span id="uploaded_image"></span>-->
                        <tbody id="uploaded_image">

                        </tbody>
                    </table>

                    <div class="form-group" align="justify">&nbsp;</div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <form id="asset_allocation">
                <div class="box  box-primary">
                    <div class="box-header with-border">
                        <h4><?php echo load_message('ITEM_LIST'); ?> </h4>
                    </div>
                    <div class="box-body ">


                        <table id="buildyourform" class="table table-striped">
                            <thead>
                            <tr id="field1">
                                <th>
                                    <label class="control-label"><?php echo load_message('ASSET_TITLE'); ?></label>
                                </th>
                                <th>
                                    <label class="control-label"><?php echo load_message('REQUIRED_QTY'); ?></label>
                                </th>
                                <th>
                                    <label class="control-label"></label>
                                </th>
                            </tr>
                            </thead>
                            <!--  <tbody id="clear-all">-->
                            <tbody id="selected_asset">

                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h4><?php echo load_message('ALLOCATION_DETAILS'); ?></h4>
                    </div>
                    <div class="box-body form-horizontal">
						<!-- <h4>Allocation Details</h4>-->

                        <div>
                            <label class="control-label"><?php echo load_message('ALLOCATED_TO'); ?>
                                <span class="text-danger">*</span></label>
                            <select class="form-control" id="allocate_to" name="allocate_to">
                                <option value=""><?php echo load_message('SELECT_USER'); ?>
                                </option>
                                <?php
                                foreach ($user_list as $uslist) {
                                    $allocate_userid = $uslist->userid;
                                    $allocate_name = $uslist->fullname;
                                    $allocate_designation = $uslist->designation;
                                    ?>
                                    <option value="<?php echo $allocate_userid; ?>"><?php echo $allocate_name; ?>
                                        -<?php echo $allocate_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <label class="control-label"><?php echo load_message('ON_BEHALF'); ?> <span
                                        class="text-danger">*</span></label>
                            <select class="form-control" id="on_behalf" name="on_behalf">
                                <option value=""><?php echo load_message('SELECT_USER'); ?>
                                </option>
                                <?php
                                foreach ($user_list as $uslist) {
                                    $allocate_userid = $uslist->userid;
                                    $allocate_name = $uslist->fullname;
                                    $allocate_designation = $uslist->designation;
                                    ?>
                                    <option value="<?php echo $allocate_userid; ?>"><?php echo $allocate_name; ?>
                                        -<?php echo $allocate_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <label class="control-label"><?php echo load_message('ALLOCATION_LOCATION'); ?></label>
                            <select class="form-control" id="asset_location" name="asset_location">
                                <option value='0'><?php echo load_message('SELECT_LOCATION'); ?></option>
                                <?php
                                foreach ($room_list as $rmlist) {
                                    $room_id = $rmlist->room_id;
                                    $room_name = $rmlist->room_name;
                                    $room_location = $rmlist->room_location;
                                    ?>
                                    <option value="<?php echo $room_id; ?>"><?php echo $room_name; ?>
                                        -<?php echo $room_location; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <label class="control-label"><?php echo load_message('APPROVER'); ?>
                            <span class="text-danger">*</span></label>
                            <select class="form-control" id="approved_by" name="approved_by">
                                <option value=""><?php echo load_message('SELECT_APPROVER'); ?></option>
                                <?php
                                foreach ($approver_list as $avlist) {
                                    $approver_id = $avlist->userid;
                                    $approver_name = $avlist->fullname;
                                    $approver_designation = $avlist->designation;
                                    ?>
                                    <option value="<?php echo $approver_id; ?>"><?php echo $approver_name; ?>
                                        -<?php echo $approver_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <label class="control-label"><?php echo load_message('REFERENCE'); ?><span
                                        class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="reference_no" id="reference_no" value="<?php echo trim( date("dmyHis")); ?>">
                        </div>

                        <div>
                            <label class="control-label"><?php echo load_message('VALUE_OR_CONTEXT'); ?></label>
                            <textarea class="form-control" type="textarea" id="value_context"
                                      name="value_context"></textarea>
                        </div>


                        <div>
                            <label class="control-label"><?php echo load_message('REMARKS'); ?></label>
                            <textarea class="form-control" type="textarea" id="remarks" name="remarks"></textarea>
                        </div>

                        <br>
                    </div>
                    <div class="box-footer">
                        <div align="center">
                            <button class="btn btn-primary" id="allocation_submit"
                                    onclick="handle_form('asset_allocation', asset_allocation, 'ajax3/allocation_submit',0,'manual', true,'clear-all')">
                                <i class="fa fa-send"></i> &nbsp; <?php echo load_message('ALLOCATE'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--    Modal for assets show end here -->

</section>
<!-- /.content -->

<script type="text/javascript">
    var assets =
        <?php echo json_encode($assets_name);?>;
</script>
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--<script src="dist/jquery.imageuploader.js"></script>-->
<script>
    (function () {
        var options = {};
        $('.js-uploader__box').uploader(options);
    }());

    function clickAnother() {
        $("#file").click();
        return false;
    }
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>