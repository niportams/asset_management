<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/jsonmap.js"); ?>
<?php load_js("dist/js/asset_script.js"); ?>

<?php
foreach ($requisition_details as $cdetails) {
    $request_id = $cdetails->requisition_id;
    $request_title = $cdetails->title;
    $office_id = $cdetails->office_id;
    $office_name = $cdetails->office_name;
    $requested_by = $cdetails->userid;
    $approved_by = $cdetails->approved_by;
    $requested_date = $cdetails->created_date;
    $requested_date = date("d-m-Y", strtotime($requested_date));
    $approved_date = $cdetails->create_date;
    $approved_date = date("d-m-Y", strtotime($approved_date));
    $remark = $cdetails->remark;
}

?>
<!-- Main content -->
<div class="box-body">
    <div class="col-md-12" style="background-color:#FFF;">
        <div class="form-group">
            <div align="left"><strong>Requisition From:</strong> &nbsp; <?php echo $office_name; ?></div>
        </div>

        <div class="form-group">
            <strong>Requested By: </strong>&nbsp;<?php echo get_user_fullname($requested_by); ?>
            &nbsp; <strong>Date: </strong>&nbsp; <?php echo $requested_date; ?>
        </div>
        <div class="form-group">
            <strong>Approved By: </strong>&nbsp;<?php echo get_user_fullname($approved_by); ?>
            &nbsp; <strong>Date: </strong>&nbsp<?php echo $approved_date; ?>
        </div>

        <hr/>
        <!--<div class="form-group" id="requisition_list" style="overflow-y:scroll; height:250px;" >-->
        <div class="form-group" id="requisition_list">
            <b>Requested Item List:</b>
            <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr style="background-color:#FFFFFF;">
                    <th>#</th>
                    <th>Item Name</th>
                    <th>Requested Qty</th>
                    <th>Available Qty</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($req_list as $rqlist) {
                    $available = 0;
                    $rq_id = $rqlist->requisition_id;
                    $category_id = $rqlist->category_id;
                    $ast_name = $rqlist->name;
                    $ast_qty = $rqlist->quantity;

                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $ast_name; ?></td>
                        <td><?php echo $ast_qty; ?></td>
                        <td><?php echo $available = get_category_available($category_id); ?> </td>
                        <td>
                            <div align="center">
                                <?php if ($available >= $ast_qty) { ?>
                                    <i class="fa  fa-check fa-x" style="color: green;"></i>
                                <?php } else { ?>
                                    <i class="fa  fa-close fa-x" style="color: red;"></i>
                                <?php } ?>
                            </div>
                        </td>
                        <td><?php if ($available >= $ast_qty) { ?> <a href="#" title="Allocate Item"
                                                                      onclick="allocate_item('<?php echo $rq_id; ?>','<?php echo $category_id; ?>','<?php echo $ast_qty; ?>','<?php echo $office_id; ?>',
                                                                              '<?php echo $requested_by; ?>','single');"><b>Allocate
                                    Item</b></a>
                            <?php } else { ?> <font color="#FF0000">Not Available</font> <?php } ?></td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
                </tbody>
            </table>
            <div class="form-group">

                <div class="col-md-12">
                    <label>Remarks:*</label>
                    <p><?php echo $remark; ?></p>
                </div>
            </div>
            <!-- <div class="form-group" align="justify">&nbsp;</div>  -->
        </div>


        <!-- <div class="box-footer" align="center">
      
       <button type="button" class="btn btn-primary" onclick="return asset_allocation_deliver('<?php //echo $request_id;?>','<?php //echo $requested_by;?>');">Allocate Assets</button>
                 
        </div>     -->

        <!--<div class="form-group" align="justify">&nbsp;</div> -->


    </div>
    <!-- /.box-body -->
       