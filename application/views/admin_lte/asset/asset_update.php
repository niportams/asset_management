<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/jsonmap.js"); ?>
<?php
foreach ($asset_details as $adetails) {
    $asset_id = $adetails->asset_id;
    $encrypt_asset = base64_encode($asset_id);
    $capital_id = $adetails->capital_id;
    $category_id = $adetails->category_id;
    $manufacture_id_edit = $adetails->manufacture_id;
    $asset_name = $adetails->asset_name;
    $asset_description = $adetails->asset_description;
    $office_id = $adetails->office_id;

    $model_name = $adetails->model_name;

    $model_no = $adetails->model_no;
    $grn_no = $adetails->grn_no;
    $sku = $adetails->sku;
    $serial_no = $adetails->serial_no;
    $reference_no = $adetails->reference_no;
    $purchase_order_no = $adetails->purchase_no;
    $purchase_date = $adetails->purchase_date;
    $purchase_price = $adetails->purchase_price;
	$pr_reference_no = $adetails->pr_reference_no;
	if($purchase_price == 0)
		{
		$purchase_price = "";
		}
    $purchase_location = $adetails->purchase_location;
    $status_id = $adetails->asset_status_id;
    $warrenty_date = $adetails->warrenty_date;
    if ($warrenty_date == NULL) {
        $warrenty_date = "";
    } else {
        $warrenty_date = date("d/m/Y", strtotime($warrenty_date));
    }
    $asset_lifetime = $adetails->asset_lifetime;
    if ($asset_lifetime == NULL) {
        $asset_lifetime = "";
    } else {
        $asset_lifetime = date("d/m/Y", strtotime($asset_lifetime));
    }
    $purchase_date = date("d/m/Y", strtotime($purchase_date));
    //$purchase_date=date("Y-m-d", strtotime($purchase_date));

    //$date = strtotime($date);
    //$date = date('d-m-Y', $date);

    $label_id = $adetails->label_id;
    $sci_id = $adetails->sci_id;
    $db_status_id = $adetails->asset_status_id;
    $asset_remarks = $adetails->asset_remarks;
}

?>
<!-- Main content -->

<form id="asset_update_form">

    <div class="box-body form-horizontal">

        <div class="col-md-12">
            <div class="form-group" align="justify"></div>

            <div class="form-group">
                <input type="hidden" name="cpid" id="cpid" value="0"/>
                <input type="hidden" name="received_item" id="received_item" value="0"/>
                <input type="hidden" name="office_id" id="office_id" value="0"/>
                <label for="exampleInputName" class="col-sm-2 control-label"><?php echo load_message('GRN'); ?><span
                            class="text-danger">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="grn_no" name="grn_no" readonly="readonly"
                           value="<?php echo $grn_no; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputName" class="col-sm-2 control-label"><?php echo load_message('ASSET_TITLE'); ?>
                    <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="asset_name" name="asset_name" placeholder="Title"
                           value="<?php echo $asset_name; ?>">
                </div>

            </div>


            <div class="form-group">
                <label for="exampleInputName"
                       class="col-sm-2 control-label"><?php echo load_message('DESCRIPTION'); ?></label>
                <div class="col-sm-10">
                    <textarea name="asset_description" id="asset_description" rows="3"
                              class="form-control"><?php echo $asset_description; ?></textarea>
                </div>

            </div>

            <div class="form-group" align="justify">

                <div class="col-md-3">
                               
                    <label><?php echo load_message('CATEGORY'); ?><span class="text-danger">*</span></label>
                
                    <?php 
					if($capital_id == 99999)
					{
					get_category_list("category_id", "category_id", "form-control select2", load_message('SELECT_CATEGORY'), $category_id,'select',true,false);
					}
					else
					{
					get_category_list("category_id", "category_id", "form-control", load_message('SELECT_CATEGORY'), $category_id,'select',true,true); 
					}
					?>

                </div>

                <div class="col-md-3">
                    <label><?php echo load_message('MANUFACTURER'); ?></label>

                    <select name="manufacture_id" id="manufacture_id" class="form-control select2">
                        <option value="" selected="selected">Select Manufacture</option>
                        <?php
                        foreach ($manufacture_list as $mlist) {
                            $manufacture_id = $mlist->manufacture_id;
                            $manufacture_name = $mlist->manufacture_name;
                            if ($manufacture_id == $manufacture_id_edit) {
                                ?>
                                <option value="<?php echo $manufacture_id; ?>"
                                        selected="selected"><?php echo $manufacture_name; ?></option>

                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $manufacture_id; ?>"><?php echo $manufacture_name; ?></option>
                                <?php
                            }
                            ?>


                            <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="col-md-3">
                    <label><?php echo load_message('WARRENTY_EXP_DATE'); ?></label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control date-picker" id="warrenty_date" name="warrenty_date"
                               placeholder="Expire Date" value="<?php echo $warrenty_date; ?>">
                    </div>
                </div>


                <div class="col-md-3">
                    <label><?php echo load_message('SKU'); ?></label>
                    <input type="text" class="form-control" id="sku" name="sku" placeholder="Stock Keeping Unit"
                           value="<?php echo $sku; ?>" title="Stock Keeping Unit">
                </div>

            </div>
            <!-- <div class="form-group" align="justify">&nbsp;</div>-->
            <div class="form-group" align="justify">

                <div class="col-md-3">
                    <label><?php echo load_message('MODEL_NAME'); ?></label>
                    <input type="text" class="form-control" id="model_name" name="model_name" placeholder="Model Name"
                           value="<?php echo $model_name; ?>">
                </div>

                <div class="col-md-2">
                    <label><?php echo load_message('MODEL_NO'); ?></label>
                    <input type="text" class="form-control" id="model_no" name="model_no" placeholder="Model(#)"
                           value="<?php echo $model_no; ?>">
                </div>

                <div class="col-md-2">
                    <label><?php echo load_message('SERIAL_NO'); ?></label>
                    <input type="text" class="form-control" id="serial_no" name="serial_no" placeholder="Serial(#)"
                           value="<?php echo $serial_no; ?>">
                </div>


                <div class="col-md-2">
                    <label><?php echo load_message('REF'); ?></label>
                    <input type="text" class="form-control" id="reference_no" name="reference_no"
                           placeholder="Reference(#)" value="<?php echo $reference_no; ?>">
                </div>

                <div class="col-md-3">
                    <label><?php echo load_message('ASSET_LABEL_ID'); ?></label>
                    <input type="text" class="form-control" id="sci_id" name="sci_id" placeholder="Label ID(#)"
                           value="<?php echo $sci_id; ?>">
                </div>


            </div>

            <!-- <div class="form-group" align="justify">&nbsp;</div>  -->
            <div class="form-group" align="justify">

                <div class="col-md-3">
                    <label><?php echo load_message('PURCHASE_DATE'); ?><span class="text-danger">*</span></label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control date-picker" id="purchase_date" name="purchase_date"
                               placeholder="Purchase Date" value="<?php echo $purchase_date; ?>">
                    </div>
                </div>


                <div class="col-md-3">
                    <label><?php echo load_message('PURCHASE_ORDER_NO'); ?></label>
                    <input type="text" class="form-control" id="purchase_no" name="purchase_no"
                           placeholder="Order No(#)" value="<?php echo $purchase_order_no; ?>">
                </div>

                <div class="col-md-3">
                    <label><?php echo load_message('PURCHASE_PRICE'); ?></label>
                    <input type="text" class="form-control" id="purchase_price" name="purchase_price"
                           placeholder="Purchase Price" value="<?php echo $purchase_price; ?>">
                </div>

                <div class="col-md-3">
                    <label><?php echo load_message('PURCHASE_LOC'); ?></label>
                    <input type="text" class="form-control" id="purchase_location" name="purchase_location"
                           placeholder="Purchase Location" value="<?php echo $purchase_location; ?>">
                </div>

            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <label><?php echo load_message('ASSET_LIFETIME'); ?></label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control date-picker" id="asset_lifetime"
                               name="asset_lifetime" value="<?php echo $asset_lifetime; ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('PR_NO'); ?></label>
                    <input type="text"  class="form-control" id="pr_reference_no" name="pr_reference_no" value="<?php echo $pr_reference_no; ?>">

                </div>
                
            </div>

            <div class="form-group" align="justify" style="display:none;">

                <div class="col-md-3">
                    <label><?php echo load_message('BULK_REGISTRATION'); ?></label> &nbsp;
                    <!--<input type="radio" id="registration_type" name="registration_type" value="bulk">-->
                    <input type="checkbox" id="registration_type" name="registration_type" value="bulk">
                </div>


                <div class="col-md-3">
                    <label><?php echo load_message('TOTAL_NUMBER_REGISTERED'); ?></label>
                    <input type="text" class="form-control" id="registered_item_no" name="registered_item_no"
                           readonly="readonly" value="0">
                </div>

                <div class="col-md-3">
                    <label><?php echo load_message('PENDING_NUMBER_REGISTRATION'); ?></label>
                    <input type="text" class="form-control" id="pending_item_no" name="pending_item_no"
                           readonly="readonly" value="0">
                </div>

            </div>

            <!--<div class="form-group" align="justify">&nbsp;</div> -->

            <div class="form-group">
                <div class="col-sm-12">
                 <label for="exampleInputName" class="control-label"><?php echo load_message('ASSET_REMARKS'); ?></label>
                    <textarea name="asset_remarks" id="asset_remarks" name="asset_remarks" rows="3" class="form-control"
                              placeholder="Asset Remarks"><?php echo $asset_remarks; ?></textarea>
                </div>
            </div>

            <div class="form-group" style="display:none;">
                <label for="exampleInputName"
                       class="col-sm-2 control-label"><?php echo load_message('STATUS'); ?></label>
                <div class="col-sm-10">
                    <select name="asset_status_id" id="asset_status_id" class="form-control">
                        <?php
                        foreach ($status_list as $statlist) {
                            $status_id = $statlist->status_id;
                            $status_name = $statlist->status_name;
                            if ($status_id == $db_status_id) {
                                ?>
                                <option value="<?php echo $status_id; ?>"
                                        selected="selected"><?php echo $status_name; ?></option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $status_id; ?>"><?php echo $status_name; ?></option>
                                <?php
                            }

                        }
                        ?>

                    </select>
                </div>
            </div>

        </div>

    </div>

    <!-- /.box-body -->

    <div align="center">
        <!-- <button type="submit" class="btn btn-primary" onClick="return asset_register();">Submit</button>-->
        <button type="submit" class="btn btn-primary"
                onClick="handle_form('asset_update_form', asset, 'asset/update','<?php echo $encrypt_asset ?>','manual')">
            <i class="fa fa-pencil-square-o"></i> &nbsp; <?php echo load_message('UPDATE_ASSET_DETAILS'); ?></button>
    </div>

    <!-- /.box-footer-->
</form>


<!-- /.box-body -->
<script>
    $(function () {
        $('.select2').select2({ width: '100%' })

    });
</script>