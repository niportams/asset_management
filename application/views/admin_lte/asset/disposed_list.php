<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="110"><?php echo load_message('CATEGORY'); ?></th>
                    <th width="100"><?php echo load_message('MODEL_NAME'); ?></th>
                    <th width="70"><?php echo load_message('MODEL_NO'); ?></th>
                    <th width="90"><?php echo load_message('ASSET_ID_L'); ?></th>
                    <th width="70"><?php echo load_message('ASSET_LABEL_ID'); ?></th>
                    <th width="70"><?php echo load_message('PURCHASE_DATE'); ?></th>
                    <th width="50"><?php echo load_message('STATUS'); ?></th>
                    <th width="100"><?php echo load_message('REGISTRATION_DATE'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="abc">
                <?php
                $i = 1;
                if ($asset_list <> "") {
                    foreach ($asset_list as $aslist) {
                        $asset_id = $aslist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $category_id = $aslist->category_id;
                        $category_name = $aslist->category_name;
                        $asset_name = $aslist->asset_name;
                        $office_id = $aslist->office_id;

                        $model_name = $aslist->model_name;

                        $model_no = $aslist->model_no;
                        $grn_no = $aslist->grn_no;
                        $sku = $aslist->sku;
                        $purchase_no = $aslist->purchase_no;
                        $purchase_date = $aslist->purchase_date;
                        $status_id = $aslist->asset_status_id;
                        $status_name = $aslist->status_name;
                        $purchase_date = date("Y-m-d", strtotime($purchase_date));
                        $label_id = $aslist->asset_readable_id;
                        $create_date = $aslist->create_date;
                        //$create_date=date("Y-m-d", strtotime($create_date));


                        $sci_id = $aslist->sci_id;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $asset_name; ?></td>
                            <td><?php get_parent_category_name($category_id);  echo ' > '.$category_name;?></td>
                            <td><?php echo $model_name; ?></td>
                            <td><?php echo $model_no; ?></td>
                            <td><?php echo $sci_id; ?></td>
                            <td><?php echo $label_id; ?></td>
                            <td><?php echo $purchase_date; ?></td>
                            <td><?php echo $status_name; ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                            <td><button class="btn btn-primary btn-xs asset-detail" data-asset_id="<?=$asset_id;?>" title="Details" data-toggle="modal"
                            data-target="#modal-asset-details" onclick="return asset_details('<?php echo $encrypt_asset; ?>');">
                            <i class="fa fa-list-alt"></i> </button>          
                            <a href="#" data-toggle="modal" data-target="#modal-attachment" class="btn btn-success btn-xs asset-detail"
                            title="Click to Show Attachment" onclick="return dispose_attachment('<?php echo $encrypt_asset;?>');">
                            <span class="text"><i class="fa fa-paperclip fa-x"></i></span></a>            
                           </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>

            </table>
        </div>

        <!-- /.box-body -->

        <!-- ------------Modal for Asset edit Start--------------- -->
        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal for Capital Edit end here -->
    <div class="modal fade" id="modal-attachment">
            <div class="modal-dialog" style="width:550px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ATTACHED_FILES') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="attached_files"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
      </div>   


    <!-- /.box -->
</section>
<!-- /.content -->