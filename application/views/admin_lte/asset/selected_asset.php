<?php
if ($tmp_list <> "") {
    foreach ($tmp_list as $tlist) {
        $item_id = $tlist->item_id;
        $category_id = $tlist->category_id;
        $qty = $tlist->qty;


        ?>

        <tr>
            <td>
                <?php echo get_category_name($category_id); ?>
            </td>
            <td>
                <div align="left">
                    <?php echo $qty; ?></div>
            </td>
            <td>
                <a href="#" title="Delete Item" class="btn btn-danger btn-xs"
                   onclick="return tmp_delete('<?php echo $item_id; ?>');"><i class="fa fa-trash fa-x"
                                                                              aria-hidden="true"></i></a>
            </td>
        </tr>
        <?php
    }
}
?>