<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
foreach ($requisition_details as $cdetails) {
    $request_id = $cdetails->requisition_id;
    $request_title = $cdetails->title;
    $office_id = $cdetails->office_id;
    $office_name = $cdetails->office_name;
    $requested_by = $cdetails->userid;
    $approved_by = $cdetails->approved_by;
    $requested_date = $cdetails->created_date;
    $requested_date = date("d-m-Y", strtotime($requested_date));
    $approved_date = $cdetails->approved_date;
    $approved_date = date("d-m-Y", strtotime($approved_date));
    $remark = $cdetails->remark;
}

?>
<!-- Main content -->

<div class="box-body" id="transfer_form">
    <div class="row">
        <div class="col-md-6">
            <strong><?php echo load_message('REQUISITION_FROM'); ?>:</strong>
            &nbsp;<?php echo $office_name; ?>
        </div>
        <div class="col-md-6">
<!--            <div align="right">-->
<!--                <button id="export" name="Export" class="btn btn-success btn-xs" title="Click to Export" value="Export"-->
<!--                        onclick="ExportToExcel('transfer_form');"><i class="fa  fa-file-excel-o fa-x"></i></button>-->
<!--            </div>-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <strong><?php echo load_message('REQUISITION_REQUEST_INITIATED_BY'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($requested_by); ?>
        </div>
        <div class="col-md-6">
            <strong><?php echo load_message('REQUISITION_REQUESTED_DATE'); ?>
                : </strong>&nbsp; <?php echo $requested_date; ?>
        </div>
    </div>
    <div class="row">
        <?php
        if ($approval_list) {
            foreach ($approval_list as $approver) {?>
                <div class="col-md-6">
                    <?php if ($approver->authorization == "RECOMMANDED") {
                        echo "<b>Recommended By: </b> ". $approver->fullname;
                    }elseif ($approver->authorization == "APPROVED"){
                        echo "<b>Approved By: </b> ". $approver->fullname;
                    }elseif ($approver->authorization == "PROCEED"){
                        echo "<b>Acknowledged By: </b> ". $approver->fullname;
                    }
                    ?>
                </div>
            <?php  }
        }
        ?>
        <div class="col-md-6">
            <strong><?php echo load_message('APPROVED_DATE'); ?>: </strong>&nbsp<?php echo $approved_date; ?>

        </div>
    </div>
    <br>
    <div class="box box-success">
        <div class="box-header">
            <h4 class="box-title"><?php echo load_message('REQUESTED_ITEM_LIST'); ?>:</h4>
        </div>
        <!--<div class="form-group" id="requisition_list" style="overflow-y:scroll; height:250px;" >-->
        <div class="form-group">
            <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr style="background-color:#FFFFFF;">
                    <th>#</th>
                    <th><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th><?php echo load_message('REQUESTED_QTY'); ?></th>
                    <th><?php echo load_message('AVAILABLE_QTY'); ?></th>
                    <th><?php echo load_message('STATUS'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="requisition_list">
                <?php
                $i = 1;$flag = 0;$j=0;
                if ($req_list <> "") {

                    foreach ($req_list as $rqlist) {
                        $available = 0;
                        $asset_item_id = $rqlist->id;
                        $rq_id = $rqlist->requisition_id;
                        $encrypt_request = base64_encode($rq_id);
                        $category_id = $rqlist->category_id;
                        $ast_name = $rqlist->name;
                        $ast_qty = $rqlist->quantity;
                        $requisition_state = $rqlist->requisition_state;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $ast_name; ?></td>
                            <td><?php echo $ast_qty; ?></td>
                            <td><?php echo $available = get_category_available($category_id); ?> </td>
                            <td>
                            
                             <div align="center">
                                    <?php if ($available >= $ast_qty) { ?>
                                    <font color="green">
									<?php if($requisition_state == "Delivered") { echo $requisition_state; }else{ echo"Available";  } ?></font>
                                    <?php } 
									else 
									{ 
									if($requisition_state == "Delivered") 
										{ 
										
										echo "<font color='green'>".$requisition_state."</font>"; 
										
										}
									else
										{
											$flag = 1; //Item not available
											$info[]=  array(
											"category_id" =>$category_id,
											//"asset_name" =>explode('','',$ast_name),
											"asset_name" =>$ast_name,
											"quantity" =>$ast_qty,
											) ;
											?>
                                             <font color="#FF0000">Not Available</font>
                                            <?php
										}		
									
								
									?>
                                    
                                    <?php 
									$j++;
									} 
									?>
                                </div>
                                                   
                            </td>
                            <td><?php if ($available >= $ast_qty) {
                                    if ($requisition_state == "Delivered") /*This item is already delivered*/ {
                                        echo "<font color='#0000FF'>Transferred.</font>";
                                    } else {
                                        ?>
                                        <a href="#" title="Transfer Item" id="transfer_item<?php echo $i;?>" 
                                           onClick="transfer_item('<?php echo $i;?>','<?php echo $asset_item_id ?>','<?php echo $rq_id; ?>','<?php echo $category_id; ?>','<?php echo $ast_qty; ?>','<?php echo $office_id; ?>',
                                                   '<?php echo $requested_by; ?>','single', event, '<?php echo $encrypt_request ?>');"><b>Transfer
                                                Item</b></a>
                                        <?php
                                    }
                                    ?>
                                <?php } 
								elseif(($available < $ast_qty)and($requisition_state == "Delivered"))
								{
									echo "<font color='#0000FF'>Transferred.</font>";
								} 
								else {
                                    echo "-";
                                } ?></td>
                        </tr>
                        <?php
                        $i++;
                    }

                }


                ?>
                </tbody>
            </table>
            <div class="form-group">

                <div class="col-md-12">
                    <label><?php echo load_message('REMARKS'); ?>:*</label>
                    <p><?php echo $remark; ?></p>
                </div>
                <?php 
		if(($flag == 1) and ($pr_status == 0))
		{
		$reference_no = date("Y-m-d H:i:s");
        $reference_no = str_replace('-', '', $reference_no);
        $reference_no = str_replace(':', '', $reference_no);
        $reference_no = str_replace(' ', '', $reference_no);
        $reference_no = substr($reference_no, 6);

       //var_dump($pr_list);
	     $pr_list =  json_encode($info,JSON_FORCE_OBJECT);
		//var_dump($jsonarray);
	   	//echo $jsonarray = json_decode($pr_ist,true);
	   	//echo $jsonarray[1];
		?>
        <div class="col-md-12">
         <p align="center">  
          <input type="hidden" name="pr_list" id="pr_list" value='<?php echo $pr_list;?>' /> 
          <div id="pr_button" align="center">          
          <a class="btn btn-warning btn-flat" aria-expanded="true" 
          onclick="return generate_pr_request('<?php echo $reference_no;?>','<?php echo $request_id;?>');" id="request_next">
          <strong>Generate Procurement Request</strong>
          </a> 
          </div>
          </p>
        </div>
        <?php 
		}
		?>
            </div>
            <!-- <div class="form-group" align="justify">&nbsp;</div>  -->
        </div>

        <!-- <div class="box-footer" align="center">
      
       <button type="button" class="btn btn-primary" onclick="return asset_allocation_deliver('<?php //echo $request_id;?>','<?php //echo $requested_by;?>');">Allocate Assets</button>
                 
        </div>     -->

        <!--<div class="form-group" align="justify">&nbsp;</div> -->


    </div>
    <!-- /.box-body -->
       