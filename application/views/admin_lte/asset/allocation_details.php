<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<!-- Main content -->
<?php
foreach ($allocation_details as $rqlist) {
    $requisition_no = $rqlist->requisition_no;
    $reference_no = $rqlist->reference_no;
    $approved_by = $rqlist->approved_by;
    $allocated_by = $rqlist->allocated_by;
    $allocate_to = $rqlist->allocate_to;
    $on_behalf = $rqlist->on_behalf;
    $asset_location = $rqlist->asset_location;
    $remarks = $rqlist->remarks;
    $value_context = $rqlist->value_context;
    $create_date = $rqlist->create_date;
}
?>
<div class="box-body" id="allocation_form">
    <div class="row">
        <div class="col-md-4">
            <strong><?php echo load_message('REF');?>:</strong> &nbsp; <?php echo $reference_no; ?>&nbsp;

        </div>
        <div class="col-md-5">
            <strong><?php echo load_message('REQUISITION_NUMBER');?>:</strong> &nbsp; <?php echo $requisition_no; ?>&nbsp;

        </div>
        <div class="col-md-3">
           <!-- <div align="right">
                <button id="export" name="Export" class="btn btn-success btn-xs" title="Click to Export" value="Export"
                        onclick="ExportToExcel('allocation_form');"><i class="fa  fa-file-excel-o fa-x"></i></button>
            </div>-->
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <strong><?php echo load_message('ALLOCATED_TO'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($allocate_to); ?>
        </div>
        <div class="col-md-4">
            <strong><?php echo load_message('APPROVED_BY'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($approved_by); ?>
        </div>
        <div class="col-md-4">
            <strong><?php echo load_message('ALLOCATED_BY'); ?>:</strong>
            &nbsp;<?php echo get_user_fullname($allocated_by); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <strong>
                <?php echo load_message('ALLOCATED_DATE'); ?></strong>
            &nbsp;<?php echo get_format_date($create_date); ?>
        </div>
        <div class="col-md-4">
            <strong><?php echo load_message('VALUE-CONTEXT'); ?>:</strong>&nbsp;<?php echo $value_context; ?>
        </div>
        <div class="col-md-4">
            <strong><?php echo load_message('REMARKS'); ?>:</strong>&nbsp;<?php echo $remarks; ?>
        </div>
    </div>
    <br>
    <!--<div class="form-group" id="requisition_list" style="overflow-y:scroll; height:250px;" >-->
    <div class="col-md-12">
        <div class="box box-success modal-table">
            <b><?php echo load_message('ALLOCATED_ASSET_LIST'); ?>:</b>

            <table id="example4" class="table table-bordered table-striped"
                   style="border:dotted; border-width:1px; border-color:#999;">
                <thead>
                <tr style="background-color:#FFFFFF;">
                    <th>#</th>
                    <th><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th><?php echo load_message('CATEGORY'); ?></th>
                    <th><?php echo load_message('ASSET_ID_L'); ?></th>
                    <th><?php echo load_message('ASSET_LOCATION'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="allocation_list">
                <?php
                $i = 1;
                if ($allocation_details <> "") {
                    foreach ($allocation_details as $rqlist) {
                        $asset_id = $rqlist->asset_id;
                        $asset_label_id = $rqlist->asset_label_id;
                        $category_id = $rqlist->category_id;
                        $asset_location = $rqlist->asset_location;

                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo get_asset_name($asset_id); ?></td>
                            <td><?php echo get_category_name($category_id); ?></td>
                            <td><?php echo $asset_label_id; ?> </td>
                            <td><?php echo get_office_room($asset_location); ?> </td>
                            <td></td>
                        </tr>
                        <?php
                        $i++;
                    }

                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="box box-primary modal-table">
            <label><?php echo load_message('ATTACHED_FILES'); ?>:*</label>
            <!--   <p></p>-->
            <?php
            if ($allocation_files <> "")
            {
            ?>
            <table id="example4" class="table table-bordered table-striped"
                   style="border:dotted; border-width:1px; border-color:#999;">
                <thead>
                <tr style="background-color:#FFFFFF;">

                    <th>File Name</th>
                    <th>Upload Date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="allocation_list">
                <?php
                foreach ($allocation_files as $afiles) {
                    $file_name = $afiles->file_name;
                    $create_date = $afiles->create_date;
                    $created_by = $afiles->created_by;
                    ?>
                    <tr>
                        <td><?php echo $file_name; ?></td>
                        <td><?php echo get_format_date($create_date); ?></td>
                        <td>
                            <a href="<?php echo base_url(); ?>uploads/<?php echo $file_name; ?>" class="btn btn-primary btn-xs" title="Show File"
                               target="_blank"><i class="fa fa-external-link"></i> </a>
                        </td>
                    </tr>
                    <?php
                }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- <div class="form-group" align="justify">&nbsp;</div>  -->


        <!--<div class="form-group" align="justify">&nbsp;</div> -->


    </div>
    <!-- /.box-body -->
       