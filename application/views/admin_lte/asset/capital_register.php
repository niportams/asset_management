<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<?php
if ($asset_capital <> "") {
    //echo"Asset capital not empty!";
    foreach ($asset_capital as $capital_details) {
        $capital_id = $capital_details->capital_id;
        $asset_name = $capital_details->asset_name;
        $category_id = $capital_details->category_id;
        $receiving_office = $capital_details->receiving_office;
        $received_item = $capital_details->received_item;
        $registered_item = $capital_details->registered_item_no;
        $pending_item = $received_item - $registered_item;
        if ($pending_item < 0) {
            $pending_item = 0;
        }
    }
	$category_lifetime =  get_category_lifeime($category_id);
			if(($category_lifetime <> 0)and($category_lifetime<>NULL))
			{
			 //echo $category_lifetime;
			  $end_life = date('d/m/Y', strtotime('+'.$category_lifetime .'years'));
			}
			else
				{
					$end_life = "";
				}
}
?>
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <?php
        if ($asset_capital == "") {
            echo "<br/><div align='center' class='text-danger'>Sorry! Invaild / Already Registered / Expired GRN Found. Please Try Again.</div>";
            ?>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <?php
        } else {
            ?>
            <form id="asset_register_form">
                <div class="box-header">
                    <!--          <h3 class="box-title">Asset Register</h3>-->
                    <!--	      <div class="box-tools pull-right">-->
                    <!--            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
                    <!--              <i class="fa fa-minus"></i></button>-->
                    <!--               <!--<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
                    <!--               <i class="fa fa-times"></i></button>-->
                    <!--          </div>-->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12"><?php echo load_message('MANDATORY_TEXT'); ?></div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-12">
                            <div class="col-sm-6" style="background-color:#F5F9EC; border:dotted; border-width:1px;">
                                <label><?php echo load_message('TOTAL_NUMBER_REGISTERED'); ?>
                                    :</label>&nbsp; <?php echo $registered_item; ?> <br/>
                                <label><?php echo load_message('PENDING_NUMBER_REGISTRATION'); ?>
                                    :</label>&nbsp; <?php echo $pending_item; ?>

                            </div>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <input type="hidden" name="cpid" id="cpid" value="<?php echo $capital_id; ?>"/>
                                <input type="hidden" name="received_item" id="received_item"
                                       value="<?php echo $received_item; ?>"/>
                                <input type="hidden" name="office_id" id="office_id"
                                       value="<?php echo $receiving_office; ?>"/>
                                <input type="hidden" class="form-control" id="registered_item_no"
                                       name="registered_item_no"
                                       readonly="readonly"
                                       value="<?php echo $registered_item; ?>">
                                <input type="hidden" class="form-control" id="pending_item_no" name="pending_item_no"
                                       readonly="readonly" value="<?php echo $pending_item; ?>">

                                <label for="exampleInputName" class="control-label"><?php echo load_message('GRN'); ?>
                                    <span
                                            class="text-danger">*</span></label>

                                <input type="text" class="form-control" id="grn_no" name="grn_no"
                                       value="<?php echo $grn_no; ?>"
                                       title="<?php echo load_message('GRN_TOOLTIP'); ?>" readonly="readonly">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputName"
                                       class="control-label"><?php echo load_message('ASSET_TITLE'); ?>
                                    <span class="text-danger">*</span></label>

                                <input type="text" class="form-control" id="asset_name" name="asset_name"
                                       placeholder="<?php echo load_message('ASSET_TITLE'); ?>"
                                       value="<?php echo $asset_name; ?>">


                            </div>
                            <div class="form-group">
                                <label><?php echo load_message('CATEGORY'); ?><span class="text-danger">*</span></label>
                                <select name="category_id" id="category_id" class="form-control" disabled="disabled">
                                    <option value=""><?php echo load_message('SELECT_CATEGORY'); ?></option>
                                    <?php
                                    foreach ($category_list as $clist) {
                                        $db_category_id = $clist->category_id;
                                        $category_name = $clist->category_name;
                                        $parent_id = $clist->parent_id;
                                        $registration_type = $clist->registration_type;

                                        if ($db_category_id == $category_id) {
                                            $get_registration_type = $registration_type;
                                            ?>
                                            <option value="<?php echo $db_category_id; ?>" selected="selected">
                                                <?php
                                                echo $category_name;
                                                ?>
                                            </option>
                                            <?php
                                        }

                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputName"
                                       class="control-label"><?php echo load_message('DESCRIPTION'); ?></label>

                                <textarea name="asset_description" id="asset_description" rows="8"
                                          class="form-control"></textarea>


                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label><?php echo load_message('MANUFACTURER'); ?></label>
                            <select name="manufacture_id" id="manufacture_id" class="form-control select2">
                                <option value=""
                                        selected="selected"><?php echo load_message('SELECT_MANUFACTURER'); ?></option>
                                <?php
                                foreach ($manufacture_list as $mlist) {
                                    $manufacture_id = $mlist->manufacture_id;
                                    $manufacture_name = $mlist->manufacture_name;
                                    ?>
                                    <option value="<?php echo $manufacture_id; ?>"><?php echo $manufacture_name; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label><?php echo load_message('WARRENTY_EXP_DATE'); ?></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control date-picker" id="warrenty_date" name="warrenty_date" placeholder="dd/mm/yyyy">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label><?php echo load_message('SKU'); ?></label>
                            <input type="text" class="form-control" id="sku" name="sku"
                                   placeholder="<?php echo load_message('SKU'); ?>" title="Stock Keeping Unit">
                        </div>
                        <div class="col-md-3">
                            <label>Label ID(#)</label>
                            <input type="text" class="form-control" id="sci_id" name="sci_id" placeholder="Label ID(#)">
                        </div>

                    </div>
                    <!-- <div class="form-group" align="justify">&nbsp;</div>-->
                    <div class="form-group row">

                        <div class="col-md-3">
                            <label><?php echo load_message('MODEL_NAME'); ?></label>
                            <input type="text" class="form-control" id="model_name" name="model_name"
                                   placeholder="<?php echo load_message('MODEL_NAME'); ?>">
                        </div>

                        <div class="col-md-3">
                            <label><?php echo load_message('MODEL_NO'); ?></label>
                            <input type="text" class="form-control" id="model_no" name="model_no"
                                   placeholder="<?php echo load_message('MODEL_NO'); ?>">
                        </div>

                        <div class="col-md-3">
                            <label><?php echo load_message('SERIAL_NO'); ?></label>
                            <input type="text" class="form-control" id="serial_no" name="serial_no"
                                   placeholder="<?php echo load_message('SERIAL_NO'); ?>">
                        </div>


                        <div class="col-md-3">
                            <label><?php echo load_message('REF'); ?></label>
                            <input type="text" class="form-control" id="reference_no" name="reference_no"
                                   placeholder="<?php echo load_message('REF'); ?>">
                        </div>


                    </div>

                    <!-- <div class="form-group" align="justify">&nbsp;</div>  -->
                    <div class="form-group row">

                        <div class="col-md-3">
                            <label><?php echo load_message('PURCHASE_DATE'); ?> <span
                                        class="text-danger">*</span></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control date-picker" id="purchase_date"
                                       name="purchase_date" placeholder="dd/mm/yyyy">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label><?php echo load_message('PURCHASE_ORDER_NO'); ?></label>
                            <input type="text" class="form-control" id="purchase_no" name="purchase_no"
                                   placeholder="<?php echo load_message('PURCHASE_ORDER_NO'); ?>">
                        </div>

                        <div class="col-md-3">
                            <label><?php echo load_message('PURCHASE_PRICE'); ?></label>
                            <input type="text" class="form-control number-only" id="purchase_price" name="purchase_price"
                                   placeholder="<?php echo load_message('PURCHASE_PRICE'); ?>">
                        </div>

                        <div class="col-md-3">
                            <label><?php echo load_message('PURCHASE_LOC'); ?></label>
                            <input type="text" class="form-control" id="purchase_location" name="purchase_location"
                                   placeholder="<?php echo load_message('PURCHASE_LOC'); ?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label><?php echo load_message('ASSET_LIFETIME'); ?></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control date-picker" id="asset_lifetime"
                                       name="asset_lifetime" placeholder="dd/mm/yyyy" value="<?php echo $end_life;?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="exampleInputName" class="control-label"><?php echo load_message('PR_NO'); ?></label>
                            <input type="text" class="form-control" id="pr_reference_no" name="pr_reference_no"
                                   placeholder="<?php echo load_message('PR_NO'); ?>"
                                   title="Procurement Request Number">
                        </div>
                    </div>
                    <div class="form-group row">

                        <?php
                        if ($get_registration_type == 'single') {
                            ?>
                            <div class="col-md-6" style="display:none">
                                <label for="registration_type"><?php echo load_message('BULK_REGISTRATION'); ?></label>
                                &nbsp;
                                <input type="checkbox" id="registration_type" name="registration_type" value="bulk">
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="col-md-6">
                                <label for="registration_type"><?php echo load_message('BULK_REGISTRATION'); ?></label>
                                &nbsp;
                                <!-- <input type="radio" id="registration_type" name="registration_type" value="bulk">-->
                                <input type="checkbox" id="registration_type" name="registration_type" value="bulk">
                            </div>
                            <?php

                        }
                        ?>

                        <!-- <div class="col-md-3">
                         <label>Single Registration</label>
                         <input type="radio" id="registration_type" name="registration_type" value="single" checked="checked">
                        </div>
                        -->
                        <div class="col-md-3">


                        </div>


                    </div>

                    <!--<div class="form-group" align="justify">&nbsp;</div> -->

                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="exampleInputName"
                                   class="control-label"><?php echo load_message('REMARKS'); ?></label>

                            <textarea name="asset_remarks" id="asset_remarks" name="asset_remarks" class="form-control"
                                      placeholder="<?php echo load_message('REMARKS'); ?>"></textarea>
                        </div>
                    </div>

                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <div align="right">
                        <!-- <button type="submit" class="btn btn-primary" onClick="return asset_register();">Submit</button>-->
                        <button type="submit" class="btn btn-success btn-lg"
                                onClick="handle_form('asset_register_form', asset, 'ajax/asset_register',0,'manual')">
                            <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_ASSET'); ?>
                        </button>
                    </div>
                </div>
                <!-- /.box-footer-->
            </form>
            <?php
        }
        ?>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->

<script type="text/javascript">
    // function check(event){
    //  // event.preventDefault();
    //   ;
    // }
</script>