<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<?php
if (isset($asset_capital)) {
    //echo"Asset capital not empty!";
    /*
    foreach($asset_capital as $capital_details)
        {
        $capital_id = $capital_details->capital_id;
        $asset_name =  $capital_details->asset_name;
        $category_id =  $capital_details->category_id;
        $receiving_office =  $capital_details->receiving_office;
        $received_item =  $capital_details->received_item;
        $registered_item =  $capital_details->registered_item_no;
        $pending_item = $received_item - $registered_item;
        }
        */
}
?>
<section class="content">
    <!-- Default box -->
    <div class="row" id="registration_btn">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="headingTwo">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <b><?php echo load_message('NEW_ASSET_REGISTRATION'); ?></b> (See Details)
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="card-block">
                                    <p><?php echo load_message('NEW_ASSET_REGISTRATION_MSG'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div align="center">
                        <button class="btn btn-primary btn-lg" onclick="registration_form_call(1)">
                            <i class="fa fa-cubes"></i> &nbsp; <?php echo load_message('NEW_ASSET_REGISTRATION'); ?>
                        </button>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <b><b><?php echo load_message('OLD_ASSET_REGISTRATION'); ?>:</b></b> (See Details)
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block">
                                    <p><?php echo load_message('OLD_ASSET_REGISTRATION_MSG'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div align="center">
                        <button class="btn btn-warning btn-lg" onclick="registration_form_call(2)">
                            <i class="fa fa-cubes"></i> &nbsp; <?php echo load_message('OLD_ASSET_REGISTRATION'); ?>
                        </button>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div align="left" id="back_btn" hidden>
        <button class="btn btn-default" onclick="registration_form_call(0)">
            <i class="fa fa-arrow-left "></i> &nbsp; <?php echo load_message('BACK'); ?>
        </button>
    </div>
    <br>
    <div class="box box-success" id="registration_details" hidden>
        <div class="box-header with-border">
            <h4 id="new_asset_register" hidden><?php echo load_message('NEW_ASSET_REGISTRATION'); ?></h4>
            <h4 id="old_asset_register" hidden><?php echo load_message('OLD_ASSET_REGISTRATION'); ?></h4>
        </div>
        <div class="box-body" id="asset_registration_form_type">

        </div>
    </div>

    <!-- /.box -->
</section>
