<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/jsonmap.js"); ?>
<?php
foreach ($capital_details as $cdetails) {
    $capital_id = $cdetails->capital_id;
    $asset_name = $cdetails->asset_name;
    $category_id_update = $cdetails->category_id;
    $receiving_office_id = $cdetails->receiving_office;
    $supplier_id = $cdetails->supplier_id;
    $voucher_no = $cdetails->voucher_no;
    $received_item = $cdetails->received_item;
    $registered_item = $cdetails->registered_item_no;
    $grn_no = $cdetails->grn_no;
    $gik = $cdetails->gik;
    $funded_by = $cdetails->funded_by;
    $own_funded = $cdetails->own_funded;
    $received_by = $cdetails->received_by;
	$unit_db = $cdetails->department_id;
    $create_date = $cdetails->create_date;
}

?>
<!-- Main content -->
<div class="box-body">

    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleInputName" class="control-label"><?php echo load_message('ASSET_TITLE'); ?><span
                        class="text-danger">*</span></label>
            <input type="text" class="form-control" id="asset_name" value="<?php echo $asset_name; ?>">

        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group" align="justify">
                    <label class="control-label"><?php echo load_message('CATEGORY'); ?><span
                                class="text-danger">*</span></label>
                    <?php get_category_list("category_id", "category_id", "form-control select2", load_message('SELECT_CATEGORY'), $category_id_update); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" align="justify">
                    <label class="control-label"><?php echo load_message('SUPPLIER'); ?><span class="text-danger">*</span></label>
					 <select name="supplier_id" id="supplier_id" class="form-control select2">
                        <?php
                        foreach ($supplier_list as $suplist) {
                            $supplier_id_db = $suplist->supplier_id;
                            $supplier_name_db = $suplist->supplier_name;
                            if ($supplier_id_db == $supplier_id) {
                                ?>
                                <option value="<?php echo $supplier_id; ?>"
                                        selected="selected"><?php echo $supplier_name_db; ?></option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $supplier_id_db; ?>"><?php echo $supplier_name_db; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
           
                <?php
                if ($user_center == 1) {
                    ?>
                     <div class="col-md-6">
                    <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('OFFICE'); ?></label>

                        <select name="office_id" id="office_id" class="form-control select2">
                            <?php
                            foreach ($office_list as $olist) {
                                $office_id_db = $olist->office_id;
                                $office_name = $olist->office_name;

                                if ($receiving_office_id == $office_id_db) {
                                    ?>
                                    <option value="<?php echo $receiving_office_id; ?>" selected="selected"><?php echo $office_name; ?></option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="<?php echo $office_id_db; ?>"><?php echo $office_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
 						</div>
                    </div>
                    <?php
                } else {
                    ?>
                    <input type="text" class="form-control" id="office_id" value="<?php echo $receiving_office_id; ?>"
                    style="display:none;">
                    <?php
                }
                ?>
           
            <div class="col-md-6">
                <div class="form-group" align="justify">
                    <label class="control-label"><?php echo load_message('VOUCHER'); ?><span
                                class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="voucher_no" value="<?php echo $voucher_no; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" align="justify">
                    <label class="control-label number-only"><?php echo load_message('ITEM_RECEIVED'); ?><span
                     class="text-danger">*</span></label>
                    <?php
                    if ($registered_item == $received_item) {
                        ?>
                        <input type="text" class="form-control number-only" id="total_received"
                               value="<?php echo $received_item; ?>"
                               readonly="readonly">
                        <?php
                    } else {
                        ?>
                        <input type="text" class="form-control number-only" id="total_received"
                               value="<?php echo $received_item; ?>">
                        <?php
                    }
                    ?>
                    <input type="text" class="form-control" id="registered_item" placeholder="#" style="display:none;"
                           value="<?php echo $registered_item; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" align="justify">
                    <label class="control-label"><?php echo load_message('RECEIVED_BY'); ?></label>
					 <select name="received_by" id="received_by" class="form-control select2">
                            <?php
                            foreach ($user_list as $ulist) {
                                $userid_db = $ulist->userid;
                                $fullname = $ulist->fullname;

                                if ($received_by == $userid_db) {
                                    ?>
                                    <option value="<?php echo $received_by; ?>"
                                    selected="selected"><?php echo $fullname; ?></option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="<?php echo $userid_db; ?>"><?php echo $fullname; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                   <!-- <input type="text" class="form-control" id="received_by" value="<?php // echo $received_by; ?>">-->

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" align="justify">
                    <label class="control-label"><?php echo load_message('GIK'); ?>
                        &nbsp; <?php if ($gik == true) { ?> <input type="radio"
                                                                       id="gik_check"
                                                                       name="gik_check"
                                                                       value="1"
                                                                       checked="checked"> <?php } else { ?>
                            <input type="radio" id="gik_check" name="gik_check" value="1"> <?php } ?></label>

                    <select name="funded_by" id="funded_by" class="form-control select2">
                        <option value="" selected="selected"><?php echo load_message('SELECT_FUNDED_BY'); ?></option>
                        <?php
                        foreach ($org_list as $org_list) {
                            $org_name = $org_list->organization_name;

                            if ($funded_by == $org_name) {
                                ?>
                                <option value="<?php echo $org_name; ?>"
                                        selected="selected"><?php echo $org_name; ?></option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $org_name; ?>"><?php echo $org_name; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" align="justify">
                    <label class="control-label"><?php echo load_message('OWN_FUNDED'); ?>
                        &nbsp; <?php if ($gik == false) { ?> <input
                                type="radio" id="gik_check" name="gik_check" value="2"
                                checked="checked"><?php } else { ?>
                            <input type="radio" id="gik_check" name="gik_check" value="2"> <?php } ?></label>

                    <input type="text" class="form-control" id="own_funded" value="<?php echo $own_funded; ?>"
                           placeholder="NIPORT">

                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group" align="justify">
                     <label for="exampleInputName" class="control-label">Received <?php echo load_message('DEPARTMENT');?></label>
                        <select name="department_id" id="department_id" class="form-control select2">
                            <option value="0" selected="selected"><?php echo load_message('SELECT_DEPARTMENT'); ?></option>
                            <?php
                            foreach($dept_list as $dlist) {
                                $department_id = $dlist->department_id;
                                $department_name = $dlist->department_name;
								
								 if ($department_id == $unit_db) {
                                    ?>
                                    <option value="<?php echo $department_id; ?>" selected="selected">
									<?php echo $department_name; ?></option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="<?php echo $department_id; ?>"><?php echo $department_name; ?></option>
                                    <?php
                                }
                            }
                             
                            
                            ?>
                        </select>

                </div>
            </div>
        </div>
        <?php
        if ($registered_item == 0) {

            ?>
            <div class="form-group" align="center">
                <div class="">
                    <button type="submit" class="btn btn-primary"
                            onclick="return update_capital_submit('<?php echo $capital_id; ?>');">
                        <i class="fa fa-pencil-square-o"></i> &nbsp;
                        <?php echo load_message('UPDATE_ASSET_CAPITALIZATION'); ?>
                    </button>
                    <!-- <button type="submit" class="btn btn-danger" onClick="return form_validation('asset/capital_update',capital,'example1','<?php //echo $capital_id;
                    ?>');" id="capital_update">Update Asset Capitalization</button>-->
                </div>

            </div>
            <?php
        }
        ?>
    </div>

</div>
<script>
    $(function () {
        $('.select2').select2({ width: '100%' });
        // using default options
        $("#tree").fancytree({
            autoExpand: true
        });
    });
</script>
<!-- /.box-body -->
     