<table > <!-- table-striped table-hover -->
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="110"><?php echo load_message('CATEGORY'); ?></th>
                    <th width="100"><?php echo load_message('MODEL_NAME'); ?></th>
                    <th width="70"><?php echo load_message('MODEL_NO'); ?></th>
                    <th width="70"><?php echo load_message('ASSET_LABEL_ID'); ?></th>
                    <th width="90"><?php echo load_message('ASSET_ID_L'); ?></th>
                    <th width="70"><?php echo load_message('PURCHASE_DATE'); ?></th>
                    <th width="50"><?php echo load_message('STATUS'); ?></th>
                    <th width="100"><?php echo load_message('REGISTRATION_DATE'); ?></th>
<!--                    <th width="70"></th>-->
                </tr>
                </thead>
                <tbody id="abc">
                <?php
                $i = 1;
                if ($asset_list <> "") {
                    foreach ($asset_list as $aslist) {
                        $asset_id = $aslist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $category_id = $aslist->category_id;
                        $category_name = $aslist->category_name;
                        $asset_name = $aslist->asset_name;
                        $office_id = $aslist->office_id;

                        $model_name = $aslist->model_name;
                        $model_no = $aslist->model_no;
                        $grn_no = $aslist->grn_no;
                        $sku = $aslist->sku;
                        $purchase_no = $aslist->purchase_no;
                        $purchase_date = $aslist->purchase_date;

                        $status_id = $aslist->asset_status_id;
                        $status_name = $aslist->status_name;
                        $purchase_date = date("Y-m-d", strtotime($purchase_date));
                        $label_id = $aslist->asset_readable_id;
                        $sci_id = $aslist->sci_id;
                        $create_date = $aslist->create_date;

                        //$create_date=date("Y-m-d", strtotime($create_date));
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php echo $asset_name; ?>

                                <?php //echo $asset_name;
                                ?>
                                <div class="float-bar">
                                   
                                </div>
                            </td>
                            <td><?php echo $category_name; ?>
                             <div class="small"><?php //get_parent_category_name($category_id);  echo ' > '.$category_name; ?></div>
                            </td>
                            <td><?php echo $model_name; ?></td>
                            <td><?php echo $model_no; ?></td>
                            <td><?php echo $sci_id; ?></td>
                            <td><?php echo $label_id; ?></td>
                            <td><?php echo get_format_date($purchase_date); ?></td>
                            <td><?php echo $status_name; ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
<!--                            <td>
                              
                                    
                            </td>-->
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>