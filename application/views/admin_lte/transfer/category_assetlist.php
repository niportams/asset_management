<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<?php
foreach ($requisition_details as $cdetails) 
{
    $request_id = $cdetails->requisition_id;
    $encrypt_request = base64_encode($request_id);
    $request_title = $cdetails->title;
    $transfer_office_id = $cdetails->office_id;
    $office_name = $cdetails->office_name;
    $requested_by = $cdetails->userid;
    $approved_by = $cdetails->approved_by;
    $requested_date = $cdetails->created_date;
    $requested_date = date("d-m-Y", strtotime($requested_date));
    $approved_date = $cdetails->approved_date;
    $approved_date = date("d-m-Y", strtotime($approved_date));
    $remark = $cdetails->remark;
    $approval_remark = $cdetails->remarks;
	$asset_location = $cdetails->asset_location;
	$on_behalf = $cdetails->on_behalf;
}

?>
<section class="content">
    
    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
             <h5><strong>Category: </strong>&nbsp;<?php echo get_parent_category_name($category_id);  echo ' > ' ?>
			 <?php echo get_category_name($category_id); ?></h5>
        	<h5><strong>Expected Quantity:</strong> &nbsp;<?php echo $quantity;?></h5>
            <table id="example" class="table table-bordered table-striped table-hover dataTable-full-functional"> <!-- table-striped table-hover -->
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="120"><?php echo load_message('DESCRIPTION'); ?></th>
                    <th width="120"><?php echo load_message('ASSET_LABEL_ID'); ?></th>
                    <th width="100"><?php echo load_message('ASSET_ID_L'); ?></th>
                 	<th></th>
                </tr>
                </thead>
                <tbody id="abc">
                <?php
                $i = 1;
                if ($asset_list <> "") {
                    foreach ($asset_list as $aslist) {
                        $asset_id = $aslist->asset_id;
                        $asset_description = $aslist->asset_description;
                        $category_id = $aslist->category_id;
                       	$asset_name = $aslist->asset_name;
                        $office_id = $aslist->office_id;
                        //$purchase_date = date("Y-m-d", strtotime($purchase_date));
                        $label_id = $aslist->asset_readable_id;
                        $sci_id = $aslist->sci_id;
						
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><div><?php echo $asset_name; ?></div></td>
                            <td><?php echo $asset_description;?></td>
                            <td><?php echo $sci_id; ?></td>
                            <td><?php echo $label_id; ?></td>
                            <td><input type="checkbox" name="asset_check[]" value="<?php echo $asset_id;?>" /></td>
                            </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
            <div class="col-md-12">
         	<p align="center">  
              <div id="alc_button" align="center">          
                  <a class="btn btn-warning btn-flat" aria-expanded="true" 
                  onclick="return new_transfer('<?php echo $request_id; ?>','<?php echo $category_id; ?>','<?php echo $quantity; ?>','<?php echo $transfer_office_id; ?>','<?php echo $requested_by; ?>','<?php echo $requisition_pkid;?>','<?php echo $index;?>');" id="allocate_next">
                  <strong>Transfer</strong>
                  </a> 
              </div>
          	</p>
        </div>
        </div>

        <!-- /.box-body -->

           
    </div>

    <!-- /.box -->
</section>
<!-- /.content -->
<script language="javascript">
$(document).ready(function() {
    $('#example').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
		"searching": true,
    } );
} );
</script>