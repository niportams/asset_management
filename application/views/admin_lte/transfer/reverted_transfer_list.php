<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-success">
        <!-- <div class="box-header">
           <h3 class="box-title">Data Table of Asset Allocation</h3>
         </div>-->
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="204"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="100"><?php echo load_message('CATEGORY'); ?></th>
                    <th width="100"><?php echo load_message('ASSET_ID_L'); ?></th>
                    <th width="145"><?php echo load_message('TRANSFER_DATE'); ?></th>
                    <th width="124"><?php echo load_message('TRANSFER_BY'); ?></th>
                    <th width="124"><?php echo load_message('TRANSFER_TO'); ?></th>
                    <th width="130"><?php echo load_message('OFFICE_TO'); ?></th>
                    <th width="90"><?php echo load_message('REQUISITION_NUMBER'); ?></th>
                    <th width="60"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($transfer_list <> "") {
                    foreach ($transfer_list as $aclist) {
                        $transfer_id = $aclist->transfer_id;
                        $asset_id = $aclist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $asset_label_id = $aclist->asset_label_id;
                        $category_id = $aclist->category_id;
                        $requisition_no = $aclist->requisition_no;
                        $transfer_by = $aclist->transfer_by;
                        $transfer_to = $aclist->transfer_to;
                        $transfer_office = $aclist->transfer_office;
                        $from_office = $aclist->from_office;
                        $create_date = $aclist->create_date;

                        $create_m = date("M", strtotime($create_date));
                        $create_d = date("d", strtotime($create_date));
                        $create_y = date("Y", strtotime($create_date));
                        ?>
                        <tr>
                            <td align="center"><?php echo $i; ?></td>
                            <td><?php echo get_asset_name($asset_id); ?>
                            </td>
                            <td><?php echo get_category_name($category_id);  echo ' > '; get_category_name($category_id); ?></td>
                            <td><?php echo $asset_label_id; ?></td>
                            <td><?php echo $create_d; ?>-<?php echo $create_m; ?>-<?php echo $create_y; ?></td>
                            <td><?php echo get_user_fullname($transfer_by); ?></td>
                            <td><?php echo get_user_fullname($transfer_to); ?></td>
                            <td><?php echo get_office_name($transfer_office); ?></td>
                            <td align="center"><?php echo $requisition_no; ?></td>
                            <td>
                                <button title="Details" data-asset_id="<?=$asset_id;?>" class="btn btn-primary btn-xs asset-detail" data-toggle="modal"
                                 data-target="#modal-asset-details" onclick="return asset_details('<?php echo $encrypt_asset; ?>');">
                                 <i class="fa fa-list-alt"></i></button>
                                 <a href="#" data-toggle="modal" data-target="#modal-attachment" class="btn btn-success btn-xs asset-detail"
                            title="Click to Show Attachment" onclick="return revert_attachment('<?php echo $transfer_id;?>');">
                            <span class="text"><i class="fa fa-paperclip fa-x"></i></span></a>
                               <!-- <button title="Click to revert transfer" class="btn btn-warning btn-xs"
                                   onclick="return asset_revert('<?php //echo $from_office; ?>','<?php //echo $transfer_id; ?>','<?php //echo $asset_id; ?>','transfer');"><span class="text"><i class="fa fa-undo"></i></span></button>-->
                                   
                                 
                                   
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->

        <!-- Modal for Privilege details-->
        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>

                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        
           <!--  Show Attachment of this allocation    -->
        <div class="modal fade" id="modal-attachment">
            <div class="modal-dialog" style="width:550px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ATTACHED_FILES') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="attached_files"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
      </div>   
    <!--    Modal end here -->
    <div class="clearfix"></div>
    <!-- Modal for Role Edit end here -->
    <!-- /.box -->
</section>
<!-- /.content -->