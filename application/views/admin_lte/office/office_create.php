<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header">
            <!--          <h3 class="box-title">Office Create</h3>-->
            <!---->
            <!--          <div class="box-tools pull-right">-->
            <!--           <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
            <!--           <i class="fa fa-minus"></i></button>-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
            <!--              <i class="fa fa-times"></i></button>-->
            <!--          </div>-->
        </div>
        <form id="office_form">
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('OFFICE'); ?>
                        <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="office_name" name="office_name" placeholder="Office Name">
                    </div>
                   <!-- <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('GEO_LOCATION'); ?>
                            <span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="geo_loc" name="geo_loc"
                               placeholder="GEO Location">

                    </div>-->
                     <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('LATITUDE'); ?>
                            <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="latitude" name="latitude" 
                        placeholder="28.618288999999997">

                    </div>
                     <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('LONGITUDE'); ?>
                            <span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="longitude" name="longitude" placeholder="28.618288999999997">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('OFFICE_PREFIX'); ?>
                       <span class="text-danger">*</span></label>
                       <input type="text" class="form-control" id="office_prefix" name="office_prefix" placeholder="Office Prefix">


                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('AREA_COVERED'); ?>
                            <span
                                    class="text-danger">*</span></label>
                        <textarea class="form-control" id="office_area" name="allocated_area" rows="2"
                                  placeholder="Area Covered"></textarea>


                    </div>
                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('ADDRESS'); ?><span
                                    class="text-danger">*</span></label>
                        <textarea class="form-control" id="office_address" name="office_address" rows="2" cols="10" placeholder="Office Address"></textarea>

                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <div align="right">
                    <button type="submit" class="btn btn-success btn-lg"
                            onClick="handle_form('office_form', office, 'ajax/office_submit',0);" id="office_add">
                        <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_OFFICE'); ?>
                    </button>
                </div>
            </div>
            <!-- /.box-footer-->
        </form>
    </div>

    <!-- /.box -->

</section>
<!-- /.content -->