<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table of Office</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive" id="office_list">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="100"><?php echo load_message('OFFICE'); ?></th>
                    <th width="250"><?php echo load_message('AREA_COVERED'); ?></th>
                    <th width="124"><?php echo load_message('CREATE_DATE'); ?></th>
                    <th width="120"><?php echo load_message('UPDATE_DATE'); ?></th>
                    <th width="70"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
				
                if ($office_list <> "") {
                    foreach ($office_list as $off_list) {
                        $office_id = $off_list->office_id;
                        $office_name = $off_list->office_name;
                        $office_area = $off_list->allocated_area;
                        $create_date = $off_list->create_date;
                        $update_date = $off_list->update_date;
						
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $office_name; ?></td>
                            <td><?php echo $office_area; ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                            <td><?php if($update_date <> NULL){ echo get_format_date($update_date);}else{ echo ""; } ?></td>
                            <td>
                                <?php if (permission_check('office/office_edit')) { ?>
                                    <a href="#" title="Edit Office" class="btn btn-primary btn-xs" data-toggle="modal"
                                       data-target="#modal-office"
                                       onclick="office_edit('<?php echo $office_id; ?>');"><i
                                                class="fa fa-pencil fa-x"></i></a>
                                <?php }
                                if (permission_check('office/office_delete')) { ?>
                                    &nbsp;
                                    <a href="#" title="Delete Office" class="btn btn-danger btn-xs"
                                       onclick="return office_delete('<?php echo $office_id; ?>');">
                                       <i class="fa fa-trash fa-x" aria-hidden="true"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->


        <!--------------Modal for Role edit Start----------------->
        <div class="modal fade" id="modal-office">
            <div class="modal-dialog" style="width:560px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('OFFICE_DETAILS'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="office_edit"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal for Role Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->