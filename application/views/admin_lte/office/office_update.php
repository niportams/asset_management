<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/jsonmap.js"); ?>
<?php
foreach ($office_details as $offdetails) {
    $office_id = $offdetails->office_id;
    $office_name = $offdetails->office_name;
    $allocated_area = $offdetails->allocated_area;
    $geo_loc = $offdetails->geo_loc;
	$latitude = $offdetails->latitude;
	$longitude = $offdetails->longitude;
    $prefix = $offdetails->office_prefix;
    $office_address = $offdetails->office_address;

}

?>
<!-- Main content -->
<div class="box-body">
    <div class="col-md-12">
        <form id="office_form">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputName"><?php echo load_message('OFFICE'); ?>
                        <font color="#FF0000">*</font></label>
                        <input type="text" class="form-control" id="office_name" name="office_name"
                               value="<?php echo $office_name; ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputName"><?php echo load_message('AREA_COVERED'); ?><font color="#FF0000">*</font></label>
                        <textarea class="form-control" id="office_area" name="allocated_area" rows="2"
                                  cols="10"><?php echo $allocated_area; ?></textarea>
                    </div>
                    
                   <!-- <div class="form-group">
                        <label for="exampleInputName"><?php //echo load_message('GEO_LOCATION'); ?><span
                                    class="text-danger">*</span></label>

                        <input type="text" class="form-control" id="geo_loc" name="geo_loc" value="<?php echo $geo_loc; ?>">

                    </div>
-->
				 <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('LATITUDE'); ?>
                            <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="latitude" name="latitude" value="<?php echo $latitude; ?>" >

                    </div>
                     <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('LONGITUDE'); ?>
                            <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="longitude" name="longitude" value="<?php echo $longitude; ?>">

                    </div>
				
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="exampleInputName"><?php echo load_message('ADDRESS'); ?><font
                                    color="#FF0000">*</font></label>
                        <textarea class="form-control" id="office_address" name="office_address" rows="2"
                                  cols="10"><?php echo $office_address; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputName"><?php echo load_message('OFFICE_PREFIX'); ?><span
                                    class="text-danger">*</span></label>

                        <input type="text" class="form-control" id="office_prefix" name="office_prefix"
                               value="<?php echo $prefix; ?>">

                    </div>
                </div>
            </div>

            <div class="form-group" align="center">
                <div class="">
                    <!--  <button type="submit" class="btn btn-danger" onclick="return update_office_submit('<?php echo $office_id; ?>');">
                  Update Office</button>-->
                    <button type="submit" class="btn btn-primary"
                            onClick="handle_form('office_form', office, 'office/office_edit',<?php echo $office_id; ?>);"
                            id="office_update"><i class="fa fa-pencil-square-o"></i>
                        &nbsp; <?php echo load_message('UPDATE_OFFICE'); ?>
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- /.box-body -->
     