<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header">
            <!--          <h3 class="box-title">Office Create</h3>-->
            <!---->
            <!--          <div class="box-tools pull-right">-->
            <!--           <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
            <!--           <i class="fa fa-minus"></i></button>-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
            <!--              <i class="fa fa-times"></i></button>-->
            <!--          </div>-->
        </div>
        <form id="room_form">
            <div class="box-body">
                <div class="col-md-6">
                    <?php
                    if ($this->session->userdata('user_center') == 1 && (get_user_role_name() == "NIPORT_ADMIN" || get_user_role_name() == "SUPER_ADMIN")) {
                    ?>
                        <label class="control-label"><?= load_message('OFFICE'); ?></label>
                        <select name="office_id" id="office_id" class="form-control"
                                onchange="return report_allocation_asset();">
                            <option value="">Select Office</option>
                            <?php
                            foreach ($office_list as $olist) {
                                $office_id = $olist->office_id;
                                $office_name = $olist->office_name;
                                if ($user_center == $office_id) {
                                    ?>
                                    <option value="<?php echo $office_id; ?>"
                                            selected="selected"><?php echo $office_name; ?></option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="<?php echo $office_id; ?>"><?php echo $office_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    <?php }else{?>
                    <input type="hidden" id="office_id" name="office_id"
                           value="<?php echo $this->session->userdata('user_center'); ?>"/>
                    <?php }?>
                    <div class="form-group">
                        <label for="exampleInputName" class=" control-label"><?php echo load_message('ROOM'); ?><span
                                    class="text-danger">*</span></label>

                        <input type="text" class="form-control" id="room_name" name="room_name"
                               placeholder="<?php echo load_message('ROOM'); ?>">
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('LOCATION'); ?><span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="room_location"
                               name="<?php echo load_message('LOCATION'); ?>"
                               placeholder="Room Location">

                    </div>

                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <div align="right">
                    <button type="submit" class="btn btn-success btn-lg"
                            onClick="handle_form('room_form', room, 'ajax3/room_submit',0);" id="room_add"><i
                                class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_ROOM'); ?>
                    </button>
                </div>
            </div>
            <!-- /.box-footer-->
        </form>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->