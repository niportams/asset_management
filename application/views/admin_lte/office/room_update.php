<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/jsonmap.js"); ?>
<?php
foreach ($room_details as $rdetails) {
    $room_id = $rdetails->room_id;
    $room_name = $rdetails->room_name;
    $room_location = $rdetails->room_location;

}

?>
<!-- Main content -->
<div class="box-body">
    <div class="col-md-12">
        <form id="room_form">
            <?php
            if ($this->session->userdata('user_center') == 1 && (get_user_role_name() == "NIPORT_ADMIN" || get_user_role_name() == "SUPER_ADMIN")) {
                ?>
                <label class="control-label"><?= load_message('OFFICE'); ?></label>
                <select name="office_id" id="office_id" class="form-control"
                        onchange="return report_allocation_asset();">
                    <option value="">Select Office</option>
                    <?php
                    foreach ($office_list as $olist) {
                        $office_id = $olist->office_id;
                        $office_name = $olist->office_name;
                        if ($room_id == $office_id) {
                            ?>
                            <option value="<?php echo $office_id; ?>"
                                    selected="selected"><?php echo $office_name; ?></option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $office_id; ?>"><?php echo $office_name; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            <?php }else{?>
                <input type="hidden" id="office_id" name="office_id"
                       value="<?php echo $this->session->userdata('user_center'); ?>"/>
            <?php }?>
            <div class="form-group">
                <label for="exampleInputName"><?php echo load_message('ROOM'); ?><font
                            color="#FF0000">*</font></label>
                <input type="text" class="form-control" id="room_name" name="room_name"
                       value="<?php echo $room_name; ?>">
            </div>

            <div class="form-group">
                <label for="exampleInputName"><?php echo load_message('LOCATION'); ?><font
                            color="#FF0000">*</font></label>
                <textarea class="form-control" id="room_location" name="room_location" rows="3"
                          cols="10"><?php echo $room_location; ?></textarea>
            </div>

            <div class="form-group" align="justify">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"
                            onClick="handle_form('room_form', room, 'office/updateroom',<?php echo $room_id; ?>);"
                            id="room_update"><i class="fa fa-pencil-square-o"></i>
                        &nbsp; <?php echo load_message('UPDATE_ROOM'); ?>
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- /.box-body -->
     