<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
foreach ($supplier_details as $supdetails) {
    $supplier_id = $supdetails->supplier_id;
    $supplier_name = $supdetails->supplier_name;
    $supplier_address = $supdetails->supplier_address;
    $supplier_phone = $supdetails->supplier_phone;
    $supplier_email = $supdetails->supplier_email;
}

?>
<!-- Main content -->
<div class="box-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('SUPPLIER_NAME'); ?><font
                                color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="supplier_name" value="<?php echo $supplier_name; ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('PHONE') ?><font color="#FF0000">*</font></label>
                    <input type="text" class="form-control number-only" maxlength="11" id="supplier_phone" value="<?php echo $supplier_phone; ?>">
                </div>

                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('EMAIL') ?><font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="supplier_email" value="<?php echo $supplier_email; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('ADDRESS'); ?><font color="#FF0000">*</font></label>
                    <textarea name="office_area" class="form-control" id="supplier_address" rows="4" cols="10"
                              placeholder="Address"><?php echo $supplier_address; ?></textarea>
                </div>
            </div>
        </div>

        <div class="form-group" align="center">
            <div class="">
                <button type="submit" class="btn btn-primary"
                        onclick="return update_supplier_submit('<?php echo $supplier_id; ?>');">
                    <i class="fa fa-pencil-square-o"></i> &nbsp; <?php echo load_message('UPDATE_SUPPLIER_DETAILS') ?>
                </button>
            </div>
        </div>
    </div>

    <!-- /.box-body -->
     