<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table of Supplier</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive" id="supplier_list">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="100"><?php echo load_message('SUPPLIER_NAME'); ?></th>
                    <th width="100"><?php echo load_message('ADDRESS'); ?></th>
                    <th width="100"><?php echo load_message('EMAIL') ?></th>
                    <th width="100"><?php echo load_message('PHONE') ?></th>
                    <th width="124"><?php echo load_message('CREATE_DATE') ?></th>
                    <th width="70"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
				if($supplier_list <> "")
				{
                foreach ($supplier_list as $slist) {
                    $supplier_id = $slist->supplier_id;
                    $supplier_name = $slist->supplier_name;
                    $supplier_address = $slist->supplier_address;
                    $supplier_phone = $slist->supplier_phone;
                    $supplier_email = $slist->supplier_email;
                    $create_date = $slist->create_date;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $supplier_name; ?></td>
                        <td><?php echo $supplier_address; ?></td>
                        <td><?php echo $supplier_phone; ?></td>
                        <td><?php echo $supplier_email; ?></td>
                        <td><?php echo get_format_date($create_date); ?></td>

                        <td>
                            <?php if (permission_check('supplier/supplier_edit')) { ?>
                                <a href="#" title="Edit Supplier" class="btn btn-primary btn-xs" data-toggle="modal"
                                   data-target="#modal-supplier"
                                   onclick="supplier_edit('<?php echo $supplier_id; ?>');"><i
                                            class="fa fa-pencil fa-x"></i></a>
                                <?php
                            }
                            if (permission_check('supplier/supplier_delete')) { ?>
                                &nbsp;
                                <a href="#" title="Delete Supplier" class="btn btn-danger btn-xs"
                                   onclick="return supplier_delete('<?php echo $supplier_id; ?>');">
                                    <i class="fa fa-trash fa-x" aria-hidden="true"></i></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
				}
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->

        <!--------------Modal for Supplier edit Start----------------->
        <div class="modal fade" id="modal-supplier">
            <div class="modal-dialog" style="width:560px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('SUPPLIER_UPDATE') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="supplier_edit"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal for Role Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->