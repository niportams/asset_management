<?php
foreach ($asset_details as $adetails) {
    $asset_id = $adetails->asset_id;
    $encrypt_asset = base64_encode($asset_id);
    $capital_id = $adetails->capital_id;
    $category_id = $adetails->category_id;
    $manufacture_id_edit = $adetails->manufacture_id;
    $asset_name = $adetails->asset_name;
    $asset_description = $adetails->asset_description;
    $office_id = $adetails->office_id;

    $model_name = $adetails->model_name;

    $model_no = $adetails->model_no;
    $grn_no = $adetails->grn_no;
    $sku = $adetails->sku;
    $serial_no = $adetails->serial_no;
    $reference_no = $adetails->reference_no;
    $purchase_order_no = $adetails->purchase_no;
    $purchase_date = $adetails->purchase_date;
    $purchase_price = $adetails->purchase_price;
    $asset_readable_id = $adetails->asset_readable_id;

    $warrenty_date = $adetails->warrenty_date;
    $warrenty_date = date("d/m/Y", strtotime($warrenty_date));
    $purchase_date = date("d/m/Y", strtotime($purchase_date));
    //$purchase_date=date("Y-m-d", strtotime($purchase_date));

    //$date = strtotime($date);
    //$date = date('d-m-Y', $date);

    $label_id = $adetails->label_id;
    $sci_id = $adetails->sci_id;
    $db_status_id = $adetails->asset_status_id;
    $asset_remarks = $adetails->asset_remarks;
}
//echo $status_desired_state;
?>
<form id="physical_form">
    <input type="hidden" name="asset_id" id="asset_id" value="<?php echo $asset_id; ?>"/>
    <input type="hidden" name="checked_by" id="checked_by" value="<?php echo $checked_by; ?>"/>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputName"
                           class="control-label"><?php echo load_message('ASSET_TITLE'); ?></label>

                    <input type="text" class="form-control" value="<?php echo $asset_name; ?>"
                           readonly="readonly" style="background-color:#FFF;">

                </div>

                <div class="form-group">
                    <label for="exampleInputName"
                           class="control-label"><?php echo load_message('ASSET_LABEL_ID'); ?></label>

                    <input type="text" class="form-control" value="<?php echo $sci_id; ?>"
                           readonly="readonly" style="background-color:#FFF;">

                </div>


                <div class="form-group">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('CATEGORY'); ?></label>

                    <input type="text" class="form-control" value="<?php echo get_category_name($category_id); ?>"
                           readonly="readonly" style="background-color:#FFF;">

                </div>

                <div class="form-group">
                    <label for="exampleInputName"
                           class="control-label"><?php echo load_message('MODEL_NAME'); ?></label>

                    <input type="text" class="form-control" value="<?php echo $model_name; ?>"
                           readonly="readonly" style="background-color:#FFF;">

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputName"
                           class="control-label"><?php echo load_message('ASSET_ID_L'); ?></label>

                    <input type="text" class="form-control" value="<?php echo $asset_readable_id; ?>"
                           readonly="readonly" style="background-color:#FFF;">

                </div>

                <div class="form-group">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('STATUS'); ?><span
                                class="text-danger">*</span></label>

                    <select name="condition" id="condition" class="form-control">
                        <option value="Good">Good</option>
                        <option value="Moderate">Moderate</option>
                        <option value="Not Working">Not Working</option>
                        <option value="Lost">Lost</option>
                        <option value="Damaged">Damaged</option>
                        <option value="Other">Other</option>
                    </select>

                </div>

                <div class="form-group">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('COMMENTS'); ?></label>

                    <textarea rows="4" name="comments" id="comments" class="form-control"></textarea>

                    <div class="form-group">&nbsp;</div>
                </div>
            </div>
        </div>
        <!-- <div id="asset_status_change" style="background-color:#FFFFFF; border-style:dotted; border-width:1px;"  align="center">-->

        <div class="form-group">
            <div align="right">
                <!-- <button type="submit" class="btn btn-primary" onClick="return asset_register();">Submit</button>-->
                <!-- <button type="button" class="btn btn-primary" onClick="asset_status_update('<?php //echo $encrypt_asset;?>')">Save Comment</button>-->

                <button type="submit" class="btn btn-success btn-lg"
                        onClick="handle_form('physical_form', physical, 'ajax2/physical_check_submit',0,'manual')">
                    <i class="fa fa-save"></i> &nbsp; <?php echo load_message('SUBMIT') ?>
                </button>

            </div>
        </div>
        <!--<div class="form-group">&nbsp;</div>  -->
        <!--</div> -->
    </div>
</form>
                    
                      