<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="110"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="70"><?php echo load_message('CONDITION'); ?></th>
                    <th width="120"><?php echo load_message('COMMENTS'); ?></th>
                    <th width="90"><?php echo load_message('CHECKED_BY'); ?></th>
                    <th width="90"><?php echo load_message('CHECKED_DATE'); ?></th>
                    <th width="70"></th>
                    <!--<th width="110">Action</th>-->
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($check_list <> "") {
                    foreach ($check_list as $cklist) {
                        $check_id = $cklist->check_id;
                        $encrypt_check = base64_encode($check_id);
                        $asset_id = $cklist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $checked_by = $cklist->checked_by;
                        $condition = $cklist->condition;
                        $comments = $cklist->comments;
                        $create_date = $cklist->create_date;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo get_asset_name($asset_id); ?>
                            </td>
                            <td><?php echo $condition; ?></td>
                            <td><?php echo $comments; ?></td>
                            <td><?php echo get_user_fullname($checked_by); ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                            <td><button class="btn btn-info btn-xs asset-detail" data-asset_id="<?=$asset_id;?>" title="Details" data-toggle="modal"
                                   data-target="#modal-asset-details"
                                   onclick="return asset_details('<?php echo $encrypt_asset; ?>');">
                                    <i class="fa fa-list-alt"></i>
                                </button>
                            </td>

                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->


        <!-- ------------Modal for Cpitalization edit Start--------------- -->
        <div class="modal fade" id="modal-physical">
            <div class="modal-dialog" style="width:660px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Asset Physical Check Edit</h4>
                    </div>
                    <div class="modal-body">
                        <div id="physical_edit"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal -->
        </div>
        <!--    Modal for Capital Edit end here -->

        <!-- /.box -->
</section>
<!-- /.content -->