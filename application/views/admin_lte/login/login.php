<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= load_message('SITE_TITLE'); ?></title>
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>theme/dist/img/logo.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <?php load_css("bower_components/bootstrap/dist/css/bootstrap.min.css"); ?>
    <!-- Font Awesome -->
    <?php load_css("bower_components/font-awesome/css/font-awesome.min.css"); ?>
    <!-- Ionicons -->
    <?php load_css("bower_components/Ionicons/css/ionicons.min.css"); ?>
    <!-- Theme style -->
    <?php load_css("dist/css/AdminLTE.min.css"); ?>
    <!-- iCheck -->
    <?php load_css("plugins/iCheck/square/blue.css"); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .container {

        }

        .login-page {
            background-image: url("theme/dist/img/bg-niport.jpg");
        }

        .login-box {
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0px 0px 1px 1px #C6C9E6;
            background-color: #3c8dbc;
            margin-top: 5px;
            margin-bottom: 100px;
        }

        .login-box-body {
            background-color: #e6f6ff;
        }

        .login-logo {
            margin-bottom: 10px;
        }

        .login-logo a {
            color: #FFF;
            font-size: 20px;
        }

        /*.logo-area div{
            position:absolute;
            font-family:Oswald,sans-serif;
        }*/

        .ministry_title {
            top: 55px;
            color: #FFF;
            font-size: 15px;
            margin-left: 15px;
        }

        .site_title {
            font-size: 26px;
            color: #FFF;
            margin-top: 5px;
            margin-left: 15px;
        }

        .niport-logo {
            right: 0px;
        }

        .ams-title {
            text-align: center;
            margin: 50px auto;
            color: #3d9397;
            text-shadow: 1px 2px 1px rgba(150, 150, 150, 0.39);
        }

        .footer {
            position: fixed;
            bottom: 0px;
            padding: 10px;
            background-color: #000;
            width: 100%;
            left: 0px;
            color: #ffffff;
        }

        .banner-login {
            background-color: #3c8dbc;
            overflow: auto;
            padding: 10px;
        }

        .area {
            box-shadow: 0px 0px 2px 2px #CCC;
        }

        /*.alert-danger{*/
        /*position:absolute;*/
        /*right:0px;*/
        /*width:350px;*/
        /*}*/

        .skin-green-light .banner-login, .skin-green-light .login-box {
            background-color: #00a65a;
        }

        .skin-green-light .login-box-body {
            background-color: #FFF;
        }

        .skin-green-light .btn-login {
            background-color: #00a65a;
            border-color: #008d4c;
            color: #FFF;
        }

    </style>
</head>
<body class="hold-transition <?= $this->config->item('theme-skin'); ?> login-page">


<div class="row banner-login">
    <div class="container ">


        <div class="logo-area hidden-sm hidden-xs ">
            <div class="col-xs-1"><img src="<?php echo base_url(); ?>theme/dist/img/logo/bd-government-logo-128.png"
                                       width="90px"></div>
            <div class="col-xs-10">
                <div class="site_title" style="color: #FFF;"><?= load_message('NIPORT'); ?></div>
                <div class="ministry_title">Medical Education and Family Welfare Division<br><?= load_message('MINISTRY'); ?></div>
            </div>

            <div class="col-xs-1"><img src="<?php echo base_url(); ?>theme/dist/img/logo/niport-128.png" width="90px">
            </div>
        </div>

        <div class="logo-area visible-sm visible-xs">
            <div class="col-xs-3"><img src="<?php echo base_url(); ?>theme/dist/img/logo/bd-government-logo-128.png"
                                       width="90px"></div>
            <div class="col-xs-5">
                <div class="site_title"><?= load_message('NIPORT_SHORT'); ?></div>
                <div class="site_title"></div>
                <div class="ministry_title"><?= load_message('MINISTRY_SHORT'); ?></div>
            </div>
            <div class="col-xs-3">
                <div class="niport-logo"><img src="<?php echo base_url(); ?>theme/dist/img/logo/niport-128.png"
                                              width="90px"></div>
            </div>
        </div>
    </div>

</div>
<?php
if ($this->config->item('maintenance_mode') == TRUE) { ?>
    <div class="alert alert-danger"> <?= load_message('MAINTAINENCE'); ?> </div>

<?php }
?>

<div class="row">

    <div class="container">
        <div class="col-md-12 ams-title">
            <h2><strong><?= load_message('SITE_TITLE'); ?></strong></h2>
        </div>


        <div class="row">
            <!-- login error -->
            <div class="col-md-4 col-md-offset-4">
                <?php
                $a = $this->session->flashdata('message');
                if ($a != '') {
                    ?>
                    <div class="alert alert-danger"><i
                                class="icon fa fa-ban"></i><?php echo $this->session->flashdata('message'); ?></div>
                <?php } ?>
            </div>
        </div>


        <div class="login-box">
            <div class="login-logo">
                <a href="<?php echo base_url(); ?>"><span
                            class="fa fa-unlock-alt "></span> <?= load_message('LOGIN_TITLE'); ?></a>
            </div>

            <!-- /.login-logo -->
            <div class="login-box-body">
                <!-- <p class="login-box-msg">Sign in to start your session</p>-->

                <form action="<?= base_url(); ?>login/process" method="post">
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <input type="text" class="form-control" placeholder="User ID" name="em" required>

                    </div>
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <input type="password" class="form-control" placeholder="Password" name="pass" required>

                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label id="forgot">
                                    <?= load_message('FORGOT_PASSWORD'); ?>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-login btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <!--<a href="#">I forgot my password</a><br>-->


            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
        <br><br><br><br><br><br>

    </div><!-- container -->
</div>

    <div class="footer" style="background-color:#FFF">
    <div class="container">
        <div style="margin-bottom: 5px; color: #000;"><strong>Supported By:</strong></div>
        <img src="<?= base_url(); ?>theme/dist/img/footer_logos.png" class="img-responsive">
    </div>
        <div class="text-center" style="border-top: solid 1px #DDD; font-size: 10px; padding-top: 10px; color:#000">
         <strong>Copyright &copy; <?php echo date("Y")-1;  ?>-<?php echo date("Y");  ?> <a href="<?php echo base_url();?>">NIPORT</a>.</strong> All rights reserved.
     </div>
<!--    <div class="pull-right hidden-xs">
        <b>Technical Support: </b> MaMoni  HSSdfg
    </div>
    <strong>Copyright &copy; 2017-<?php //echo date('Y'); ?>&nbsp; <a
                href="<?php //echo base_url(); ?>">NIPORT</a>.</strong> All rights
    reserved.
    <div align="center" style="display:inline" class="hidden-xs">
        <a href="<?php //echo base_url()?>page/contact"> <strong>Contact Us </strong> </a>

    </div>-->
</div>

<!-- jQuery 3 -->
<?php load_js("bower_components/jquery/dist/jquery.min.js"); ?>
<!-- Bootstrap 3.3.7 -->
<?php load_js("bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>
<?php load_js("dist/js/jquery.validate.js"); ?>
<!-- iCheck -->
<?php load_js("plugins/iCheck/icheck.min.js"); ?>
<?php load_js("dist/js/sweetalert.min.js"); ?>
<?php load_js("dist/js/custom_msg.js"); ?>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });

    $("form").validate();
</script>

<script>
    $(document).ready(function (e) {
        $("#forgot").click(function () {
            sci_alert("<?=load_message('FORGOT_PASSWORD_MSG');?>", 'Forgot Password', 'info');
        });
    });
</script>


<style type="text/css">
    .error {
        color: red;
    }
</style>

</body>
</html>
