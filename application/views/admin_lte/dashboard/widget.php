<div class="row">
    <div class="col-md-12">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php sci_total_capitalize_asset(); ?></h3>

                    <p><?php echo load_message('TOTAL_CAPITALIZE_ASSET'); ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-cubes"></i>
                </div>
                <a target="_blank" href="<?php echo base_url(); ?>asset/capitallist" class="small-box-footer">More
                    info <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3><?php sci_total_store_asset(); ?></h3>

                    <p><?php echo load_message('TOTAL_REGISTERED_ASSET'); ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a target="_blank" href="<?php echo base_url(); ?>asset/assetlist" class="small-box-footer">More
                    info <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php sci_total_category(); ?></h3>

                    <p><?php echo load_message('TOTAL_CATEGORIES'); ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-sitemap"></i>
                </div>
                <a target="_blank" href="<?php echo base_url(); ?>category/categorylist" class="small-box-footer">More
                    info
                    <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3><?php sci_total_allocated_asset(); ?></h3>

                    <p><?php echo load_message('TOTAL_ALLOCATED_ASSET'); ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-check"></i>
                </div>
                <a target="_blank" href="<?php echo base_url(); ?>asset/allocated_list" class="small-box-footer">More
                    Info
                    <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
</div>
<!-- /.row -->