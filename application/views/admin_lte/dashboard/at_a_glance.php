<div class="row">
    <?php $asset = sci_asset_value(); ?>
    <!-- at a glance widget -->
    <div class="col-md-12 at-a-glance-widget">
        <div class="col-sm-3">
            <a href="<?= base_url(); ?>report/asset_value">
                <div class="small-box" style="background-color: #1BC98E; color: #FFF">
                    <div class="inner text-right">
                        Total Asset Value (<?php get_office_name($this->session->userdata('user_center')); ?>)
                        <h3> <?= $asset['total_val']; ?> <small style="color:#FFF">BDT</small></h3>
                        <div>
                            Total Asset Counted <strong><?= $asset['total_asset_with_price']; ?>/<?= $asset['total_asset']; ?></strong><br>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- End of at a glance widget -->

        <!-- start -->
        <div class="col-sm-3">
            <a href="<?= base_url(); ?>asset/capitallist">
                <div class="small-box" style="background-color: #9F86FF; color: #FFF">
                    <div class="inner text-right">
                        <?php
                        $asset_capital_info = sci_pending_reg_asset();

                        // var_dump($asset_capital_info);

                        if (is_array($asset_capital_info)) {
                            $registered_asset = $asset_capital_info[0]->registered;
                            $received_asset = $asset_capital_info[0]->received;
                            $asset_type_count = $asset_capital_info[0]->total;
                            $reg_pending = $received_asset - $registered_asset;
                        }
                        ?>
                        Asset Registration Pending
                        <h3><?php echo @$reg_pending; ?></h3>
                        <div>
                            From <strong><?php echo @$received_asset; ?></strong> Capitalized Asset of <strong><?php echo @$asset_type_count; ?></strong> Types
                        </div>
                    </div>
                </div>
            </a>    
        </div><!-- End -->

        <!-- start -->
        <div class="col-sm-3">
            <a href="<?= base_url(); ?>asset/capitallist">
                <div class="small-box" style="background-color: #E64759; color: #FFF">
                    <div class="inner text-right">
                        <?php
                        $exp_asset = sci_asset_reg_expire();
                        if (is_array($exp_asset)) {
                            $exp_asset_no = $exp_asset[0]->exp_item;
                        }
                        ?>
                        Registration Time will Expires for
                        <h3><?= @$exp_asset_no; ?></h3>
                        <div>
                            Unregistered Asset In next 10 Day's
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- End -->

        <!-- start -->
        <div class="col-sm-3">
            <a href="<?= base_url(); ?>report/asset_life_expire">
                <div class="small-box" style="background-color: #1997c6; color: #FFF">
                    <div class="inner text-right">

                        <?php
                        $asset_life_exp = sci_asset_life_expire();
                        if (is_array($asset_life_exp)) {
                            $exp_asset = $asset_life_exp[0]->total_life_expire;
                        }
                        ?>
                        Asset Life Time Ends
                        <h3><?= @$exp_asset; ?></h3>
                        <div>
                            Life time will be expired in this year
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- End -->

    </div><!-- col -12 -->

</div>