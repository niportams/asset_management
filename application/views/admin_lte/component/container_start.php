<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-md-12">
                <div class="title-container">
                    <div class="col-md-10">
                        <h1>
                            <span class="fa fa-th-large"></span> &nbsp; <?php page_title(); ?>
                            <small><?php page_sub_title(); ?></small>
                        </h1>
                    </div>


                    <div class="col-md-2 text-right">
                        <?php moduler_helper(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div style="position:absolute; height: 150px;top:0px; width:100%; overflow: auto;">
            <?= get_message(); ?>
        </div>