<style>
    @media only screen and (min-width: 768px) {

        #custom-nav-icon{
            position: absolute;
            left: 0px;
            top: 100%;
            color: #000;
            padding: 3px;
            background-color: #DDD;
            border: solid 1px #DADDE4;
            box-shadow: 1px 1px 2px 0px #a39d9d;
        }
        .content-header{
            margin-top: 25px;

        }

        #fixed-banner-top{
            text-align: left;
            padding-left: 0px;
            position: fixed;
            z-index: 1050;
            width: auto;
            height: 96px;
        }
    }


</style>
<div class="niport-logo-small-screen">
    <img src="<?php echo base_url(); ?>theme/dist/img/logo/niport-128.png" width="40px" height="40px"/>
</div>

<header class="main-header">

    <div id="left-logo-area" class="logo"
         style="background-color:#00a65a;  padding-top: 5px; border-bottom: dotted 1px #666">
        <!--        <span class="logo-mini">
            <img src="<?php //echo base_url();   ?>theme/dist/img/logo/niport-128.png" width="40px" height="40px"/>

        </span>
        <div style="text-align: center;">
            <span class="logo-lg custom-size">
                <img class="logo-min-hide-" src="<?php //echo base_url();   ?>theme/dist/img/logo/niport-128.png"
                     height="80px"/>
            </span>
        </div>-->
    </div>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" id="custom-nav-icon">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <!-- loading status
       	<div class="btn btn-warning" id="site-status" style="margin-top:7px;">
          Loading ...
        </div>
        <!-- loading status -->



        <div class="niport-logo" style="height:96px;">
            <!--            <img src="<?= base_url(); ?>theme/dist/img/logo/banner-inner.png" class="img-responsive"/>-->
        </div>


        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu" id="notification-popup-menu">
                    <a href="notifications/all_notifications" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="footer">
                            <a href="<?= base_url(); ?>notifications/"><?php load_message('SHOW_ALL_NOTIFICATION'); ?></a>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if (@getimagesize(base_url() . 'theme/dist/img/user/' . $this->session->userdata('user_db_id') . '.jpg')) { ?>
                            <img class="user-image"
                                 src="<?php echo base_url(); ?>theme/dist/img/user/<?php echo (int) $this->session->userdata('user_db_id'); ?>.jpg"
                                 alt="User profile picture">
                        <?php } else { ?>
                            <img class="user-image" src="<?php echo base_url(); ?>theme/dist/img/user2-160x160.jpg">
                        <?php }
                        ?>
                        <span class="hidden-xs"><?= get_user_nice_name(); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <div>
                                <?php if (@getimagesize(base_url() . 'theme/dist/img/user/' . $this->session->userdata('user_db_id') . '.jpg')) { ?>
                                    <img class="img-circle"
                                         src="<?php echo base_url(); ?>theme/dist/img/user/<?php echo (int) $this->session->userdata('user_db_id'); ?>.jpg"
                                         alt="User profile picture">
                                <?php } else { ?>
                                    <img class="img-circle" src="<?php echo base_url(); ?>theme/dist/img/user2-160x160.jpg">
                                <?php }
                                ?>
                                <p class="user-detail">
                                    <b><?php get_user_fullname($this->session->userdata('user_db_id')) ?></b>
                                    <br>
                                    <small> <?php echo get_user_designation($this->session->userdata('user_db_id')); ?></small>
                                </p>
                            </div>
                        </li>


                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="<?= base_url(); ?>login/logout" class="btn btn-danger btn-flat" style="background-color: #d73925 !important;" >Sign out</a>
                            </div>
                            <div class="pull-right" style="margin-right: 5px;">
                                <a href="<?= base_url(); ?>user/profile" class="btn btn-primary btn-flat" style="background-color: #3c8dbc !important;">Profile</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->

                <!-- setting icon is disabled
                <li>
                  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>
<audio style="visibility:hidden;" id="background-audio-player"
       src="<?= base_url(); ?>theme/sound/notification.mp3"></audio>

<script>
    function adjust_width() {
        h = $(".niport-logo").height() + 16;
        $(".logo").css({"height": h + "px"});
    }

    $(document).ready(function (e) {
        adjust_width();
    });
</script>