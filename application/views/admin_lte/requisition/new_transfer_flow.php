<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header"></div>
        
        <form id="transfer_flow_form">
            <div class="box-body" id="flow_form">
                <div class="col-md-6">
                    
                   
                     <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('NEXT_FLOW_ID'); ?>
                        <span class="text-danger">*</span></label>

                        <select name="user_id" id="user_id" class="form-control select2">
                            <option value="" selected="selected"><?php echo load_message('NEXT_FLOW_ID'); ?></option>
                            <?php
                            foreach ($user_list as $ulist) {
                                $userid = $ulist->userid;
                                $fullname = $ulist->fullname;
								$designation = $ulist->designation;
                                ?>
                                <option value="<?php echo $userid; ?>"><?php echo $fullname ; ?>-<?php echo $designation ; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                     <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('STEP'); ?>
                        <span class="text-danger">*</span></label>
						<select name="step" id="step" class="form-control select2">
                            <option value="1" selected="selected">Step 1</option>
                           	<option value="2">Step 2</option>
                            <option value="3">Step 3</option>
                            <option value="4">Step 4</option>
                            <option value="5">Step 5</option>
                            <option value="6">Step 6</option>
                            <option value="7">Step 7</option>
                            <option value="8">Step 8</option>
                            <option value="9">Step 9</option>
                            <option value="10">Step 10</option>
                        </select>
                    </div>
                     <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('ACCESS_TYPE'); ?>
                        <span class="text-danger">*</span></label>
						<select name="access_type" id="access_type" class="form-control select2">
                            <option value="RECOMMANDED" selected="selected">RECOMMANDED</option>
                           	<option value="APPROVED">APPROVED</option>
                            <option value="PROCEED">PROCEED</option>
                           	<option value="HANDOVER">HANDOVER</option>
                        </select>
                    </div>
                </div>
                
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <div align="right">
                    <!-- <button type="submit" class="btn btn-primary" onClick="return category_validation();">Submit</button>-->
                    <button type="submit" class="btn btn-success btn-lg"
                            onClick="handle_form('transfer_flow_form', transfer_flow, 'ajax/transfer_flow_submit',0,'manual')"><i
                                class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_FLOW'); ?>
                    </button>
                </div>
            </div>
            <!-- /.box-footer-->
        </form>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->