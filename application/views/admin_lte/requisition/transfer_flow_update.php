<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/jsonmap.js"); ?>
<?php
foreach ($transfer_flow_details as $fdetails) {
    $flow_id = $fdetails->id;
   
    $next_flow_id_db = $fdetails->user_id;
	$step = $fdetails->step;
	$access_type = $fdetails->access_type;
}

?>
<!-- Main content -->
<form id="transfer_flow_update_form">
    <div class="box-body">
        <div class="col-md-12">
            <div class="form-group" align="justify"></div>
          
                  <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('NEXT_FLOW_ID'); ?>
                        <span class="text-danger">*</span></label>

                        <select name="user_id" id="user_id" class="form-control select2">
                            <option value="" selected="selected"><?php echo load_message('NEXT_FLOW_ID'); ?></option>
                            <?php
                            foreach ($user_list as $ulist) {
                                $userid = $ulist->userid;
                                $fullname = $ulist->fullname;
								$designation = $ulist->designation;
								if($next_flow_id_db == $userid)
								{
								?>
                                <option value="<?php echo $userid; ?>" selected="selected"><?php echo $fullname ; ?></option>
                                <?php
								}
								else
								{
								?>
                                  <option value="<?php echo $userid; ?>"><?php echo $fullname ; ?></option>
                                <?php	
								}
                                ?>
                              
                                <?php
                            }
                            ?>
                        </select>
                 	</div>              
               		<div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('STEP'); ?>
                        <span class="text-danger">*</span></label>
						<select name="step" id="step" class="form-control select2">
                        <option value="<?php echo $step;?>" selected="selected">Step <?php echo $step;?></option>
                        <option value="1">Step 1</option>
                        <option value="2">Step 2</option>
                        <option value="3">Step 3</option>
                        <option value="4">Step 4</option>
                        <option value="5">Step 5</option>
                        <option value="6">Step 6</option>
                        <option value="7">Step 7</option>
                        <option value="8">Step 8</option>
                        <option value="9">Step 9</option>
                        <option value="10">Step 10</option>
                        </select>
                    </div>
            		<div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('ACCESS_TYPE'); ?>
                        <span class="text-danger">*</span></label>
						<select name="access_type" id="access_type" class="form-control select2">
                         <option value="<?php echo $access_type;?>" selected="selected"> <?php echo $access_type;?></option>
                            <option value="RECOMMANDED">RECOMMANDED</option>
                           	<option value="APPROVED">APPROVED</option>
                            <option value="PROCEED">PROCEED</option>
                           <option value="HANDOVER">HANDOVER</option>
                        </select>
                    </div>

            <div class="form-group">
                <div align="center">
                    <!--<button type="submit" class="btn btn-primary" onclick="return update_category_submit('<?php //echo $category_id;?>');">
                      Update Category</button>-->
                   <button type="submit" class="btn btn-primary"
                   onClick="handle_form('transfer_flow_update_form', transfer_flow, 'requisition/transfer_flow_update','<?php echo $flow_id ?>','manual')">
                    <i class="fa fa-pencil-square-o"></i>
                    &nbsp; <?php echo load_message('UPDATE_FLOW_DETAILS'); ?>
                    </button>
                </div>
            </div>
        </div>
</form>
<!-- /.box-body -->
     <script>
         $('.select2').select2({width:'100%'});
     </script>