<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<style>

</style>
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table of All User</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="160"><?php echo load_message('PROCESS_NAME'); ?></th>
                    <th width="140"><?php echo load_message('DEPARTMENT'); ?></th>
                    <th width="130"><?php echo load_message('NEXT_FLOW_ID'); ?></th>
                    <th width="80"><?php echo load_message('STEP');?></th>
                    <th width="80"><?php echo load_message('ACCESS_TYPE'); ?></th>
                    <th width="140"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
				if($flow_list <> "")
				{
                 foreach ($flow_list as $flist) {
                    $flowid = $flist->id;
                    $process_name = $flist->process_name;
                    $department_id = $flist->department_id;
                    $next_flow_id = $flist->next_flow_id;
                    $step = $flist->step;
                    $access_type = $flist->access_type;
                   
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $process_name; ?></td>
                        <td><?php echo get_department_name($department_id); ?></td>
                        <td><?php echo get_user_fullname($next_flow_id); ?></td>
                        <td><?php echo $step; ?></td>
                        <td><?php echo $access_type; ?></td>
                        <td>  
                        <?php
                            if (permission_check('requisition/flow_update')) { ?>
                                &nbsp;<a href="#" class="btn btn-primary btn-xs" data-toggle="modal"
                                data-target="#modal-default" title="Edit Requisition Flow" onclick="flow_edit('<?php echo $flowid; ?>');">
                                <i class="fa fa-pencil fa-x"></i></a>
                            <?php }
                            if (permission_check('requisition/flow_delete')) { ?>
                                &nbsp;<a href="#" title="Delete Profile"
                                         onclick="return flow_delete('<?php echo $flowid; ?>');"
                                         class="btn btn-danger btn-xs">
                                <i class="fa fa-trash fa-x" aria-hidden="true"></i></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
			  }
                ?>
                </tbody>
            </table>
        </div>

        <!-- /.box-body -->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('FLOW_DETAILS'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="flow_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal end here -->
    <!-- /.box -->


</section>
<!-- /.content -->