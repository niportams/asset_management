<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header">
            <!--          <h3 class="box-title">New Manufacture</h3>-->
            <!---->
            <!--          <div class="box-tools pull-right">-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
            <!--            <i class="fa fa-minus"></i></button>-->
            <!--          </div>-->
        </div>
        <form id="organization_form">
            <div class="box-body" id="">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('ORGANIZATION'); ?>
                            <span
                                    class="text-danger">*</span></label>

                        <input type="text" class="form-control" id="organization_name" name="organization_name"
                               placeholder="<?php echo load_message('ORGANIZATION'); ?>">

                    </div>

                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('EMAIL'); ?></label>

                        <input type="text" class="form-control" id="organization_email" name="organization_email"
                               placeholder="<?php echo load_message('EMAIL'); ?>">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('PHONE'); ?></label>

                        <input type="text" class="form-control number-only" maxlength="11" id="organization_phone" name="organization_phone"
                               placeholder="<?php echo load_message('PHONE'); ?>">

                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="exampleInputName"
                               class=" control-label"><?php echo load_message('ADDRESS'); ?></label>

                        <textarea name="organization_address" class="form-control" id="organization_address" rows="4"
                                  placeholder="<?php echo load_message('ADDRESS'); ?>"></textarea>

                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <div align="right">
                    <button type="submit" class="btn btn-success btn-lg" onClick="handle_form('organization_form', organization, 'ajax3/organization_submit',0);">
                        <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_ORGANIZATION'); ?>
                    </button>
                </div>
            </div>
        </form>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
<script>
    $(document).ready(function ($) {
        $cf = $('#manufacture_phone');
        $cf.blur(function (e) {
            phone = $(this).val();
            phone = phone.replace(/[^0-9]/g, '');
            $('#manufacture_phone').val(phone);
            if (phone.length != 11) {
                //alert('Phone number must be 11 digits.');
                $('#manufacture_phone').val(phone);
                $('#manufacture_phone').focus();
            } else {
                $('#manufacture_phone').val(phone);
            }
        });
    });
</script>