<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
foreach ($organization_details as $org_details) {
    $organization_id = $org_details->organization_id;
    $organization_name = $org_details->organization_name;
    $organization_email = $org_details->organization_email;
    $organization_phone = $org_details->organization_phone;
    $organization_address = $org_details->organization_address;
}

?>
<form id="update_organization_form">
    <div class="box-body" id="">

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('ORGANIZATION'); ?>
                    <span
                            class="text-danger">*</span></label>

                <input type="text" class="form-control" id="organization_name" name="organization_name"
                       value="<?php echo $organization_name; ?>">

            </div>

            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('EMAIL'); ?></label>

                <input type="text" class="form-control" id="organization_email" name="organization_email"
                       value="<?php echo $organization_email; ?>">

            </div>
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('PHONE'); ?></label>

                <input type="text" class="form-control number-only" maxlength="11" id="organization_phone" name="organization_phone"
                       value="<?php echo $organization_phone; ?>">

            </div>
        </div>
        <div class="col-md-6">

            <div class="form-group">
                <label for="exampleInputName"
                       class=" control-label"><?php echo load_message('ADDRESS'); ?></label>

                <textarea name="organization_address" class="form-control" id="organization_address" rows="4"
                ><?php echo $organization_address; ?></textarea>

            </div>
        </div>
    </div>
        <div align="center">
            <button type="submit" class="btn btn-primary btn-lg" onClick="handle_form('update_organization_form', organization, 'organization/organization_edit',<?php echo $organization_id; ?>);">
                <i class="fa fa-edit"></i> &nbsp; <?php echo load_message('UPDATE_ORGANIZATION'); ?>
            </button>
        </div>

</form>

<!-- /.box-body -->
     