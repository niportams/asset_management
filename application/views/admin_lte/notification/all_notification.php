<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">
    <?php //var_dump($notifications);?>
    <!-- Default box -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Notifications</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="user_list">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                <tr>
                    <th>From</th>
                    <th>Subject</th>
                    <th>Detail</th>
                    <th>Date</th>
                </tr>
                </thead>

                <tbody>
                <?php if (sizeof($notifications) > 0): ?>
                    <?php foreach ($notifications as $n): ?>
                        <tr <?php if ($n->status) { ?> class="noti_bold"<?php } ?> >
                            <td><?= $n->fullname; ?></td>
                            <td><?= $n->notic_title; ?></td>
                            <td>
                                <?= $n->detail; ?>
                                <a data-notice_id="<?= $n->id; ?>" data-notice_url="<?= $n->action; ?>"
                                   class="btn btn-xs btn-warning notice-item">Run Action</a>
                            </td>
                            <td><?= $n->arrival_date; ?></td>
                        </tr>
                    <?php endforeach; ?>

                <?php endif; ?>

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <!-- /.box -->


</section>
<!-- /.content -->
