<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
foreach ($department_details as $deptdetails) {
    $department_id = $deptdetails->department_id;
    $department_name = $deptdetails->department_name;
}

?>
<form id="update_department_submit">
    <!-- Main content -->
    <div class="box-body">
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputName"><?php echo load_message('DEPARTMENT') ?><span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="department_name" name="department_name"
                       value="<?php echo $department_name; ?>">
            </div>

            <div class="form-group" align="center">
                <div>
                    <button type="submit" class="btn btn-primary"
                            onclick="handle_form('update_department_submit', department,'department/department_edit',<?php echo $department_id; ?>);">
                        <i class="fa fa-pencil-square-o"></i> &nbsp; <?php echo load_message('UPDATE_DEPARTMENT') ?>
                    </button>
                </div>
            </div>
        </div>
</form>
<!-- /.box-body -->
     