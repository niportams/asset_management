<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <form id="department_form">
            <div class="box-header">
                <!--            <h3 class="box-title">Department Create</h3>-->
                <!---->
                <!--            <div class="box-tools pull-right">-->
                <!--              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
                <!--                <i class="fa fa-minus"></i></button>-->
                <!--              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
                <!--                <i class="fa fa-times"></i></button>-->
                <!--            </div>-->
            </div>

            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group"></div>
                    <div class="form-group">
                        <label for="exampleInputName" class="col-md-2"><?php echo load_message('DEPARTMENT_NAME'); ?>
                            <span class="text-danger">*</span></label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="department_name" name="department_name"
                                   placeholder="<?php echo load_message('DEPARTMENT_NAME'); ?>">
                        </div>
                    </div>


                </div>

            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <div align="right">
                    <button type="submit" class="btn btn-success btn-lg"
                            onClick="handle_form('department_form', department, 'ajax/department_submit',0);">
                        <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_DEPARTMENT'); ?>
                    </button>
                </div>
            </div>
        </form>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content