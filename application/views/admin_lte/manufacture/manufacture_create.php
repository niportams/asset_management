<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header">
            <!--          <h3 class="box-title">New Manufacture</h3>-->
            <!---->
            <!--          <div class="box-tools pull-right">-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
            <!--            <i class="fa fa-minus"></i></button>-->
            <!--          </div>-->
        </div>
        <div class="box-body" id="manufacture_form">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('MANUFACTURER'); ?><span
                                class="text-danger">*</span></label>

                    <input type="text" class="form-control" id="manufacture_name"
                           placeholder="<?php echo load_message('MANUFACTURER'); ?>">

                </div>

                <div class="form-group">
                    <label for="exampleInputName"
                           class=" control-label"><?php echo load_message('MANUFACTURER_CODE'); ?></label>

                    <input type="text" class="form-control" id="manufacture_code"
                           placeholder="<?php echo load_message('MANUFACTURER_CODE'); ?>">

                </div>

                <div class="form-group">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('EMAIL'); ?></label>

                    <input type="text" class="form-control" id="manufacture_email"
                           placeholder="<?php echo load_message('EMAIL'); ?>">

                </div>
            </div>
            <div class="col-md-6">

                <div class="form-group">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('ADDRESS'); ?></label>

                    <textarea name="address" class="form-control" id="manufacture_address" rows="4"
                              placeholder="<?php echo load_message('ADDRESS'); ?>"></textarea>

                </div>

                <div class="form-group">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('PHONE'); ?>
                    <span class="text-danger">*</span></label>

                    <input type="text" class="form-control number-only" maxlength="11" id="manufacture_phone"
                           placeholder="<?php echo load_message('PHONE'); ?>">

                </div>

            </div>
        </div>

        <!-- /.box-body -->
        <div class="box-footer">
            <div align="right">
                <button type="submit" class="btn btn-success btn-lg" onClick="return manufacture_validation();">
                    <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_MANUFACTURE'); ?>
                </button>
            </div>
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
