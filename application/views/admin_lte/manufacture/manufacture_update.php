<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
foreach ($manufacture_details as $mandetails) {
    $manufacture_id = $mandetails->manufacture_id;
    $manufacture_name = $mandetails->manufacture_name;
    $manufacture_code = $mandetails->manufacture_code;
    $manufacture_address = $mandetails->manufacture_address;
    $manufacture_phone = $mandetails->manufacture_phone;
    $manufacture_email = $mandetails->manufacture_email;
}

?>
<!-- Main content -->
<div class="box-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('MANUFACTURER_NAME'); ?><font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="manufacture_name" value="<?php echo $manufacture_name; ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('MANUFACTURER_CODE'); ?></label>
                    <input type="text" class="form-control" id="manufacture_code" value="<?php echo $manufacture_code; ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('PHONE'); ?><font color="#FF0000">*</font></label>
                    <input type="text" class="form-control number-only" maxlength="11" id="manufacture_phone" value="<?php echo $manufacture_phone; ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('EMAIL'); ?></label>
                    <input type="text" class="form-control" id="manufacture_email" value="<?php echo $manufacture_email; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('ADDRESS'); ?></label>
                    <textarea name="manufacture_address" class="form-control" id="manufacture_address" rows="4" cols="10"
                              placeholder="Address"><?php echo $manufacture_address; ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group" align="center">
            <div class="">
                <button type="submit" class="btn btn-primary"
                        onclick="return update_manufacture_submit('<?php echo $manufacture_id; ?>');">
                    <i class="fa fa-pencil-square-o"></i>
                    &nbsp; <?php echo load_message('UPDATE_MANUFACTURE_DETAILS'); ?>
                </button>
            </div>
        </div>
    </div>

    <!-- /.box-body -->