<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table of Manufacturer</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="100"><?php echo load_message('MANUFACTURER_NAME'); ?></th>
                    <th width="100"><?php echo load_message('MANUFACTURER_CODE'); ?></th>
                    <th width="100"><?php echo load_message('ADDRESS'); ?></th>
                    <th width="100"><?php echo load_message('EMAIL'); ?></th>
                    <th width="100"><?php echo load_message('PHONE'); ?></th>
                    <th width="124"><?php echo load_message('CREATE_DATE'); ?></th>
                    <th width="70"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
				if($manufacture_list <> "")
				{
                foreach ($manufacture_list as $mlist) {
                    $manufacture_id = $mlist->manufacture_id;
                    $manufacture_name = $mlist->manufacture_name;
                    $manufacture_code = $mlist->manufacture_code;
                    $manufacture_address = $mlist->manufacture_address;
                    $manufacture_phone = $mlist->manufacture_phone;
                    $manufacture_email = $mlist->manufacture_email;
                    $create_date = $mlist->create_date;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $manufacture_name; ?></td>
                        <td><?php echo $manufacture_code; ?></td>
                        <td><?php echo $manufacture_address; ?></td>
                        <td><?php echo $manufacture_phone; ?></td>
                        <td><?php echo $manufacture_email; ?></td>
                        <td><?php echo get_format_date($create_date); ?></td>

                        <td>
                            <?php if (permission_check('manufacture/manufacture_edit')) { ?>
                                <a href="#" title="Edit Manufacture" class="btn btn-primary btn-xs" data-toggle="modal"
                                   data-target="#modal-manufacture"
                                   onclick="manufacture_edit('<?php echo $manufacture_id; ?>');"><i
                                            class="fa fa-pencil fa-x"></i></a>
                            <?php }
                            if (permission_check('manufacture/manufacture_delete')) { ?>
                                &nbsp;
                                <a href="#" title="Delete Manufacture" class="btn btn-danger btn-xs"
                                   onclick="return manufacture_delete('<?php echo $manufacture_id; ?>');">
                                    <i class="fa fa-trash fa-x" aria-hidden="true"></i></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
				}
				
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->


        <!--------------Modal for Manufacture edit Start----------------->
        <div class="modal fade" id="modal-manufacture">
            <div class="modal-dialog" style="width:560px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('MANUFACTURER_UPDATE'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="manufacture_edit"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal for Role Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->