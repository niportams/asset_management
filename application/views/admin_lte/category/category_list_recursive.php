<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">
    <div class="row">
         <div class="col-sm-6">
            <!-- Default box -->
            <div class="box box-warning cat-box" overflow-y: auto;>


                <!-- custom category tree -->

                <!-- <div class="box-header">
                  <h3 class="box-title">Data Table of Asset category</h3>
                </div> -->
                <!-- /.box-header -->
                <div class="box-body" id="role_list">
                    <div class="example">
						<!-- <div id="tree">-->
                         <?php
							/*********** New Category List View *************/
								function multilevel($id)
								{
									$rs1 = get_child_name($id);
								
									if(($rs1)== false) return;
									echo "<ul>";
									foreach($rs1 as $row1)
									{
									$category_name = $row1->category_name;
									$category_id = $row1->category_id;
									echo "<li><a href='#'>".$category_name."</a></li>";
									multilevel($category_id);
									}
									echo "</ul>";
								}
								
								$i = 1;
								echo "<ul>";
								foreach ($parent_list as $plist)
								 {
                                    $pcategory_id = $plist->category_id;
                                    //$parent_id = $plist->parent_id;
                                    $pcategory_name = $plist->category_name;
									?>
                                    <li><?php echo $pcategory_name;?></li>
									                                    
                                    <?php
									multilevel($pcategory_id);
                                    ?>
                                   
								<?php 
								$i++;
								 }
								 echo "</ul>";
								 /********** New category list view end here *******/
								?>
                                
                            <!--</div>-->
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div><!-- col-sm-6 -->
    </div>

    <!--------------Modal for Category edit Start----------------->
    <div class="modal fade" id="modal-category">
        <div class="modal-dialog" style="width:660px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('CATEGORY_DETAILS'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="category_edit"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    </div>
    <!--    Modal for Role Edit end here -->
    <!-- /.box -->
</section>
<!-- /.content -->

<style>
    /*Fance tree for category view */
    .cat-box {
        min-height: 450px;
    }

    ul.fancytree-container {
        border: none !important;
        outline: none;
    }

    li .fancytree-node:hover .tree-item .btn-action {
        display: inline-block;

    }

    .btn-cat-edit {
        display: none;
        font-size: 6px;
    }

    .btn-action {
        display: none;
    }
</style>

<script>
    $(document).ready(function () {

        $(document).on('dblclick', '.tree-item', function () {

            cat_id = $(this).data('cat_id');

            //load modal
            $('#modal-category').modal('show');

            category_edit(cat_id);


        });
    });
</script>