<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">
    <div class="row">
         <div class="col-sm-6">
            <!-- Default box -->
            <div class="box box-warning cat-box" overflow-y: auto;>


                <!-- custom category tree -->

                <!-- <div class="box-header">
                  <h3 class="box-title">Data Table of Asset category</h3>
                </div> -->
                <!-- /.box-header -->
                <div class="box-body" id="role_list">
                    <div class="example">

                        <div id="tree">
                            <ul id="treeData" style="display: none;">
                                <?php

                                $i = 1;
                                foreach ($parent_list as $plist) {
                                    $pcategory_id = $plist->category_id;
                                    //$parent_id = $plist->parent_id;
                                    $pcategory_name = $plist->category_name;
                                    ?>
                                    <li id="id<?php echo $i; ?>" title="parent" class="folder">
                                        <div class="tree-item" data-cat_id="<?php echo $pcategory_id; ?>"
                                             title="Double Click to Edit<?php load_message('DBL_CLICK_OPEN'); ?>"><?php echo $pcategory_name; ?></div>
                                        <ul>
                                            <?php
                                            foreach ($child_list as $chlist) {
                                                $chcategory_id = $chlist->category_id;
                                                $parent_id = $chlist->parent_id;
                                                $chcategory_name = $chlist->category_name;

                                                if ($pcategory_id == $parent_id) {
                                                    $j = 1;
                                                    ?>

                                                    <?php
                                                    $result = get_child_name($chcategory_id);
                                                    //var_dump($result);
                                                    if ($result <> false) {
                                                        //echo"Child";
                                                        //echo $chcategory_name;
                                                        ?>
                                                        <li id="id<?php echo $i; ?>.<?php echo $j; ?>" class="folder">
                                                            <?php if (permission_check('category/category_edit')) { ?>
                                                                <div class="tree-item"
                                                                     data-cat_id="<?php echo $chcategory_id; ?>"
                                                                     title="Double Click to Edit<?php load_message('DBL_CLICK_OPEN'); ?>"><?php echo $chcategory_name; ?></div>
                                                            <?php } ?>
                                                            <ul>
                                                                <?php

                                                                foreach ($result as $rest1) {
                                                                    $child_cat = $rest1->category_id;
                                                                    $child_par_id = $rest1->parent_id;
                                                                    $child_name = $rest1->category_name;
                                                                    ?>

                                                                    <li id="id<?php echo $i; ?>.<?php echo $j; ?>">
                                                                        <?php if (permission_check('category/category_edit')) { ?>
                                                                            <div class="tree-item"
                                                                                 data-cat_id="<?php echo $child_cat; ?>"
                                                                                 title="Double Click to Edit<?php load_message('DBL_CLICK_OPEN'); ?>"><?php echo $child_name; ?></div><?php } ?>
                                                                    </li>
                                                                    <?php
																	
                                                                }

                                                                //$result1 = get_child_name($parent_id);
                                                                ?>
                                                            </ul>
                                                        </li>

                                                        <?php
                                                    } else {

                                                        ?>
                                                        <li id="id<?php echo $i; ?>.<?php echo $j; ?>">
                                                            <?php if (permission_check('category/category_edit')) { ?>
                                                                <div class="tree-item"
                                                                     data-cat_id="<?php echo $chcategory_id; ?>"
                                                                     title="Double Click to Edit<?php load_message('DBL_CLICK_OPEN'); ?>"><?php echo $chcategory_name; ?>

                                                                </div><?php } ?></li>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                    $j++;
                                                } else {
                                                    ?>

                                                    <?php
                                                }

                                            }

                                            ?>
                                        </ul>
                                    </li>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div><!-- col-sm-6 -->
    </div>

    <!--------------Modal for Category edit Start----------------->
    <div class="modal fade" id="modal-category">
        <div class="modal-dialog" style="width:660px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('CATEGORY_DETAILS'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="category_edit"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    </div>
    <!--    Modal for Role Edit end here -->
    <!-- /.box -->
</section>
<!-- /.content -->

<style>
    /*Fance tree for category view */
    .cat-box {
        min-height: 450px;
    }

    ul.fancytree-container {
        border: none !important;
        outline: none;
    }

    li .fancytree-node:hover .tree-item .btn-action {
        display: inline-block;

    }

    .btn-cat-edit {
        display: none;
        font-size: 6px;
    }

    .btn-action {
        display: none;
    }
</style>

<script>
    $(document).ready(function () {

        $(document).on('dblclick', '.tree-item', function () {

            cat_id = $(this).data('cat_id');

            //load modal
            $('#modal-category').modal('show');

            category_edit(cat_id);


        });
    });
</script>