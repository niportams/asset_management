<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/jsonmap.js"); ?>
<?php
foreach ($category_details as $cdetails) {
    $category_id = $cdetails->category_id;
    $parent_id_update = $cdetails->parent_id;
    $category_name = $cdetails->category_name;
    $registration_type = $cdetails->registration_type;
	$lifetime = $cdetails->lifetime;
	$cat_prefix = $cdetails->cat_prefix;
    $category_status = $cdetails->category_status;
}

?>
<!-- Main content -->
<form id="category_update_form">
    <div class="box-body">
        <div class="col-md-12">
            <div class="form-group" align="justify"></div>
            <div class="form-group">
                <label for="exampleInputName"><?php echo load_message('CATEGORY'); ?><span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="category_name" name="category_name"
                       value="<?php echo $category_name; ?>">
            </div>
            <div class="form-group" align="justify">
                <label><?php echo load_message('PARENT_CATEGORY'); ?></label>
                <select name="parent_id" id="parent_id" class="form-control select2">
                    <?php
                    if ($parent_id_update == NULL) {
                        ?>
                        <option value="0"
                                selected="selected"><?php echo load_message('SELECT_PARENT_CATEGORY'); ?></option>
                        <?php
                    }

                    foreach ($category_list as $clist) {
                        $category_id_db = $clist->category_id;
                        $parent_id = $clist->parent_id;
                        $category_name_db = $clist->category_name;

                        if ($parent_id_update == $category_id_db) {
                            ?>
                            <option value="<?php echo $parent_id_update; ?>"
                                    selected="selected"><?php echo $category_name_db; ?></option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $category_id_db; ?>"><?php echo $category_name_db; ?></option>
                            <?php
                        }

                    }
                    ?>

                </select>
            </div>
            <div class="form-group" align="justify">
                <label><?php echo load_message('REGISTRATION_TYPE'); ?></label>

                <select name="registration_type" id="registration_type" class="form-control select2">
                    <?php
                    if ($registration_type == 'single') {
                        ?>
                        <option value="single" selected="selected">Single</option>
                        <option value="bulk">Bulk</option>
                        <?php
                    } else {
                        ?>
                        <option value="single">Single</option>
                        <option value="bulk" selected="selected">Bulk</option>
                        <?php
                    }
                    ?>
                </select>

            </div>
              <div class="form-group">
                <label for="exampleInputName"><?php echo load_message('CATEGORY_LIFETIME'); ?></label>
                <input type="text" class="form-control number-only" id="lifetime" name="lifetime"
                       value="<?php echo $lifetime; ?>" title="Number of Years">
               
            </div>
            
            <div class="form-group" align="justify">
            	<label class="control-label"><?php echo load_message('CATEGORY_PREFIX'); ?> <span class="text-danger">*</span></label>
            	<input type="text" class="form-control" id="cat_prefix" name="cat_prefix" value="<?php echo $cat_prefix; ?>">      
            </div>
            <div class="form-group">
                <label for="exampleInputName"><?php echo load_message('STATUS'); ?></label>
                <select name="category_status" id="category_status" class="form-control select2">
                    <?php
                    if ($category_status == 1) {
                        ?>
                        <option value="1" selected="selected">Active</option>
                        <option value="0">Inactive</option>
                        <?php
                    } else {
                        ?>
                        <option value="1">Active</option>
                        <option value="0" selected="selected">Inactive</option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <div align="center">
                    <!--<button type="submit" class="btn btn-primary" onclick="return update_category_submit('<?php //echo $category_id;?>');">
                      Update Category</button>-->
                    <button type="submit" class="btn btn-primary"
                            onClick="handle_form('category_update_form', category, 'category/category_edit','<?php echo $category_id ?>','manual')">
                        <i class="fa fa-pencil-square-o"></i>
                        &nbsp; <?php echo load_message('UPDATE_CATEGORY_DETAILS'); ?>
                    </button>
                </div>
            </div>
        </div>
</form>
<!-- /.box-body -->
<script>
    $('.select2').select2({width:'100%'});
</script>