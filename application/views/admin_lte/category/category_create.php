<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<div class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header">
            <!--          <h3 class="box-title">Asset Category Create</h3>-->
            <!---->
            <!--          <div class="box-tools pull-right">-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
            <!--              <i class="fa fa-minus"></i></button>-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
            <!--              <i class="fa fa-times"></i></button>-->
            <!--          </div>-->
        </div>
        <form id="category_form">
            <div class="box-body" id="category_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="category_status" name="category_status" value="1">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('CATEGORY'); ?><span
                                    class="text-danger">*</span></label>

                        <input type="text" class="form-control" id="category_name" name="category_name"
                               placeholder="Category Name">

                    </div>
                    <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('PARENT_CATEGORY'); ?></label>
                        <select name="parent_id" id="parent_id" class="form-control select2">
                            <option value="0" selected="selected">
							<?php echo load_message('SELECT_PARENT_CATEGORY'); ?></option>
                            <?php
                            foreach ($category_list as $clist) {
                                $category_id = $clist->category_id;
                                $category_name = $clist->category_name;
                                ?>
                                <option value="<?php echo $category_id; ?>"><?php echo $category_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('REGISTRATION_TYPE'); ?></label>
                        <select name="registration_type" id="registration_type" class="form-control select2">
                        <option value="single" selected="selected">Single</option>
                        <option value="bulk">Bulk</option>
                        </select>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('CATEGORY_LIFETIME'); ?>(Years)</label>
                         <input type="text" class="form-control number-only" id="lifetime" name="lifetime" placeholder="Category Lifetime">      
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('CATEGORY_PREFIX'); ?>
                        <span class="text-danger">*</span>
                        </label>
                         <input type="text" class="form-control" id="cat_prefix" name="cat_prefix" placeholder="Category Prefix">      
                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <div align="right">
                    <!-- <button type="submit" class="btn btn-primary" onClick="return category_validation();">Submit</button>-->
                    <button type="submit" class="btn btn-success btn-lg"
                            onClick="handle_form('category_form', category, 'ajax/category_submit',0,'manual')"><i
                                class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_CATEGORY'); ?>
                    </button>
                </div>
            </div>
            <!-- /.box-footer-->
        </form>
    </div>
    <!-- /.box -->
</div>
</section>
<!-- /.content -->