<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-success">
        <!-- <div class="box-header">
           <h3 class="box-title">Data Table of Asset Allocation</h3>
         </div>-->
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="204">File Name</th>
                    <th width="60"></th>
                </tr>
                </thead>
                <tbody>
                <?php
				$i=1;
				$dir = "db_backup/"; // Path to folder
				foreach ($db_list as $k)
				{?>
                <tr>
                <td width="10"><?php echo $i;?></td>
                <td width="204"><a href="<?php echo base_url();?>db_backup/<?php echo $k;?>" title="Click to download"><?php echo $k;?></a></td>
                <td width="60"><button title="Delete Asset" class="btn btn-danger btn-xs"
                 onclick="return backup_delete('<?php echo $k; ?>');">
                 <i class="fa fa-trash fa-x" aria-hidden="true"></i></button></td>
                 </tr>
								
				<?php 
				$i++;
				}
				
				?> 
                        
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->

        <!-- Modal for Privilege details-->
        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>

                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        
           <!--  Show Attachment of this allocation    -->
        <div class="modal fade" id="modal-attachment">
            <div class="modal-dialog" style="width:550px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ATTACHED_FILES') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="attached_files"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
      </div>   
    <!--    Modal end here -->
    <div class="clearfix"></div>
    <!-- Modal for Role Edit end here -->
    <!-- /.box -->
</section>
<!-- /.content -->