<section class="content">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <!--<h3 class="box-title">Collapsible Accordion</h3>-->
                Import result
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div style="overflow: scroll; max-height: 400px;">
                    <strong>Total Record Added: <?= $total_added; ?></strong>
                    <table class="table">
                        <?php
                        //var_dump($get_all_new_asset);

                        if (is_array($get_all_new_asset)) {

                            foreach ($get_all_new_asset[0] as $key => $val) {
                                $rows[] = $key;
                            }
                        }
                        ?>

                        <thead>
                            <tr>
                                <?php foreach ($rows as $row): ?>
                                    <th><?php echo $row; ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php foreach ($get_all_new_asset as $asset): ?>
                            <tr>
                               <?php
                                    foreach($rows as $row){
                                        echo '<td>',$asset->$row,'</td>';
                                    }
                                ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>