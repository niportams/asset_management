<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= load_message('SITE_TITLE'); ?></title>
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>theme/dist/img/logo.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <?php load_css("bower_components/bootstrap/dist/css/bootstrap.min.css"); ?>
    <!-- Font Awesome -->
    <?php load_css("bower_components/font-awesome/css/font-awesome.min.css"); ?>
    <!-- Ionicons -->
    <?php load_css("bower_components/Ionicons/css/ionicons.min.css"); ?>
    <!-- Theme style -->
    <?php load_css("dist/css/AdminLTE.min.css"); ?>
    <!-- iCheck -->
    <?php load_css("plugins/iCheck/square/blue.css"); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .container {

        }

        .login-page {
            background-image: url("../theme/dist/img/bg-niport.jpg");
        }

        .login-box {
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0px 0px 1px 1px #C6C9E6;
            background-color: #3c8dbc;
            margin-top: 5px;
            margin-bottom: 100px;
        }

        .login-box-body {
            background-color: #e6f6ff;
        }

        .login-logo {
            margin-bottom: 10px;
        }

        .login-logo a {
            color: #FFF;
            font-size: 20px;
        }

        /*.logo-area div{
            position:absolute;
            font-family:Oswald,sans-serif;
        }*/

        .ministry_title {
            top: 55px;
            color: #FFF;
            font-size: 15px;
            margin-left: 15px;
        }

        .site_title {
            font-size: 26px;
            color: #FFF;
            margin-top: 5px;
            margin-left: 15px;
        }

        .niport-logo {
            right: 0px;
        }

        .ams-title {
            text-align: center;
            margin: 50px auto;
            color: #3d9397;
            text-shadow: 1px 2px 1px rgba(150, 150, 150, 0.39);
        }

        .footer {
            position: fixed;
            bottom: 0px;
            padding: 10px;
            background-color: #000;
            width: 100%;
            left: 0px;
            color: #ffffff;
        }

        .banner-login {
            background-color: #3c8dbc;
            overflow: auto;
            padding: 10px;
        }

        .area {
            box-shadow: 0px 0px 2px 2px #CCC;
        }

        /*.alert-danger{*/
        /*position:absolute;*/
        /*right:0px;*/
        /*width:350px;*/
        /*}*/

        .skin-green-light .banner-login, .skin-green-light .login-box {
            background-color: #00a65a;
        }

        .skin-green-light .login-box-body {
            background-color: #FFF;
        }

        .skin-green-light .btn-login {
            background-color: #00a65a;
            border-color: #008d4c;
            color: #FFF;
        }

    </style>
</head>
<body class="hold-transition <?= $this->config->item('theme-skin'); ?> login-page">


<div class="row banner-login">
    <div class="container ">


        <div class="logo-area hidden-sm hidden-xs ">
            <div class="col-xs-1"><img src="<?php echo base_url(); ?>theme/dist/img/logo/bd-government-logo-128.png"
                                       width="90px"></div>
            <div class="col-xs-10">
                <div class="site_title" style="color: #FFF;"><?= load_message('NIPORT'); ?></div>
                <div class="ministry_title">Medical Education and Family Welfare Division<br><?= load_message('MINISTRY'); ?></div>
            </div>

            <div class="col-xs-1"><img src="<?php echo base_url(); ?>theme/dist/img/logo/niport-128.png" width="90px">
            </div>
        </div>

        <div class="logo-area visible-sm visible-xs">
            <div class="col-xs-3"><img src="<?php echo base_url(); ?>theme/dist/img/logo/bd-government-logo-128.png"
                                       width="90px"></div>
            <div class="col-xs-5">
                <div class="site_title"><?= load_message('NIPORT_SHORT'); ?></div>
                <div class="site_title"></div>
                <div class="ministry_title"><?= load_message('MINISTRY_SHORT'); ?></div>
            </div>
            <div class="col-xs-3">
                <div class="niport-logo"><img src="<?php echo base_url(); ?>theme/dist/img/logo/niport-128.png"
                                              width="90px"></div>
            </div>
        </div>
    </div>

</div>
<?php
if ($this->config->item('maintenance_mode') == TRUE) { ?>
    <div class="alert alert-danger"> <?= load_message('MAINTAINENCE'); ?> </div>

<?php }
?>

<div class="row">

    <div class="container">
        <div style="margin-top: 20px;margin-bottom:10px;  font-size: 20px">
            <a href="<?php echo base_url();?>"><i class="fa fa-arrow-left"></i> Back To Home</a>
        </div>
        <div class="row">
            <?php foreach ($all_office as $office){?>
                <div class="col-md-4">
                    <div class="box box-default box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $office->office_name?></h3>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <b>Address: </b> <?php echo $office->office_address?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            <?php }
            ?>
        </div>
        <div class="clear-fix"></div>
        <br><br>
    </div><!-- container -->
</div>

<div class="footer">
    <div class="pull-right hidden-xs">
        <b>Technical Support: </b> MaMoni  HSS
    </div>

    <strong>Copyright &copy; 2017-<?php echo date('Y'); ?>&nbsp; <a
                href="<?php echo base_url(); ?>">NIPORT</a>.</strong> All rights
    reserved.

    <div align="center" style="display:inline" class="hidden-xs">
        <a href="<?php echo base_url()?>page/contact"> Our Centers </a>
    </div>
</div>

<!-- jQuery 3 -->
<?php load_js("bower_components/jquery/dist/jquery.min.js"); ?>
<!-- Bootstrap 3.3.7 -->
<?php load_js("bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>
<?php load_js("dist/js/jquery.validate.js"); ?>
<!-- iCheck -->
<?php load_js("plugins/iCheck/icheck.min.js"); ?>
<?php load_js("dist/js/sweetalert.min.js"); ?>
<?php load_js("dist/js/custom_msg.js"); ?>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });

    $("form").validate();
</script>

<script>
    $(document).ready(function (e) {
        $("#forgot").click(function () {
            sci_alert("<?=load_message('FORGOT_PASSWORD_MSG');?>", 'Forgot Password', 'info');
        });
    });
</script>


<style type="text/css">
    .error {
        color: red;
    }
</style>

</body>
</html>
