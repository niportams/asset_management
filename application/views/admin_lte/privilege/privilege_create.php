<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<div class="content">

    <!-- Default box -->
    <div class="box box-success ">
        <form id="privilege_form">
            <div class="box-header">
                <!--            <h3 class="box-title">User Privilege Create</h3>-->
                <!---->
                <!--            <div class="box-tools pull-right ">-->
                <!--              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
                <!--                <i class="fa fa-minus"></i></button>-->
                <!--              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
                <!--                <i class="fa fa-times"></i></button>-->
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputName"
                               class="control-label"><?php echo load_message('PRIVILEGE_NAME'); ?><span
                                    class="text-danger">*</span></label>

                        <input type="text" class="form-control" id="privilege_name" name="privilege"
                               placeholder="Privilege Name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" align="justify">
                        <label class="control-label"><?php echo load_message('STATUS'); ?><span
                                    class="text-danger">*</span></label>

                        <select name="privilege_status" id="privilege_status" name="privilege_status"
                                class="form-control">
                            <option value="1" selected="selected">Active</option>
                            <option value="0">Inactive</option>
                        </select>


                    </div>

                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <div align="right">
                    <button type="submit" class="btn btn-success btn-lg"
                            onClick="handle_form('privilege_form', privilege_table, 'ajax/privilege_submit',0);"
                            id="privilege_add"><i class="fa fa-plus"></i>
                        &nbsp; <?php echo load_message('ADD_NEW_PRIVILEGE'); ?>
                    </button>
                </div>
                <!--    <div align="center"><button type="submit" class="btn btn-primary" onClick="return privilege_validation();">Submit</button></div>-->
            </div>
        </form>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
</div>

</section>
<!-- /.content -->