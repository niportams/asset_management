<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<?php load_js("dist/js/office.js"); ?>
<?php
foreach ($privilege_details as $pdetails) {
    $privilege_id = $pdetails->privilege_id;
    $privilege_name = $pdetails->privilege;
    $privilege_status = $pdetails->privilege_status;
}

?>
<!-- Main content -->
<div class="box-body">
    <div class="col-md-12">

        <div class="form-group">
            <label for="exampleInputName"><?php echo load_message('PRIVILEGE_NAME'); ?><span
                        class="text-danger">*</span></label>
            <input type="text" class="form-control" id="privilege_name" value="<?php echo $privilege_name; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputName"><?php echo load_message('STATUS'); ?></label>
            <select name="privilege_status" id="privilege_status" class="form-control">
                <?php
                if ($privilege_status == 1) {
                    ?>
                    <option value="1" selected="selected">Active</option>
                    <option value="0">Inactive</option>
                    <?php
                } else {
                    ?>
                    <option value="1">Active</option>
                    <option value="0" selected="selected">Inactive</option>
                    <?php
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <div align="center">
                <!--<button type="submit" class="btn btn-danger" onclick="return update_privilege_submit('<?php //echo $privilege_id;?>');">
                      Update Privilege</button>-->
                <button type="submit" class="btn btn-primary"
                        onClick="return form_validation('privilege/privilege_edit',privilege_table,'example1','<?php echo $privilege_id; ?>');"
                        id="privilege_update">
                    <i class="fa fa-pencil-square-o"></i> &nbsp; <?php echo load_message('UPDATE_PRIVILEGE'); ?>
                </button>
            </div>
        </div>
    </div>

    <!-- /.box-body -->
     