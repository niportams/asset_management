<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table of Role Privilege</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="100"><?php echo load_message('PRIVILEGE'); ?></th>
                    <th width="150"><?php echo load_message('STATUS'); ?></th>
                    <th width="124"><?php echo load_message('CREATE_DATE'); ?></th>
                    <th width="150"><?php echo load_message('UPDATE_DATE'); ?></th>
                    <th width="89"></th>
                </tr>
                </thead>
                <tbody>

                <?php
                $i = 1;
                foreach ($privilege_list as $plist) {
                    $privilege_id = $plist->privilege_id;
                    $privilage_name = $plist->privilege;
                    $privilege_status = $plist->privilege_status;
                    $create_date = $plist->create_date;
                    $update_date = $plist->update_date;

                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $privilage_name; ?></td>
                        <td><?php if ($privilege_status == 1) {
                                echo "Active";
                            } else {
                                echo "Inactive";
                            } ?></td>
                        <td><?php echo get_format_date($create_date); ?></td>
                        <td><?php if($update_date<> NULL){echo get_format_date($update_date); } ?></td>
                        <td>
                            <?php if (permission_check('privilege/privilege_edit')) { ?>
                                <a href="#" title="Edit Profile" class="btn btn-primary btn-xs" data-toggle="modal"
                                   data-target="#modal-privilege"
                                   onclick="privilege_edit('<?php echo $privilege_id; ?>');"><i
                                            class="fa fa-pencil fa-x"></i></a>
                            <?php }
                            if (permission_check('privilege/privilege_delete')) { ?>
                                &nbsp;

                                <a href="#" title="Delete Profile" class="btn btn-danger btn-xs"
                                   onclick="return privilege_delete('<?php echo $privilege_id; ?>');"><i
                                            class="fa fa-trash fa-x" aria-hidden="true"></i>
                                </a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!--------------Modal for Role edit Start----------------->
    <div class="modal fade" id="modal-privilege">
        <div class="modal-dialog" style="width:560px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('USER_PRIVILEGE_DETAILS'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="privilege_edit"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!--    Modal for Role Edit end here -->
</section>
<!-- /.content -->