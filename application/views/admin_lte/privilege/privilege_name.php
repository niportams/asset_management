<div class="form-group" align="justify">
    <?php
    //Columns must be a factor of 12 (1,2,3,4,6,12)
    $numOfCols = 2;
    $rowCount = 0;
    $bootstrapColWidth = 12 / $numOfCols;
    ?>
    <div class="row">
        <?php
        $i = 1;
        foreach ($privilege_name as $row) {

            $privilege = $row->privilege;
            ?>
            <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                <div class="item">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <?php echo $privilege; ?>
                </div>
            </div>
            <?php
            $rowCount++;
            if ($rowCount % $numOfCols == 0) echo '</div><div class="row">';
        }
        ?>
    </div>
</div>
                 
             