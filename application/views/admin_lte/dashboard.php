<!-- Small boxes (Stat box) -->

<?php if (get_user_role_name() == "USER" || get_user_role_name() == "VISITOR") { ?>
    <div class="row">
        <div class="col-md-12">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php sci_total_requested_requisition(); ?></h3>

                    <p><?php echo load_message('NUMBER_OF_ASSET_REQUESTED'); ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-list-alt"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3><?php sci_total_request_approve(); ?></h3>

                    <p><?php echo load_message('NUMBER_OF_REQUESTED_ASSET_APPROVED'); ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-check"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php sci_total_request_declined(); ?></h3>

                    <p><?php echo load_message('NUMBER_OF_REQUESTED_ASSET_DECLINED'); ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-close"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3><?php sci_total_personal_allocate_count(); ?></h3>

                    <p><?php echo load_message('NUMBER_OF_ASSET_ALLOCATED'); ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-send"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo load_message('ALLOCATED_ITEM_TO_ME'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1"
                           class="table table-bordered table-striped table-hover dataTable-full-functional">
                        <thead>
                        <tr>
                            <th width="10">#</th>
                            <th width="204"><?php echo load_message('ASSET_TITLE'); ?></th>
                            <th width="100"><?php echo load_message('CATEGORY'); ?></th>
                            <th width="100"><?php echo load_message('ASSET_ID_L'); ?></th>
                            <th width="145"><?php echo load_message('ALLOCATED_DATE'); ?></th>
                            <th width="124"><?php echo load_message('ALLOCATED_BY'); ?></th>
                            <th width="120"><?php echo load_message('OFFICE_TO'); ?></th>
                            <th width="90"><?php echo load_message('REQUISITION_NUMBER'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 1;
                        if ($alloc_list != null) {
                            foreach ($alloc_list as $aclist) {
                                $allocate_id = $aclist->allocate_id;
                                $asset_id = $aclist->asset_id;
                                $encrypt_asset = base64_encode($asset_id);
                                $asset_label_id = $aclist->asset_label_id;
                                $category_id = $aclist->category_id;
                                $requisition_no = $aclist->requisition_no;
                                $allocated_by = $aclist->allocated_by;
                                $allocate_to = $aclist->allocate_to;
                                $allocate_office = $aclist->allocate_office;
                                $create_date = $aclist->create_date;


                                $create_m = date("M", strtotime($create_date));
                                $create_d = date("d", strtotime($create_date));
                                $create_y = date("Y", strtotime($create_date));

                                ?>
                                <tr>
                                    <td align="center"><?php echo $i; ?></td>
                                    <td><a href="#" title="Details" data-toggle="modal"
                                           data-target="#modal-asset-details"
                                           onclick="return asset_details('<?php echo $encrypt_asset; ?>');"><?php echo get_asset_name($asset_id); ?></a>
                                    </td>
                                    <td><?php echo get_category_name($category_id); ?></td>
                                    <td><?php echo $asset_label_id; ?></td>
                                    <td><?php echo $create_d; ?>-<?php echo $create_m; ?>-<?php echo $create_y; ?></td>
                                    <td><?php echo get_user_fullname($allocated_by); ?></td>
                                    <td><?php echo get_office_name($allocate_office); ?></td>
                                    <td align="center"><?php echo $requisition_no; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- end of small box -->
<?php if (get_user_role_name() == "SYSTEM_ADMIN" || get_user_role_name() == "NIPORT_ADMIN" || get_user_role_name() == "NIPORT_STORE_KEEPER" || get_user_role_name() == "RTC_ADMIN" || get_user_role_name() == "RTC_STORE_KEEPER" || get_user_role_name() =="DIRECTOR_PANEL" || get_user_role_name() =="DEVELOPER" || get_user_role_name() =="INSPECTOR") { ?>
   <?php $this->load->view('admin_lte/dashboard/at_a_glance');?>
    <?php $this->load->view('admin_lte/dashboard/widget');?>
<?php } ?>
<!-- end of small box -->

<!-- Main row -->
<div class="row">
    <div class="col-md-12">
    <!-- Left col -->
    <?php if (get_user_role_name() == "SYSTEM_ADMIN" || get_user_role_name() == "NIPORT_ADMIN" || get_user_role_name() == "NIPORT_STORE_KEEPER" || get_user_role_name() == "RTC_ADMIN" || get_user_role_name() == "RTC_STORE_KEEPER" || get_user_role_name() =="DEVELOPER" || get_user_role_name() =="INSPECTOR") { ?>
        <section class="col-md-8 connectedSortable">
            <div>
                <?php Capital_V_Reg(); ?>
            </div>
        </section>
    <?php } ?>
    <?php if (get_user_role_name() == "RTC_ADMIN" || get_user_role_name() == "RTC_STORE_KEEPER") { ?>
        <section class="col-md-4 connectedSortable" id="">
            <?php load_google_map(get_geo_loc($this->session->userdata('user_center')), $type = 'roadmap', $zoom = 10); ?>
        </section>
    <?php } ?>


    <?php if (get_user_role_name() == "SYSTEM_ADMIN" || get_user_role_name() == "NIPORT_ADMIN" || get_user_role_name() == "NIPORT_STORE_KEEPER" || get_user_role_name() =="DEVELOPER") { ?>
        <section class="col-md-4 connectedSortable" id="rtc-map"></section>
    <?php } ?>
    </div>

</div>
<!-- /.row (main row) -->


<script>
    $(document).ready(function (e) {
        $("#rtc-map").html('<iframe src="http://www.google.com/maps/d/embed?mid=1q1mlSZYIiy53dn4zQeioX2kc884&z=6" width="100%" height="500"></iframe>');
    });
    $(document).ready(function (e) {
        //get_bar_chart('capitalVsRegiistration',chart);
    });
</script>