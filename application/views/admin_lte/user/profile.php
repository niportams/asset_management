<!--<section class="content">-->
    <div id="loadit">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <form id="uploadForm" action="<?php echo base_url(); ?>user/pic_upload" method="post">
                            <?php
                            if (@getimagesize(base_url() . 'theme/dist/img/user/' . $this->session->userdata('user_db_id') . '.jpg')) { ?>
                                <img class="profile-user-img img-responsive img-circle"
                                     src="<?php echo base_url(); ?>theme/dist/img/user/<?php echo (int)$this->session->userdata('user_db_id'); ?>.jpg"
                                     alt="User profile picture" title="Change Profile Picture"
                                     onclick="clickAnother();">
                            <?php } else { ?>
                                <img class="profile-user-img img-responsive img-circle"
                                     src="<?php echo base_url(); ?>theme/dist/img/user2-160x160.jpg"
                                     alt="User profile picture" title="Change Profile Picture"
                                     onclick="clickAnother();">
                            <?php }
                            ?>
                            <input name="userImage" id="userImage" type="file" class="inputFile"
                                   accept="image/x-png,image/gif,image/jpeg" onChange="showPreview(this);"/>
                            <input type="submit" hidden value="Upload Photo" class="btnSubmit" id="btnPicSubmit"/>

                        </form>


                        <h3 class="profile-username text-center"><?php get_user_fullname($this->session->userdata('user_db_id')); ?></h3>

                        <p class="text-muted text-center"><?php get_user_designation($this->session->userdata('user_db_id')); ?></p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b><?php echo load_message('OFFICE'); ?></b> <span
                                        class="pull-right"><?php get_office_name($this->session->userdata('user_center')); ?></span>
                            </li>

                        </ul>
                        <a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal-user-edit"
                           onclick="user_edit('<?php echo $this->session->userdata('user_db_id'); ?>');">
                            <b><?php echo load_message('EDIT_PROFILE'); ?></b>
                        </a>
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal"
                           data-target="#modal-change-password"
                           onclick="password_change('<?php echo $this->session->userdata('user_db_id'); ?>');">
                            <b><?php echo load_message('CHANGE_PASSWORD'); ?></b>
                        </a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo load_message('ABOUT_ME'); ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-phone margin-r-5"></i><?php echo load_message('PHONE'); ?>: </strong>

                        <span class="text-muted">
                            <?php get_user_user_phone($this->session->userdata('user_db_id')); ?>
                        </span>

                        <hr>
                        <strong><i class="fa fa-mobile margin-r-5"></i><?php echo load_message('MOBILE'); ?>: </strong>

                        <span class="text-muted">
                            <?php get_user_user_mobile($this->session->userdata('user_db_id')); ?>
                        </span>
                        <hr>
                        <strong><i class="fa fa-envelope margin-r-5"></i><?php echo load_message('EMAIL'); ?>: </strong>

                        <span class="text-muted">
                            <?php $this->session->userdata('user_email'); ?>
                        </span>
                        <hr>

                        <strong><i class="fa fa-map-marker margin-r-5"></i> <?php echo load_message('LOCATION'); ?>
                        </strong>

                        <p class="text-muted"><?php get_user_location($this->session->userdata('user_db_id')); ?></p>

                        <hr>

                        <strong><i class="fa fa-pencil margin-r-5"></i> <?php echo load_message('ROLE'); ?>: </strong>

                        <span class="label label-success"><?php echo get_user_role_name(); ?></span>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo load_message('ALLOCATED_ITEM_TO_ME'); ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example1"
                               class="table table-bordered table-striped table-hover dataTable-full-functional">
                            <thead>
                            <tr>
                                <th width="10">#</th>
                                <th width="204"><?php echo load_message('ASSET_TITLE'); ?></th>
                                <th width="100"><?php echo load_message('CATEGORY'); ?></th>
                                <th width="100"><?php echo load_message('ASSET_ID_L'); ?></th>
                                <th width="145"><?php echo load_message('ALLOCATED_DATE'); ?></th>
                                <th width="124"><?php echo load_message('ALLOCATED_BY'); ?></th>
                                <th width="120"><?php echo load_message('OFFICE_TO'); ?></th>
                                <th width="90"><?php echo load_message('REQUISITION_NUMBER'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            if ($alloc_list != null) {
                                foreach ($alloc_list as $aclist) {
                                    $allocate_id = $aclist->allocate_id;
                                    $asset_id = $aclist->asset_id;
                                    $encrypt_asset = base64_encode($asset_id);
                                    $asset_label_id = $aclist->asset_label_id;
                                    $category_id = $aclist->category_id;
                                    $requisition_no = $aclist->requisition_no;
                                    $allocated_by = $aclist->allocated_by;
                                    $allocate_to = $aclist->allocate_to;
                                    $allocate_office = $aclist->allocate_office;
                                    $create_date = $aclist->create_date;


                                    $create_m = date("M", strtotime($create_date));
                                    $create_d = date("d", strtotime($create_date));
                                    $create_y = date("Y", strtotime($create_date));

                                    ?>
                                    <tr>
                                        <td align="center"><?php echo $i; ?></td>
                                        <td><a href="#" title="Details" data-toggle="modal"
                                               data-target="#modal-asset-details"
                                               onclick="return asset_details('<?php echo $encrypt_asset; ?>');"><?php echo get_asset_name($asset_id); ?></a>
                                        </td>
                                        <td><?php echo get_category_name($category_id); ?></td>
                                        <td><?php echo $asset_label_id; ?></td>
                                        <td><?php echo $create_d; ?>-<?php echo $create_m; ?>
                                            -<?php echo $create_y; ?></td>
                                        <td><?php echo get_user_fullname($allocated_by); ?></td>
                                        <td><?php echo get_office_name($allocate_office); ?></td>
                                        <td align="center"><?php echo $requisition_no; ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo load_message('TRANSFERED_ITEM_TO_ME'); ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example3"
                               class="table table-bordered table-striped table-hover dataTable-full-functional">
                            <thead>
                            <tr>
                                <th width="204"><?php echo load_message('ASSET_TITLE'); ?></th>
                                <th width="100"><?php echo load_message('CATEGORY'); ?></th>
                                <th width="100"><?php echo load_message('ASSET_ID_L'); ?></th>
                                <th width="145"><?php echo load_message('TRANSFER_DATE'); ?></th>
                                <th width="124"><?php echo load_message('TRANSFER_BY'); ?></th>
                                <th width="120"><?php echo load_message('OFFICE_TO'); ?></th>
                                <th width="90"><?php echo load_message('REQUISITION_NUMBER'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            if ($transfer_list != null) {
                                foreach ($transfer_list as $tlist) {
                                    $asset_id = $tlist->asset_id;
                                    $encrypt_asset = base64_encode($asset_id);
                                    $asset_label_id = $tlist->asset_label_id;
                                    $category_id = $tlist->category_id;
                                    $requisition_no = $tlist->requisition_no;
                                    $transfer_by = $tlist->transfer_by;
                                    $transfer_to = $tlist->transfer_to;
                                    $transfer_office = $tlist->transfer_office;
                                    $create_date = $tlist->create_date;

                                    $create_m = date("M", strtotime($create_date));
                                    $create_d = date("d", strtotime($create_date));
                                    $create_y = date("Y", strtotime($create_date));
                                    ?>
                                    <tr>
                                        <td align="center"><?php echo $i; ?></td>
                                        <td><a href="#" title="Details" data-toggle="modal"
                                               data-target="#modal-asset-details"
                                               onclick="return asset_details('<?php echo $encrypt_asset; ?>');"><?php echo get_asset_name($asset_id); ?></a>
                                        </td>
                                        <td><?php echo get_category_name($category_id); ?></td>
                                        <td><?php echo $asset_label_id; ?></td>
                                        <td><?php echo $create_d; ?>-<?php echo $create_m; ?>
                                            -<?php echo $create_y; ?></td>
                                        <td><?php echo get_user_fullname($transfer_by); ?></td>
                                        <td><?php echo get_office_name($transfer_office); ?></td>
                                        <td align="center"><?php echo $requisition_no; ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!--   Modal for Asset details-->
    <div class="modal fade" id="modal-asset-details">
        <div class="modal-dialog" style="width:850px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="asset_details"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!--    Modal end here -->
    <div class="clearfix"></div>
    <!--    Modal for Role Edit end here -->


    <!--   Modal for User details edit-->
    <div class="modal fade" id="modal-user-edit">
        <div class="modal-dialog" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('UPDATE_USER_DETAILS'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="user_edit"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!--    Modal end here -->


    <!--   Modal for change password-->
    <div class="modal fade" id="modal-change-password">
        <div class="modal-dialog" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('CHANGE_PASSWORD'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="change_pass"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!--    Modal end here -->
<!--</section>-->
<!-- /.content -->

<style>
    #userImage {
        position: absolute;
        opacity: 0.0;
        margin-top: -100px;
        margin-left: 80px;
        width: 48px;
        height: 48px;
    }

</style>
<script type="text/javascript">
    function clickAnother() {
        $("#userImage").click();
    }

    var target_src = '';

    function showPreview(objFileInput) {
        if (objFileInput.files[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                target_src = e.target.result;
                $("#modalLoading").modal({backdrop: 'static', keyboard: false});

            }
            fileReader.readAsDataURL(objFileInput.files[0]);
            $("#btnPicSubmit").click();
        }
    }

    $(document).ready(function (e) {
        $("#uploadForm").on('submit', (function (e) {
            e.preventDefault();
            $.ajax({
                url: "<?php echo base_url();?>user/pic_upload",
                type: "POST",
                data: new FormData(this),
                beforeSend: function () {
                },
                contentType: false,
                processData: false,
                success: function (data) {

                    if (data.type == 'error') {
                        sci_alert($(data.error).text(), "<?php echo $this->lang->line('USER_IMAGE_UPDATE_ERROR');?>", 'error');
                    } else {
                        $(".profile-user-img").attr("src", target_src);
                        sci_alert('', "<?php echo $this->lang->line('USER_IMAGE_UPDATE');?>", 'success');

                    }


                    $("#modalLoading").modal('hide');
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }));
    });


</script>