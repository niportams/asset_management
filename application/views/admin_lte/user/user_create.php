<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header">

        </div>
        <div class="box-body" id="registration_form">
            <div class="">
                <div class="col-md-4" style="background: #eee;">
                    <div class=" box-header">
                        <p align="center"><b><?php echo load_message('USER_CREDENTIAL'); ?></b></p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputusername" class="control-label"><?php echo load_message('USER_NAME'); ?>
                            <span
                                    class="text-danger">*</span></label>

                        <input type="text" class="form-control" id="user_name"
                               placeholder="<?php echo load_message('USER_NAME'); ?>">


                    </div>

                    <div class="form-group">
                        <label for="exampleInputName"
                               class="control-label"><?php echo load_message('EMPLOYEE_ID'); ?></label>

                        <input type="text" class="form-control" id="employee_id"
                               placeholder="<?php echo load_message('EMPLOYEE_ID'); ?>">


                    </div>

                    <div class="form-group">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('NICE_NAME'); ?>
                            <span
                                    class="text-danger">*</span></label>

                        <input type="text" class="form-control" id="nice_name"
                               placeholder="<?php echo load_message('NICE_NAME'); ?>">


                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="control-label"><?php echo load_message('PASSWORD'); ?>
                            <span
                                    class="text-danger">*</span></label>

                        <input type="password" class="form-control" id="user_pass"
                               placeholder="<?php echo load_message('PASSWORD'); ?>">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"
                               class="control-label"><?php echo load_message('EMAIL'); ?></label>

                        <input type="text" class="form-control" id="user_email"
                               placeholder="<?php echo load_message('EMAIL'); ?>">

                    </div>

                    <div class="form-group">

                        <?php if($this->session->userdata('user_center')!=1){ ;?>
                            <input type="hidden" class="form-control" id="posting_center" value="<?php echo $this->session->userdata('user_center');?>">
                        <?php } else{?>

                        <label class="control-label"><?php echo load_message('OFFICE'); ?> <span
                                        class="text-danger">*</span></label>
                        <select class="form-control select2" id="posting_center">
                            <option value=""><?php echo load_message('SELECT_OFFICE'); ?></option>
                            <?php
                            //var_dump($role_list);
                            foreach ($office_list as $olist) {
                                $office_id = $olist->office_id;
                                $office_name = $olist->office_name;
                                ?>
                                <option value="<?= $office_id; ?>"><?php echo $office_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo load_message('USER_ROLE'); ?> <span class="text-danger">*</span></label>
                        <select class="form-control select2" id="user_role">
                            <option value=""><?php echo load_message('SELECT_USER_ROLE'); ?></option>
                            <?php
                            //var_dump($role_list);
                            foreach ($role_list as $rlist) {
                                $role_id = $rlist->role_id;
                                $role_name = $rlist->role_name;
                                if (get_user_role_name() != "SUPER_ADMIN" && $role_name != "SUPER_ADMIN") {
                                    ?>
                                    <option value="<?= $role_id; ?>"><?php echo $role_name; ?></option>
                                    <?php
                                } elseif (get_user_role_name() == "SUPER_ADMIN") {
                                    ?>
                                    <option value="<?= $role_id; ?>"><?php echo $role_name; ?></option>
                                <?php }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="user_status" id="user_status" class="form-control" style="display: none">
                            <option value="1" selected="selected">Active</option>
                        </select>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class=" box-header">
                        <p align="center"><b><?php echo load_message('PERSONAL_DETAILS'); ?></b></p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputusername"
                               class="control-label"><?php echo load_message('FULL_NAME'); ?></label>

                        <input type="text" class="form-control" id="fullname"
                               placeholder="<?php echo load_message('FULL_NAME'); ?>">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputName"
                               class="control-label"><?php echo load_message('DESIGNATION'); ?></label>

                        <input type="text" class="form-control" id="designation"
                               placeholder="<?php echo load_message('DESIGNATION'); ?>">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1"
                               class="control-label"><?php echo load_message('DEPARTMENT'); ?></label>

                        <select class="form-control select2" id="department">
                            <option value="0"><?php echo load_message('SELECT_DEPARTMENT'); ?></option>
                            <?php
                            //var_dump($role_list);
                            foreach ($dept_list as $deptlist) {
                                $dept_id = $deptlist->department_id;
                                $dept_name = $deptlist->department_name;
                                ?>
                                <option value="<?= $dept_id; ?>"><?php echo $dept_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"
                               class="control-label"><?php echo load_message('PHONE'); ?></label>

                        <input type="text" class="form-control number-only" maxlength="11" id="phone"
                               placeholder="<?php echo load_message('PHONE'); ?>">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"
                               class="control-label"><?php echo load_message('MOBILE'); ?></label>

                        <input type="text" class="form-control number-only" maxlength="11" id="mobile"
                               placeholder="<?php echo load_message('MOBILE'); ?>">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"
                               class="control-label"><?php echo load_message('HR_ID'); ?></label>

                        <input type="text" class="form-control" id="hrid"
                               placeholder="<?php echo load_message('HR_ID'); ?>">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"
                               class="control-label"><?php echo load_message('N_ID'); ?></label>

                        <input type="text" class="form-control" id="national_id"
                               placeholder="<?php echo load_message('N_ID'); ?>">

                    </div>
                </div>
                <div class="col-md-4" style="background: #eee;">
                    <div class=" box-header">
                        <p align="center"><b><?php echo load_message('CURRENT_POSTING'); ?></b></p>
                    </div>
                    <div class="form-group">
                        <label class=" control-label"><?php echo load_message('DISTRICT'); ?></label>
                        <?php get_districts_list('','district','form-control select2',"Select District") ?>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"
                               class="control-label"><?php echo load_message('UPAZELLA'); ?></label>

                        <input type="text" class="form-control" id="upozella"
                               placeholder="<?php echo load_message('UPAZELLA'); ?>">
                    </div>
                    <div class="form-group" style="display:none;">
                        <label for="exampleInputEmail1"
                               class="control-label"><?php echo load_message('VILLAGE'); ?></label>

                        <input type="text" class="form-control" id="village"
                               placeholder="<?php echo load_message('VILLAGE'); ?>">

                    </div>
                    <div class="form-group" style="display:none;">
                        <label for="exampleInputEmail1"
                               class="control-label"><?php echo load_message('POSTCODE'); ?></label>

                        <input type="text" class="form-control" id="postcode"
                               placeholder="<?php echo load_message('POSTCODE'); ?>">

                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div align="right">
                <button type="submit" class="btn btn-success btn-lg"
                        onClick="return form_validation('ajax/user_registration',user,'registration_form',0);"
                        id="user_add"><i class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_USER'); ?>
                </button>
            </div>
            <!-- /.box-footer-->
        </div>
    </div>
</section>

<!-- /.content -->