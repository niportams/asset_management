<form id="allocation_form2">
    <input type="hidden" name="asset_id" id="asset_id" value="<?php echo $asset_id; ?>"/>
    <input type="hidden" name="category_id" id="category_id" value="<?php echo $category_id; ?>"/>
     <input type="text" class="form-control"  value="<?php echo $asset_label_id; ?>"
                           readonly="readonly" style="background-color:#FFF;" name="asset_label_id" id="asset_label_id">
   <div class="box-body">
       <div class="panel box box-info">
           		<div class="box-header with-border">
               		<h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        Allocation Details &nbsp;&nbsp;&nbsp;<li class="fa fa-sort-desc"></li>
                      </a>
                    </h4>
                  </div>
  
                         
                  <div id="collapseTwo" class="panel-collapse ">
                  		 <div class="box-body" style="display:none;">
                            <label class="control-label"><?php echo load_message('REFERENCE'); ?>
                             <span class="text-danger">*</span></label>
                             <input class="form-control" type="text" name="ref_no" id="ref_no" value="<?php echo $reference_no;?>" readonly="readonly">
                        </div>
  
                       <div class="row form-group" align="justify">
                        <div class="col-md-6">
                         <div class="box-body">
                            <label class="control-label"><?php echo load_message('ALLOCATED_TO'); ?>
                            <span class="text-danger">*</span></label>
                            <select class="form-control select2" id="req_user_id" name="req_user_id">
                              <option value=""><?php echo load_message('SELECT_USER'); ?></option>
                               <?php
                                foreach ($user_list as $uslist) {
                                    $allocate_userid = $uslist->userid;
                                    $allocate_name = $uslist->fullname;
                                    $allocate_designation = $uslist->designation;
                                    ?>
                                    <option value="<?php echo $allocate_userid; ?>"><?php echo $allocate_name; ?>
                                        -<?php echo $allocate_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="box-body">
                             <label class="control-label"><?php echo load_message('ON_BEHALF'); ?> 
                            <span class="text-danger">*</span></label>
                            <select class="form-control select2" id="on_behalf" name="on_behalf">
                                <option value=""><?php echo load_message('SELECT_USER'); ?>
                                </option>
                                <?php
                                foreach ($user_list as $uslist) {
                                    $allocate_userid = $uslist->userid;
                                    $allocate_name = $uslist->fullname;
                                    $allocate_designation = $uslist->designation;
                                    ?>
                                    <option value="<?php echo $allocate_userid; ?>"><?php echo $allocate_name; ?>
                                        -<?php echo $allocate_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select> 
                            </div>
                            </div>
                        </div>                
                    
                     <div class="row form-group" align="justify">
                        <div class="col-md-6">
                         <div class="box-body">
                          <label class="control-label"><?php echo load_message('APPROVER'); ?>
                            <span class="text-danger">*</span></label>
                            <select class="form-control select2" id="approved_by" name="approved_by">
                                <option value=""><?php echo load_message('SELECT_APPROVER'); ?></option>
                                <?php
                                foreach($approver_list as $avlist)
								 {
                                    $approver_id = $avlist->userid;
                                    $approver_name = $avlist->fullname;
                                    $approver_designation = $avlist->designation;
                                    ?>
                                    <option value="<?php echo $approver_id; ?>"><?php echo $approver_name; ?>
                                     -<?php echo $approver_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            </div>
                          </div>
                         <div class="col-md-6">
                         <div class="box-body">
                        <label class="control-label"><?php echo load_message('ALLOCATION_LOCATION'); ?></label>
                        <select class="form-control select2" id="asset_location" name="asset_location">
                             <option value=""><?php echo load_message('SELECT_LOCATION'); ?></option>
                             <option value="<?php $this->session->userdata('user_center'); ?>">
							 <?php echo get_office_name($this->session->userdata('user_center'));?></option>
                                <?php
                                foreach ($room_list as $rmlist) {
                                    $room_id = $rmlist->room_id;
                                    $room_name = $rmlist->room_name;
                                    $room_location = $rmlist->room_location;
                                    ?>
                                    <option value="<?php echo $room_id; ?>"><?php echo $room_name; ?>
                                     -<?php echo $room_location; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            </div>
                            </div>
                        </div>                         
                        <div class="box-body">
                         <label class="control-label"><?php echo load_message('REMARKS'); ?></label>
                         <textarea class="form-control" type="textarea" id="remarks" name="remarks"></textarea>
                        </div>
                    </div>
                </div>
                
                 <div class="panel box box-warning">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                       Associate Document Attachment
                        &nbsp;&nbsp;&nbsp;<li class="fa fa-sort-desc"></li>
                      </a>                   
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                    <div class="box-body">
                      <div class="box-body form-horizontal">
                   
                        <a href="#" onclick="return clickAnother();">
                        <div class="upload">
                            <i class="fa fa-upload" aria-hidden="true" style="font-size: 5em;"></i>
                            <h4><?php echo load_message('CLICK_TO_UPLOAD');?></h4>
                            <label class="control-label"><?php echo load_message('SELECT_FILE_JPG_PNG_PDF_DOCX'); ?></label>
                        </div>
                        </a>
                    <input type="file" name="allo_file" id="allo_file" style="display: none;"/>
                    <hr/>
                    <table id="buildyourform" style="width:350px;" class="table table-striped">
                        <thead>
                        <tr>
                            <th width="12px;"><?php echo load_message('FILE_NAME'); ?></th>
                            <th width="12px;"></th>
                        </tr>
                        </thead>
                        <!-- <span id="uploaded_image"></span>-->
                        <tbody id="uploaded_image">

                        </tbody>
                    </table>
                    <div class="form-group" align="justify">&nbsp;</div>
                </div>
                    </div>
                  </div>
                </div>
                  <div class="form-group">
                   <div align="center">
                    <button type="submit" id="submit_button" class="btn btn-primary"  onclick="return direct_allocation_submit('allocation_form2');">
                     <i class="fa fa-send">&nbsp;Allocate</i></button>     
                    </div>
                </div>
                </div>
        
        <!--</div> -->
    </div>
</form>
<!--<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>-->
<!--<script src="dist/jquery.imageuploader.js"></script>-->
<script>
    $('.select2').select2({ width: '100%' });
    (function () {
        var options = {};
        $('.js-uploader__box').uploader(options);
    }());

    function clickAnother() {
        $("#allo_file").click();
        return false;
    }
</script>
