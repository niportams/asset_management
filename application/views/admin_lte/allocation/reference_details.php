<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<!-- Main content -->
<?php

//$arq_id = $reference_details[0]->arq_id;
 $reference_no = $reference_details[0]->ref_no;
 $requested_user = $reference_details[0]->req_user_id;
 $office_id = $reference_details[0]->office_id;
 $on_behalf = $reference_details[0]->on_behalf;
 $create_date = $reference_details[0]->create_date;

?>
<input type="hidden" id="reference_no" name="reference_no" value="<?php echo $reference_no;?>" />
<input type="hidden" id="on_behalf" name="on_behalf" value="<?php echo $on_behalf;?>" />
<input type="hidden" id="requested_user" name="requested_user" value="<?php echo $requested_user;?>" />

<div class="box-body" id="reference_form">

    <h5 class="box-title"> <?php echo load_message('MANDATORY_TEXT'); ?></h5>
      
    <div class="row">
    
        <div class="col-md-4">
            <strong><?php echo load_message('REF');?>:</strong> &nbsp; <?php echo $reference_no; ?>&nbsp;
		</div>
        
          <div class="col-md-5">
            <strong><?php echo load_message('OFFICE_TO');?>:</strong> &nbsp; <?php echo get_office_name($office_id); ?>&nbsp;
		  </div>
 
     </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <strong><?php echo load_message('ALLOCATED_TO'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($requested_user); ?>
        </div>
        <div class="col-md-4">
            <strong><?php echo load_message('ON_BEHALF'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($on_behalf); ?>
        </div>
       
    </div>
    <div class="row">
        <div class="col-md-4">
            <strong>
                <?php echo load_message('CREATE_DATE'); ?></strong>
            &nbsp;<?php echo get_format_date($create_date); ?>
        </div>
       
    </div>    
    <br>
    <!--<div class="form-group" id="requisition_list" style="overflow-y:scroll; height:250px;" >-->  
    <div class="box box-solid">
       <div class="box-header with-border">
          <h3 class="box-title">Allocation Process</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                     <!-- <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">-->
                      <a data-toggle="" data-parent="#" href="#">Requested Item List</a>                    
                    </h4>
                  </div>
               <div id="" class="">
               <div class="box-body">
               <table id="example4" class="table table-bordered table-striped" style="border:dotted; border-width:1px; border-color:#999;">
                <thead>
                <tr style="background-color:#FFFFFF;">
                    <th>#</th>
                    <th><?php echo load_message('CATEGORY'); ?></th>
                    <th><?php echo load_message('QUANTITY'); ?></th>
                    <th><?php echo load_message('STATUS'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="item_list">
                <?php
                $i = 1;
                if ($reference_details <> "") {
                    foreach($reference_details as $rqlist) {
                        $arq_id = $rqlist->arq_id;
						$ref_no = $rqlist->ref_no;
                        $category_id = $rqlist->category_id;
                        $quantity = $rqlist->quantity;
						$req_status = $rqlist->req_status;
						$create_date = $rqlist->create_date;
						$update_date = $rqlist->update_date;
						
						$allo_date = date("Y-m-d", strtotime($create_date));
                        $cur_date = date("Y-m-d");

                        $diff = abs(strtotime($cur_date) - strtotime($allo_date));
						$years = floor($diff / (365 * 60 * 60 * 24));
                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
	
                 ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                            <td><?php echo get_category_name($category_id); ?></td>
                            <td><?php echo $quantity; ?> </td>
                            <td id="rstatus<?php echo $i;?>">
								<?php 
                                if(($req_status == "Available")or($req_status == "Allocated"))
                                {
                                ?>
                                <font color="green"><?php echo $req_status;?></font>
                                <?php	
                                }
                                else { ?><font color="#FF0000"><?php echo $req_status; ?></font><?php }
                                ?>
                            
                            </td>
                            <td id="rtest<?php echo $i;?>"><?php if($req_status == "Available"){ ?> 
                          
                            <?php }elseif(($req_status == "Allocated")and($days<=1)) { ?>
                            <a href="#" class="btn btn-warning btn-xs" onclick="return cancel_allocation('<?php echo $arq_id;?>','<?php echo $ref_no;?>','<?php echo $category_id;?>');">Cancel</a><?php }
							elseif(($req_status == "Allocated")and($days>1)){ ?> <i class="fa fa-check"></i><?php }
							 ?></td>
                        </tr>
                        <?php
                        $i++;
                    }

                }
			  ?>
             </tbody>
            </table>
           </div>
          </div>
         </div>
            
        <div class="panel box box-success">
           <div class="box-header with-border">
               		<h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        Allocation Approver &nbsp;&nbsp;&nbsp;<li class="fa fa-sort-desc"></li>
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse ">
                    <div class="box-body">
                     <label class="control-label"><?php echo load_message('ALLOCATION_LOCATION'); ?></label>
                        <select class="form-control" id="asset_location" name="asset_location">
                             <option value=""><?php echo load_message('SELECT_LOCATION'); ?></option>
                             <option value="<?php $this->session->userdata('user_center'); ?>"><?php echo get_office_name($this->session->userdata('user_center'));?></option>
                                <?php
                                foreach ($room_list as $rmlist) {
                                    $room_id = $rmlist->room_id;
                                    $room_name = $rmlist->room_name;
                                    $room_location = $rmlist->room_location;
                                    ?>
                                    <option value="<?php echo $room_id; ?>"><?php echo $room_name; ?>
                                     -<?php echo $room_location; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <label class="control-label"><?php echo load_message('APPROVER'); ?>
                            <span class="text-danger">*</span></label>
                            <select class="form-control" id="approved_by" name="approved_by">
                                <option value=""><?php echo load_message('SELECT_APPROVER'); ?></option>
                                <?php
                                foreach($approver_list as $avlist)
								 {
                                    $approver_id = $avlist->userid;
                                    $approver_name = $avlist->fullname;
                                    $approver_designation = $avlist->designation;
                                    ?>
                                    <option value="<?php echo $approver_id; ?>"><?php echo $approver_name; ?>
                                     -<?php echo $approver_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <label class="control-label"><?php echo load_message('VALUE_OR_CONTEXT'); ?></label>
                            <textarea class="form-control" type="textarea" id="value_context" name="value_context"></textarea>
                        </div>
                        <div>
                            <label class="control-label"><?php echo load_message('REMARKS'); ?></label>
                            <textarea class="form-control" type="textarea" id="remarks" name="remarks"></textarea>
                        </div>
                    </div>
                </div>
            
               <div class="panel box box-danger">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                       Associate Document Attachment
                        &nbsp;&nbsp;&nbsp;<li class="fa fa-sort-desc"></li>
                      </a>                   
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                    <div class="box-body">
                      <div class="box-body form-horizontal">
                   
                        <a href="#" onclick="return clickAnother();">
                        <div class="upload">
                            <i class="fa fa-upload" aria-hidden="true" style="font-size: 5em;"></i>
                            <h4><?php echo load_message('CLICK_TO_UPLOAD');?></h4>
                            <label class="control-label"><?php echo load_message('SELECT_FILE_JPG_PNG_PDF_DOCX'); ?></label>
                        </div>
                        </a>
                    <input type="file" name="file" id="file" style="display: none;"/>
                    <hr/>
                    <table id="buildyourform" style="width:350px;" class="table table-striped">
                        <thead>
                        <tr>
                            <th width="12px;"><?php echo load_message('FILE_NAME'); ?></th>
                            <th width="12px;"></th>
                        </tr>
                        </thead>
                        <!-- <span id="uploaded_image"></span>-->
                        <tbody id="uploaded_image">

                        </tbody>
                    </table>
                    <div class="form-group" align="justify">&nbsp;</div>
                </div>
                    </div>
                  </div>
                </div>
                <br />
                 <div class="form-group">
                   <div align="center">
                    <button type="submit" class="btn btn-primary"  onclick="return allocation_submit('<?php //echo $category_id;?>');">
                     <i class="fa fa-send">&nbsp;Allocate</i></button>     
                    </div>
                </div>
                </div>
                 </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->          
          </div>
          <!-- /.box -->
    
    <!-- /.box-body -->
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--<script src="dist/jquery.imageuploader.js"></script>-->
<script>
    (function () {
        var options = {};
        $('.js-uploader__box').uploader(options);
    }());

    function clickAnother() {
        $("#file").click();
        return false;
    }
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
       