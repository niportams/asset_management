<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <!-- <div class="box-header">
           <h3 class="box-title">Data Table of Asset Allocation</h3>
         </div>-->
        <!-- /.box-header -->
        <div class="box-body table-responsive" id="allocate_list">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="204"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="100"><?php echo load_message('CATEGORY'); ?></th>
                    <th width="100"><?php echo load_message('ASSET_ID_L'); ?></th>
                    <th width="145"><?php echo load_message('ALLOCATED_DATE'); ?></th>
                    <th width="145"><?php echo load_message('REVERT_DATE'); ?></th>
                    <th width="124"><?php echo load_message('ALLOCATED_BY'); ?></th>
                    <th width="124"><?php echo load_message('ALLOCATED_TO'); ?></th>
                    <th width="120"><?php echo load_message('REFERENCE'); ?></th>
                    <th width="120"><?php echo load_message('ASSET_LOCATION'); ?></th>
                    <th width="110"><?php echo load_message('REQUISITION_NUMBER'); ?></th>
                    <th width="30"><?php echo load_message('ATTACHMENT'); ?></th>
                    <th width="60"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($alloc_list <> "") {
                    foreach ($alloc_list as $aclist) {
                        $allocate_id = $aclist->allocate_id;
                        $asset_id = $aclist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $asset_label_id = $aclist->asset_label_id;
                        $category_id = $aclist->category_id;
                        $requisition_no = $aclist->requisition_no;
                        $allocated_by = $aclist->allocated_by;
                        $allocate_to = $aclist->allocate_to;
                        $allocate_office = $aclist->allocate_office;
                        $asset_location = $aclist->asset_location;
                        $reference_no = $aclist->reference_no;
                        $create_date = $aclist->create_date;
						$returned_date = $aclist->update_date;
						$revert_id = $aclist->revert_id;

                        ?>
                        <tr>
                            <td align="center"><?php echo $i; ?></td>
                            <td> <?php echo get_asset_name($asset_id); ?></td>
                            <td><?php echo get_parent_category_name($category_id);  echo ' > '; get_category_name($category_id);?></td>
                            <td><?php echo $asset_label_id; ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                            <td><?php echo get_format_date($returned_date); ?></td>
                            <td><?php echo get_user_fullname($allocated_by); ?></td>
                            <td><?php echo get_user_fullname($allocate_to); ?></td>
                            <td><?php echo $reference_no; ?></td>
                            <td><?php echo get_office_room($asset_location); ?></td>
                            <td align="center"><?php echo $requisition_no; ?></td>
                            <td align="center"><a href="#" data-toggle="modal" data-target="#modal-attachment"
                            title="Click to Show Attachment" onclick="return revert_attachment('<?php echo $allocate_id;?>');">
                            <span class="text"><i class="fa fa-paperclip fa-x"></i></span></a></td>
                            <td>
                                <button class="btn btn-primary btn-xs asset-detail" data-asset_id="<?=$asset_id;?>" title="Details" data-toggle="modal"
                                   data-target="#modal-asset-details"
                                   onclick="return asset_details('<?php echo $encrypt_asset; ?>');"><i
                                            class="fa fa-list-alt"></i></button>
                               
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
		
        <!--   Modal for Asset details-->
        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
            
        </div>
    </div>
    
      <!--  Show Attachment of this allocation    -->
        <div class="modal fade" id="modal-attachment">
            <div class="modal-dialog" style="width:550px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ATTACHED_FILES') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="attached_files"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
    <!--    Modal end here -->
    <div class="clearfix"></div>
    <!--    Modal for Role Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->