<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <!-- <div class="box-header">
           <h3 class="box-title">Data Table of Asset Allocation</h3>
         </div>-->
        <!-- /.box-header -->
        <div class="box-body table-responsive" id="allocate_list">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="204"><?php echo load_message('REF'); ?></th>
                    <th width="100"><?php echo load_message('REQUESTED_USER'); ?></th>
                    <th width="100"><?php echo load_message('ON_BEHALF'); ?></th>
                    <th width="100"><?php echo load_message('CREATE_DATE'); ?></th>
                    <th width="60"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($request_list <> "") {
                    foreach ($request_list as $reclist) {
                        $reference_no = $reclist->ref_no;
                        $requested_user = $reclist->req_user_id;
                        $on_behalfe = $reclist->on_behalf;
                        //$req_status = $reclist->req_status;
                        $create_date = $reclist->create_date;

                        ?>
                        <tr>
                            <td align="center"><?php echo $i; ?></td>
                            <td><?php echo $reference_no; ?></td>
                            <td><?php echo get_user_fullname($requested_user); ?></td>
                            <td><?php echo get_user_fullname($on_behalfe); ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                            <td>
                                <button class="btn btn-success btn-xs" title="Allocate" data-toggle="modal"
                                 data-target="#reference-details" onclick="return reference_details('<?php echo $reference_no; ?>');">
                                 <i class="fa fa-list-alt"></i>
                                </button>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->

        <!--   Modal for Asset details-->
        <div class="modal fade" id="reference-details">
            <div class="modal-dialog" style="width:750px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('REQUEST_DETAILS') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="reference_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!--    Modal end here -->
        <div class="clearfix"></div>
        <!--    Modal for Role Edit end here -->

        <!-- /.box -->
    </div>
</section>
<!-- /.content -->
<!--    <iframe style="display:none" id="printf" name="printf"></iframe>-->