<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<!-- Main content -->

<section class="content">

    <!-- Default box -->

    <?php //echo $state_id;?>
    <?php //echo $chosen_state_name;?>
    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title"><?= load_message('FIND_TITLE'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body form-horizontal">

            <div class="col-md-12">
                <div class="form-group" align="justify"></div>
                <div id="asset_dead_form">
                    <div class="form-group">
                        <label for="exampleInputName"
                               class="col-sm-4 control-label"><?php echo load_message('ASSET_OR_LABEL_ID'); ?>
                               <span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="asset_search" name="asset_search"
                                   placeholder="<?php echo load_message('ASSET_OR_LABEL_ID'); ?>">
                        </div>
                        <div class="col-sm-3">
                            <button name="search_asset" class="btn btn-default" onclick="allocation_asset_search();">
                            <i class="fa fa-binoculars"></i>&nbsp;<?= load_message('FIND_TITLE'); ?></button>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title"><?= load_message('FOUND_ITEM'); ?></h3>
        </div>
        <div class="box-body" id="asset_info_form">

        </div>
    </div>

    <!-- /.box-body -->

    <!-- /.box-footer-->

    <!-- /.box -->

</section>
<!-- /.content -->

   