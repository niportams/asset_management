<div class="box" align="center">
    <table id="example2" class="table table-striped">
        <thead>
        <tr>
            <th width="12px;"><?php echo load_message('FILE_NAME') ?></th>
            <th width="12px;"><?php echo load_message('UPLOAD_DATE') ?></th>
            <th width="12px;"></th>
        </tr>
        </thead>
        <!-- <span id="uploaded_image"></span>-->
        <tbody>
        <?php
        if ($file_list <> "") {
            foreach ($file_list as $flist) {
                $file_id = $flist->file_id;
                $file_name = $flist->file_name;
				$requisition_id = $flist->requisition_id;
                $create_date = $flist->create_date;
                ?>

                <tr>
                    <td>
                      <a href="<?php echo base_url(); ?>uploads/<?php echo $file_name; ?>" title="Show File" target="_blank">
                       <?php echo $file_name; ?></a>
                    </td>
                    <td><?php echo get_format_date($create_date); ?></td>                  
                    
                 <td>
                    <a href="<?php echo base_url(); ?>uploads/<?php echo $file_name; ?>" class="btn btn-success btn-xs"
                    title="Show File" target="_blank"><i class="fa fa-external-link " aria-hidden="true"></i></a>
                    <a href="#" class="btn btn-danger btn-xs" title="Delete File"
                    onclick="return alloc_file_delete('<?php echo $file_id; ?>','<?php echo $file_name; ?>','<?php echo $requisition_id; ?>');">
                    <i class="fa fa-trash" aria-hidden="true"></i></a>
            	</td>
                    
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>
                        
						 