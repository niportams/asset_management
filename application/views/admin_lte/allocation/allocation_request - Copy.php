<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
-->

<section class="content">
<div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <!--<h3 class="box-title">Collapsible Accordion</h3>-->
              <?php echo load_message('MANDATORY_TEXT'); ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-primary">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Generate Allocation Request List &nbsp;&nbsp;&nbsp;<li class="fa fa-sort-desc"></li>
                          </a>
                        </h4>
                      </div>
                   
                  <div id="collapseOne" class="panel-collapse collapse in">
                   	
                     <div class="box-body">
                       <div id="add_product">
                        <!--<h4><?php //echo load_message('ADD_ITEM_TO_ALLOCATION_LIST'); ?></h4>-->
                        <div>
                            <label class="control-label"><?php echo load_message('SELECT_ITEM'); ?><span
                                        class="text-danger">*</span></label>
                                        <?php //var_dump(get_category_list("item","item","form-control",load_message('SELECT_ITEM'),"","value"));?>
                                        <?php select_chain();?>
<!--                           <select class="form-control" id="item" name="item">
                                <option value=""><?php //echo load_message('SELECT_ITEM'); ?></option>
                                <?php //foreach ($assets_name as $value) {
                                    //echo '<option value=""' . $value->category_id .' >' . $value->category_name . "</option>";
                               // } ?>
                            </select>-->
                        </div>
                        <div>
                            <label class="control-label"><?php echo load_message('REQUIRED_QTY'); ?><span
                                        class="text-danger">*</span></label>
                            <input class="form-control" type="text" id="qty" name="qty">
                        </div>
                        <br>
                        <div class="box-footer">
                            <div align="center">
                                <!-- <button class="btn btn-primary add_item">Add to List</button>-->
                                <button class="btn btn-success select_item"
                         onclick="add_to_list();"><i class="fa fa-plus"></i> &nbsp;<?php echo load_message('ADD_TO_LIST'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
          
               <hr />
                  
                    <div class="box-body ">
                        <h4>Selected <?php echo load_message('ITEM_LIST'); ?> </h4>

                        <table id="buildyourform" class="table table-striped">
                            <thead>
                            <tr id="field1">
                                <th>
                                    <label class="control-label"><?php echo load_message('ASSET_TITLE'); ?></label>
                                </th>
                                <th>
                                    <label class="control-label"><?php echo load_message('REQUIRED_QTY'); ?></label>
                                </th>
                                 <th>
                                    <label class="control-label"><?php echo load_message('REQUIRED_STATUS'); ?></label>
                                </th>
                                <th>
                                    <label class="control-label"></label>
                                </th>
                            </tr>
                            </thead>
                            <!--  <tbody id="clear-all">-->
                            <tbody id="selected_asset">

                            </tbody>
                        </table>
                        <br>
                    </div>
               
                         
                      <p align="center">
                      <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="btn btn-primary btn-flat" aria-expanded="true">
                      <strong>Next</strong>
                      </a> -->
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="btn btn-primary btn-flat" aria-expanded="true"  
                      onclick="return generate_pr_list('<?php echo $reference_no;?>');" id="request_next">
                      <strong>Next</strong>
                      </a> 
                      
                      <!--<a href="#" class="btn btn-warning btn-flat" onclick="return generate_pr_list('<?php //echo $reference_no;?>');" title="Click to Request PR" data-toggle="modal" data-target="#modal-pr_details"><strong>Generate PR List</strong>
                      </a>-->
                      </p>
                      <p>&nbsp;</p>
                    </div>
                  </div>
                </div>
           <form id="request_allocation">
               <div class="panel box box-success">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                       Allocation User Select &nbsp;&nbsp;&nbsp;<li class="fa fa-sort-desc"></li>
                      </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                    <div class="box-body form-horizontal">
                     <div class="box-body form-horizontal">
						<!-- <h4>Allocation Details</h4>-->
						 <div>
                            <label class="control-label"><?php echo load_message('REFERENCE'); ?>
                             <span class="text-danger">*</span></label>
                             <input class="form-control" type="text" name="ref_no" id="ref_no" value="<?php echo $reference_no;?>" readonly="readonly">
                        </div>
 
                        <div>
                            <label class="control-label"><?php echo load_message('ALLOCATED_TO'); ?>
                            <span class="text-danger">*</span></label>
                            <select class="form-control" id="req_user_id" name="req_user_id">
                              <option value=""><?php echo load_message('SELECT_USER'); ?></option>
                               <?php
                                foreach ($user_list as $uslist) {
                                    $allocate_userid = $uslist->userid;
                                    $allocate_name = $uslist->fullname;
                                    $allocate_designation = $uslist->designation;
                                    ?>
                                    <option value="<?php echo $allocate_userid; ?>"><?php echo $allocate_name; ?>
                                        -<?php echo $allocate_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <label class="control-label"><?php echo load_message('ON_BEHALF'); ?> 
                            <span class="text-danger">*</span></label>
                            <select class="form-control" id="on_behalf" name="on_behalf">
                                <option value=""><?php echo load_message('SELECT_USER'); ?>
                                </option>
                                <?php
                                foreach ($user_list as $uslist) {
                                    $allocate_userid = $uslist->userid;
                                    $allocate_name = $uslist->fullname;
                                    $allocate_designation = $uslist->designation;
                                    ?>
                                    <option value="<?php echo $allocate_userid; ?>"><?php echo $allocate_name; ?>
                                        -<?php echo $allocate_designation; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                      </div>
                      <p align="center">
                       <button class="btn btn-primary" id="allocation_submit" title="Click to place an allocation request for selected items."
                         onclick="handle_form('request_allocation', request_allocation, 'ajax3/request_submit',0,'manual', true,'clear-all')">
                        <i class="fa fa-send"></i> &nbsp; <?php echo load_message('SUBMIT'); ?>
                        </button>&nbsp; &nbsp; 
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-warning btn-flat" aria-expanded="true" >
                      <strong>Previous</strong>
                      </a> 
                      
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
	</form>

     <div class="modal fade" id="modal-pr_details">
            <div class="modal-dialog" style="width:550px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">PR List</h4>
                    </div>
                    <div class="modal-body">
                        <div id="pr_list"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
     </div>
    <!--    Modal for assets show end here -->

</section>
<!-- /.content -->

<script type="text/javascript">
    var assets =
        <?php echo json_encode($assets_name);?>;
		$("#request_next").hide();
</script>
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--<script src="dist/jquery.imageuploader.js"></script>-->
<!--<script>
    (function () {
        var options = {};
        $('.js-uploader__box').uploader(options);
    }());

    function clickAnother() {
        $("#file").click();
        return false;
    }
</script>
-->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>