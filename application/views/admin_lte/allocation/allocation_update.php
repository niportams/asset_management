<?php 
foreach ($allocation_details as $aclist) 
{
	$allocate_id = $aclist->allocate_id;
	$asset_id = $aclist->asset_id;
	$encrypt_asset = base64_encode($asset_id);
	$asset_label_id = $aclist->asset_label_id;
	$category_id = $aclist->category_id;
	$requisition_no = $aclist->requisition_no;
	$allocated_by = $aclist->allocated_by;
	$allocate_to = $aclist->allocate_to;
	$on_behalf = $aclist->on_behalf;
	$allocate_office = $aclist->allocate_office;
	$asset_location = $aclist->asset_location;
	$reference_no = $aclist->reference_no;
	$create_date = $aclist->create_date;
}
?>

       <div class="box-body">
       <div class="panel box box-info">
        <div class="box-header with-border">
            <h4 class="box-title">Allocated Asset Information Edit</h4>
          </div>
          
     <div class="box-body" id="reference_form">
    
    <div class="row">
    	 <div class="col-md-4">
            <strong><?php echo load_message('ASSET_TITLE');?>:</strong> &nbsp; <?php echo get_asset_name($asset_id); ?> &nbsp;
		</div>
        
        <div class="col-md-4">
            <strong><?php echo load_message('REF');?>:</strong> &nbsp; <?php echo $reference_no; ?>&nbsp;
		</div>
        
          <div class="col-md-4">
            <strong><?php echo load_message('OFFICE_TO');?>:</strong> &nbsp; <?php echo get_office_name($allocate_office); ?>&nbsp;
		  </div>
 
     </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <strong><?php echo load_message('ALLOCATED_TO'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($allocate_to); ?>
        </div>
        <div class="col-md-4">
            <strong><?php echo load_message('ON_BEHALF'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($on_behalf); ?>
        </div>
        
         <div class="col-md-4">
            <strong>
                <?php echo load_message('ASSET_LABEL_ID'); ?>:</strong>
            &nbsp;<?php echo $asset_label_id; ?>
        </div> 
       
       
    </div>
    
    </div>    
    <hr/>                
      <div id="collapseTwo" class="panel-collapse ">
                  	<div class="row form-group" align="justify">
                       <div class="col-md-12">
                        <div class="box-body">
                        <label class="control-label"><?php echo "Allocated Asset Location"; ?></label>
                        <select class="form-control select2" id="asset_location" name="asset_location">
                             <option value=""><?php echo load_message('SELECT_LOCATION'); ?></option>
                             <option value="<?php $this->session->userdata('user_center'); ?>">
							 <?php echo get_office_name($this->session->userdata('user_center'));?></option>
                                <?php
                                foreach ($room_list as $rmlist) {
                                    $room_id = $rmlist->room_id;
                                    $room_name = $rmlist->room_name;
                                    $room_location = $rmlist->room_location;
									if($asset_location == $room_id)
									{
										?>
                                         <option value="<?php echo $room_id; ?>" selected="selected"><?php echo $room_name; ?>
                                     -<?php echo $room_location; ?></option>
                                        <?php		
									}
									else
									{
										?>
                                        <option value="<?php echo $room_id; ?>"><?php echo $room_name; ?>
                                     -<?php echo $room_location; ?></option>
                                        <?php
									}
                                   
                                }
                                ?>
                            </select>
                          </div>
                         </div>
                        </div>                         
                      </div>
                    <hr />
                 <div class="form-group">
                   <div align="center">
                    <button type="button" class="btn btn-primary"  onclick="return allocation_update_submit('<?php echo $allocate_id;?>');" 
                    title="Update Allocated Location">
                     <i class="fa fa-pencil">&nbsp;Update Allocated Location</i></button>     
                    </div>
                </div>
                </div>
                
               
                </div>
        
        <!--</div> -->
        
    </div>
<script>
    $('.select2').select2({ width: '100%' });
</script>
