 <?php
                $i = 1;
                if ($reference_details <> "") {
                    foreach($reference_details as $rqlist) {
                        $arq_id = $rqlist->arq_id;
						$ref_no = $rqlist->ref_no;
                        $category_id = $rqlist->category_id;
                        $quantity = $rqlist->quantity;
						$req_status = $rqlist->req_status;
						
						$create_date = $rqlist->create_date;
						$update_date = $rqlist->update_date;
						
						$allo_date = date("Y-m-d", strtotime($create_date));
                        $cur_date = date("Y-m-d");

                        $diff = abs(strtotime($cur_date) - strtotime($allo_date));
						$years = floor($diff / (365 * 60 * 60 * 24));
                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                        ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                            <td><?php echo get_category_name($category_id); ?></td>
                            <td><?php echo $quantity; ?> </td>
                            <td id="rstatus<?php echo $i;?>">
								<?php 
                                if(($req_status == "Available")or($req_status == "Allocated"))
                                {
                                ?>
                                <font color="green"><?php echo $req_status; ?></font>
                                <?php	
                                }
                                else { ?><font color="#FF0000"><?php echo $req_status; ?></font><?php }
                                ?>
                            </td>
                            <td id="rtest<?php echo $i;?>"><?php if($req_status == "Available"){ ?> 
                           
                            <?php }elseif(($req_status == "Allocated")and($days<=1)) { ?>
                            <a href="#" class="btn btn-warning btn-xs" onclick="return cancel_allocation('<?php echo $arq_id;?>',
                            '<?php echo $ref_no;?>','<?php echo $category_id;?>');">Cancel</a><?php }
							elseif(($req_status == "Allocated")and($days>1)){ ?> <i class="fa fa-check"></i><?php }?>
							</td>
                        </tr>
                        <?php
                        $i++;
                    }

                }

                ?>