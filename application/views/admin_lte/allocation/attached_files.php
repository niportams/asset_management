<div class="box" align="center">
    <table id="example2" class="table table-striped">
        <thead>
        <tr>
            <th width="12px;"><?php echo load_message('FILE_NAME') ?></th>
            <th width="12px;"><?php echo load_message('UPLOAD_DATE') ?></th>
            <th width="12px;"></th>
        </tr>
        </thead>
        <!-- <span id="uploaded_image"></span>-->
        <tbody>
        <?php
        if ($file_list <> "") {
            foreach ($file_list as $flist) {
                $file_id = $flist->revert_id;
                $file_name = $flist->filename;
                $create_date = $flist->create_date;
                ?>

                <tr>
                    <td>
                        <a href="<?php echo base_url(); ?>uploads/<?php echo $file_name; ?>" title="Show File"
                           target="_blank">
                            <?php echo $file_name; ?></a>
                    </td>
                    <td><?php echo get_format_date($create_date); ?></td>
                  
                    <td>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>
                        
						 