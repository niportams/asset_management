<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<!-- Main content -->
<?php

//$arq_id = $reference_details[0]->arq_id;
 $allocate_id = $allocation_details[0]->allocate_id;
 $asset_id = $allocation_details[0]->asset_id;
 $reference_no = $allocation_details[0]->reference_no;
 $office_id = $allocation_details[0]->allocate_office;
 $allocate_to = $allocation_details[0]->allocate_to;
 $on_behalf = $allocation_details[0]->on_behalf;
 $asset_location = $allocation_details[0]->asset_location;
 $asset_label_id = $allocation_details[0]->asset_label_id;
 $create_date = $allocation_details[0]->create_date;

?>


<div class="box-body" id="reference_form">
    
    <div class="row">
    	 <div class="col-md-4">
            <strong><?php echo load_message('ASSET_TITLE');?>:</strong> &nbsp; <?php echo get_asset_name($asset_id); ?> &nbsp;
		</div>
        
        <div class="col-md-4">
            <strong><?php echo load_message('REF');?>:</strong> &nbsp; <?php echo $reference_no; ?>&nbsp;
		</div>
        
          <div class="col-md-4">
            <strong><?php echo load_message('OFFICE_TO');?>:</strong> &nbsp; <?php echo get_office_name($office_id); ?>&nbsp;
		  </div>
 
     </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <strong><?php echo load_message('ALLOCATED_TO'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($allocate_to); ?>
        </div>
        <div class="col-md-6">
            <strong><?php echo load_message('ON_BEHALF'); ?>
                : </strong>&nbsp;<?php echo get_user_fullname($on_behalf); ?>
        </div>
       
    </div>
    <div class="row">
        <div class="col-md-6">
            <strong>
                <?php echo load_message('CREATE_DATE'); ?></strong>
            &nbsp;<?php echo get_format_date($create_date); ?>
        </div>
       <div class="col-md-6">
            <strong>
                <?php echo load_message('ASSET_LABEL_ID'); ?></strong>
            &nbsp;<?php echo $asset_label_id; ?>
        </div> 
       
    </div>    
    <br>
    <!--<div class="form-group" id="requisition_list" style="overflow-y:scroll; height:250px;" >-->  
    <div class="box box-solid">
       <div class="box-header with-border">
          <!--<h3 class="box-title">Allocation Revert</h3>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
               
			<div class="panel box box-warning">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                       Acknowledgement Paper 
                        &nbsp;&nbsp;&nbsp;<li class="fa fa-sort-desc"></li>
                      </a>                   
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse">
                    <div class="box-body">
                      <div class="box-body form-horizontal">
                        <a href="#" onclick="return clickAnother();">
                            <div class="upload">
                                <i class="fa fa-upload" aria-hidden="true" style="font-size: 5em;"></i>
                                <h4><?php echo load_message('CLICK_TO_UPLOAD');?></h4>
                                <label class="control-label"><?php echo load_message('SELECT_FILE_JPG_PNG_PDF_DOCX'); ?></label>
                            </div>
                        </a>
                        <input type="file" name="file" id="revert_file" style="display: none;"/>
                        <input type="hidden" name="reference_no" id="reference_no" style="display: none;" value="<?php echo $reference_no;?>"/>
                        <input type="hidden" name="revert_id" id="revert_id" style="display: none;" value="<?php echo $allocate_id;?>"/>
                        <input type="hidden" name="revert_purpose" id="revert_purpose" style="display: none;" value="revert_allocation"/>
                    <hr/>
                    <table id="buildyourform" style="width:350px;" class="table table-striped">
                        <thead>
                            <tr>
                                <th width="12px;"><?php echo load_message('FILE_NAME'); ?></th>
                                <th width="12px;"></th>
                            </tr>
                        </thead>
                        <!-- <span id="uploaded_image"></span>-->
                        <tbody id="reverted_image">
							<?php
                            if ($file_list <> "") 
							{
                                foreach ($file_list as $flist) {
                                    $file_id = $flist->revert_id;
                                    $file_name = $flist->filename;
                                    $reference_number = $flist->reference_number;
									$purpose = $flist->revert_type;
                                    ?>
                            
                                    <tr>
                                        <td>
                                             <!--<img src="<?php //echo base_url();
                                            ?>uploads/<?php //echo $file_name; ?>" height="50px;" width="50px;" border="0"/>-->
                                            <?php echo $file_name; ?>
                                        </td>
                            
                                        <td>
                                            <a href="<?php echo base_url(); ?>uploads/<?php echo $file_name; ?>" class="btn btn-success btn-xs"
                                               title="Show File" target="_blank"><i class="fa fa-external-link " aria-hidden="true"></i></a>
                                            <a href="#" class="btn btn-danger btn-xs" title="Delete File"
                                           onclick="return revert_file_delete('<?php echo $file_id; ?>','<?php echo $file_name; ?>','<?php echo $reference_number;?>','<?php echo $allocate_id;?>','<?php echo $purpose;?>');"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="form-group" align="justify">&nbsp;</div>
                </div>
                    </div>
                  </div>
                </div>
                <br />
                <div class="form-group">
                 <div align="center">
                  <button type="submit" class="btn btn-primary"  onclick="return asset_revert('','<?php echo $allocate_id; ?>','<?php echo $asset_id; ?>','allocation','<?php echo $reference_no;?>');"><i class="fa fa-undo">&nbsp;Revert Allocation</i></button>     
                    </div>
                </div>
                </div>
                 </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->          
          </div>
          <!-- /.box -->
    
    <!-- /.box-body -->
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--<script src="dist/jquery.imageuploader.js"></script>-->
<script>
    (function () {
        var options = {};
        $('.js-uploader__box').uploader(options);
    }());

    function clickAnother() {
		
        $("#revert_file").click();
        return false;
    }
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
       