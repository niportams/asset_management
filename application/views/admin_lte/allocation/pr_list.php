                
			<div class="box-body">
                  <div class="form-group">
                    <div align="center">
                    <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                    <thead>
                    <tr>
                    <th width="10">#</th>
                    <th>Category Name</th>
                    <th>Quantity</th>
                    <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    <?php
					if ($pr_list <> "") 
						{
						$i = 1;
						
						foreach($pr_list as $prlist) {
							$arq_id = $prlist->item_id;
							$category_id = $prlist->category_id;
							$quantity = $prlist->qty;
							$item_status = $prlist->item_status;
							?>
                            <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo get_category_name($category_id); ?></td>
                            <td><?php echo $quantity;?></td>
                            <td><?php echo $item_status;?></td>
                            </tr>
							<?php
							$i++;
						}
						
						}
						else
						{
						?>
                        <tr>
                          <td colspan="4" align="center">No Item Selected</td>
                        </tr>
                        <?php
						}
					
                	?>
                   
                    </tbody>
                    </table>
                        
                    </div>
                   </div>
                 <?php 
				 if ($pr_list <> "") 
				{
				 ?>    
                 <div class="form-group">
                    <div align="center">
                    
                        <button type="submit" class="btn btn-primary" onClick="return pr_request_all('<?php echo $reference_no;?>');"> 
                        <i class="fa fa-save"></i> &nbsp; <?php echo "Submit PR" ?>
  
                        </button>
                    </div>
                  </div>
                  <?php 
				  }
				  ?>
                 </div>