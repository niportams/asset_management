<!-- ------------Modal for Loading--------------- -->
<div class="modal fade" id="modalLoading">
    <div class="modal-dialog" style="width:250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please Wait ... </h4>
            </div>
            <div class="modal-body">
                <div style="text-align: center;">
                    <i style="font-size: 30px;" class="fa fa-refresh fa-spin"></i>
                    <br><br>
                    Wait until the process is finished.
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--    Modal for Capital Edit end here -->


<!-- Common Modal -->
<div class="modal fade" id="common-modal">
    <div class="modal-dialog" style="width:850px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="common-modal-title"></h4>
            </div>
            <div class="modal-body" id="common-modal-body">
                
            </div>
            <div class="modal-footer">
<!--                <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT'); ?></button>-->
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Common Modal End -->
<!-- Maintainence Modal -->
<div class="modal fade" id="maintenance-modal">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">System Maintenance Notice </h4>
            </div>
            <div class="modal-body">
                Our maintenance work is going to start from <?php echo $this->config->item('maintenance_mode_start');?>  . Please Complete the current Task and logout from the system.
                After <?php echo $this->config->item('maintenance_mode_start');?>  all the account will be logged out from the system. System will go under maintenance mood.
                You are not going to access the system till <?php echo $this->config->item('maintenance_mode_end');?> . Hopefully we will get your kind support.
            </div>
            <div class="modal-footer">
                <!--                <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT'); ?></button>-->
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Common Modal End -->
<footer class="main-footer">
    
    <div>
        <div style="margin-bottom: 5px;"><strong>Supported By:</strong></div>
        <img src="<?= base_url(); ?>theme/dist/img/footer_logos.png" class="img-responsive">
    </div>
    
    <div class="text-center" style="border-top: solid 1px #DDD; font-size: 10px; padding-top: 10px;">
         <strong>Copyright &copy; <?php echo date("Y")-1;  ?>-<?php echo date("Y");  ?> <a href="<?php echo base_url();?>">NIPORT</a>.</strong> All rights reserved.
     </div>
        
</footer>


<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                            <p>Will be 23 on April 24th</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-user bg-yellow"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                            <p>New phone +1(800)555-1234</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                            <p>nora@example.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-file-code-o bg-green"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                            <p>Execution time 5 seconds</p>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Custom Template Design
                            <span class="label label-danger pull-right">70%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Update Resume
                            <span class="label label-success pull-right">95%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Laravel Integration
                            <span class="label label-warning pull-right">50%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Back End Framework
                            <span class="label label-primary pull-right">68%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Settings</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Report panel usage
                        <input type="checkbox" class="pull-right" checked>
                    </label>

                    <p>
                        Some information about this general settings option
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Allow mail redirect
                        <input type="checkbox" class="pull-right" checked>
                    </label>

                    <p>
                        Other sets of options are available
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Expose author name in posts
                        <input type="checkbox" class="pull-right" checked>
                    </label>

                    <p>
                        Allow the user to show his name in blog posts
                    </p>
                </div>
                <!-- /.form-group -->

                <h3 class="control-sidebar-heading">Chat Settings</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Show me as online
                        <input type="checkbox" class="pull-right" checked>
                    </label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Turn off notifications
                        <input type="checkbox" class="pull-right">
                    </label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Delete chat history
                        <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                    </label>
                </div>
                <!-- /.form-group -->
            </form>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->

<?php load_js("bower_components/jquery/dist/jquery.min.js"); ?>
<!-- jQuery UI 1.11.4 -->
<?php load_js("dist/js/jquery-ui.min.js"); ?>
<?php load_js("dist/js/jquery.validate.js"); ?>

<!-- select chain -->
<script src="<?= base_url(); ?>theme/dist/js/sci/select_chain.js" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<?php load_js("bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>

<?php //load_js("bower_components/Chart.js/Chart.js"); ?>

<!-- peace page -->
<?php load_js("bower_components/PACE/pace.min.js"); ?>

<!-- Morris.js charts -->
<?php // load_js("bower_components/raphael/raphael.min.js"); ?>
<?php //load_js("bower_components/morris.js/morris.min.js"); ?>
<!-- Sparkline -->
<?php load_js("bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"); ?>
<!-- jvectormap -->
<?php load_js("plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"); ?>
<?php load_js("plugins/jvectormap/jquery-jvectormap-world-mill-en.js"); ?>
<!-- jQuery Knob Chart -->
<?php load_js("bower_components/jquery-knob/dist/jquery.knob.min.js"); ?>
<!-- daterangepicker -->
<?php load_js("bower_components/moment/min/moment.min.js"); ?>
<?php load_js("bower_components/bootstrap-daterangepicker/daterangepicker.js"); ?>
<!--<!-- datepicker -->
<?php load_js("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"); ?>
<!-- Bootstrap WYSIHTML5 -->
<?php load_js("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"); ?>
<!-- Slimscroll -->
<?php load_js("bower_components/jquery-slimscroll/jquery.slimscroll.min.js"); ?>
<!-- FastClick -->
<?php load_js("bower_components/fastclick/lib/fastclick.js"); ?>
<!-- AdminLTE App -->
<?php load_js("dist/js/adminlte.min.js"); ?>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<?php //load_js("dist/js/pages/dashboard.js"); ?>
<!-- AdminLTE for demo purposes -->
<?php //load_js("dist/js/demo.js"); ?>
<?php load_js("dist/js/notifications.js"); ?>
<?php //load_json("dist/js/sci/project.js"); ?>

<!-- fullCalendar -->
<?php load_js("bower_components/moment/moment.js"); ?>
<?php load_js("bower_components/fullcalendar/dist/fullcalendar.min.js"); ?>

<!-- iCheck 1.0.1 -->
<?php load_js('plugins/iCheck/icheck.min.js') ?>


<!-- page script -->

<!-- data table -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>theme/dist/datatables/datatables.min.css"/>
<!-- jquery Datatables -->
<

<!-- Bootstrap Datatables -->
<?php load_js("bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"); ?>
<?php load_js("dist/datatables/datatables.min.js"); ?>
<?php load_js("dist/datatables/buttons.print.min.js"); ?>

<!-- //data table Full functional options -->
<?php load_js("dist/js/sci/data_table_full_functional.js"); ?>

<!-- Date picker -->
<?php load_js("dist/js/sci/date_picker.js"); ?>

<?php
if (sizeof($js) > 0) {
    //var_dump($js);
    foreach ($js as $j)
        load_js($j);
};
?>

<!-- custom Message SCI Message -->
<?php load_js("dist/js/custom_msg.js"); ?>


<!-- custom category tree -->


<link href="<?= base_url(); ?>theme/dist/third-party/fancy-tree/src/skin-win8/ui.fancytree.css" rel="stylesheet"
      type="text/css">
<script src="<?= base_url(); ?>theme/dist/third-party/fancy-tree/src/jquery.fancytree.js"
type="text/javascript"></script>

<!-- Start_Exclude: This block is not part of the sample code -->
<link href="<?= base_url(); ?>theme/dist/third-party/fancy-tree/lib/prettify.css" rel="stylesheet">
<script src="<?= base_url(); ?>theme/dist/third-party/fancy-tree/lib/prettify.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>theme/dist/third-party/fancy-tree/sample.js" type="text/javascript"></script>
<!-- End_Exclude -->

<script src="<?= base_url(); ?>theme/dist/js/sci/sci_print.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>theme/dist/js/sci/table_style.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>theme/dist/js/num_validation.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>theme/bower_components/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
<!-- Fancy Tree -->
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        // using default options
        $("#tree").fancytree({
            autoExpand: true
        });
    });
    <?php if($this->config->item('upcoming_maintenance_mode')){ ?>

        $("#maintenance-modal").modal({backdrop: 'static', keyboard: false});
    <?php }?>
</script>

</body>
</html>