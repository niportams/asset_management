<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<style>

</style>
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table of All User</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="160">User Name</th>
                    <th width="100">Profile Name</th>
                    <th width="120">USER ROLE</th>
                    <th width="140">USER Email</th>
                    <th width="100"><?php echo load_message('POSTING_CENTER'); ?></th>
                    <th width="140">User IP Address</th>
                    <th width="140">USER Device</th>
                    <th width="140"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
				if($user_list<>"")
				{
                foreach ($user_list as $ulist) {
                    if(sizeof($ulist)>10) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $ulist['user_name']; ?>
                            </td>
                            <td><?php echo $ulist['user_nice_name']; ?></td>
                            <td><?php echo get_user_role_name_by_id($ulist['user_role_id']); ?></td>
                            <td><?php echo $ulist['user_email']; ?></td>
                            <td><?php get_office_name($ulist['user_center']); ?></td>
                            <td><?php echo $ulist['ip_address']; ?></td>
                            <td><?php echo $ulist['user_agent']; ?></td>
                            <td>
                                <a href="#" title="Logout This User"
                                   onclick="return user_logout('<?php echo $ulist['session_id']; ?>');"
                                   class="btn btn-danger btn-xs">
                                    <i class="fa fa-power-off fa-x" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    $i++;
                }
				}
                ?>
                </tbody>
            </table>
        </div>

        <!-- /.box-body -->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('USER_PROFILE_DETAILS'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="profile_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal end here -->
    <!-- /.box -->


</section>
<!-- /.content -->