<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header">
            <!--          <h3 class="box-title">Office Create</h3>-->
            <!---->
            <!--          <div class="box-tools pull-right">-->
            <!--           <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
            <!--           <i class="fa fa-minus"></i></button>-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
            <!--              <i class="fa fa-times"></i></button>-->
            <!--          </div>-->
        </div>
        <form id="entry_form">
            <div class="box-body">
            <div class="col-md-10">
            <div class="form-group">
           
                <label for="exampleInputName" class="control-label"><?php echo load_message('GRN'); ?>
                <span class="text-danger">*</span></label>
    
                <input type="text" class="form-control" id="grn_no" name="grn_no" value="<?php echo $grn; ?>"
                     readonly="readonly"  title="<?php echo load_message('GRN_TOOLTIP'); ?>">
    
            </div>
            </div>
            
                <div class="col-md-4">
                
                <input type="hidden" name="capital_id" id="capital_id" value="99999"/>
                <input type="hidden" class="form-control" id="pr_reference_no" name="pr_reference_no" value="0" />
                           
                <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('ASSET_TITLE'); ?>
                <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="asset_name" name="asset_name" placeholder="<?php echo load_message('ASSET_TITLE'); ?>"
                value="">
                </div>                     
                   
                </div>
                           
               
               <div class="col-md-4">
                    
                    <div class="form-group" align="justify">
                        <label for="exampleInputName" class="control-label"><?php echo load_message('CATEGORY'); ?><span
                         class="text-danger">*</span></label>
                        <?php get_category_list("category_id", "category_id", "form-control", load_message('SELECT_CATEGORY')); ?>
                    </div>
                  
               </div>
               
                 
                <div class="col-md-4">
                    <div class="form-group">
                    <label for="exampleInputName" class="control-label"><?php echo load_message('DESCRIPTION'); ?></label>
                    <textarea name="asset_description" rows="3" id="asset_description" class="form-control"></textarea>
                    </div>
               </div>
               
               <div class="row form-group" align="justify">
               
		<div class="col-md-3">
            <label><?php echo load_message('MANUFACTURER'); ?></label>
            <select name="manufacture_id" id="manufacture_id" class="form-control">
                <option value="" selected="selected"><?php echo load_message('SELECT_MANUFACTURER'); ?></option>
                <?php
                foreach ($manufacture_list as $mlist) {
                    $manufacture_id = $mlist->manufacture_id;
                    $manufacture_name = $mlist->manufacture_name;
                    ?>
                    <option value="<?php echo $manufacture_id; ?>"><?php echo $manufacture_name; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('WARRENTY_EXP_DATE'); ?></label>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control date-picker" id="warrenty_date" name="warrenty_date" placeholder="dd/mm/yyyy">
            </div>
        </div>
        <div class="col-md-3">
            <label><?php echo load_message('SKU'); ?></label>
            <input type="text" class="form-control" id="sku" name="sku"
            placeholder="<?php echo load_message('SKU'); ?>" title="<?php echo load_message('SKU_TOOLTIP'); ?>">
        </div>
       <div class="col-md-3">
            <label><?php echo load_message('ASSET_LABEL_ID'); ?></label>
            <input type="text" class="form-control" id="sci_id" name="sci_id"
                   placeholder="<?php echo load_message('ASSET_LABEL_ID'); ?>">
        </div>
       
       </div>  
       
        <div class="row form-group" align="justify">
        <div class="col-md-3">
            <label><?php echo load_message('MODEL_NAME'); ?></label>
            <input type="text" class="form-control" id="model_name" name="model_name"
                   placeholder="<?php echo load_message('MODEL_NAME'); ?> ">
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('MODEL_NO'); ?></label>
            <input type="text" class="form-control" id="model_no" name="model_no" placeholder="<?php echo load_message('MODEL_NO'); ?>">
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('SERIAL_NO'); ?></label>
            <input type="text" class="form-control" id="serial_no" name="serial_no"
                   placeholder="<?php echo load_message('SERIAL_NO'); ?>" title="Serial Number of the product.">
        </div>


        <div class="col-md-3">
            <label><?php echo load_message('REF'); ?></label>
            <input type="text" class="form-control" id="reference_no" name="reference_no"
                   placeholder="<?php echo load_message('REF'); ?>">
        </div>
    </div>
    
     <div class="row form-group" align="justify">

        <div class="col-md-3">

            <label><?php echo load_message('PURCHASE_DATE'); ?></label>

            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right date-picker" id="purchase_date" name="purchase_date"
                       placeholder="dd/mm/yyyy">
            </div>
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('PURCHASE_ORDER_NO'); ?></label>
            <input type="text" class="form-control" id="purchase_no" name="purchase_no"
                   placeholder="<?php echo load_message('PURCHASE_ORDER_NO'); ?>">
        </div>

        <div class="col-md-3">
          <label><?php echo load_message('PURCHASE_PRICE'); ?></label>
          <input type="text" class="form-control" id="purchase_price" name="purchase_price"
         		placeholder="<?php echo load_message('PURCHASE_PRICE'); ?>">
        </div>

        <div class="col-md-3">
            <label><?php echo load_message('PURCHASE_LOC'); ?></label>
            <input type="text" class="form-control" id="purchase_location" name="purchase_location"
                   placeholder="<?php echo load_message('PURCHASE_LOC'); ?>">
        </div>
    </div>
    
    <div class="row form-group">
    
        <div class="col-md-3">
       <label for="exampleInputName" class="control-label">Other Information / Asset Condition</label>
		<textarea name="asset_remarks" id="asset_remarks" name="asset_remarks" class="form-control" placeholder="<?php echo load_message('REMARKS'); ?>"></textarea>
        </div>

        <div class="col-md-3">
            <br/>
            <label><?php echo load_message('GIK'); ?> &nbsp; <input type="radio" name="gik" value="1"
                                                                    checked="checked"></label>
            <!-- <input type="text" class="form-control" id="funded_by" placeholder="Funded By" hidden="true"/>-->
            <div id="textboxes">
                <select name="funded_by" id="funded_by" class="form-control">
                    <option value="" selected="selected"><?php echo load_message('SELECT_FUNDED_BY'); ?></option>
                    <?php
                    foreach ($org_list as $org_list) {
                        $organization_name = $org_list->organization_name;
                        ?>
                        <option value="<?php echo $organization_name; ?>"><?php echo $organization_name; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <br/>
            <label><?php echo load_message('OWN_FUNDED'); ?> &nbsp; <input type="radio" name="gik"
                                                                         value="0"></label>
            <!-- <input type="text" class="form-control" id="own_funded" placeholder="NIPORT" value="NIPORT"  />-->
            <div id="textbox" style="display: none">
                <input type="text" class="form-control" id="own_funded" name="own_funded"
                       value="<?php echo load_message('NIPORT_SHORT'); ?>"/>
            </div>
        </div>

                   
        
         <div class="col-md-3">
			<label>Asset Lifetime</label>
			<div class="input-group date">
                <div class="input-group-addon">
                 <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right date-picker" id="asset_lifetime" name="asset_lifetime" placeholder="dd/mm/yyyy">
            </div>
        </div>

       
    </div> 
    <hr />
    
    <div class="row form-group" align="justify">

        <div class="col-md-3">

            <label>Asset Allocated Date</label>

            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right date-picker" id="allocation_date" name="allocation_date" placeholder="dd/mm/yyyy">
            </div>
        </div>

        <div class="col-md-3">
            <label>Asset Status</label>
            <select name="asset_status_id" id="asset_status_id" class="form-control" onchange="return allocate_to();">
               <option value="1" selected="selected">Registered</option>
              <!-- <option value="2">Transferred</option>-->
               <option value="3">DeadState</option>
               <option value="4">Maintenance</option>
               <option value="5">Allocated</option>
               <option value="6">Disposed</option>
            </select>
        </div>
        
         <div class="col-md-3">
            <label>Allocated Person</label>
             <select class="form-control" id="assigned_to" name="assigned_to" onchange="return show_button();">
              <option value=""><?php echo load_message('SELECT_USER'); ?></option>
               <?php
                foreach ($user_list as $uslist) {
                    $allocate_userid = $uslist->userid;
                    $allocate_name = $uslist->fullname;
                    $allocate_designation = $uslist->designation;
                    ?>
                    <option value="<?php echo $allocate_userid; ?>"><?php echo $allocate_name; ?>
                        -<?php echo $allocate_designation; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        
        <div class="col-md-3">
            <label class="control-label"><?php echo load_message('ALLOCATION_LOCATION'); ?></label>
            <select class="form-control" id="asset_location" name="asset_location">
              <option value=""><?php echo load_message('SELECT_LOCATION'); ?></option>
                <?php
                foreach ($room_list as $rmlist) {
                    $room_id = $rmlist->room_id;
                    $room_name = $rmlist->room_name;
                    $room_location = $rmlist->room_location;
                    ?>
                    <option value="<?php echo $room_id; ?>"><?php echo $room_name; ?>
                     -<?php echo $room_location; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
   </div>  
</div>
<!-- /.box-body -->
            <div class="box-footer">
                <div align="right">
                    <button type="submit" class="btn btn-success btn-lg" id="data_entry_button"
                      onClick="handle_form('entry_form', dataentry, 'ajax/entry_submit',0,'manual');" id="register_add">
                        <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('REGISTER_ASSET'); ?>
                    </button>
                </div>
            </div>
            <!-- /.box-footer-->
        </form>
    </div>

    <!-- /.box -->

</section>
<!-- /.content -->
<script language="javascript">
function allocate_to()
{
	
	var asset_status_id =  document.getElementById("asset_status_id").value;
	if(asset_status_id == 5)
		{
		 
		var assigned_to =  document.getElementById("assigned_to").value;
		if(assigned_to == "")
			{ 
			sci_alert("", "Please Select Allocated Person", "warning");
			document.getElementById("assigned_to").focus();
			document.getElementById("assigned_to").style.background = "#e2eef5";
			
			$('#data_entry_button').attr('disabled','disabled');
			return false;
			}
			else
				//$('#data_entry_button').hide();
				$('#data_entry_button').prop('disabled',false);
		}
		else
			{
				$('#data_entry_button').prop('disabled',false);
				
			}
	return false;	
}
function show_button()
{
	
	var assigned_to =  document.getElementById("assigned_to").value;
		if(assigned_to !="")
		{
		//$('#data_entry_button').show();
		$('#data_entry_button').prop('disabled', false);
		}
		else
			{
			$('#data_entry_button').prop('disabled', true);
			}	
return false;
}
</script>