<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header">
            <!--          <h3 class="box-title">User Role Create with Access Rights</h3>-->
            <!---->
            <!--          <div class="box-tools pull-right">-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">-->
            <!--              <i class="fa fa-minus"></i></button>-->
            <!--            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
            <!--              <i class="fa fa-times"></i></button>-->
            <!--          </div>-->
        </div>
        <div class="box-body" id="role_create">
            <div class="col-md-12">
                <div class="form-group" align="justify"></div>
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('ROLE_NAME'); ?><span
                                class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="role_name"
                           placeholder="<?php echo load_message('ROLE_NAME'); ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputName"><?php echo load_message('ROLE_RANK'); ?><span
                                class="text-danger">*</span></label>
                    <select class="form-control" id="role_rank">
                        <option value="">Select Rank</option>
                        <?php for($i=$this->session->userdata('user_role_rank'); $i<=sizeof($role_list)+1;$i++){
                            echo "<option value='".$i."'>".$i."</option>";
                        } ?>
                    </select>
                </div>
                <label><?php echo load_message('USER_PRIVILEGE_LIST'); ?><span class="text-danger">*</span></label>

                <div class="form-group" align="justify">
                    <?php
                    //Columns must be a factor of 12 (1,2,3,4,6,12)
                    $numOfCols = 4;
                    $rowCount = 0;
                    $bootstrapColWidth = 12 / $numOfCols;
                    ?>
                    <div class="row">
                        <?php
                        foreach ($privilege_list as $row) {
                            $privilege_id = $row->privilege_id;
                            $privilege = $row->privilege;
                            ?>
                            <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                                <div class="item">
                                    <label class="normal">
                                        <input class="flat-red" type="checkbox" name="access_right[]" id="access_right"
                                               value="<?php echo $privilege_id; ?>"/> &nbsp; <?php echo $privilege; ?>
                                    </label>
                                </div>
                            </div>
                            <?php
                            $rowCount++;
                            if ($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.box-body -->
        <div class="box-footer">
            <div align="right">
                <button type="submit" class="btn btn-success btn-lg" onClick="return access_validation();"><i
                            class="fa fa-plus"></i> &nbsp; <?php echo load_message('ADD_NEW_ROLE'); ?>
                </button>
            </div>
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        })
    });
</script>