<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
foreach ($role_details as $rdetails) {
    $role_id = $rdetails->role_id;
    $role_name = $rdetails->role_name;
    $role_rank  = $rdetails->role_rank;
    $db_privilage = $rdetails->privilage_id;
    $role_status = $rdetails->role_status;
    $privilege_array = explode(",", $db_privilage);
}
//var_dump($privilege_array);
$len = sizeof($privilege_array);
?>
<!-- Main content -->
<div class="box-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <label for="exampleInputName"><?php echo load_message('ROLE_NAME'); ?><span
                            class="text-danger">*</span></label>
                <input type="text" class="form-control" id="role_name" value="<?php echo $role_name; ?>">
            </div>
            <div class="col-md-6">
                <label for="exampleInputName"><?php echo load_message('ROLE_RANK'); ?><span
                            class="text-danger">*</span></label>
                <select class="form-control" id="role_rank">
                    <option value="">Select Rank</option>
                    <?php for($i=$this->session->userdata('user_role_rank'); $i<=sizeof($role_list)+1;$i++){
                        if($role_rank==$i){
                            echo "<option value='".$i."' selected>".$i."</option>";
                        }else{
                            echo "<option value='".$i."'>".$i."</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="col-md-6">
                <label for="exampleInputName"><?php echo load_message('STATUS'); ?></label>
                <select name="role_status" id="role_status" class="form-control">
                    <?php
                    if ($role_status == 1) {
                        ?>
                        <option value="1" selected="selected">Active</option>
                        <option value="0">Inactive</option>
                        <?php
                    } else {
                        ?>
                        <option value="1">Active</option>
                        <option value="0" selected="selected">Inactive</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <br>
        <div class="box box-success">
            <div class="box-header">
                <h4 class="box-title"><?php echo load_message('USER_PRIVILEGE_LIST'); ?><span
                            class="text-danger">*</span></h4>
            </div>
            <div class="box-body">
                <div class="form-group" style="overflow-y: scroll;overflow-x: hidden; height:330px;">
                    <?php
                    //Columns must be a factor of 12 (1,2,3,4,6,12)
                    $numOfCols = 3;
                    $rowCount = 0;
                    $bootstrapColWidth = 12 / $numOfCols;
                    ?>
                    <div class="row">
                        <?php
                        foreach ($privilege_list as $row) {
                            $privilege_id = $row->privilege_id;
                            $privilege = $row->privilege;
                            ?>
                            <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                                <div class="item">
                                    <?php
                                    $found = 0;
                                    for ($i = 0; $i <= $len - 1; $i++) {
                                        if ($privilege_id == $privilege_array[$i]) {
                                            $found = $privilege_id;
                                            $i = $len - 1;
                                        }
                                    }
                                    if ($privilege_id == $found) {
                                        ?>
                                        <label class="normal">
                                            <input class="flat-red" type="checkbox" name="access_right[]"
                                                   id="access_right"
                                                   value="<?php echo $privilege_id; ?>" checked="checked"/>
                                            &nbsp;
                                            <?php echo $privilege; ?>
                                        </label>
                                        <?php
                                    } else {
                                        ?>
                                        <label class="normal">
                                            <input class="flat-red" type="checkbox" name="access_right[]"
                                                   id="access_right"
                                                   value="<?php echo $privilege_id; ?>"/>
                                            &nbsp; <?php echo $privilege; ?>
                                        </label>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>
                            <?php
                            $rowCount++;
                            if ($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="form-group">
            <div align="center">
                <button type="submit" class="btn btn-primary" onclick="return update_role('<?php echo $role_id; ?>');">
                    <i class="fa fa-pencil-square-o"></i> &nbsp; <?php echo load_message('UPDATE_ROLE'); ?>
                </button>
            </div>
        </div>
    </div>

    <!-- /.box-body -->
    <script>
        $(document).ready(function () {
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            })
        });
    </script>