<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <!--  <div class="box-header">
           <h3 class="box-title">Data Table of User Role</h3>
         </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive" id="role_list">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="204"><?php echo load_message('ROLE_NAME'); ?></th>
                    <th width="100"><?php echo load_message('PRIVILEGE'); ?></th>
                    <th width="100"><?php echo load_message('STATUS'); ?></th>
                    <th width="124"><?php echo load_message('CREATE_DATE'); ?></th>
                    <th width="120"><?php echo load_message('UPDATE_DATE'); ?></th>
                    <th width="70"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($role_list as $rlist) {
                    $role_id = $rlist->role_id;
                    $role_name = $rlist->role_name;

                    $privilage_id = $rlist->privilage_id;
                    $role_status = $rlist->role_status;
                    $create_date = $rlist->create_date;
                    $update_date = $rlist->update_date;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $role_name; ?></td>
                        <td><a href="#">
                                <button type="button" class="btn btn-default btn-xs" data-toggle="modal"
                                        data-target="#modal-default"
                                        onclick="show_privilege('<?php echo $privilage_id; ?>');"
                                        title="Click to Show details">Show Privilege
                                </button>
                            </a>
                        </td>
                        <td><?php if ($role_status == 1) {
                                echo "Active";
                            } else {
                                echo "Inactive";
                            } ?></td>
                        <td><?php echo get_format_date($create_date); ?></td>
                        <td><?php echo get_format_date($update_date); ?></td>
                        <td>
                            <?php if (permission_check('role/role_edit')) { ?>
                                <a href="#" title="Edit Role" class="btn btn-primary btn-xs" data-toggle="modal"
                                   data-target="#modal-role"
                                   onclick="role_edit('<?php echo $role_id; ?>');"><i class="fa fa-pencil fa-x"></i></a>
                            <?php }
                            if (permission_check('role/role_delete')) { ?>
                                &nbsp;
                                <a href="#" title="Delete Role" class="btn btn-danger btn-xs"
                                   onclick="return role_delete('<?php echo $role_id; ?>');"><i class="fa fa-trash fa-x"
                                                                                               aria-hidden="true"></i></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->

        <!--   Modal for Privilege details-->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('PRIVILEGE_NAME') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="privilege_name"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal end here -->

    <!-- ------------Modal for Role edit Start--------------- -->

    <div class="modal fade" id="modal-role">
        <div class="modal-dialog" style="width:860px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('USER_ROLE_DETAILS') ?></h4>
                </div>
                <div class="modal-body">
                    <div id="role_edit"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE') ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="clearfix"></div>
    <!--    Modal for Role Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->
