<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?php page_title() ?> | NIPORT Asset Manager</title>
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>theme/dist/img/logo.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap 3.3.7 -->
    <?php load_css("bower_components/bootstrap/dist/css/bootstrap.min.css"); ?>
    <!-- DataTables -->
    <?php load_css("bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"); ?>
    <!-- Font Awesome -->
    <?php load_css("bower_components/font-awesome/css/font-awesome.min.css"); ?>
    <!-- Ionicons -->
    <?php load_css("bower_components/Ionicons/css/ionicons.min.css"); ?>
    <!-- Select2 -->
    <?php load_css("bower_components/select2/dist/css/select2.min.css"); ?>
    <!-- Theme style -->
    <?php load_css("dist/css/AdminLTE.min.css"); ?>
    <?php load_css("dist/css/jquery-ui.min.css"); ?>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <?php load_css("dist/css/skins/_all-skins.min.css"); ?>
    <!-- Morris chart -->
    <?php //load_css("bower_components/morris.js/morris.css");?>
    <!-- jvectormap -->
    <?php load_css("bower_components/jvectormap/jquery-jvectormap.css"); ?>
    <!-- Date Picker -->
    <?php load_css("bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"); ?>
    <!-- Daterange picker -->
    <?php load_css("bower_components/bootstrap-daterangepicker/daterangepicker.css"); ?>
    <!-- bootstrap wysihtml5 - text editor -->
<!--    --><?php //load_css("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"); ?>
    <?php load_css("dist/css/custom_sci.css"); ?>
    <?php load_css("dist/css/style.css"); ?>

    <!-- Pace style -->
    <?php load_css("plugins/pace/pace.min.css"); ?>

    <!-- iCheck for checkboxes and radio inputs -->
    <?php load_css('plugins/iCheck/all.css') ?>



    <?php load_js("dist/js/jquery.form.js"); ?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <?php load_js("dist/js/jquery-2.1.4.js"); ?>
    <?php load_js("dist/js/global_script.js"); ?>
    <?php load_js("dist/js/general_script.js"); ?>
    <?php load_js("dist/js/asset_script.js"); ?>
    <?php load_js("dist/js/setting_script.js"); ?>
    <?php load_js("dist/js/allocation_script.js"); ?>
<!--    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>-->

    <?php //load_json("dist/json/rules.json");?>
    <?php //load_json("dist/json/messages.json");?>

    <script>
        var globalserver = '<?php echo base_url(); ?>';
        var base_url = '<?php echo base_url(); ?>';
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <!-- Custom alert sweet alert plugins -->
    <?php load_js("dist/js/sweetalert.min.js"); ?>
<!--    --><?php //load_js("dist/js/custom_msg.js"); ?>
<!--    --><?php //load_js("dist/js/global_script.js");?>
    <?php load_js("dist/js/sci_export.js");?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>
    <?php load_json("dist/js/chart.js"); ?>

    <script type="text/javascript">

        function ExportToExcel(mytblId) {
            var htmltable = document.getElementById(mytblId);
            var html = htmltable.outerHTML;
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
            return false;
        }
    </script>
    
</head>

<body class="hold-transition <?= $this->config->item('theme-skin'); ?>  sidebar-mini fixed"> <!-- skin-green-light -->
<div class="wrapper">
    <div id="fixed-banner-top">
        <img src="<?= base_url(); ?>theme/dist/img/logo/banner-inner.png" style="height: auto; width: 80%;"/>
        </div>