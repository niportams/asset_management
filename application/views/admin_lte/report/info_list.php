<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->

<section class="content">

    <!-- Search box -->
    <div class="box box-primary" overflow-y: auto;>
        <div class="box-header">
            <h3 class="box-title"><?= load_message('SEARCH_OPTION'); ?></h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('ASSET_TITLE'); ?></label>
                        <input type="text" class="form-control" id="asset_name"
                               placeholder="<?= load_message('ASSET_TITLE'); ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="control-label"><?= load_message('CATEGORY'); ?></label>
                        <?php get_category_list("category_id", "category_id", "form-control select2", load_message('SELECT_CATEGORY')); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('ASSET_ID_L'); ?></label>
                        <input type="text" class="form-control" id="asset_label" placeholder="Asset ID"
                        >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('ASSET_STATUS'); ?></label>
                        <select name="status_id" id="status_id" class="form-control select2">
                            <option value="" selected="selected">Select Status</option>
                            <?php
                            foreach ($sts_list as $stlist) {
                                $status_id = $stlist->status_id;
                                $status_name = $stlist->status_name;
                                ?>
                                <option value="<?php echo $status_id; ?>"><?php echo $status_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('FROM'); ?></label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control date-picker" id="from" placeholder="From"
                            >
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="" class="control-label"><?= load_message('TO'); ?></label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control date-picker" id="to" placeholder="To">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('ASSIGNED_PERSON'); ?></label>
                        <input type="text" class="form-control" id="user_name" placeholder="Allocated Person Name">
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="" class="control-label"><?= load_message('FUNDED_BY'); ?></label>
                    <select name="funded_by" id="funded_by" class="form-control select2">
                        <option value="" selected="selected"><?php echo load_message('SELECT_FUNDED_BY'); ?></option>
                        <?php
                        foreach ($org_list as $org_list) {
                            $organization_name = $org_list->organization_name;
                            ?>
                            <option value="<?php echo $organization_name; ?>"><?php echo $organization_name; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('LIFE_TIME_EXPIRE'); ?></label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control date-picker" id="lifetime"
                                   placeholder="<?= load_message('LIFE_TIME_EXPIRE'); ?>">
                        </div>
                    </div>
                </div>
                <?php
                if ($user_center == 1 && (get_user_role_name() == "DEVELOPER" || get_user_role_name() == "NIPORT_ADMIN" || get_user_role_name() == "SYSTEM_ADMIN")) {
                    ?>
                    <div class="col-md-4">
                        <label class="control-label"><?= load_message('OFFICE'); ?></label>
                        <select name="office_id" id="office_id" class="form-control select2">
                            <option value="" selected="selected">Select Office</option>
                            <?php
                            foreach ($office_list as $olist) {
                                $office_id = $olist->office_id;
                                $office_name = $olist->office_name;
                                ?>
                                <option value="<?php echo $office_id; ?>"><?php echo $office_name; ?></option>
                                <?php
                            }
                            ?>

                        </select>
                    </div>
                        <?php
                    } else {
                        ?>
                    <div class="col-md-4">
                        <input type="hidden" name="office_id" id="office_id" value="<?php echo $user_center; ?>"/>
                        <label class="control-label"><?= load_message('OFFICE'); ?></label>
                        <select name="office_id" id="office_id" class="form-control select2">
                            <option value="" selected="selected">Select Office</option>
                            <?php
                            foreach ($office_list as $olist) {
                                $office_id = $olist->office_id;
                                $office_name = $olist->office_name;
                                if ($user_center == $office_id) {
                                    ?>
                                    <option value="<?php echo $office_id; ?>" selected="selected"
                                    ><?php echo $office_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <?php
                }
                ?>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <div class="input-group">

                            <button class="btn btn-primary btn-md" onclick="return report_asset();" style="width: 150px">
                                <span class="fa fa-bar-chart"></span> Report
                            </button>
                        </div>
                    </div>

                </div>
            </div>





        </div>


    </div><!-- box body -->


    <div class="box box-success" overflow-y: auto;>
        <div id="abc">
            <p>&nbsp;</p>
            <p align="center"><?php echo load_message('REPORT'); ?></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>

    </div><!-- box -->

    <!-- ------------Modal for Asset edit Start--------------- -->

    <!-- /.modal -->
    <div class="modal fade" id="modal-asset-details">
        <div class="modal-dialog" style="width:850px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="asset_details"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-asset_id=""
                            class="btn btn-success pull-left single-asset-detail-print-modal"><span
                                class="fa fa-print"></span> <?php echo load_message('PRINT'); ?></button>
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--    Modal for Capital Edit end here -->
</section>
<!-- /.content -->