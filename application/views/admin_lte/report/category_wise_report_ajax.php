
<!-- at a glance -->
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">Showing result for : </div>
            <?php //var_dump($rooms);?>
            <div class="box-body">
                <table class="table table-hover dataTable-full-functional">
                    <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Office</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                         $i=1;
                        if (isset($office) && is_array($office)):
                            foreach ($office as $off):
                                ?>
                                <tr>
                                    <td><?= $i++; ?></td>
                                    <td><?= $off->office_name; ?></td>
                                    <td><?= $off->category; ?></td>
                                    <td><?= $off->quantity; ?></td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>

                </table>
            </div>
        </div>

    </div>
<?php load_js("dist/js/sci/data_table_full_functional.js"); ?>