<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<div class="content">

    <!-- Search option box -->
    <div class="box box-primary" overflow-y: auto;>
        <div class="box-header">
            <h3 class="box-title"><?= load_message('SEARCH_OPTION'); ?></h3>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-4">

                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('ASSET_TITLE'); ?></label>
                        <input type="text" class="form-control" id="asset_name"
                               placeholder="<?= load_message('ASSET_TITLE'); ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="" class="control-label"><?= load_message('CATEGORY'); ?></label>
                    <?php get_category_list("category_id", "category_id", "form-control select2", load_message('ALL_CATEGORY')); ?>

                </div>
                <!-- <div class="col-md-4">
                    <label for="" class="control-label">Asset Status</label>
                    <select name="asset_status" id="asset_status" class="form-control">
                       <option value="" selected="selected">Select Status</option>
                <?php
                // foreach($sts_list as $status_list)
                //{
                //$status_id_db = $status_list->status_id;
                //$status_name_db = $status_list->status_name;
                ?>
                                <option value="<?php // echo $status_id_db;   ?>"><?php // echo $status_name_db;   ?></option>
                <?php
                // }
                ?>
                    </select>
                  </div> -->
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('FROM'); ?></label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control date-picker" id="from"
                                   placeholder="<?= load_message('FROM'); ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="" class="control-label"><?= load_message('TO'); ?></label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control date-picker" id="to"
                               placeholder="<?= load_message('TO'); ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label for="" class="control-label"><?= load_message('FUNDED_BY'); ?></label>
                    <select name="funded_by" id="funded_by" class="form-control select2">
                        <option value="" selected="selected"><?php echo load_message('SELECT_FUNDED_BY'); ?></option>
                        <?php
                        foreach ($org_list as $org_list) {
                            $organization_name = $org_list->organization_name;
                            ?>
                            <option value="<?php echo $organization_name; ?>"><?php echo $organization_name; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <?php
                if ($user_center == 1 && (get_user_role_name() == "NIPORT_ADMIN" || get_user_role_name() == "SYSTEM_ADMIN")) {
                    ?>
                    <div class="col-md-4">
                        <label class="control-label"><?= load_message('OFFICE'); ?></label>
                        <select name="office_id" id="office_id" class="form-control">
                            <option value=""><?= load_message('SELECT_OFFICE'); ?></option>
                            <?php
                            foreach ($office_list as $olist) {
                                $office_id = $olist->office_id;
                                $office_name = $olist->office_name;
                                if ($user_center == $office_id) {
                                    ?>
                                    <option value="<?php echo $office_id; ?>"
                                            selected="selected"><?php echo $office_name; ?></option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="<?php echo $office_id; ?>"><?php echo $office_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <?php
                }

                ?>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('LIFE_TIME_EXPIRE'); ?></label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control date-picker" id="lifetime"
                                   placeholder="<?= load_message('LIFE_TIME_EXPIRE'); ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <div class="input-group">

                            <button class="btn btn-primary btn-md" onclick="return report_store_asset();"
                                    style="width: 150px"><span class="fa fa-bar-chart"></span> Report
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- box body -->
    </div>   <!-- box -->

    <!-- Search result box -->
    <div class="box box-success" overflow-y: auto;>
        <div id="abc">
            <p>&nbsp;</p>
            <p align="center">Please select any parameter from above span!</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </div><!--box -->
</div>
    <!-- /.box -->
</section>