<div class="box-body table-responsive" >
            <table id="example1" class="table table-hover table-bordered table-striped dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="120"><?php echo load_message('ASSET_ID_L'); ?></th>
                    <th width="110"><?php echo load_message('CATEGORY'); ?></th>
                    <th width="120"><?php echo load_message('ASSET_LABEL_ID'); ?></th>
                    <th width="100"><?php echo load_message('ALLOCATED_BY'); ?></th>
                    <th width="100"><?php echo load_message('ALLOCATED_TO'); ?></th>
                    <th width="100"><?php echo load_message('ALLOCATED_FOR'); ?></th>
                    <th width="100"><?php echo load_message('LOCATION'); ?></th>
                    <th width="100"><?php echo load_message('STATUS'); ?></th>
                    <th width="100"><?php echo load_message('REMARKS'); ?></th>
                    <th width="100"><?php echo load_message('ALLOCATED_DATE'); ?></th>
                    <th width="100"><?php echo load_message('RETURN_DATE'); ?></th>
                </tr>

                </thead>
                <tbody id="abc">
                <?php
                $i = 1;
                if ($allocation_list) {

                    foreach ($allocation_list as $aslist) {
                        $asset_id = $aslist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $category_id = $aslist->category_id;
                        $asset_label_id = $aslist->asset_label_id;
                        $allocated_by = $aslist->allocated_by;
                        $allocate_to = $aslist->allocate_to;
                        $allocate_office = $aslist->allocate_office;
                        $allocate_state = $aslist->allocate_state;
                        $remarks = $aslist->remarks;
                        $sci_id = $aslist->sci_id;
                        $on_behalf = $aslist->on_behalf;
                        $asset_location = $aslist->asset_location;
                        $create_date = $aslist->create_date;

                        $update_date = $aslist->update_date;
                        ?>

                        <tr class="asset-detail" data-toggle="modal" data-asset_id="<?=$asset_id;?>" data-target="#modal-asset-details"
                            onclick="return asset_details('<?php echo $encrypt_asset; ?>');" style="cursor: pointer;">

                            <td><?php echo $i; ?></td>
                            <td>
                                <?php echo get_asset_name($asset_id); ?>
                                <?php //echo get_asset_name($asset_id);  ?></td>
                            <td><?php echo $asset_label_id; ?></td>
                            <td><?php echo get_parent_category_name($category_id);  echo ' > '; get_category_name($category_id);?></td>
                            <td><?php echo $sci_id; ?></td>
                            <td><?php echo get_user_fullname($allocated_by); ?></td>
                            <td><?php echo get_user_fullname($allocate_to); ?></td>
                            <td><?php echo get_user_fullname($on_behalf); ?></td>
                            <td><?php echo get_office_room($asset_location); ?></td>
                            <td><?php echo $allocate_state; ?></td>
                            <td><?php echo $remarks; ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                            <td><?php echo get_format_date($update_date); ?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
      </div>
<!-- Bootstrap Datatables -->
<?php load_js("bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"); ?>
<?php load_js("dist/datatables/datatables.min.js"); ?>
<?php load_js("dist/datatables/buttons.print.min.js"); ?>

<!-- //data table Full functional options -->
<?php load_js("dist/js/sci/data_table_full_functional.js"); ?>


