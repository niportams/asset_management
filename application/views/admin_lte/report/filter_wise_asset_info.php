<div class="box-body table-responsive">

            <table id="example1" class="table table-bordered table-hover table-striped dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                    <th width="120"><?php echo load_message('ASSET_ID_L'); ?></th>
                    <th width="120"><?php echo load_message('DESCRIPTION'); ?></th>
                    <th width="110"><?php echo load_message('CATEGORY'); ?></th>
                    <th width="120"><?php echo load_message('ASSET_LABEL_ID'); ?></th>
                    <th width="100"><?= load_message('OFFICE'); ?></th>
                    <th width="100"><?php echo load_message('ASSIGNED_TO'); ?></th>
                    <th width="100"><?php echo load_message('MODEL_NAME'); ?></th>
                    <th width="90"><?php echo load_message('STATUS'); ?></th>
                    <th width="120"><?php echo load_message('REGISTRATION_DATE'); ?></th>
                </tr>
                </thead>
                <tbody>

                <?php
                $i = 1;
                if ($asset_info) {
                    foreach ($asset_info as $aslist) {
                        $asset_id = $aslist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $asset_description = $aslist->asset_description;
                        $category_id = $aslist->category_id;
                        $asset_read_id = $aslist->asset_readable_id;
                        $assigned_to = $aslist->assigned_to;
                        $office_id = $aslist->office_id;
                        $model_name = $aslist->model_name;
                        $model_no = $aslist->model_no;
                        $label_id = $aslist->sci_id;
                        $status = $aslist->asset_status_id;
                        $create_date = $aslist->create_date;
                        ?>
                        <tr class="asset-detail"  data-toggle="modal" data-asset_id="<?=$asset_id;?>" data-target="#modal-asset-details"
                            onclick="return asset_details('<?php echo $encrypt_asset; ?>');" style="cursor: pointer;">
                            <td><?php echo $i; ?></td>
                            <td>   <?php echo get_asset_name($asset_id); ?>
                            </td>
                            <td><?php echo $asset_read_id; ?></td>
                            <td><?php echo $asset_description; ?></td>
                            <td><?php echo get_parent_category_name($category_id);   echo ' > '; get_category_name($category_id);?></td>
                            <td><?php echo $label_id; ?></td>
                            <td><?php echo get_office_name($office_id); ?></td>
                            <td><?php echo get_user_fullname($assigned_to); ?></td>
                            <td><?php echo $model_name; ?>-<?php echo $model_no; ?></td>
                            <td align="center"><?php
                                if ($status == 1) {
                                    echo "<span class='label label-success'>Free</span>";
                                } else {
                                    ?>
                                    <span class='label label-warning'><?php echo get_asset_status($status); ?></span>
                                    <?php
                                }
                                ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>

            </table>

        </div><!-- /.box-body -->

<!-- Bootstrap Datatables -->
<?php load_js("bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"); ?>
<?php load_js("dist/datatables/datatables.min.js"); ?>
<?php load_js("dist/datatables/buttons.print.min.js"); ?>

<!-- //data table Full functional options -->
<?php load_js("dist/js/sci/data_table_full_functional.js"); ?>
