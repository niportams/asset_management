<section class="content">
    <div class="box box-primary">
        <div class="box-header"><?php echo load_message('NIPORT_INSTITUTES') ?></div>
        <div class="box-body">

            <div class="col-md-6 col-md-offset-3">
                <div class="col-md-10">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="" ><?php echo load_message('NIPORT_INSTITUTES') ?></label>

                            <select class="form-control select2" id="rtc-id">
                                <?php var_dump($office_list); ?>
                                <option value="-1" selected="selected" >Please select</option>
                                <?php if (isset($office_list) && is_array($office_list)): ?>
                                    <?php foreach ($office_list as $list): ?>
                                        <?php if ($this->session->userdata('user_center') == 1): ?>
                                            <option value="<?= $list->office_id; ?>" ><?= $list->office_name; ?></option>
                                            <?php
                                        else:
                                            if ($list->office_id == $this->session->userdata('user_center')):
                                                ?>
                                                <option value="<?= $list->office_id; ?>" ><?= $list->office_name; ?></option>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="" >Person</label>
                            <select class="form-control select2" id="rtc-person-id" disabled=""></select>
                        </div>
                    </div>
                </div>

                <div class="col-md-1">
                    <div class="form-group">
                        <label for="" >&nbsp;</label><br>
                        <button type="button" class="btn btn-primary" id="rtc-search"><span class="fa fa-bar-chart"></span> Report</button>
                    </div>
                </div>
            </div>

        </div>

    </div><!-- panel -->

    <div class="row" id="report-view">

    </div>

</section>
<script>
    $(document).ready(function () {

        $("#rtc-id").change(function () {
            $.ajax({
                type: "POST",
                data: {rtc_id: $("#rtc-id").val()},
                url: globalserver + 'ajax/get_person_by_office_id',
                success: function (result) {

                    //console.log(result);
                    $("#rtc-person-id").empty();
                    $("#rtc-person-id").append(result);
                    $("#rtc-person-id").removeAttr('disabled');
                }
            });
        });

        $("#rtc-search").click(function () {

            if ($("#rtc-id").val() === '-1')
            {
                sci_alert("Please select a facility");
            }
            else if ($("#rtc-room-id").val() === '-1')
            {
                sci_alert("Please select a Person");
            }
            else
            {
                $.ajax({
                    type: "POST",
                    data: {
                        rtc_id: $("#rtc-id").val(),
                        rtc_room_id: $("#rtc-person-id").val()
                    },
                    url: globalserver + 'report/rtc_person_wise_report',
                    success: function (result) {

                        //console.log(result);
                        $("#report-view").html(result);
                    }
                });
            }
        });
    });
</script>
<style>
    tbody tr{
        cursor: pointer;
    }
</style>