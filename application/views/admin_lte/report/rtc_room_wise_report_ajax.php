
<!-- at a glance -->
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header"><!--Showing result for :--> </div>
            <?php //var_dump($rooms);?>
            <div class="box-body">
                <table class="table table-hover dataTable-full-functional">
                    <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Asset Name</th> 
                            <th>Category</th> 
                            <th>Label ID</th> 
                            <th>Allocated To</th> 
                            
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                         $i=1;
                        if (isset($rooms) && is_array($rooms)):
                            foreach ($rooms as $room):
                                ?>
                                <tr data-asset_id="<?= $room->asset_id; ?>" data-toggle="modal" data-target="#modal-asset-details" onclick="return asset_details('<?= base64_encode($room->asset_id);?>');">
                                    <td><?= $i++; ?></td>
                                    <td><?= $room->asset_name; ?></td> 
                                    <td><?= $room->category; ?></td>
                                    <td><?= $room->asset_label_id; ?></td>
                                    <td><?= $room->fullname; ?></td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>

                </table>
            </div>
        </div>

    </div>

<!-- /.modal -->
        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
<?php load_js("dist/js/sci/data_table_full_functional.js"); ?>