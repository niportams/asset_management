<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->

<section class="content">

    <!-- Default box -->
    <div class="box box-primary" overflow-y: auto;>

         <div class="box-header">
            <h3 class="box-title"><?= load_message('INST_ASSET_VALUE'); ?></h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="box-body ">
                <div class="col-md-12">
                    <p> <strong>Asset Count:</strong> This column is only showing the number of asset which has purchase price setup. </p>
                    <br>
                    <?php if (is_array($asset_value) && sizeof($asset_value) > 0): ?>
                        <table class="table table-hover table-striped dataTable-full-functional">
                            <thead>
                                <tr>
                                    <th>Institute</th>
                                    <th class="text-right">Asset Count <sup class="text-danger">*</sup></th>
                                    <th class="text-right"> Approximate Value (BDT.)</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $asset_val = 0;
                                $asset_count = 0; ?>
    <?php foreach ($asset_value as $asset): ?>
                                    <tr>

        <?php $asset_val += $asset->asset_value;
        $asset_count +=$asset->total_asset; ?>
                                        <td><?= $asset->office_name; ?></td>
                                        <td align="right"><?= $asset->total_asset; ?></td>
                                        <td align="right"><?= $asset->asset_value; ?></td>

                                    </tr> 
    <?php endforeach; ?>
                                <tr style="background-color: #DDD; font-weight: bold">
                                    <td>Total NIPORT Asset </td>
                                    <td align="right"><?= $asset_count; ?></td>
                                    <td align="right"><?= $asset_val ?></td>

                                </tr> 
                            </tbody>


                        </table>
<?php endif; ?>
                </div>
            </div>
        </div><!-- box body end -->

    </div>
    <!-- end of search panel box -->

</section>
<!-- /.content -->