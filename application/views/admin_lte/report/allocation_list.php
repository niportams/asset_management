<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->

<section class="content">

    <!-- search panel box -->
    <!-- Default box -->
    <div class="box box-primary" overflow-y: auto;>

        <div class="box-header">
            <h3 class="box-title"><?= load_message('SEARCH_OPTION'); ?></h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="box-body ">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('ASSET_TITLE'); ?></label>
                        <input type="text" class="form-control" id="asset_name" placeholder="Product Title">
                    </div>
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('ASSET_ID_L'); ?></label>
                        <input type="text" class="form-control" id="asset_label" placeholder="Asset ID">
                    </div>

                </div>

                <div class="col-md-4">
                    <label for="" class="control-label"><?= load_message('CATEGORY'); ?></label>
                    <?php get_category_list("category_id", "category_id", "form-control select2", load_message('SELECT_CATEGORY')); ?>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('ASSIGNED_PERSON'); ?></label>
                        <input type="text" class="form-control" id="user_name" placeholder="Allocated Person Name">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="" class=" control-label"><?= load_message('FROM'); ?></label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control date-picker" id="from" placeholder="From">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="" class="control-label"><?= load_message('TO'); ?></label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control date-picker" id="to" placeholder="To">
                    </div>
                </div>
                <div class="col-md-3">
                    <?php
                    if ($user_center == 1 && (get_user_role_name() == "NIPORT_ADMIN" || get_user_role_name() == "SYSTEM_ADMIN")) {
                        ?>
                        <label class="control-label"><?= load_message('OFFICE'); ?></label>
                        <select name="office_id" id="office_id" class="form-control select2">
                            <option value="selected">Select Office</option>
                            <?php
                            foreach ($office_list as $olist) {
                                $office_id = $olist->office_id;
                                $office_name = $olist->office_name;
                                ?>
                                  <option value="<?php echo $office_id; ?>"><?php echo $office_name; ?></option>
                                <?php
                               
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>

                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <div class="input-group">

                            <button class="btn btn-primary btn-md"  onclick="return report_allocation_asset();" style="width: 150px" ><span class="fa fa-bar-chart"></span> Report</button>
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- box body end -->

    </div>
    <!-- end of search panel box -->


    <!-- Result box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- /.box-header -->
		<div id="abc">
            <p>&nbsp;</p>  
            <p align="center"><?php echo load_message('REPORT');?></p>
            <p>&nbsp;</p>  
            <p>&nbsp;</p>   
        </div>

        <!-- /.box-body -->

        <!-- ------------Modal for Asset edit Start--------------- -->

        <!-- /.modal -->
        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Asset Details Information</h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!--    Modal for Capital Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->