<?php if (isset($result) && is_array($result)): ?>
    <table class="table table-dark dataTable-full-functional">
        <thead>
            <tr>
                <th>Asset Name</th> 
                <th>Asset Label</th>
                <th>Category</th>
                <th>Allocated To</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach($result as $res):?>
            <tr onclick="return asset_details('MjEzMjA=');" data-toggle="modal" data-target="#asset-detail-modal">
                <td><?=$res->asset_name;?></td> 
                <td><?=$res->sci_id;?></td>
                <td><?=$res->category_name;?></td>
                <td><?=$res->fullname;?></td>
            </tr>
            <?php endforeach;?>
        </tbody>

    </table>

    <?php

else:
    echo "Sorry! Nothing Found";
endif;
load_js("dist/js/sci/data_table_full_functional.js"); 

?>