<!-- at a glance -->
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header"></div>

                <div class="box-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Status</th> 
                                <th><div class="text-right">Total</div></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            // var_dump($status);
                            if (isset($status) && is_array($status)):
                                $total =0;
                                foreach ($status as $stat):
                                    $total +=$stat->total;
                                    ?>
                                    <tr class="status-detail" data-status="<?= $stat->status_name; ?>">
                                        <td><?= $stat->status_name; ?></td> 
                                        <td><div class="text-right"><?= $stat->total; ?></div></td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                                    
                            <tr>
                                <td><strong>Total Asset</strong></td> 
                                <td><div class="text-right"><strong><?= $total; ?></strong></div></td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>

        </div>

        <!-- asset list -->
        <div class="col-md-6" >
            <div class="box box-info">
                <div class="box-header"></div>
                <div class="box-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Asset</th> 
                                <th><div class="text-right">Total</div></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($category) && is_array($category)):
                                foreach ($category as $cat):
                                    ?>
                            <tr class="asset-detail cursor_pointer" data-cat_id="<?=$cat->category_id;?>" data-cat_name="<?=$cat->category;?>" >
                                        <td><?= $cat->category; ?></td> 
                                        <td><div class="text-right"><?= $cat->total; ?></div></td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div><!-- row -->
</section>

<?php //load_js("dist/js/sci/data_table_full_functional.js"); ?>
<script>
    $(document).ready(function () {
        $(".status-detail").click(function () {
            //$('#status-detail .box-header').html($(this).data('status') + ' Asset(s)');
            
            title=$(this).data('status') + ' Asset(s)'
            current_tr = $(this);

            $.ajax({
                type: "POST",
                data: {action: 'status',action_status:$(this).data('status'),rtc_id:$("#rtc-id").val()},
                url: globalserver + 'report/rtc_quick_report',
                success: function (resp) {
                    //$('#status-detail .box-body').html(resp);
                    //current_tr.closest('tr').after('<tr><td>'+resp+'<td></tr>');
                    $("#common-modal-title").html(title)
                    $("#common-modal-body").html(resp)
                    $("#common-modal").modal('show');

                }
            });
        });
        
        
        $(".asset-detail").click(function () {
            //$('#status-detail .box-header').html($(this).data('status') + ' Asset(s)');
            
            title=$(this).data('cat_name') + '(s)'
            current_tr = $(this);

            $.ajax({
                type: "POST",
                data: {action: 'cat_status',action_status:$(this).data('cat_id'),rtc_id:$("#rtc-id").val()},
                url: globalserver + 'report/rtc_quick_report',
                success: function (resp) {
                    //$('#status-detail .box-body').html(resp);
                    //current_tr.closest('tr').after('<tr><td>'+resp+'<td></tr>');
                    $("#common-modal-title").html(title)
                    $("#common-modal-body").html(resp)
                    $("#common-modal").modal('show');

                }
            });
        });

    });


</script>

<style>
    .status-detail{
        cursor: pointer;
    }
</style>