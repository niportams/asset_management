<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <?php
    $months = array(
        'Please select', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    );
    $selected_year = isset($_POST['exp_year']) && $_POST['exp_year']!=-1 ? $_POST['exp_year'] : date('Y');
    $selected_month = isset($_POST['exp_month']) ? $_POST['exp_month'] : '0';
    ?>
    <!-- Search option box -->
    <div class="box box-primary">
         <div class="box-header">
            <h3 class="box-title"><?= load_message('SEARCH_OPTION'); ?></h3>
        </div>

        <div class="box-body">
            <form action="" method="post">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exp_year" class="control-label"><?= load_message('YEAR'); ?></label>
                            <select class="form-control select2" name="exp_year" id="exp_year">
                                <option value="-1" selected="selected">Please Select</option>
                                <?php
                                for ($i = date('Y')+20; $i >= 1970; $i--) {
                                    
                                    if($i==$selected_year){
                                        $sel='selected="selected"';
                                    }
                                    else{
                                        $sel='';
                                    }
                                    echo '<option '.$sel.'>' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exp_month" class="control-label"><?= load_message('MONTH'); ?></label>
                            <select class="form-control select2" name="exp_month" id="exp_month">

                                <?php
                                foreach ($months as $key => $val) {
                                    if($key==$selected_month){
                                        $sel='selected="selected"';
                                    }
                                    else{
                                        $sel='';
                                    }
                                    echo '<option value="' . $key . '" '.$sel.'>' . $val . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="" class="control-label">&nbsp;</label>
                            <button type="submit" name="go" class="form-control btn btn-primary" ><span class="fa fa-bar-chart"></span> Report</button>

                        </div>
                    </div>
                </div>
            </form>
        </div><!-- box body -->
    </div>   <!-- box -->

    <!-- Search result box -->
    <div class="box box-success" overflow-y: auto;>

         <div class="box-header">
            <strong>Asset Lifetime Expired in : <?php echo ($selected_month!='0')?$months[$selected_month].', ':'',$selected_year; ?></strong>
        </div>
        <div class="box-body">
            <table class="table table-bordered sci-table sci-table-blue dataTable-full-functional"> <!-- table-striped table-hover -->
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th width="120"><?php echo load_message('ASSET_TITLE'); ?></th>
                        <th width="110"><?php echo load_message('CATEGORY'); ?></th>
                        <th width="70"><?php echo load_message('ASSET_LABEL_ID'); ?></th>
                        <th width="90"><?php echo load_message('ASSET_ID_L'); ?></th>
                        <th width="100"><?php echo load_message('WARRENTY_EXP_DATE'); ?></th>
                        <th width="70"><?php echo load_message('ASSET_LIFETIME'); ?></th>
                        <th width="70"><?php echo load_message('PURCHASE_DATE'); ?></th>
                        <th width="100"><?php echo load_message('REGISTRATION_DATE'); ?></th>
                        <th width="50"><?php echo load_message('STATUS'); ?></th>
                    </tr>
                </thead>
                <tbody id="abc">
                    <?php
                    $i = 1;
                    if ($asset_list <> "") {
                        foreach ($asset_list as $aslist) {
                            $asset_id = $aslist->asset_id;
                            $encrypt_asset = base64_encode($asset_id);
                            $category_id = $aslist->category_id;
                            $category_name = $aslist->category_name;
                            $asset_name = $aslist->asset_name;
                            $office_id = $aslist->office_id;

                            $model_name = $aslist->model_name;
                            $model_no = $aslist->model_no;
                            $grn_no = $aslist->grn_no;
                            $sku = $aslist->sku;
                            $purchase_no = $aslist->purchase_no;
                            $purchase_date = $aslist->purchase_date;

                            $status_id = $aslist->asset_status_id;
                            $status_name = $aslist->status_name;
                            //$purchase_date = date("Y-m-d", strtotime($purchase_date));
                            $label_id = $aslist->asset_readable_id;
                            $sci_id = $aslist->sci_id;
                            $warrenty_exp_date = $aslist->warrenty_date;
                            $lifetime_date = $aslist->asset_lifetime;
                            $create_date = $aslist->create_date;

                            //$create_date=date("Y-m-d", strtotime($create_date));
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <div id="permission_details<?php $i; ?>" onmouseover="call_permission('<?php echo $asset_id; ?>', '<?php echo $i; ?>');" 
                                         onmouseout="free_permission('<?php echo $asset_id; ?>', '<?php echo $i; ?>');" >
                                        <?php echo $asset_name; ?></div>
                                    <div class="float-bar" id="abc<?php echo $i; ?>"></div>
                                </td>
                                <td><?php echo @$category_name; ?>
                                    <div class="small"><?php
                                get_parent_category_name($category_id);
                                echo ' > ' . @$category_name;
                                        ?></div>
                                </td>

                                <td><?php echo $sci_id; ?></td>
                                <td><?php echo $label_id; ?></td>
                                <td><?php
                                    if ($warrenty_exp_date <> NULL) {
                                        echo get_format_date($warrenty_exp_date);
                                    }
                                    ?></td>
                                <td><?php
                                    if ($lifetime_date <> NULL) {
                                        echo get_format_date($lifetime_date);
                                    }
                                    ?></td>
                                <td><?php echo get_format_date($purchase_date); ?></td>
                                <td><?php echo get_format_date($create_date); ?></td>
                                <td><?php label($status_name); ?></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div><!--box -->
    <?php //var_dump($asset_list);  ?>

    <!-- ------------Modal for Asset edit Start--------------- -->
    <div class="modal fade" id="modal-asset">
        <div class="modal-dialog" style="width:850px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_EDIT'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="asset_edit"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left"
                            data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="modal fade" id="modal-asset-details">
        <div class="modal-dialog" style="width:850px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO'); ?></h4>
                </div>
                <div class="modal-body">
                    <div id="asset_details"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT'); ?></button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!--    Modal for Capital Edit end here -->
</section>

