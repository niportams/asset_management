<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header"><?php echo load_message('NIPORT_INSTITUTES') ?></div>
                <div class="box-body">

                    <div class="col-md-6 col-md-offset-3">
                        <div class="col-md-8">
                            <div class="col-md-12">
                                <div class="form-group" align="justify">
                                    <label for="exampleInputName"
                                           class="control-label"><?php echo load_message('CATEGORY'); ?><span
                                                class="text-danger">*</span></label>
                                    <?php get_category_list("category_id", "category_id", "form-control select2", load_message('SELECT_CATEGORY')); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="">&nbsp;</label><br>
                                <button type="button" class="btn btn-primary" id="rtc-search"><span
                                            class="fa fa-bar-chart"></span> Report
                                </button>
                            </div>
                        </div>
                    </div>

                </div>

            </div><!-- panel -->

            <div class="row" id="report-view">

            </div>
        </div>
    </div>
</div>

</section>
<script>
    $(document).ready(function () {


        $("#rtc-search").click(function () {

            if ($("#category_id").val() === '-1') {
                sci_alert("Please select an item");
            }
            else {
                $.ajax({
                    type: "POST",
                    data: {
                        cat_id: $("#category_id").val()
                    },
                    url: globalserver + 'report/category_wise_report',
                    success: function (result) {

                        console.log(result);
                        $("#report-view").html(result);
                    }
                });
            }
        });
    });
</script>
<style>
    tbody tr {
        cursor: pointer;
    }
</style>