<div class="box-body table-responsive">
    <table id="export-table" class="table table-bordered table-striped dataTable-full-functional">
        <thead>
        <tr>
            <th width="10">#</th>
            <th width="120"><?= load_message('ASSET_TITLE'); ?></th>
            <th width="110"><?= load_message('CATEGORY'); ?></th>
            <th width="100"><?= load_message('QUANTITY'); ?></th>
        </tr>
        </thead>
        <tbody id="abc">

        <?php
        $i = 1;
        if ($asset_list) {
            foreach ($asset_list as $aslist) {
                $parent_id = $aslist->parent_id;
                $category_name = $aslist->category_name;
                $asset_name = $aslist->asset_name;
                $quantity = $aslist->quantity;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $asset_name; ?></td>
                    <td><?php echo get_category_name($parent_id); echo " > ". $category_name;?></td>
                    <td><?php echo $quantity; ?></td>
                </tr>
                <?php
                $i++;
            }
        }
        ?>

        </tbody>
        <!--                <tfoot>
                            <tr>
                                <th width="10">#</th>
                                <th width="120">Asset Name</th>
                                <th width="110">Category Name</th>
                                <th width="100">Quantity</th>
                            </tr>
                        </tfoot>-->
    </table>
</div><!-- box body-->