<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success" overflow-y: auto;>
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table of Department</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body table-responsive" id="office_list">
            <table id="example1" class="table table-bordered table-striped table-hover dataTable-full-functional">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="100"><?php echo load_message('PR_ITEM'); ?></th>
                    <th width="124"><?php echo load_message('PR_QUANTITY');?></th>
                    <th width="124"><?php echo load_message('PR_USER'); ?></th>
                    <th width="124"><?php echo load_message('PR_REFERENCE'); ?></th>
                    <th width="124"><?php echo load_message('PR_REMARK'); ?></th>
                    <th width="124"><?php echo load_message('CREATE_DATE'); ?></th>
                    <th width="70"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($pr_list <> "") {
                    foreach ($pr_list as $pr_list) {
                        $pr_id = $pr_list->id;
                        $title = $pr_list->title;
						$quantity = $pr_list->quantity;
						$remark = $pr_list->remark;
						$userid = $pr_list->userid;
						$reference_no = $pr_list->reference_no;
						$office_id = $pr_list->office_id;
                        $create_date = $pr_list->created_date;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $title; ?></td>
                            <td><?php echo $quantity; ?></td>
                            <td><?php echo get_user_fullname($userid); ?></td>
                            <td><?php echo $reference_no; ?></td>
                            <td><?php echo $remark; ?></td>
                            <td><?php echo get_format_date($create_date); ?></td>
                            <td>-</td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->


       
        <!-- /.modal -->
    </div>
    <!--    Modal for Role Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->