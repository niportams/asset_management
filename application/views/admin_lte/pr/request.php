<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->

<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">PR Request</h3>

            <div class="box-tools pull-right">
                <input type="button" value="Add a new Asset" class="btn btn-success add" id="add"/>

            </div>
        </div>
        <form id="pr_request">
            <div class="box-body form-horizontal" id="asset_requsition_form">
                <div class="col-md-8 col-md-offset-2 form_shape">
                    <label class="control-label">Title<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="title">
                    <div id="buildyourform">
                        <div id="field1">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Asset Name<span class="text-danger">*</span></label>
                                    <input class="form-control" id="asset_name" type="text" name="asset_name">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Asset Quantity<span
                                                class="text-danger">*</span></label>
                                    <input class="form-control" type="text" id="asset_quantity" name="asset_quantity">
                                </div>
                                <div class="col-md-2">
                                    <a class="remove btn btn-danger" style="margin-top:25px" disabled>
                                        <i class="fa fa-trash-o"> </i>
                                    </a>
                                    <a class="btn btn-success add" style="margin-top:25px">
                                        <i class="fa fa-plus"> </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <label class="control-label">Remarks</label>
                    <textarea class="form-control" type="textarea" id="remark" name="remark"></textarea>
                    <br><br>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <div align="center">
                    <button class="btn btn-primary"
                            onclick="handle_form('pr_request', pr_request, 'ajax2/pr_request_submit',0,'serialize')">
                        Submit
                    </button>
                </div>
            </div>
        </form>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->

    