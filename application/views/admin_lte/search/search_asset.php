<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->
<!-- Main content -->
<section class="content">
    <form class="search-form" action="<?php base_url(); ?>search" method="get">
        <div class="input-group">
            <input name="q" class="form-control" value="<?php echo $search_q; ?>" placeholder="Search" type="text">

            <div class="input-group-btn">
                <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-search"></i>
                </button>
            </div>
        </div>
        <!-- /.input-group -->
    </form>
    <!-- Default box -->
    <br>
    <div class="box">
        <!-- <div class="box-header">
           <h3 class="box-title">Data Table of Asset Allocation</h3>
         </div>-->
        <!-- /.box-header -->
        <div class="box-body" id="allocate_list">
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th width="204">Asset Name</th>
                    <th width="100">Category</th>
                    <th width="100">Asset ID</th>
                    <th width="145">Registered date</th>
                    <th width="124">Capital ID</th>
                    <th width="124">Label ID</th>
                    <th width="120">Status</th>
                    <th width="90">Office</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($alloc_list <> "") {
                    foreach ($alloc_list as $aclist) {
                        // $allocate_id = $aclist->allocate_id;
                        $asset_id = $aclist->asset_id;
                        $encrypt_asset = base64_encode($asset_id);
                        $asset_readable_id = $aclist->asset_readable_id;
                        $category_id = $aclist->category_id;
                        $create_date = $aclist->create_date;
                        $capital_id = $aclist->capital_id;
                        $label = $aclist->sci_id;
                        $status = $aclist->asset_status_id;
                        $Office = $aclist->office_id;
                        ?>
                        <tr>
                            <td align="center"><?php echo $i; ?></td>
                            <td><a href="#" title="Details" data-toggle="modal" data-target="#modal-asset-details"
                                   onclick="return asset_details('<?php echo $encrypt_asset; ?>');"><?php echo get_asset_name($asset_id); ?></a>
                            </td>
                            <td><?php echo get_category_name($category_id); ?></td>
                            <td><?php echo $asset_readable_id; ?></td>
                            <td><?php echo $create_date; ?></td>
                            <td><?php echo $capital_id; ?></td>
                            <td><?php echo $label; ?></td>
                            <td><?php echo get_asset_status($status); ?></td>
                            <td><?php echo get_office_name($Office); ?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <th width="10">#</th>
                    <th width="204">Asset Name</th>
                    <th width="100">Category</th>
                    <th width="100">Asset ID</th>
                    <th width="145">Registered date</th>
                    <th width="124">Capital ID</th>
                    <th width="124">Label ID</th>
                    <th width="120">Status</th>
                    <th width="90">Office</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->

        <!--   Modal for Privilege details-->
        <div class="modal fade" id="modal-asset-details">
            <div class="modal-dialog" style="width:850px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Asset Details Information</h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!--    Modal end here -->
    <div class="clearfix"></div>
    <!--    Modal for Role Edit end here -->

    <!-- /.box -->
</section>
<!-- /.content -->