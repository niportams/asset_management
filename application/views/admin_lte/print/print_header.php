<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Requisition List</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <?php load_css('bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>
        <!-- Font Awesome -->
        <?php load_css('bower_components/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Ionicons -->
        <?php load_css('bower_components/Ionicons/css/ionicons.min.css'); ?>
        <!-- Theme style -->
        <?php load_css('dist/css/AdminLTE.min.css'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <?php load_js("dist/js/jquery-2.1.4.js"); ?>

        
        <style type="text/css" media="print">
            .table-bordered{
                border: 1px solid #171515;
                -webkit-print-color-adjust: exact; 
            }
            
            .table-bordered>thead>tr{
                
                -webkit-print-color-adjust: exact; 
                background-image: url("<?=base_url();?>/theme/dist/img/printing/gray-bg.png");
            }
            
            .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                border: 1px solid #171515;
                -webkit-print-color-adjust: exact; 
            }
            @media print
            {
                @page {
                    margin-top: 0;
                    margin-bottom: 0;
                }
                body  {
                    padding-top: 72px;
                    padding-bottom: 72px ;
                }
            }
        </style>
    </head>
    <body onload="window.print()">
        <div class="wrapper">

            <section>
                <!-- title row -->
                <div class="row">
                    <div class="col-xs-12">
                        <img src="<?php echo base_url(); ?>theme/dist/img/logo/niport_banner.png" class="img-responsive"><br>
                        <hr>
                    </div>
                    <!-- /.col -->
                </div>
            </section>