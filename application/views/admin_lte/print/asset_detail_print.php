

<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                Asset Detail information
                <small class="pull-right" id="allocate-date">Date: <?= date("d M,Y"); ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    
    <?php
foreach ($result as $adetails) :
    $asset_id = $adetails->asset_id;
    $encrypt_asset = base64_encode($asset_id);
    $capital_id = $adetails->capital_id;
    $category_id = $adetails->category_id;
    $manufacture_id = $adetails->manufacture_id;
    $asset_name = $adetails->asset_name;
    $asset_description = $adetails->asset_description;
    $office_id = $adetails->office_id;

    $model_name = $adetails->model_name;

    $model_no = $adetails->model_no;
    $grn_no = $adetails->grn_no;
    $sku = $adetails->sku;
    $serial_no = $adetails->serial_no;
    $reference_no = $adetails->reference_no;
    $purchase_order_no = $adetails->purchase_no;
    $purchase_date = $adetails->purchase_date;
    $purchase_price = $adetails->purchase_price;
    $purchase_location = $adetails->purchase_location;
    $status_id = $adetails->asset_status_id;
    $warrenty_date = $adetails->warrenty_date;
    $assigned_to = $adetails->assigned_to;

    $warrenty_date = date("d/m/Y", strtotime($warrenty_date));
    $purchase_date = date("d/m/Y", strtotime($purchase_date));
    //$purchase_date=date("Y-m-d", strtotime($purchase_date));
    //$date = strtotime($date);
    //$date = date('d-m-Y', $date);

    $label_id = $adetails->label_id;
    $read_id = $adetails->asset_readable_id;
    $sci_id = $adetails->sci_id;
    $db_status_id = $adetails->asset_status_id;
    $asset_remarks = $adetails->asset_remarks;

?>

    <table class="table">

        <tr>
            <td><strong>
                    <?= load_message('ASSET_TITLE'); ?>:
                </strong></td>
            <td><?= $asset_name; ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('CATEGORY'); ?>:
                </strong></td>
            <td><?= get_category_name($category_id); ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('ASSET_ID_L'); ?>:
                </strong></td>
            <td><?php echo $read_id; ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('ASSET_LABEL_ID'); ?>:
                </strong></td>
            <td><?= $sci_id; ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('MODEL_NAME'); ?>:
                </strong></td>
            <td><?= $model_name; ?></td>
        </tr>


        <tr>
            <td><strong>
                    <?= load_message('MODEL_NO'); ?>:
                </strong></td>
            <td><?= $model_no; ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('SERIAL_NO'); ?>:
                </strong></td>
            <td><?= $serial_no; ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('OFFICE'); ?>:
                </strong></td>
            <td><?= get_office_name($office_id); ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('DESCRIPTION'); ?>:
                </strong></td>
            <td><textarea style="width:100%; border:none"><?= $asset_description; ?></textarea></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('REF'); ?>:
                </strong></td>
            <td><?= $reference_no; ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('WARRENTY_EXP_DATE'); ?>:
                </strong></td>
            <td><?= $warrenty_date; ?></td>
        </tr>


        <tr>
            <td><strong>
                    <?= load_message('MANUFACTURER'); ?>:
                </strong></td>
            <td><?= get_manufacture_name($manufacture_id); ?></td>
        </tr>
        
        <tr>
            <td><strong>
                    <?= load_message('SKU'); ?>:
                </strong></td>
            <td><?= $sku; ?></td>
        </tr>
        
         <tr>
            <td><strong>
                    <?= load_message('PURCHASE_DATE'); ?>:
                </strong></td>
            <td><?= $purchase_date; ?></td>
        </tr>


        <tr>
            <td><strong>
                    <?= load_message('PURCHASE_PRICE'); ?>:
                </strong></td>
            <td><?= $purchase_price; ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('PURCHASE_ORDER_NO'); ?>:
                </strong></td>
            <td><?= $purchase_order_no; ?></td>
        </tr>

        <tr>
            <td><strong>
                    <?= load_message('ASSIGNED_TO'); ?>:
                </strong></td>
            <td><?= get_user_fullname($assigned_to); ?></td>
        </tr>

    </table>
    
    <?php endforeach; ?>

</section>