  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          Asset Receipt Form
          <small class="pull-right" id="allocate-date"><b>Print Date</b> <span id="print-date"><?=date("M d, Y");?></span></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
          <strong>Requested From</strong>
        <address>
          <span id="requester-name"><?=get_user_fullname($result[0]->allocate_to);?></span><br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
          <strong>Approved By</strong>
        <address>
          <span id="approver-name"><?=get_user_fullname($result[0]->approved_by);?></span><br>
         	
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
          <b>Reference No. :</b> <span id="ref"><?=$result[0]->allocation_ref;?></span><br>
        
          <b>Allocation Date:</b> <?=date("d M,Y",  strtotime($result[0]->allocation_date));?><br>
       
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    
    <?php //var_dump($result);?>
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive" style="margin-top:50px;margin-bottom:50px;">
        <table class="table table-striped table-bordered" id="item-table">
          <thead>
          <tr>
            <th>Sn.</th>
            <th>Asset</th>
            <th>Model Name</th>
            <th>Model Number</th>
            <th>Serial No.</th>
            <th>Label ID</th>
           
          </tr>
          </thead>
          <tbody>
         
              <?php if(is_array($result)): $sn=1;?>
                
                <?php foreach($result as $res):?>
                    
                    <tr>
                        <td><?=$sn++;?></td>
                        <td><?= $res->asset_name;?></td>
                        <td><?= $res->model_name;?></td>
                        <td><?= $res->model_no;?></td>
                        <td><?= $res->serial_no;?></td>
                        <td><?= $res->asset_label_id;?></td>
                        
                      </tr>
              
                <?php endforeach;?>
              
              <?php endif;?>
          
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
     
      <div class="col-md-12">
        
         <!-- received by -->
         <div class="col-sm-4 invoice-col">
             <strong>Received By</strong>
        <address>
          <span id="received-by"><?=get_user_fullname($result[0]->on_behalf);?></span><br>
          <br>
          
          <hr>
          Signature with Date
        </address>
      </div>
      <!-- End received by -->
      
      
      <!-- Delivery by -->
         <div class="col-sm-4 invoice-col">
             <strong>Allocated By</strong>
        <address>
            <span id="allocated-by"><?=get_user_fullname($result[0]->allocated_by);?></span><br>
          <br>
          
          <hr>
          Signature with Date
        </address>
      </div>
      <!-- End delivery by -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->