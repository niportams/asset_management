<?php
/****************************************************/
// Filename: category_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: category_prefix,categorylist,categorylistwithoutparent,categoryParentlist,
// categoryChildist,getparent,category_create,category_details,category_update,category_submit,
// category_update,category_delete,get_category_by_id,
//
/****************************************************/

/**
 * AMS category model class
 *
 * This method demonstrates the asset category data of AMS.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class category_model extends CI_Model {

    function __construct() {
        parent:: __construct();
        $this->load->database();
    }


    
    /****************  Get Category Prefix from a category ID  ***************** */

    function category_prefix($category_id) {

        //$this->db->select('LEFT(category_name, 4) as category_prefix', FALSE);
        $this->db->select('cat_prefix as category_prefix');
        $this->db->from('fams_category');
        $this->db->where('category_id', $category_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    //******Get Asset Category************/
    function categorylist() {

        $this->db->select('a.*,b.category_name as parrent_name');
        $this->db->from('fams_category a');
        $this->db->join('fams_category b', 'a.parent_id = b.category_id', 'left');
        //$this->db->where_in('a.parent_id','b.category_id');
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    //******Get category list with having parent id************/
    function categorylistwithoutparent() {

        $this->db->select('a.*');
        $this->db->from('fams_category a');
        $this->db->where('a.parent_id is NOT NULL');
//      $this->db->where_in('a.parent_id','b.category_id');
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

   //******Get category list without having parent id************/

    function categoryParentlist() {
        $this->db->select('*');
        $this->db->from('fams_category');
        $this->db->where('parent_id', NULL);

        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /** ************ Category Child List ***************** */

    function categoryChildist() {
        $this->db->select('*');
        $this->db->from('fams_category');
        $this->db->where('parent_id != ', 'NULL');
        //$this->db->limit(1);
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* * ************ Get child category of a parent***************** */

    function getparent($cat_id) {
        $this->db->select('*');
        $this->db->from('fams_category');
        //$this->db->where('category_id',$child_id);
        $this->db->where('parent_id', $cat_id);

        $query = $this->db->get();
        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* **********Category Create*************** */

    function category_create($category_name, $parrent_id) {
        $data = array(
            'category_id' => NULL,
            'parent_id' => $parrent_id,
            'category_name' => $category_name,
            'category_status' => 1,
        );
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->insert('fams_category', $data);
        return true;
    }

    /* **********Get Category Name from Category ID*********** */

    function category_details($category_id) {
        $this->db->select('*');
        $this->db->from('fams_category');
        $this->db->where('category_id', $category_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* *************Category Update************** */

    function category_update($category_id, $category_name, $parent_id, $category_status) 
    {
        $updateData = array(
            'category_name' => $category_name,
            'parent_id' => $parent_id,
            'category_status' => $category_status,
        );
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->where('category_id', $category_id);
        $query = $this->db->update('fams_category', $updateData);
        return $query;
    }

    /* ************Category Delete*********** */

    function category_delete($category_id) {
        $this->db->where('category_id', $category_id);
        $this->db->delete('fams_category');
    }

    //Shahed
    function get_category_by_id($id = NULL) {

        $this->db->select('*');
        $this->db->from('category');
        //$this->db->join('fams_category b', 'a.parent_id = b.category_id', 'left');
        $this->db->where('parent_id', $id);
        $this->db->order_by("category_name", "asc");
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}