<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: department_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: department_list,department_create,department_details,department_update,
// department_delete,
//
/****************************************************/

/**
 * AMS department model class
 *
 * This method demonstrates the department data of AMS.
 */
class department_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    //******Get Department List************/
    function department_list()
    {
        $this->db->order_by('department_name', 'asc'); // or 'DESC'
        $this->db->select('*');
        $this->db->from('fams_department');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********Department Create****************/
    function department_create($json_array)
    {
        /*
        $data = array(
        'department_id'=>NULL,
        'department_name' => $json_array->department_name,
        );
         */
        $this->db->insert('department', $json_array);
        //return true;
        return $this->db->insert_id();

    }

    /***********Department details from department id************/
    function department_details($department_id)
    {
        $this->db->select('*');
        $this->db->from('fams_department');
        $this->db->where('department_id', $department_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************** Department Update ***************/
    function department_update($department_id, $department_name)
    {
        $updateData = array(
            'department_name' => $department_name,
        );

        //sci_update_db('fams_department',$data,['department_id'=>$department_id])
        $this->db->where('department_id', $department_id);
        $this->db->update('fams_department', $updateData);
    }

    /********** Department Delete ************/
    function department_delete($department_id)
    {
        $this->db->where('department_id', $department_id);
        $this->db->delete('fams_department');
    }

}

?>
