<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: supplier_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: supplierlist,supplier_create,supplier_details,supplier_update,supplier_delete
// 
/****************************************************/

 /**
 * AMS Asset Supplier Model Class
 */
class supplier_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();

    }

    //****** Get Supplier list ************/
    function supplierlist()
    {

        $this->db->select('*');
        $this->db->from('fams_supplier');
        $this->db->order_by('supplier_name', 'asc'); // or 'DESC'
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********Supplier Create****************/
    function supplier_create($supplier_name, $supplier_address, $supplier_phone, $supplier_email)
    {
        $data = array(
            'supplier_id' => NULL,
            'supplier_name' => $supplier_name,
            'supplier_address' => $supplier_address,
            'supplier_phone' => $supplier_phone,
            'supplier_email' => $supplier_email,
        );
        $this->db->insert('fams_supplier', $data);
        return true;

    }

    /***********Supplier Name from supplier ID************/
    function supplier_details($supplier_id)
    {
        $this->db->select('*');
        $this->db->from('fams_supplier');
        $this->db->where('supplier_id', $supplier_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************Supplier Update***************/
    function supplier_update($supplier_id, $supplier_name, $supplier_address, $supplier_phone, $supplier_email)
    {
        $updateData = array(
            'supplier_name' => $supplier_name,
            'supplier_address' => $supplier_address,
            'supplier_phone' => $supplier_phone,
            'supplier_email' => $supplier_email,
        );
        $this->db->where('supplier_id', $supplier_id);
        $this->db->update('fams_supplier', $updateData);
    }

    /*************Category Delete************/
    function supplier_delete($supplier_id)
    {
        $this->db->where('supplier_id', $supplier_id);
        $this->db->delete('fams_supplier');
    }

}

?>
