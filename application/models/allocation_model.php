<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: allocation_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: get_allocated_file,item_details,requested_list,get_unavailable,
// get_pr,get_available_item,update_request_status,allocation_update,requested_pr_available,
// delete_tmp_image,revert_request,allocation_reference,revert_allocation,revert_asset,
// delete_allocated_files,delete_attach_file,delete_revert_files,revert_file_include,allo_file_include,
// allo_filelist,delete_alloc_files,get_revert_file,reverted_allocation_list,reverted_transfer_list,get_files
// 
/****************************************************/

 /**
 * AMS Asset Allocation Model Class
 */

class allocation_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }


    /********* Get Allocated Item  ***********/
    function get_allocated_file($requisition_no)
    {
        $this->db->select('*');
        $this->db->from('fams_allocation');
        $this->db->where('allocate_office', $this->session->userdata('user_center'));
        $this->db->where('requisition_no =', $requisition_no);
        $this->db->where('allocate_state =', "Delivered");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    /********** Item details of requested ID ************/
    function item_details($request_id)
    {
        $this->db->select('*');
        $this->db->from('fams_allocation_request');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->where('arq_id =', $request_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /************* Requested list for allocation *****************/
    function requested_list()
    {
        $this->db->select('*');
        $this->db->from('fams_allocation_request');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        //$this->db->where('allocate_state =', "Delivered");
        $this->db->group_by('ref_no');
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    } 

   /***********Unavailable requested list for pr generation************/
   function get_unavailable($ref_no)
   {

        $this->db->select('a.*,b.category_id,b.category_name');
        $this->db->from('fams_allocation_tmp a, fams_category b');
        //$this->db->where('office_id', $this->session->userdata('user_center'));
        //$this->db->where('ref_no =', $ref_no);
        $this->db->where('a.category_id = b.category_id');
        $this->db->where('item_status =', "Not Available");
        $this->db->where('created_by', $this->session->userdata('user_db_id'));

        //$this->db->group_by('ref_no');
        //$this->db->order_by('create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }
   /********** Check existing PR available **************/
   function get_pr($ref_no)
   {
        $this->db->select('*');
        $this->db->from('fams_pr_request');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->where('reference_no', $ref_no);
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }

   /************** Get Available Item of an asset  ***************/
   function get_available_item($reference_no)
   {
        $this->db->select('*');
        $this->db->from('fams_allocation_request');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->where('ref_no', $reference_no);
        $this->db->where('req_status =', "Available");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }
   /********  Allocation Request status change ***************/
   function update_request_status($request_id)
   {
    $updateData = array(
            'req_status' => "Allocated",
        );
        $this->db->where('arq_id', $request_id);
        $this->db->set('update_date', 'NOW()', FALSE);
        $query = $this->db->update('fams_allocation_request', $updateData);
      
        return $query;
   }

   /************ Allocation Update************/
   function allocation_update($allocate_id,$location_id)
   {
    $updateData = array(
            'asset_location' => $location_id,
        );
        $this->db->where('allocate_id', $allocate_id);
        //$this->db->set('update_date', 'NOW()', FALSE);
        $query = $this->db->update('fams_allocation', $updateData);
      
        return $query;
   }

   /********** Reference number of PR avaiable cheking**************/
   function requested_pr_available($reference_no,$category_id,$quantity)
   {

        $this->db->select('count(*) as available');
        $this->db->from('fams_asset');
        $this->db->where('asset_status_id = 1');
        $this->db->where('category_id', $category_id);
        $this->db->where('pr_reference_no', $reference_no);
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->having('available >=', $quantity);

        $query = $this->db->get();
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }
   /************  Delete Temporary Image file*************/
   function delete_tmp_image()
   {
       //$this->db->where('file_id', $file_id);
        $this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->delete('fams_allocation_file_tmp');
        return true;
   }

   /***********  Revert Requested allocated Item **************/
   function revert_request($request_id)
   {
    $updateData = array(
            'req_status' => "Available",
        );
        $this->db->where('arq_id', $request_id);
        $this->db->set('update_date', 'NOW()', FALSE);
        $query = $this->db->update('fams_allocation_request', $updateData);
        
        return $query;
   }

   /*********** allocation_reference  *************/
   
   function allocation_reference($reference_no,$category_id)
   {
        $this->db->select('*');
        $this->db->from('fams_allocation');
        $this->db->where('allocate_office', $this->session->userdata('user_center'));
        $this->db->where('reference_no =', $reference_no);
        $this->db->where('category_id =', $category_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }
   /************* Revert Allocation **************/
   function revert_allocation($allocate_id)
   {
    $updateData = array(
            'allocate_state' => "Cancel",
        );
        $this->db->where('allocate_id', $allocate_id);
        $this->db->set('update_date', 'NOW()', FALSE);
        $query = $this->db->update('fams_allocation', $updateData);
       
        return $query;
   }

   /************* Revert Asset State/Status **************/
   function revert_asset($asset_id)
   {
        $updateData = array(
                        'asset_status_id' => 1,
                        'assigned_to' => 0,
                    );
        $this->db->where('asset_id', $asset_id);
        //$this->db->set('update_date', 'NOW()', FALSE);
        $query = $this->db->update('asset', $updateData);
        return $query;
   }

   /*************** Delete Allocation File of a requisition/reference_no ********/
   function delete_allocated_files($reference_no)
    {
        $this->db->where('requisition_id', $reference_no);
        $this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->delete('fams_allocation_file');
        return true;
    }
  /*************** Delete one attach File of a requisition/reference_no ********/
   function delete_attach_file($file_id)
    {
        $this->db->where('file_id', $file_id);
        $this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->delete('fams_allocation_file');
        return true;
    }
    /*********  Delete allocated Revert files****************/
    function delete_revert_files($file_id)
    {
       $this->db->where('revert_id', $file_id);
        //$this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->delete('fams_revert');
        return true;
    }
    /********** Revert allocation files**************/
    function revert_file_include($allocation_id,$name,$reference_no,$purpose)
    {
      $data = array(
            'allocation_id'=>$allocation_id,
            'reference_number' => $reference_no,
            'filename' => $name,
            'revert_type' => $purpose,
            'office_id' => $this->session->userdata('user_center'),           
        );
        $this->db->set('create_date', 'CURDATE()', FALSE);
        $query = $this->db->insert('fams_revert', $data);
        return $query;
    }

    /*********** Single direct allocation file uploading***********/
    function allo_file_include($reference_no,$name)
    {
        $data = array(
            'file_name' => $name,
            'requisition_id' => $reference_no,
            'created_by' => $this->session->userdata('user_db_id'),           
        );
        $this->db->set('create_date', 'CURDATE()', FALSE);
        $query = $this->db->insert('fams_allocation_file', $data);
        return $query;

    }

    /**********Get single allocation uploaded files**********/
    function allo_filelist($reference_no)
    {
        $this->db->select('*');
        $this->db->from('fams_allocation_file');
        $this->db->where('requisition_id', $reference_no);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********  Allocation file delete *************/
    function delete_alloc_files($file_id)
    {
        $this->db->where('file_id', $file_id);
        $query = $this->db->delete('fams_allocation_file');
        return $query;

    }
    /************** Get allocation revert file  ******************/
    function get_revert_file($allocation_id,$reference_no,$purpose)
    {
        $this->db->select('*');
        $this->db->from('fams_revert');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->where('reference_number', $reference_no);
        $this->db->where('allocation_id', $allocation_id);
        $this->db->where('revert_type =', $purpose);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /************* Get Reverted Allocation List ************/
    function reverted_allocation_list()
    {

        $this->db->select('a.*,b.*');
        $this->db->from('fams_allocation a');
        $this->db->join('fams_revert b', 'a.allocate_id = b.allocation_id');
        $this->db->where('a.allocate_office', $this->session->userdata('user_center'));
        $this->db->where('a.allocate_state =', "Returned");
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    /************* Get Reverted Transfer List ************/
    function reverted_transfer_list()
    {

        $this->db->select('a.*,b.*');
        $this->db->from('fams_transfer a');
        $this->db->join('fams_revert b', 'a.transfer_id = b.allocation_id');
        $this->db->where('a.from_office', $this->session->userdata('user_center'));
        $this->db->where('a.transfer_state =', "Returned");
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }
    /************** Get Reverted Allocated files  *****************/
    function get_files($allocate_id)
    {
        $this->db->select('*');
        $this->db->from('fams_revert');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->where('allocation_id', $allocate_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }  
    }
    
}

?>
