<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: location_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: locationlist,location_create,location_details,location_update,location_delete,
//
/****************************************************/

/**
 * AMS location model class
 *
 * This method demonstrates the location data of AMS.
 */
class location_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    //******Get Location List************/
    function locationlist()
    {
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $this->db->select('*');
        $this->db->from('fams_location');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********Location Create****************/
    function location_create($location_name)
    {
        $data = array(
            'location_id' => NULL,
            'location_name' => $location_name,
        );
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->insert('fams_location', $data);
        return true;

    }

    /***********Location details from location_ID************/
    function location_details($location_id)
    {
        $this->db->select('*');
        $this->db->from('fams_location');
        $this->db->where('location_id', $location_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************Location Update***************/
    function location_update($location_id, $location_name)
    {
        $updateData = array(
            'location_name' => $location_name,
        );
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->where('location_id', $location_id);
        $this->db->update('fams_location', $updateData);
    }

    /*************Location Delete************/
    function location_delete($location_id)
    {
        $this->db->where('location_id', $location_id);
        $this->db->delete('fams_location');
    }

}

?>
