<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: manufacture_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: manufacturelist,manufacture_create,manufacture_details,manufacture_details,manufacture_update,
// manufacture_delete
/****************************************************/

/**
 * AMS manufacture model class
 *
 * This method demonstrates the manufacturer data of AMS.
 */
class manufacture_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();

    }

    //******Get Manufacture list********/
    function manufacturelist()
    {

        $this->db->select('*');
        $this->db->from('fams_manufacture');
        $this->db->order_by('manufacture_name', 'ASC'); // or 'DESC'
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********Manufacture Create****************/
    function manufacture_create($manufacture_name, $manufacture_code, $manufacture_address, $manufacture_phone, $manufacture_email)
    {
        $data = array(
            'manufacture_id' => NULL,
            'manufacture_name' => $manufacture_name,
            'manufacture_code' => $manufacture_code,
            'manufacture_email' => $manufacture_email,
            'manufacture_address' => $manufacture_address,
            'manufacture_phone' => $manufacture_phone,
        );
        $result = $this->db->insert('fams_manufacture', $data);

        return true;

    }

    /***********Category Name from Category ID************/
    function manufacture_details($manufacture_id)
    {
        $this->db->select('*');
        $this->db->from('fams_manufacture');
        $this->db->where('manufacture_id', $manufacture_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************Manufacture Update***************/
    function manufacture_update($manufacture_id, $manufacture_name, $manufacture_code, $manufacture_email, $manufacture_address, $manufacture_phone)
    {
        $updateData = array(
            'manufacture_name' => $manufacture_name,
            'manufacture_code' => $manufacture_code,
            'manufacture_email' => $manufacture_email,
            'manufacture_address' => $manufacture_address,
            'manufacture_phone' => $manufacture_phone,
        );
        $this->db->where('manufacture_id', $manufacture_id);
        $this->db->update('fams_manufacture', $updateData);
    }

    /*************Category Delete************/
    function manufacture_delete($manufacture_id)
    {
        $this->db->where('manufacture_id', $manufacture_id);
        $this->db->delete('fams_manufacture');
    }

}

?>
