<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: pr_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: prlist,raised_pr,

/****************************************************/

 /**
 * AMS Asset Procurement Model Class
 */
class pr_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }


    /********** PR List************/
    function prlist()
    {
        $this->db->select('*');
        $this->db->from('fams_pr_request');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->order_by('created_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /************* Raised Pr Details **************/
    function raised_pr($requested_id)
    {
        $this->db->select('*');
        $this->db->from('fams_pr_request');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->where('requisition_id', $requested_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }
}

?>
