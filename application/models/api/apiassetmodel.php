<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: apiassetmodel.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: offname,cattotal,countcategory,DistinctCategory,
// catname,assetlist_count,assetlimit,rtcassetlist_count,rtcassetlist,groupasset_count,
// groupasset,category_instore,statuslist,catewise
/****************************************************/

 /**
 * AMS API related Model Class
 */
class apiassetmodel extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    /********** Get Office Name ***********/
    function offname($office_id)
    {
        $this->db->select('office_name');
        $this->db->from('fams_office');
        $this->db->where('office_id', $office_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }  

    }

    /********** Get category Total ***********/
    function cattotal($office_id,$category_id)
    {
        $this->db->select('DISTINCT(count(category_id)) as cnt');
        $this->db->from('fams_asset');
        $this->db->where('office_id', $office_id);
        $this->db->where('category_id', $category_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }  

    }

    /******** Count Total Category of an office***********/
    function countcategory($office_id)
    {
        $this->db->select('count(DISTINCT(category_id)) as cnt');
        $this->db->from('fams_asset');
        $this->db->where('office_id', $office_id);
        //$this->db->where('category_id', $category_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }  
    }

    /********  Distinct Category of an office***********/
    function DistinctCategory($office_id)
    {
        $this->db->select('DISTINCT(category_id) as category_id');
        $this->db->from('fams_asset');
        $this->db->where('office_id', $office_id);
        //$this->db->where('category_id', $category_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }  
    }

    /********* Get Category Name ***********/
    function catname($category_id)
    {
        $this->db->select('category_name');
        $this->db->from('fams_category');
        $this->db->where('category_id', $category_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }  
    }
    /******** Count Asset List**********/
    function assetlist_count($catid = 22)
    {
        //echo "only cat";
        $this->db->select('*');
        $this->db->from('fams_asset');
        //$this->db->where('a.category_id = b.category_id');
        $this->db->where('category_id',$catid);
        //$this->db->where('a.asset_status_id = d.status_id'); 
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        } 
    }
    //******Get asset List************/
    function assetlist($catid,$limit_from,$limit_to)
    {

        //echo $limit_from;
        //echo $limit_to;

        $this->db->select('a.*,b.*,c.*,d.*');
        $this->db->from('fams_asset a, fams_category b, fams_manufacture c, fams_status d');
        $this->db->where('a.category_id = b.category_id');
        $this->db->where('a.category_id',$catid);
        $this->db->where('a.asset_status_id = d.status_id');
        if($limit_to == 0)
        {         
         //echo "only limit from";
         $this->db->limit($limit_from);   
        }
        else
        {
          //echo "limit from and limit to";
          $this->db->limit($limit_from,$limit_to);   
        }
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    //******Get asset List without limit************/
    function assetlimit_count($limit)
    {

        $this->db->select('*');
        $this->db->from('fams_asset');
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    //******Get asset List on limit************/
    function assetlimit($limit_from,$limit_to)
    {
        //echo $limit_from;
       // echo $limit_to;

        $this->db->select('a.*,b.*,c.*,d.*');
        $this->db->from('fams_asset a, fams_category b, fams_manufacture c, fams_status d');
        $this->db->where('a.category_id = b.category_id');
        $this->db->where('a.asset_status_id = d.status_id');
        if($limit_to == $limit_from)
        {
         $this->db->limit($limit_from);   
        }
        else
        {
          $this->db->limit($limit_from,$limit_to);   
        }
        
        //$this->db->order_by('a.create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    /********** RTC Asset total count**************/
    function rtcassetlist_count($id)
    {
        $this->db->select('a.*,b.*,c.*,d.*');
        $this->db->from('fams_asset a, fams_category b, fams_manufacture c, fams_status d');
        $this->db->where('a.category_id = b.category_id');
        $this->db->where('a.asset_status_id = d.status_id');
        $this->db->where('a.office_id',$id);
        //$this->db->limit(500);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }  
    }
    /*********** RTC Asset List **************/
    function rtcassetlist($id,$limit_from,$limit_to)
    {

        $this->db->select('a.*,b.*,c.*,d.*');
        $this->db->from('fams_asset a, fams_category b, fams_manufacture c, fams_status d');
        $this->db->where('a.category_id = b.category_id');
        $this->db->where('a.asset_status_id = d.status_id');
        $this->db->where('a.office_id',$id);
        //$this->db->limit(500);
        if($limit_to == 0)
        {
         $this->db->limit($limit_from);   
        }
        else
        {
          $this->db->limit($limit_from,$limit_to);   
        }
        
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    /***********  Group Asset Count **************/
    function groupasset_count($office_id)
    {
        $this->db->select('category_name, count(*) as qty');
        $this->db->from('fams_asset a, fams_category b');
        $this->db->where('a.category_id = b.category_id');
        $this->db->where('a.office_id =', $office_id);
        $this->db->group_by('a.category_id ');
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else { 
            return false;
                }  
    }
    /************** Cat Wise Category List  ***************/
    function groupasset($office_id,$limit_from,$limit_to)
    {
        
        //$office_id = 1;
        $this->db->select('category_name, count(*) as qty');
        $this->db->from('fams_asset a, fams_category b');
        $this->db->where('a.category_id = b.category_id');
        $this->db->where('a.office_id =', $office_id);
        if($limit_to == $limit_from)
        {
         $this->db->limit($limit_from);   
        }
        else
        {
          $this->db->limit($limit_from,$limit_to);   
        }
        
        $this->db->group_by('a.category_id ');
        $query = $this->db->get();

        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else { 
            return false;
                }
    }

    /********** Category in Store ***********/
    function category_instore($category_id,$office_id,$status_id)
    {
        $this->db->select('count(*) as total');
        $this->db->from('fams_asset');
        $this->db->where('office_id =', $office_id);
        $this->db->where('category_id =', $category_id);
        $this->db->where('asset_status_id =', $status_id);
        $query = $this->db->get(); 
         if ($query->num_rows() > 0) {
            return $query->result();
        } else { 
            return false;
                }
    }

    /**************Status List of Assets**************/
    function statuslist()
    {
        $this->db->select('*');
        $this->db->from('fams_status');
        $query = $this->db->get();
        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*************** Category Wise Asset List  ***********/
    function catewise($category_id,$office_id)
    {
        if($category_id=="")
        {
            $this->db->select('category_name,status_name as status, count(*) as total');
            $this->db->from('fams_asset a, fams_category b, fams_status c');
            $this->db->where('a.category_id = b.category_id');
            $this->db->where('a.asset_status_id = c.status_id');
            $this->db->where('a.office_id =', $office_id);
            $this->db->group_by('a.category_id');
            $this->db->group_by('a.asset_status_id');
            $query = $this->db->get();
        }
        else
        {
            $this->db->select('status_name as status, count(*) as total');
            $this->db->from('fams_asset a, fams_category b, fams_status c');
            $this->db->where('a.category_id = b.category_id');
            $this->db->where('a.asset_status_id = c.status_id');
            $this->db->where('a.office_id =', $office_id);
            $this->db->where('a.category_id =', $category_id);
            //$this->db->group_by('a.category_id ');
            $this->db->group_by('a.asset_status_id');
            $query = $this->db->get(); 
        }

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else { 
            return false;
                }  
    }

}

?>
