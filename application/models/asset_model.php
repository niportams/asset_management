<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*****************************************************************/

// Filename: asset_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0

// Function list: get_asset,available_grn_list,assetlist,statuslist,
// status_wise_asset_list,asset_list,capitallist,rtccapitallist,capital_register_status,
// grn_check,grn_check_register,capital_details,asset_details,asset_search,assetstatuslist,
// capital_update,register_capital_update,registered_asset_update,capital_delete,asset_delete,
// assetNameList,requisitionlist,requisitions,approvedrequisitions,requisition_details,
// allocation_approved_list,transfer_approved_list,approved_requisition_details,approved_requisition_transfer,
// request_list,category_available,requested_category_available,cat_wise_asset,cat_wise_asset_count,
// cat_wise_asset_list,catasset_list,cat_wise_asset_qty_check,temp_allocate,temp_request,get_temp_allocation,
// temp_delete,temp_delete_all,delete_tmp_files,tmp_file_include,get_file_include,get_files,allocation_attachment,
// allocate_asset,transfer_asset,update_asset_status,transfer_asset_status,change_asset_status,get_allocated_list,
// sciid_check,sciid_update_check,allocated_list,allocated_requisition_list,allocated_list_for_user,transfered_list,
// transfered_list_for_user,state_revert,asset_revert,update_requisition_asset,get_max_office_category,
// pr_asset_check,request_asset_check,request_asset_update,history_add,disposed_file,disposed_file,dispose_file_include,
// disposed_filelist,get_asset_history,delete_disposed_files,auto_asset_allocate,get_asset_category

/******************************************************************/
 /**
 * AMS Asset Model Class
 * This class retrieves the asset related data from database.
 */

class asset_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    /********** Retrieves Asset Name from search charecter ************/  
    function get_asset($asset_search)
    {
        $this->db->select('DISTINCT(asset_name)');
        $this->db->from('fams_capitalize');
        $this->db->where("asset_name LIKE '%$asset_search%' ");
        $this->db->order_by("asset_name",'asc');
        $query = $this->db->get();
        
        //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    //************ Retrieves Available GRN # list *********/
    function available_grn_list()
    {
        $this->db->select('*,TIMESTAMPDIFF(DAY,create_date,now()) as ddiff', FALSE);
        $this->db->from('fams_capitalize');
        //$this->db->where('grn_no', $grn_no);
        $this->db->where('received_item > registered_item_no');
        $this->db->where('receiving_office', $this->session->userdata('user_center'));
        $this->db->having('ddiff <= entry_limit');
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            //return 1;
            return $query->result();
        } else {
            return 0;
        }
    }

    //****** Get All Asset List ************/
    function assetlist()
    {

        $this->db->select('a.*,b.*,c.*,d.*');
        $this->db->from('fams_asset a, fams_category b, fams_manufacture c, fams_status d');
        $this->db->where('a.category_id = b.category_id');
        //$this->db->where('a.manufacture_id = c.manufacture_id');
        $this->db->where('a.asset_status_id = d.status_id');
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************** Retrieves Status List of Assets **************/
    function statuslist()
    {
        $this->db->select('*');
        $this->db->from('fams_status');
        $query = $this->db->get();
        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************Asset List Based on Asset Status**************/
    function status_wise_asset_list($status_id)
    {
        $user_office = $this->session->userdata('user_center');
        $user_role = get_user_role_name($this->session->userdata('user_role_id'));
        $this->db->select('a.*,b.category_name,d.status_name');
        $this->db->from('fams_asset a');
        $this->db->join('fams_category b', 'a.category_id = b.category_id');
        $this->db->join('fams_status d', 'a.asset_status_id = d.status_id');
        $this->db->where('a.asset_status_id', $status_id);
        
        if ($user_role != 'SYSTEM_ADMIN') {
            $this->db->where('a.office_id', $user_office);
        } // rafi (rtc filtering)
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /********************** All Asset Lists*********************************/
    function asset_list()
    {

        $user_office = $this->session->userdata('user_center');
        $user_role = get_user_role_name($this->session->userdata('user_role_id'));

        $this->db->select('a.*,b.category_name,d.status_name');
        $this->db->from('fams_asset a');
        $this->db->join('fams_category b', 'a.category_id = b.category_id');
        $this->db->join('fams_status d', 'a.asset_status_id = d.status_id');
        $this->db->where('a.asset_status_id <>', 6);
        if ($user_role != 'SYSTEM_ADMIN') {
            $this->db->where('a.office_id', $user_office);
        }
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'
       // $this->db->limit(1000);
        $query = $this->db->get();
        
        //$this->db->last_query();
        //exit();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************Asset Capitalized list*************/
    function capitallist()
    {
        //$this->db->order_by('a.create_date', 'desc'); // or 'DESC' 
        $this->db->select('a.*,b.category_name,c.office_name,a.create_date as capitalize_date');
        $this->db->from('fams_capitalize a');
        $this->db->join('fams_category b', 'a.category_id = b.category_id');
        $this->db->join('fams_office c', 'a.receiving_office = c.office_id');
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'      
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*********Capital List for Specific RTC***********/
    function rtccapitallist($user_office)
    {
        //$this->db->order_by('a.create_date', 'desc'); // or 'DESC'  
        
        $this->db->select('a.*,b.category_name,c.office_name,a.create_date as capitalize_date');
        $this->db->from('fams_capitalize a');
        $this->db->join('fams_category b', 'a.category_id = b.category_id');
        $this->db->join('fams_office c', 'a.receiving_office = c.office_id');
        $this->db->where('a.receiving_office', $user_office);
        $this->db->order_by('a.create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************ Capital registered item update ****************/
    function capital_register_status($capital_id)
    {

        $this->db->select('(received_item - registered_item_no) as pending_item,registered_item_no');
        $this->db->from('fams_capitalize');
        $this->db->where('capital_id', $capital_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            //return 1;
            return $query->result();
        } else {
            return 0;
        }

    }
    /*********Uniqueness check of grn number*****/
    function grn_check($grn_no)
    {

        $this->db->select('*');
        $this->db->from('fams_capitalize');
        $this->db->where('grn_no', $grn_no);
        $query = $this->db->get();
        
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            //return 1;
            return $query->result();
        } else {
            return 0;
        }
    }

    /********* GRN check for registration *****/
    function grn_check_register($grn_no)
    {

        $this->db->select('*,TIMESTAMPDIFF(DAY,create_date,now()) as ddiff', FALSE);
        $this->db->from('fams_capitalize');
        $this->db->where('grn_no', $grn_no);
        $this->db->where('received_item > registered_item_no');
        //$this->db->having('ddiff <= 7');
        $this->db->having('ddiff <= entry_limit');
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            //return 1;
            return $query->result();
        } else {
            return 0;
        }
    }

    /***********Capital details from captal ID************/
    function capital_details($capital_id)
    {
        $this->db->select('*');
        $this->db->from('fams_capitalize');
        $this->db->where('capital_id', $capital_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*************** Asset Details  of asset Id *****************/

    function asset_details($asset_id)
    {
        $this->db->select('*');
        $this->db->from('fams_asset');
        $this->db->where('asset_id', $asset_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********  Asset Search in multiple ways *************/

    function asset_search($asset_search)
    {
        $this->db->select('*');
        $this->db->from('fams_asset');
        //$this->db->where('asset_readable_id',$asset_search);
        $this->db->where("asset_readable_id LIKE '$asset_search' OR sci_id LIKE '$asset_search'");
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************** Asset Status List*****************/
    function assetstatuslist()
    {
        $this->db->select('*');
        $this->db->from('fams_status');
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************Capital Update from captal ID***************/
    function capital_update($capital_id, $asset_name, $category_id, $office_id, $total_received,
                            $gik_check, $funded_by, $received_by, $own_funded, $supplier_id, $voucher_no,$department_id)
    {
        $updateData = array(
            'capital_id' => $capital_id,
            'category_id' => $category_id,
            'asset_name' => $asset_name,
            'receiving_office' => $office_id,
            'supplier_id' => $supplier_id,
            'voucher_no' => $voucher_no,
            'received_item' => $total_received,
            'gik' => $gik_check,
            'funded_by' => $funded_by,
            'own_funded' => $own_funded,
            'received_by' => $received_by,
            'department_id' => $department_id,
        );
        $this->db->where('capital_id', $capital_id);
        $this->db->update('fams_capitalize', $updateData);
    }

    /**************Capital Registration Update After Registration***************/
    function register_capital_update($capital_id, $item_registered)
    {
        $updateData = array(
            'capital_id' => $capital_id,
            'registered_item_no' => $item_registered,
        );
        $this->db->where('capital_id', $capital_id);
        $this->db->update('fams_capitalize', $updateData);
    }

    /**************Asset Registration Update After Registration***************/
    function registered_asset_update($asset_id, $readable_id, $label_id)
    {
        $updateData = array(
            'asset_readable_id' => $readable_id,
            'label_id' => $label_id,
        );
        $this->db->where('asset_id', $asset_id);
        $this->db->update('fams_asset', $updateData);
    }

    /**********Capital Delete************/
    function capital_delete($capital_id)
    {
        $this->db->where('capital_id', $capital_id);
        $query = $this->db->delete('fams_capitalize');
        return $query;
    }

    /**********Asset Delete************/
    function asset_delete($asset_id)
    {
        $this->db->where('asset_id', $asset_id);
        $query = $this->db->delete('asset');
        return $query;
    }

    /******** Asset Name list ********/
    function assetNameList()
    {
        $this->db->select('asset_name as value, asset_name as label, max(capital_id) as id, max(category_id) as catagory');
        $this->db->from('asset');
        $this->db->group_by('asset_name');
        $query = $this->db->get();

        return $query->result();
    }


    /****** Get Requisition List ************/
    function requisitionlist()
    {
        $this->db->order_by('created_date', 'desc'); // or 'DESC'
        $this->db->select('*');
        $this->db->from('fams_requisitions');
        $this->db->where('approve_of ', $this->session->userdata('user_db_id'));
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /****** Get Requisition List ************/
    function requisitions()
    {
        $this->db->order_by('created_date', 'desc'); // or 'DESC'
        $this->db->select('*');
        $this->db->from('fams_requisitions');
        $this->db->where('userid ', $this->session->userdata('user_db_id'));
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /****** Get my Approved Requisition List ************/
    function approvedrequisitions()
    {
        $this->db->order_by('created_date', 'desc'); // or 'DESC'
        $this->db->select('*, requisitions_approvals.requisition_id as id, requisitions_approvals.id as approv_id');
        $this->db->from('fams_requisitions');
        $this->db->join('requisitions_approvals', 'requisitions_approvals.requisition_id = fams_requisitions.id', 'left');
        $this->db->where('requisitions_approvals.approved_by', $this->session->userdata('user_db_id'));
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    /***************** Requisition details ***********/
    function requisition_details($requisition_id)
    {
        $this->db->select('*, fams_requisitions_assets.id as asset_id');
        $this->db->from('fams_requisitions_assets');
        $this->db->where('requisition_id', $requisition_id);
        $this->db->join('fams_requisitions', 'fams_requisitions.id = fams_requisitions_assets.requisition_id', 'left');
        //  $this->db->where('fams_requisitions.approve_of ', $this->session->userdata('user_db_id'));
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /********** Approved Requision Allocation List **************/
    function allocation_approved_list()
    {

        $this->db->select('a.*,b.*,b.create_date as approved_date,c.designation,d.*');
        $this->db->from('fams_requisitions a, fams_requisitions_approvals b, fams_user c,fams_office d');
        $this->db->where('a.id = b.requisition_id');
        $this->db->where('a.office_id = d.office_id');
        $this->db->where('b.approved_by = c.userid');
        $this->db->where('a.process = "Allocation"');
        $this->db->where('b.authorization = "proceed"');
        //$this->db->where('b.authorization = "APPROVED"');
        $this->db->where('a.approve_of', $this->session->userdata('user_db_id'));
        $this->db->order_by('created_date', 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /********** Approved Requision Transfer List **************/
    function transfer_approved_list()
    {

        $this->db->select('a.*,b.*,c.designation,d.*');
        $this->db->from('fams_requisitions a, fams_requisitions_approvals b, 
        fams_user c,fams_office d');
        $this->db->where('a.id = b.requisition_id');
        $this->db->where('a.office_id = d.office_id');
        $this->db->where('b.approved_by = c.userid');
        $this->db->where('a.process = "Transfer"');
        $this->db->where('b.authorization = "PROCEED"');
        $this->db->where('a.approve_of', $this->session->userdata('user_db_id'));
        //$this->db->group_by('a.step');
        $this->db->order_by('created_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*************** Approved Requisition Details of a requisition ********************/

    function approved_requisition_details($requisition_id)
    {
        $this->db->select('a.*,b.requisition_id,b.approved_by,b.remarks,b.create_date as approved_date,d.*');
        $this->db->from('fams_requisitions a, fams_requisitions_approvals b, fams_office d');
        $this->db->where('a.id = b.requisition_id');
        $this->db->where('a.office_id = d.office_id');
        $this->db->where('a.process = "Allocation"');
        $this->db->where('b.authorization = "PROCEED"');
        //$this->db->where('b.authorization = "approved"');
        $this->db->where('a.id', $requisition_id);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*************** Approved Requisition Details of a Transfer requisition ********************/

    function approved_requisition_transfer($requisition_id)
    {
        $this->db->select('a.*,b.requisition_id,b.approved_by,b.remarks,b.create_date as approved_date,d.*');
        $this->db->from('fams_requisitions a, fams_requisitions_approvals b, fams_office d');
        $this->db->where('a.id = b.requisition_id');
        $this->db->where('a.office_id = d.office_id');
        $this->db->where('a.process = "Transfer"');
        $this->db->where('b.authorization = "PROCEED"');
        //$this->db->where('b.authorization = "APPROVED"');
        $this->db->where('a.id', $requisition_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************** Item Name of a requisition ***********************/
    function request_list($requested_id)
    {
        $this->db->select('*');
        $this->db->from('fams_requisitions_assets');
        //$this->db->where('requisition_state = "Processed"');
        $this->db->where('requisition_id', $requested_id);

        $query = $this->db->get();
        //echo $this->db->last_query();       
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************** Asset Category Available Checking ***********************/
    function category_available($category_id)
    {

        $this->db->select('count(*) as available');
        $this->db->from('fams_asset');
        $this->db->where('asset_status_id = 1');
        $this->db->where('category_id', $category_id);
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $query = $this->db->get();
        //echo $this->db->last_query(); 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************Requested Category item available******************/
    function requested_category_available($category_id, $asset_qty)
    {

        $this->db->select('count(*) as available');
        $this->db->from('fams_asset');
        $this->db->where('asset_status_id = 1');
        $this->db->where('category_id', $category_id);
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->having('available >=', $asset_qty);

        $query = $this->db->get();
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    /****************** Category Wise Asset Get  ******************/
    function cat_wise_asset($cat_id)
    {

        $this->db->select('DISTINCT(asset_name),category_id');
        $this->db->where('asset_status_id = 1');
        $this->db->where('category_id', $cat_id);
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $query = $this->db->get('fams_asset');
        //echo $this->db->last_query();       
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /****************** Category Wise Asset Count  ******************/
    function cat_wise_asset_count($cat_id, $cat_item)
    {

        $this->db->select('*');
        $this->db->from('fams_asset');
        $this->db->where('category_id', $cat_id);
        $this->db->where('asset_status_id', 1);
        $this->db->where('LOWER(asset_name)', $cat_item);
        $query = $this->db->get();

        //echo $this->db->last_query();       
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    /*****************  Category Wise Asset List ***********************/
    function cat_wise_asset_list($cat_id, $total_qty)
    {
        $this->db->select('*');
        $this->db->from('fams_asset');
        $this->db->where('category_id', $cat_id);
        $this->db->where('asset_status_id', 1);
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->limit($total_qty);
        $query = $this->db->get();

        //echo $this->db->last_query();       
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*****************  Category Wise Asset List ***********************/
    function catasset_list($cat_id)
    {
        $this->db->select('*');
        $this->db->from('fams_asset');
        $this->db->where('category_id', $cat_id);
        $this->db->where('asset_status_id', 1);
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $query = $this->db->get();

        //echo $this->db->last_query();       
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************* Category Wise Asset Quantity Match*****************/
    function cat_wise_asset_qty_check($cat_id, $request_id, $total_qty)
    {
        $this->db->select('*');
        $this->db->from('fams_requisitions_assets');
        $this->db->where('category_id', $cat_id);
        $this->db->where('requisition_id', $request_id);
        $this->db->where('quantity =', $total_qty);

        $query = $this->db->get();

        //echo $this->db->last_query();       
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************ Temporary Allocation  *****************/
   function temp_allocate($category_id,$quantity)
    {
        $data = array(
            'category_id' => $category_id,
            'qty' => $quantity,
            'created_by' => $this->session->userdata('user_db_id'),
        );
        $this->db->set('create_date', 'CURDATE()', FALSE);
        $query = $this->db->insert('fams_allocation_tmp', $data);
        return $query;
    }

  /************ Temporary Request Selection  *****************/
   function temp_request($category_id,$quantity,$status)
    {
        $data = array(
            'category_id' => $category_id,
            'qty' => $quantity,
            'item_status' => $status,
            'created_by' => $this->session->userdata('user_db_id'),
        );
        $this->db->set('create_date', 'CURDATE()', FALSE);
        $query = $this->db->insert('fams_allocation_tmp', $data);
        return $query;
    }

    /************ Get Temporary Allocation **************/
    function get_temp_allocation()
    {
        $this->db->select('*');
        $this->db->from('fams_allocation_tmp');
        $this->db->where('create_date', 'CURDATE()', FALSE);
        $this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********  Delete Tamporary Allocation of an item****************/
    function temp_delete($item_id)
    {

        $this->db->where('item_id', $item_id);
        $query = $this->db->delete('fams_allocation_tmp');
        return true;
    }

    /***********  Delete Tamporary Allocation of current date of a user****************/
    function temp_delete_all()
    {

        $this->db->where('created_by', $this->session->userdata('user_db_id'));
        $this->db->where('create_date', 'CURDATE()', FALSE);
        $query = $this->db->delete('fams_allocation_tmp');
        return true;
    }

    /***************** Delete All Uploaded Files  *******************/
    function delete_tmp_files($file_id)
    {
        $this->db->where('file_id', $file_id);
        $this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->delete('fams_allocation_file_tmp');
        return true;
    }



    /***************** File Inclusion for allocation/transfer   ********/
    function tmp_file_include($name)
    {
        $data = array(
            //'allocation_id'=>NULL,
            'file_name' => $name,
            'created_by' => $this->session->userdata('user_db_id'),
        );
        $this->db->set('create_date', 'CURDATE()', FALSE);
        $query = $this->db->insert('fams_allocation_file_tmp', $data);
        return $query;

    }

    /************** Get attached files of this allocation *****************/

    function get_file_include()
    {
        $this->db->select('*');
        $this->db->from('fams_allocation_file_tmp');
        $this->db->where('create_date', 'CURDATE()', FALSE);
        $this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************ Get Attached files of the requisition  ******************/
    function get_files($reference_no,$requisition_no)
    {

        $this->db->select('*');
        $this->db->from('fams_allocation_file');
        //$this->db->where('reference_no',$requisition_no); 
        $this->db->where('requisition_id',$requisition_no); 
        $this->db->or_where('requisition_id',$reference_no); 
        //$this->db->where("(requisition_id = $reference_no OR requisition_id = $requisition_no)");
        $this->db->order_by('create_date', 'desc');
        $query = $this->db->get(); 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***************  Insert Allocation Attachment   **********************/
    function allocation_attachment($requisition_no, $attachment_name)
    {
        $data = array(
            'file_name' => $attachment_name,
            'requisition_id' => $requisition_no,
            'created_by' => $this->session->userdata('user_db_id'),
        );
        $this->db->set('create_date', 'NOW()', FALSE);
        $query = $this->db->insert('fams_allocation_file', $data);
        return $query;
    }


    /***************** Allocate Asset to User Finally ******************/
    function allocate_asset($approved_by,$asset_id, $asset_label_id, $category_id, $requisition_id,
    $allocated_by, $allocate_to, $allocate_office, $allocate_to, $remarks,$asset_location,$on_behalf)
    {
        $data = array(
            'asset_id' => $asset_id,
            'asset_label_id' => $asset_label_id,
            'category_id' => $category_id,
            'requisition_no' => $requisition_id,
            'approved_by' => $approved_by,
            'allocated_by' => $allocated_by,
            'allocate_to' => $allocate_to,
            'allocate_office' => $allocate_office,
            'allocate_state' => "Delivered",
            'on_behalf' => empty($on_behalf) ? NULL : $on_behalf,
            'asset_location' => empty($asset_location) ? NULL : $asset_location,
            'reference_no' => '',
            'value_context' => '',
            'remarks' => $remarks,
        );
        $this->db->set('create_date', 'NOW()', FALSE);
        $query = $this->db->insert('fams_allocation', $data);
        return $query;
    }

    /************  Transfer Asset to User Finally****************/
    function transfer_asset($asset_id, $asset_label_id, $category_id, $requisition_id,
                            $transfer_by, $transfer_to, $transfer_office, $remarks)
    {

        $data = array(
            'asset_id' => $asset_id,
            'asset_label_id' => $asset_label_id,
            'category_id' => $category_id,
            'requisition_no' => $requisition_id,
            'transfer_by' => $transfer_by,
            'transfer_to' => $transfer_to,
            'from_office' => $this->session->userdata('user_center'),
            'transfer_office' => $transfer_office,
            'transfer_state' => "Delivered",
            'remarks' => $remarks,
        );
        $this->db->set('create_date', 'NOW()', FALSE);
        $query = $this->db->insert('fams_transfer', $data);
        return $query;

    }

    /***************** Asset Status update based on allocation *********************/
    function update_asset_status($asset_id, $allocate_to)
    {
        $updateData = array(
            'assigned_to' => $allocate_to,
            'asset_status_id' => 5,
        );
        $this->db->where('asset_id', $asset_id);
        $query = $this->db->update('fams_asset', $updateData);
        // $this->db->set('update_date', 'NOW()', FALSE);
        return $query;
    }

    /***********  Update asset office id based on transfer******************/
    function transfer_asset_status($transfer_asset_id, $transfer_to, $transfer_office)
    {

        $updateData = array(
            'assigned_to' => $transfer_to,
            'office_id' => $transfer_office,
        );
        $this->db->where('asset_id', $transfer_asset_id);
        $query = $this->db->update('fams_asset', $updateData);
        // $this->db->set('update_date', 'NOW()', FALSE);
        return $query;

    }

    /***************** Asset Status update *********************/
    function change_asset_status($asset_id, $status_id)
    {
        $updateData = array(

            'asset_status_id' => $status_id,
        );
        $this->db->where('asset_id', $asset_id);
        //$this->db->where('office_id',$this->session->userdata('user_center'));
        $query = $this->db->update('fams_asset', $updateData);

        // $this->db->set('update_date', 'NOW()', FALSE);
        return $query;
    }

    /**************  Allocated Asset List of a Requisition  *****************/
    function get_allocated_list($requisition_id)
    {
        $this->db->select('*');
        $this->db->from('fams_allocation');
        $this->db->where('allocate_state =',"Delivered"); 
        $this->db->where('requisition_no',$requisition_id); 
        $query = $this->db->get(); 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********  SCI ID check  ***********/
    function sciid_check($sci_id)
    {
        $this->db->select('*');
        $this->db->from('fams_asset');
        $this->db->where('sci_id', $sci_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************ SCI Update ID Check******************/
    function sciid_update_check($updateid, $sci_id)
    {
        $this->db->select('*');
        $this->db->from('fams_asset');
        $this->db->where('sci_id', $sci_id);
        $this->db->where('asset_id <>', $updateid);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************ Allocated Assets of an Office******************/
    function allocated_list()
    {
        $this->db->select('*');
        $this->db->from('fams_allocation');
        $this->db->where('allocate_office', $this->session->userdata('user_center'));
        $this->db->where('allocate_state =', "Delivered");
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*************** Allocated Assets of a requisition *********************/
    function allocated_requisition_list()
    {
        $this->db->select('requisition_no,reference_no,create_date,allocate_to');
        $this->db->from('fams_allocation');
        $this->db->where('allocate_office', $this->session->userdata('user_center'));
        $this->db->where('allocate_state =', "Delivered");
        //$this->db->group_by('requisition_no,reference_no,create_date,allocate_to');
        $this->db->group_by('requisition_no,reference_no');
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }
    /************ Allocated Assets of an user******************/
    function allocated_list_for_user()
    {
        $this->db->select('*');
        $this->db->from('fams_allocation');
        $this->db->where('allocate_to', $this->session->userdata('user_db_id'));
        $this->db->where('allocate_state =', "Delivered");
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************ Transfered Assets of an Office******************/
    function transfered_list()
    {
        $this->db->select('*');
        $this->db->from('fams_transfer');
        $this->db->where('from_office', $this->session->userdata('user_center'));
        $this->db->where('transfer_state =', "Delivered");
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************ Allocated Assets of an user******************/
    function transfered_list_for_user()
    {
        $this->db->select('*');
        $this->db->from('fams_transfer');
        $this->db->where('transfer_to', $this->session->userdata('user_db_id'));
        $this->db->where('transfer_state =', "Delivered");
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*********** State Revert to its base state*****************/
    function state_revert($asset_id, $state_type, $revert_state)
    {
        $updateData = array(
            'asset_status_id' => $revert_state,
        );

        $this->db->where('asset_id', $asset_id);
        $this->db->set('update_date', 'NOW()', FALSE);
        $query = $this->db->update('fams_asset', $updateData);
        return $query;
    }

    /****************** Asset Reverting *******************/
    function asset_revert($allo_id, $asset_id, $revert_type, $from_office)
    {
        switch ($revert_type) {
            case 'allocation':
                {
                    $updateData = array(
                        'asset_status_id' => 1,
                        'assigned_to' => 0,
                    );
                    $this->db->where('asset_id', $asset_id);
                    $this->db->set('update_date', 'NOW()', FALSE);
                    $query = $this->db->update('asset', $updateData);

                    $updateAllocation = array(
                        'allocate_state' => "Returned",
                    );
                    $this->db->where('allocate_id', $allo_id);
                    $this->db->set('update_date', 'NOW()', FALSE);
                    $query = $this->db->update('allocation', $updateAllocation);
                    return $query;
                }
                break;

            case 'transfer':
                {
                    $updateData = array(
                        'asset_status_id' => 1,
                        'assigned_to' => 0,
                        'office_id' => $from_office,
                    );
                    $this->db->where('asset_id', $asset_id);
                    $this->db->set('update_date', 'NOW()', FALSE);
                    $query = $this->db->update('fams_asset', $updateData);

                    $updateAllocation = array(
                        'transfer_state' => "Returned",
                    );
                    $this->db->where('transfer_id', $allo_id);
                    $this->db->set('update_date', 'NOW()', FALSE);
                    $query = $this->db->update('transfer', $updateAllocation);

                    return $query;
                }
                break;

            case 'Deadstate':
                {
                    $updateData = array(
                        'asset_status_id' => 3,

                    );
                    $this->db->where('asset_id', $asset_id);
                    $this->db->set('update_date', 'NOW()', FALSE);
                    $query = $this->db->update('fams_asset', $updateData);
                }
                break;

            case 'Maintanance':
                {
                    $updateData = array(
                        'asset_status_id' => 3,

                    );
                    $this->db->where('asset_id', $asset_id);
                    $this->db->set('update_date', 'NOW()', FALSE);
                    $query = $this->db->update('fams_asset', $updateData);
                }
                break;

            case 'disposed':
                {
                    $updateData = array(
                        'asset_status_id' => 6,
                    );
                    $this->db->where('asset_id', $asset_id);
                    $this->db->set('update_date', 'NOW()', FALSE);
                    $query = $this->db->update('fams_asset', $updateData);
                }
                break;
        }
    }

    /****************   Update Requisition Asset Table********************/
    function update_requisition_asset($item_id, $requisition_id)
    {

        $updateData = array(
            'requisition_state' => "Delivered",
        );

        $this->db->where('id', $item_id);
        //$this->db->where('requisition_id', $requisition_id);
        $query = $this->db->update('fams_requisitions_assets', $updateData);
        return $query;
    }

    /***************** Get MAX Asset Category of an office  ********************/
    function get_max_office_category($office_id, $category_id)
    {

        $this->db->select('count(*) as max');
        $this->db->from('fams_asset');
        $this->db->where('office_id', $office_id);
        $this->db->where('category_id', $category_id);

        $query = $this->db->get();
        //echo $this->db->last_query();  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
   /********** Procurement Request asset registration check  ********/ 
   function pr_asset_check($reference_no,$category_id)
   {
        $this->db->select('count(*) as cnt');
        $this->db->from('fams_asset');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->where('pr_reference_no', $reference_no);
        $this->db->where('category_id', $category_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }
   /********** Procurement Request asset allocation check  ********/ 
   function request_asset_check($reference_no,$category_id)
   {
        $this->db->select('quantity');
        $this->db->from('fams_allocation_request');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->where('ref_no', $reference_no);
        $this->db->where('category_id', $category_id);
        $this->db->where('req_status =', "Not Available");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }
   /************ Asset Allocation request update******************/
   function request_asset_update($reference_no,$category_id)
   {
    $updateData = array(
            'req_status' => "Available",
        );

        $this->db->where('ref_no', $reference_no);
        $this->db->where('category_id', $category_id);
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->set('update_date', 'NOW()', FALSE);
        $query = $this->db->update('fams_allocation_request', $updateData);
        return $query;
   }

   /*************** Asset State change add as history purpose  *********/
   function history_add($update_id,$status_id,$current_state,$current_office,$assigned_user)
   {
     $data = array(
            'asset_id' => $update_id,
            'current_state' => $status_id,
            'previous_state' => $current_state,
            'office_id' => $current_office,
            'assigned_user' => $assigned_user,
            );
     
        $this->db->set('create_date', 'NOW()', FALSE);
        $query = $this->db->insert('fams_state_history', $data);
        return $query;

   }

   /*************** Asset State change add as history purpose  *********/

   function disposed_file($update_id)
   {
        $this->db->select('MAX(create_date) as create_date');
        $this->db->from('fams_disposal_file');
        $this->db->where('asset_id', $update_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }


   }
   /****************  Disposal file add  *******************/
   function dispose_file_include($asset_id,$name)
   {
    $data = array(
            'file_name' => $name,
            'asset_id' => $asset_id,
           );
     
        $this->db->set('create_date', 'NOW()', FALSE);
        $query = $this->db->insert('fams_disposal_file', $data);
        return $query;
   }
   /*********** Dispose File List *************/
   function disposed_filelist($asset_id)
   {
        $this->db->select('*');
        $this->db->from('fams_disposal_file');
        $this->db->where('asset_id', $asset_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }

   /*********** Asset State History *************/
   function get_asset_history($asset_id,$current_state)
   {
        //echo $asset_id;
        $this->db->select('previous_state,MAX(create_date) as create_date');
        $this->db->from('fams_state_history');
        $this->db->where('asset_id', $asset_id);
        $this->db->where('current_state', $current_state);
        //$this->db->having('MAX(create_date)<>NULL');
        $query = $this->db->get();

        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }
   /********* Dispose file delete ************/
   function delete_disposed_files($file_id)
   {
        $this->db->where('file_id', $file_id);
        //$this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->delete('fams_disposal_file');
        return $query;
   }
   /*************old Asset allocation : forcely inserted ****************/
function auto_asset_allocate($asset_id,$auto_asset_id, $requisition_no,$category_id,
$assigned_to,$user_center,$asset_location,$allocation_date)
   {
    $data = array(
            'asset_id' => $asset_id,
            'asset_label_id' => $auto_asset_id,
            'category_id' => $category_id,
            'requisition_no' => $requisition_no,
            'approved_by' => 0,
            'allocated_by' => $this->session->userdata('user_db_id'),
            'allocate_to' => $assigned_to,
            'allocate_office' => $user_center,
            'allocate_state' => "Delivered",
            'on_behalf' => 0,
            'asset_location' => $asset_location,
            'reference_no' => '',
            'value_context' => '',
            'remarks' => '',
            'create_date'=>$allocation_date
            );
        
        //$this->db->set('create_date', 'NOW()', FALSE); 
        $query = $this->db->insert('fams_allocation', $data);
        
        return $query;
   }

   /*********** Get category id of an asset  *************/
   function get_asset_category($asset_id)
   {
        $this->db->select('category_id');
        $this->db->from('fams_asset');
        $this->db->where('asset_id', $asset_id);
        $query = $this->db->get();

        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
   }
}

?>
