<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class search_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    /************ Allocated Assets of an Office******************/
    function allocated_list($q)
    {
        $this->db->select('*');
        $this->db->from('asset');
        $this->db->or_where('asset_name LIKE', '%' . $q . '%');
        // $this->db->or_like('asset_name',$q);
        $this->db->or_where('asset_readable_id', $q);
        $this->db->or_where('asset_id', $q);
        $this->db->or_where('capital_id', $q);
        $this->db->or_where('serial_no', $q);
        $this->db->or_where('label_id', $q);
        $this->db->or_where('grn_no', $q);
        $this->db->or_where('sci_id', $q);
        $this->db->or_where('sku', $q);
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $query = $this->db->get();
        // var_dump($this->db->last_query());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}

?>
