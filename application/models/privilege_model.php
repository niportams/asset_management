<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: privilege_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: privilegelist,privilege_create,privilege_details,privilege_update,privilege_delete

/****************************************************/

 /**
 * AMS Asset Privilege Model Class
 */
class privilege_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();

    }

    //******Get User privilege************
    function privilegelist()
    {
        $this->db->order_by('fams_privilege.privilege', 'ASC'); // or 'DESC'
        $query = $this->db->get('fams_privilege');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    /***********Privilege Create****************/
    function privilege_create($privilege_name, $privilege_status)
    {
        $data = array(
            'privilege_id' => NULL,
            'privilege' => $privilege_name,
            'privilege_status' => $privilege_status,
        );
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->insert('fams_privilege', $data);
        return true;

    }

    /***********Privilege Name from Privilege ID************/
    function privilege_details($privilege_id)
    {

        $this->db->select('*');
        $this->db->from('fams_privilege');
        $this->db->where_in('privilege_id', explode(",", $privilege_id));
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************Privilege Update***************/
    function privilege_update($privilege_id, $privilege_name, $privilege_status)
    {
        $updateData = array(
            'privilege' => $privilege_name,
            'privilege_status' => $privilege_status,
        );
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->where('privilege_id', $privilege_id);
        $this->db->update('fams_privilege', $updateData);
    }

    /*************Privilege Delete************/
    function privilege_delete($privilege_id)
    {
        $this->db->where('privilege_id', $privilege_id);
        $this->db->delete('fams_privilege');
    }

}

?>
