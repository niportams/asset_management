<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: office_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: officelist,office_room_list,office_create,office_details,office_update,
// office_delete,get_room_by_office_id,all_office_details,
/****************************************************/

/**
 * AMS office model class
 *
 * This method demonstrates the office data of AMS.
 */
class office_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    //******Get Office list************/
    function officelist()
    {
        $this->db->select('*');
        $this->db->from('fams_office');
        $this->db->order_by('office_name', 'asc');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /************** Office Room List  **************/
    function office_room_list()
    {
        $this->db->select('*');
        $this->db->from('fams_office_room');
        $this->db->where('office_id', $this->session->userdata('user_center'));
        $this->db->order_by('room_name', 'asc');
        $query = $this->db->get();
        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********Office Create****************/
    function office_create($office_name, $allocated_area)
    {
        $data = array(
            'office_id' => NULL,
            'office_name' => $office_name,
            'allocated_area' => $allocated_area,
        );
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->insert('fams_office', $data);
        return true;
    }

    /***********Office details from Office Id************/
    function office_details($office_id)
    {
        $this->db->select('*');
        $this->db->from('fams_office');
        $this->db->where('office_id', $office_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************Office Update***************/
    function office_update($office_id, $office_name, $allocated_area)
    {
        $updateData = array(
            'office_name' => $office_name,
            'allocated_area' => $allocated_area,
        );
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->where('office_id', $office_id);
        $this->db->update('fams_office', $updateData);
    }

    /*************Office Delete by Office Id************/
    function office_delete($office_id)
    {
        $this->db->where('office_id', $office_id);
        $this->db->delete('fams_office');
    }
    /************* Get Room list by Office Id ************/
    function get_room_by_office_id($office_id=null){
        $this->db->select('*');
        $this->db->from('office_room');
        $this->db->where('office_id', $office_id);
        $this->db->order_by('room_name', 'asc');
        $query = $this->db->get();
        
        if($query->num_rows>0){
            return $query->result();
        }
        
        return false;
    }
    /************* Get all office details************/
    function all_office_details(){
        $this->db->select('*');
        $this->db->from('fams_office');
        $query = $this->db->get();

        if($query->num_rows>0){
            return $query->result();
        }

        return false;
    }
}

?>
