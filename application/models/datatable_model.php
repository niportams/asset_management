<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class datatable_model extends CI_Model {

    var $table = 'fams_asset';
    var $column_order = array(null, 'asset_name', 'model_name', 'serial_no', 'label_id', 'purchase_date', 'grn_no');
    var $column_search = array('asset_name', 'model_name', 'serial_no', 'label_id', 'purchase_date', 'grn_no');

    //var $order = array('asset_id' => 'asc'); // default order 


    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {

        //$this->db->from($this->table);

       // $this->db->select('*');
        //$this->db->from($this->table);
        
        $this->db->select('asset.asset_id,asset.grn_no,asset.asset_readable_id,cat.category_name,asset.asset_name,asset.model_name,asset.model_no,asset.serial_no,asset.label_id ,status.status_name');
        $this->db->select("DATE_FORMAT(asset.purchase_date,'%d-%M-%y') as purchase_date",FALSE);
        $this->db->select("DATE_FORMAT(asset.create_date,'%d-%M-%y') as create_date",FALSE);
        
        $this->db->from('fams_asset asset'); //, fams_category b, fams_manufacture c, fams_status d');
        $this->db->join('category cat', 'cat.category_id = asset.category_id', 'left');
        $this->db->join('status', 'status.status_id = asset.asset_status_id', 'left');
        //$this->db->where('a.category_id = b.category_id');
        
        //$this->db->where('a.asset_status_id = d.status_id');
        
        //$CI->db->join('user_email', 'user_email.user_id = emails.id', 'left');
        //$this->db->order_by('a.create_date', 'desc'); // or 'DESC'

        //console_log($this->db->last_query());

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search

                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}