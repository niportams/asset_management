<?php
/****************************************************/
// Filename: report_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: filter_wise_store_asset_list,filter_wise_allocation_asset_list,office_wise_asset_info,
// filter_wise_asset_info,total_count,category_wise_total,status_wise_total,room_wise_asset_detail,
// person_wise_asset_detail,category_wise_asset_detail,asset_life_expire

/****************************************************/

 /**
 * AMS Report Model Class
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class report_model extends CI_Model {

    function __construct() {
        parent:: __construct();
        $this->load->database();
    }

    /* *************Asset List Based on Asset filter************* */

    function filter_wise_store_asset_list($name = null, $category_id = null, $office_id = null, $to = null, $from = null, $organization = null, $lifetime = null) {
        $this->db->select('asset.asset_name,category.parent_id , category.category_name, COUNT(fams_asset.asset_name) as quantity ');
        $this->db->from('asset');
        $this->db->join('category', 'category.category_id = asset.category_id', 'left');
        $this->db->join('capitalize', 'capitalize.capital_id = asset.capital_id', 'left');
        if ($name != null) {
            $this->db->like('asset.asset_name ', $name);
        }
        if ($category_id != null) {
            $this->db->where('asset.category_id ', $category_id);
        }
        if ($to != null) {
            $to = DateTime::createFromFormat('d/m/Y', $to);
            $to = $to->format('Y-m-d');
            $to = $to . ' 23:59:59';
            $this->db->where('asset.create_date <=', $to);
        }
        if ($lifetime != null) {
            $lifetime = DateTime::createFromFormat('d/m/Y', $lifetime);
            $lifetime = $lifetime->format('Y-m-d');
            $lifetime = $lifetime . ' 23:59:59';
            $this->db->where('asset.asset_lifetime <=', $lifetime);
        }
        if ($from != null) {
            $from = DateTime::createFromFormat('d/m/Y', $from);
            $from = $from->format('Y-m-d');
            $from = $from . ' 00:00:00';
            $this->db->where('asset.create_date >=', $from);
        }
        if($organization != null){
            $this->db->where('capitalize.funded_by',$organization);
        }
        $this->db->where('asset.office_id', $office_id);
        $this->db->where('asset.asset_status_id', 1);
        $this->db->group_by('asset.asset_name');
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* *************Asset List Based on Asset Allocation************* */

    function filter_wise_allocation_asset_list($name = null, $category_id = null, 
    $label_id = null, $user_name = null, $office_id = null, $to = null, $from = null)
     {

        $this->db->select('a.*,b.*,c.fullname');
        $this->db->from('allocation a,asset b,user c');
        $this->db->where('a.asset_id = b.asset_id');
        $this->db->where('a.allocate_to = c.userid');

        if (($name != null) and ( $category_id == null)) {
            $this->db->where("b.asset_name LIKE '%$name%'");
        }

        if (($name != null) and ( $category_id != null)) {
            $this->db->where("b.asset_name LIKE '%$name%'");
            $this->db->where("b.category_id", $category_id);
        }

        if (($name == null) and ( $category_id != null)) {
            $this->db->where("b.category_id", $category_id);
        }

        if ($label_id <> null) {
            $this->db->where("a.asset_label_id LIKE '%$label_id%'");
        }

        if ($user_name <> null) {
            $this->db->where("c.fullname LIKE '%$user_name%'");
        }

        if ($to != null) {
            $to = DateTime::createFromFormat('d/m/Y', $to);
            $to = $to->format('Y-m-d');
            $to = $to . ' 23:59:59';
            $this->db->where('a.create_date <=', $to);
        }
        if ($from != null) {
            $from = DateTime::createFromFormat('d/m/Y', $from);
            $from = $from->format('Y-m-d');
            $from = $from . ' 00:00:00';
            $this->db->where('a.create_date >=', $from);
        }


        if ($office_id != null) {
            $this->db->where('a.allocate_office ', $office_id);
        } else {
            $this->db->where('a.allocate_office', $this->session->userdata('user_center'));
        }


        $this->db->order_by('a.create_date', 'desc');
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* **************  Onload Office Wise Asset Info***************** */

    function office_wise_asset_info() {
        $this->db->select('b.*,c.fullname');
        $this->db->from('asset b');
        $this->db->join('user c', 'b.assigned_to = c.userid', 'left');

        $this->db->where('b.office_id', $this->session->userdata('user_center'));
        $this->db->order_by('b.create_date', 'desc');
        $query = $this->db->get();

        //echo $this->db->last_query();  
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**************Asset List Based on Asset Information************* */

    function filter_wise_asset_info($name = null, $category_id = null, $label_id = null, $user_name = null, $office_id = null, $to = null, $from = null, $status = null, $organization = null, $lifetime = nulll) {

        $this->db->select('b.*,c.fullname');
        $this->db->from('asset b');
        $this->db->join('user c', 'b.assigned_to = c.userid', 'left');
        $this->db->join('capitalize', 'capitalize.capital_id = b.capital_id', 'left');

        //$this->db->where('a.asset_id = b.asset_id');
        //$this->db->where('b.assigned_to = c.userid');

        if ($status != null) {
            $this->db->where("b.asset_status_id", $status);
        }

        if (($name != null) and ( $category_id == null)) {
            $this->db->where("b.asset_name LIKE '%$name%'");
        }

        if (($name != null) and ( $category_id != null)) {
            $this->db->where("b.asset_name LIKE '%$name%'");
            $this->db->where("b.category_id", $category_id);
        }

        if (($name == null) and ( $category_id != null)) {
            $this->db->where("b.category_id", $category_id);
        }

        if ($label_id <> null) {
            $this->db->where("b.sci_id LIKE '%$label_id%' OR  b.asset_readable_id LIKE '%$label_id%'");
        }

        if ($user_name <> null) {
            $this->db->where("c.fullname LIKE '%$user_name%'");
        }

        if ($to != null) {
            $to = DateTime::createFromFormat('d/m/Y', $to);
            $to = $to->format('Y-m-d');
            $to = $to . ' 23:59:59';
            $this->db->where('b.create_date <=', $to);
        }

        if ($lifetime != null) {
            $lifetime = DateTime::createFromFormat('d/m/Y', $lifetime);
            $lifetime = $lifetime->format('Y-m-d');
            $lifetime = $lifetime . ' 23:59:59';
            $this->db->where('b.asset_lifetime <=', $lifetime);
        }

        if ($from != null) {
            $from = DateTime::createFromFormat('d/m/Y', $from);
            $from = $from->format('Y-m-d');
            $from = $from . ' 00:00:00';
            $this->db->where('b.create_date >=', $from);
        }

        if ($office_id != null) {
            $this->db->where('b.office_id', $office_id);
        } else {
            $this->db->where('b.office_id', $this->session->userdata('user_center'));
        }
        if($organization != null){
            $this->db->where('capitalize.funded_by',$organization);
        }


        $this->db->order_by('b.create_date', 'desc');
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /********* Count total number of asset of an office **********/
    function total_count() {
        $this->db->select('Count(*) as total');
        $this->db->from('asset');
        $this->db->where('office_id ', 1);
        $query = $this->db->get()->result();

        if (isset($query[0]->total)) {
            return $query[0]->total;
        } else
            return 0;
    }

    /********* Category wise total asset of an office **********/
    function category_wise_total($office_id) 
    {
        $this->db->select('category.category_name as category,category.category_id, COUNT(*) as total');
        $this->db->from('asset');
        $this->db->join('category', 'category.category_id = asset.category_id', 'left');
        $this->db->group_by('asset.category_id');
        $this->db->order_by('category.category_name', 'asc');
        $this->db->where('asset.office_id', $office_id);

        $query = $this->db->get()->result();
        //echo $this->db->last_query();
        //exit();

        return $query;
    }
    
    /********* Status wise total asset of an office **********/
    function status_wise_total($office_id) {
        $this->db->select('status.status_name as status_name, COUNT(*) as total');
        $this->db->from('asset');
        $this->db->join('status', 'status.status_id = asset.asset_status_id', 'left');
        $this->db->group_by('asset.asset_status_id');
        $this->db->order_by('status_name', 'asc');
        $this->db->where('asset.office_id', $office_id);

        $query = $this->db->get()->result();
        //echo $this->db->last_query();
        //exit();

        return $query;
    }

    /********* Room wise allocated asset information of an office **********/
    function room_wise_asset_detail($room_id, $office_id) {
        $this->db->select('allocation.*, asset.asset_name,category.category_name as category,user.fullname'); //office.office_name,office_room.room_name
        $this->db->from('allocation');
        $this->db->join('asset', 'asset.asset_id = allocation.asset_id', 'left');
        $this->db->join('category', 'category.category_id = allocation.category_id', 'left');
        $this->db->join('user', 'user.userid = allocation.on_behalf', 'left');
        //$this->db->join('office', 'office.office_id = allocation.allocate_office', 'left');
        //$this->db->join('office_room', 'office_room.room_id = allocation.asset_location', 'left');
        //$this->db->group_by('asset.asset_status_id');
        //$this->db->order_by('status_name','asc');
        $this->db->where('allocation.asset_location ', $room_id);
        $this->db->where('allocation.allocate_office ', $office_id);
        $query = $this->db->get();
        
        if ($query->num_rows > 0) {
           
            return $query->result();
        }
        
        return false;       
    }
/********* Person wise allocated asset information of an office **********/
    function person_wise_asset_detail($person_id, $office_id) {
        $this->db->select('allocation.*, asset.asset_name,category.category_name as category,user.fullname'); //office.office_name,office_room.room_name
        $this->db->from('allocation');
        $this->db->join('asset', 'asset.asset_id = allocation.asset_id', 'left');
        $this->db->join('category', 'category.category_id = allocation.category_id', 'left');
        $this->db->join('user', 'user.userid = allocation.on_behalf', 'left');
        $this->db->where('allocation.allocate_to ', $person_id);
        $this->db->where('allocation.allocate_office ', $office_id);
        $query = $this->db->get();

        if ($query->num_rows > 0) {

            return $query->result();
        }

        return false;
    }

    /********* Category wise asset information of all office **********/
    function category_wise_asset_detail($cat_id) {
        $this->db->select('office.office_name,category.category_name as category, COUNT(*) as quantity');
        $this->db->from('asset');
        $this->db->join('category', 'category.category_id = asset.category_id', 'left');
        $this->db->join('office', 'office.office_id = asset.office_id', 'left');
        $this->db->group_by('asset.office_id');
        $this->db->order_by('category.category_name', 'asc');
        $this->db->where('asset.category_id', $cat_id);
        $query = $this->db->get();

        if ($query->num_rows > 0) {

            return $query->result();
        }

        return false;
    }
    /********* Lifetime expiration of all asset **********/
    public function asset_life_expire ($year=NULL, $month=NULL){
        
        $like_val =($year===NULL)?date('Y'):$year;
        $like_val .=($month===NULL)?'':'-'.(($month>9)?$month:'0'.$month);
        
        $this->db->select('*,category_name,status_name');
        $this->db->from('asset');
        $this->db->join('category', 'category.category_id = asset.category_id');
        $this->db->join('status', 'status.status_id = asset.asset_status_id ');
        $this->db->like('asset_lifetime',$like_val );
        $this->db->where('office_id',$this->session->userdata('user_center'));
        $query = $this->db->get();
        
        //echo $this->db->last_query();
        $result = $query->result();
       
        return $result;
    }
}

?>
