<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class requisition_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }


    function departmentlist()
    {
        $this->db->select('*');
        $this->db->from('fams_department');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function flowlist()
    {
        $this->db->select('*');
        $this->db->from('fams_requisition_flow');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function flow_delete($flow_id)
    {
        $this->db->where('id', $flow_id);
        //$this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->delete('fams_requisition_flow');
        return true;
    }

    function transfer_flowlist()
    {
        $this->db->select('*');
        $this->db->from('fams_transfer_flow');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    function transfer_flow_delete($flow_id)
    {
        $this->db->where('id', $flow_id);
        //$this->db->where('created_by', $this->session->userdata('user_db_id'));
        $query = $this->db->delete('fams_transfer_flow');
        return $query;
    }

    function approver_List($requisition_id){
        $this->db->select('a.*, fams_user.fullname');
        $this->db->from('fams_requisitions_approvals a');
        $this->db->join('fams_user','fams_user.userid = a.approved_by');
        $this->db->where('requisition_id',$requisition_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}

?>
