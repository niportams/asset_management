<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of print_model
 *
 * @author shahed.chaklader
 */
class print_model extends CI_Model {

    function __construct() {
        parent:: __construct();
        $this->load->database();
    }

    function allocated_requisition_info($req_num) {
        $this->db->select('*, allocation.create_date as allocation_date,allocation.reference_no as allocation_ref');
        $this->db->from('allocation');
        $this->db->join('asset', 'asset.asset_id = allocation.asset_id', 'left');
        $this->db->where('requisition_no', $req_num);
        //$this->db->order_by('create_date', 'desc'); // or 'DESC'

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function asset_detail($asset_id = null) {
        $this->db->select('*');
        $this->db->from('asset');
        //$this->db->join('asset', 'asset.asset_id = allocation.asset_id', 'left');
        $this->db->where('asset_id', $asset_id);


        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function asset_list($asset_id = null, $office_id = null) {
        $this->db->select('*');
        $this->db->from('asset');
        $this->db->join('category', 'category.category_id = asset.category_id', 'left');
        $this->db->join('status', 'status.status_id = asset.asset_status_id', 'left');

        if ($asset_id != null) {
            $this->db->where('asset_id', $asset_id);
        }
        
        if($office_id!=null){
            $this->db->where('office_id ', $office_id );
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
