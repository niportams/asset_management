<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: physical_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: check_list,physical_create,physical_details,check_delete,

/****************************************************/

 /**
 * AMS Asset Physical Check Model Class
 */


class physical_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    //******Get Physical Check List************/
    function check_list()
    {
        $this->db->order_by('create_date', 'desc'); // or 'DESC'
        $this->db->select('*');
        $this->db->from('fams_physical_check');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /***********Asset Physical Check Create****************/
    function physical_create($json_array)
    {

        $this->db->insert('department', $json_array);
        //return true;
        return $this->db->insert_id();

    }

    /***********Physical checking details***********/
    function physical_details($check_id)
    {
        $this->db->select('*');
        $this->db->from('fams_physical_check');
        $this->db->where('physical_id', $check_id);
        $query = $this->db->get();

        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    /**********Physical Check Delete************/
    function check_delete($check_id)
    {
        $this->db->where('physical_id', $check_id);
        $this->db->delete('fams_physical_check');
    }

}

?>
