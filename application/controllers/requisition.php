<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/****************************************************/
// Filename: requisition.php
// Created By:     Hasibul Huq
// Change history:
//
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: index,loggedinuser,loggedout,add_flow,add_transfer_flow,flowlist,transfer_flowlist,
// flow_update,flow_delete,transfer_flow_update,transfer_flow_delete
//
/****************************************************/
/**
 * Description of requisition
 *
 * @author hasibul.huq
 */
class requisition extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('requisition_model', '', TRUE);
        $this->load->model('user_model', '', TRUE);
        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Asset');
        $this->set_page_sub_title('control panel');
    }

    /******** Add New Requisition Flow **************/
    public function add_flow()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/asset_script.js');
        $this->set_js('dist/js/sci/project.js');
        $user_center = $this->session->userdata('user_center');
        //var_dump($user_center); 
        //echo $data['user_center'] = $session_data['user_name'];

        $this->set_page_title(load_message('NEW_REQUISITION_FLOW'));
        $this->set_page_sub_title('');

        $result = $this->requisition_model->departmentlist();
        if($result <> false)
            {
            $this->set_value('department_list', $result);   
            }
        else
            {
            $this->set_value('department_list', "");
            }
        $result4 = $this->user_model->userlist();

        //var_dump($result); 
        $this->set_value('user_center', $user_center);
        $this->set_value('user_list',$result4);
        $this->load_view('admin_lte/requisition/new_requisition_flow');
    }

    /************* New Transfer Flow Add ******************/

    public function add_transfer_flow()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/asset_script.js');
        $this->set_js('dist/js/sci/project.js');
        $user_center = $this->session->userdata('user_center');
        
        //var_dump($user_center); 
        //echo $data['user_center'] = $session_data['user_name'];

        $this->set_page_title(load_message('NEW_TRANSFER_FLOW'));
        $this->set_page_sub_title('');

        $result4 = $this->user_model->userlist();
                
        $this->set_value('user_list',$result4);
        $this->load_view('admin_lte/requisition/new_transfer_flow');
    }

    /******** Requisition Flow List **************/
    public function flowlist()
    {
        $this->set_js('dist/js/asset_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/sci/project.js');
        $user_office = $this->session->userdata('user_center');
        $user_role = get_user_role_name($this->session->userdata('user_role_id'));

        $this->set_page_title(load_message('FLOW_LIST'));
        $this->set_page_sub_title('');

        $result = $this->requisition_model->flowlist();
      
        //var_dump($result);
        $this->set_value('flow_list', $result);
        $this->load_view('admin_lte/requisition/flow_list');
    }

    /******** Transfer Flow List **************/
    public function transfer_flowlist()
    {
        $this->set_js('dist/js/asset_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/sci/project.js');

        $user_office = $this->session->userdata('user_center');
        $user_role = get_user_role_name($this->session->userdata('user_role_id'));

        $this->set_page_title(load_message('TRANSFER_FLOW_LIST'));
        $this->set_page_sub_title('');

        $result = $this->requisition_model->transfer_flowlist();
      
        //var_dump($result);
        $this->set_value('transfer_flow_list', $result);
        $this->load_view('admin_lte/requisition/transfer_flowlist');
    }

    /************* Requisition Flow Update  ********************/
    public function flow_update()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = $this->input->post('jsondata', true);
            $jsondata = json_decode($jsondata, true);
            $updateid = $jsondata['update_id'];
           
            unset($jsondata['update_id']);

            //$updateid = base64_decode(urldecode($jsondata['update_id'])); 
            
            $result = sci_update_db('fams_requisition_flow', $jsondata, ['id' => $updateid]);
            if ($result <> false) {
                 /**** Audit Log ****/
                $action_name = "Requisition Flow Update";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/
                echo '{"status": "success", "message": "' . $this->lang->line('FLOW_UPDATED') . '"}';
            }
        } else {
           exit('No direct script access allowed');
        }
    }
    /************  Requisition Flow Delete *************/

    public function flow_delete()
    {
      if ($this->input->is_ajax_request()) {
            $flow_id = $this->input->post('flow_id', true);
           
            $result = $this->requisition_model->flow_delete($flow_id);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Requisition Flow Update";
                log_create($action_name,$flow_id);
                /**** Audit log end here *****/

                echo $this->lang->line('FLOW_DELETED');
            }
        } else {
           exit('No direct script access allowed');
        }  
    }

    /***************  Transfer Flow Update *****************/
    public function transfer_flow_update()
    {
      if ($this->input->is_ajax_request()) {
            $jsondata = $this->input->post('jsondata', true);
            $jsondata = json_decode($jsondata, true);
            $updateid = $jsondata['update_id'];
           
            unset($jsondata['update_id']);

            //$updateid = base64_decode(urldecode($jsondata['update_id']));

            $result = sci_update_db('fams_transfer_flow', $jsondata, ['id' => $updateid]);
            if ($result <> false) 
            {

            /**** Audit Log ****/
            $action_name = "Transfer Flow Update";
            log_create($action_name,json_encode($jsondata,true));
            /**** Audit log end here *****/

            echo '{"status": "success", "message": "' . $this->lang->line('TRANSFER_FLOW_UPDATED') .'"}';
            }
        } else {
           exit('No direct script access allowed');
        }  
    }

    /************  Transfer Flow Delete *************/
    public function transfer_flow_delete()
    {
      if ($this->input->is_ajax_request()) 
      {
            
            $flow_id = $this->input->post('flow_id', true);
            $result = $this->requisition_model->transfer_flow_delete($flow_id);
            if ($result <> false) {
                /**** Audit Log ****/
                $action_name = "Transfer Flow Delete";
                log_create($action_name,$flow_id);
                /**** Audit log end here *****/

                echo $this->lang->line('FLOW_DELETED');
            }
        } else {
           exit('No direct script access allowed');
        }  
    }

}
