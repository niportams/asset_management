<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: role.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: addrole,rolelist,role_delete,role_edit
//
/****************************************************/

 /**
 * AMS user role Controller Class
 *
 * This method demonstrates the role CRUD operation of AMS.
 */

class role extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();


        $this->load->model('user_model', '', TRUE);
        $this->load->model('role_model', '', TRUE);

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('User Role');
        $this->set_page_sub_title('control panel');
        //$this->set_value('test', 'sdfsdfsdfs');

        //$this->load_view('admin_lte/dashboard');
    }

    /********Create new role**************/
    public function addrole()
    {
        $this->set_page_title(load_message('ADD_NEW_ROLE'));
        $this->set_page_sub_title('');

        $this->set_js('dist/js/sci/project.js');

        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/general_script.js');
        $result = $this->role_model->rolelist();
        $result1 = $this->role_model->privilegelist();

        //var_dump($result);
        $this->set_value('role_list', $result);
        $this->set_value('privilege_list', $result1);
        $this->load_view('admin_lte/role/role_create');
    }

    /********** Show all Roles ***************/
    public function rolelist()
    {
        $this->set_page_title(load_message('ROLE_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');

        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/general_script.js');
        $result = $this->role_model->rolelists();
        //var_dump($result);

        $this->set_value('role_list', $result);

        $this->load_view('admin_lte/role/role_list');
    }

    /***********Role Delete***************/

    public function role_delete()
    {
        if ($this->input->is_ajax_request()) {
            $db_roleid = $this->input->post('role_id', true);
            $this->role_model->role_delete($db_roleid);
            //echo"Role Deleted Permanently!";

            /***** Audit Log *****/
            $action_name = "Role Delete";
            log_create($action_name,$db_roleid);
            /***** Audit Log *****/
            echo $this->lang->line('DELETE');
        } else {
            exit('No direct script access allowed');
        }

    }

    /***********Role Update***************/
    public function role_edit()
    {
        if ($this->input->is_ajax_request()) {
            $db_roleid = $this->input->post('role_id', true);
            $role_name = $this->input->post('role_name', true);
            $role_rank = $this->input->post('role_rank', true);
            $check_string = $this->input->post('check_string', true);
            $role_status = $this->input->post('role_status', true);

            $len = strlen($check_string);
            $check_string[$len] = '';
            $privilege = trim($check_string);

            $res = $this->role_model->role_update($db_roleid, $role_name,$role_rank, $privilege, $role_status);
            if($res<>false)
            {
            // echo"Saved Changes!";
                /**** Audit Log ****/
                $action_name = "User Role Edit";
                $allocate_json["role_id"] = $db_roleid;
                $allocate_json["role_name"] = $role_name;
                $allocate_json["role_rank"] = $role_rank;
                $allocate_json["privilege"] = $privilege;
                $allocate_json["role_status"] = $role_status;

                log_create($action_name,json_encode($allocate_json,true));
                /**** Audit log end here *****/
                echo $this->lang->line('UPDATE');
            }
        } else {
            exit('No direct script access allowed');
        }
    }

}
