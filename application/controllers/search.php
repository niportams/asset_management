<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: search.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: allocated_list,transfered_list,revert,transfer,
// state,disposedlist,maintenancelist
//
/****************************************************/

 /**
 * AMS Asset Search Controller Class
 *
 * This method demonstrates the searching process of AMS.
 */
class search extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('user_model', '', TRUE);
        $this->load->model('manufacture_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('search_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);
        $this->load->model('location_model', '', TRUE);

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Search');
        $this->set_page_sub_title('');
        $q = str_replace('+', ' ', $this->input->get('q', TRUE));
        $result = $this->search_model->allocated_list($q);

        $this->set_value('search_q', $q);
        //var_dump($result); 
        if ($result <> false) {
            $this->set_value('alloc_list', $result);
        } else {
            $this->set_value('alloc_list', "");
        }
        $this->set_value('alloc_list', $result);
        $this->load_view('admin_lte/search/search_asset');
    }


    /************** Allocated list of a office**********************/
    public function allocated_list()
    {
        $this->set_page_title('Allocated Asset');
        $this->set_page_sub_title('list');

        $result = $this->asset_model->allocated_list();

        //var_dump($result); 
        if ($result <> false) {
            $this->set_value('alloc_list', $result);
        } else {
            $this->set_value('alloc_list', "");
        }
        $this->set_value('alloc_list', $result);
        $this->load_view('admin_lte/asset/allocated_list');
    }

    /************** Transfered list of an asset **********************/
    public function transfered_list()
    {
        $this->set_page_title('Transferred Asset');
        $this->set_page_sub_title('list');

        $result = $this->asset_model->transfered_list();

        //var_dump($result); 
        if ($result <> false) {
            $this->set_value('transfer_list', $result);
        } else {
            $this->set_value('transfer_list', "");
        }

        $this->load_view('admin_lte/asset/transfered_list');
    }

    /************** Asset Reverting********************/
    public function revert()
    {
        if ($this->input->is_ajax_request()) {
            $allo_id = $this->input->post('allo_id', true);
            $revert_id = $this->input->post('revert_id', true);
            $revert_type = $this->input->post('revert_type', true);

            //$decrypt_asset = base64_decode(urldecode($asset_id));

            $res = $this->asset_model->asset_revert($allo_id, $revert_id, $revert_type);
            if ($res <> false) {
                //echo $this->lang->line('REVERT');
                echo "Asset is reverted";
            }

        } else {
            exit('No direct script access allowed');
        }
    }

    /**************  Asset Transfer to Requested User ******************/

    public function transfer()
    {
        $user_center = $this->session->userdata('user_center');
        if ($this->input->is_ajax_request()) {
            $item_id = $this->input->post('item_id', true);
            $requisition_id = $this->input->post('requisition_id', true);
            $category_id = $this->input->post('category_id', true);
            $asset_qty = $this->input->post('asset_qty', true);
            $office_id = $this->input->post('office_id', true);
            $allocate_to = $this->input->post('allocate_to', true);
            $allocate_type = $this->input->post('allocate_type', true);

            $result2 = $this->asset_model->requested_category_available($category_id, $asset_qty);
            // var_dump($result2);
            if ($result2 <> false) {
                /************* Add assets to the allocation list *****************/
                $result1 = $this->asset_model->cat_wise_asset_list($category_id, $asset_qty);
                //var_dump($result1);
                if ($result1 <> false) {
                    foreach ($result1 as $rest12) {
                        $transfer_asset_id = $rest12->asset_id;
                        $transfer_category_id = $rest12->category_id;
                        $asset_label_id = $rest12->asset_readable_id;

                        $transfer_by = $this->session->userdata('user_db_id');
                        $transfer_to = $allocate_to;
                        $transfer_office = $office_id;
                        $remarks = "";

                        $req = $this->asset_model->transfer_asset($transfer_asset_id, $asset_label_id, $transfer_category_id, $requisition_id,
                            $transfer_by, $transfer_to, $transfer_office, $remarks);
                        if ($req == false) {
                            //echo"Failed to insert data";
                        } else {
                            /*********** Update Base Asset Table **************/
                            //echo"succesfully allocated!";
                            $req1 = $this->asset_model->update_asset_status($transfer_asset_id, $transfer_to);
                            $req2 = $this->asset_model->update_requisition_asset($item_id, $requisition_id);
                            //var_dump($req2);
                        }
                    }
                    echo $this->lang->line('TRANSFER_SUCCESS');

                }
                /************ Asset Transfer end here*************/
            } else {
                echo "Failed to allocate!";
            }
        } else {
            exit('No direct script access allowed');
        }

    }

    /**************** Asset Deatils Information Showing  ********************/
    /*
    public function details()
    {
     if ($this->input->is_ajax_request())
          {
             $jsondata =  $this->input->post('jsondata',true);
             $jsondata = json_decode($jsondata, true);
             echo $detailid= base64_decode(urldecode($jsondata->asset_id));

            $res = $this->asset_model->asset_details($detailid);
            var_dump($res);
             if($res<>false)
            {
              $this->set_value('asset_details',$res);
            }
            else
            {
               $this->set_value('asset_details',"");
            }

              $this->load_view('admin_lte/asset/asset_details');
            }
        else
        {
        exit('No direct script access allowed');
        }
    }
  */
    /********Asset State from Navigation**************/
    public function state()
    {

        //$state_id = $this->uri->segment(3);
        $state_name = $this->uri->segment(3);

        $this->set_page_title('Asset State');
        $this->set_page_sub_title($state_name . '&nbsp; state entry');

        //$this->set_value('state_id',$state_id);
        $this->set_value('chosen_state_name', $state_name);

        $this->load_view('admin_lte/asset/asset_search_form');
    }

    /************ Asset Disposed List  *************/

    public function disposedlist()
    {
        $this->set_page_title('Disposed Asset');
        $this->set_page_sub_title('list');


        $status_id = 6; //Disposed Item
        $result = $this->asset_model->status_wise_asset_list($status_id);
        $rests = $this->asset_model->statuslist();
        //var_dump($rests);

        if ($result <> false) {
            $this->set_value('asset_list', $result);
        } else {
            $this->set_value('asset_list', "");
        }

        $this->set_value('sts_list', $rests);
        $this->load_view('admin_lte/asset/disposed_list');
    }

    /************ Asset Maintenance List  *************/

    public function maintenancelist()
    {
        $this->set_page_title('Asset Maintenance');
        $this->set_page_sub_title('list');

        $status_id = 4; //Disposed Item

        $result = $this->asset_model->status_wise_asset_list($status_id);
        $rests = $this->asset_model->statuslist();
        //var_dump($rests);

        if ($result <> false) {
            $this->set_value('asset_list', $result);
        } else {
            $this->set_value('asset_list', "");
        }

        $this->set_value('sts_list', $rests);
        $this->load_view('admin_lte/asset/maintenance_list');
    }
}
