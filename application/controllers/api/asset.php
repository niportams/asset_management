<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: asset.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: category_api,asset_get,
//
/****************************************************/

/**
 * AMS Asset(API related) Controller Class
 * This method demonstrates the API implementation of AMS.
 */

class asset extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/apiassetmodel','apimodel',TRUE);
        $this->load->model('user_model', '', TRUE);
    }
    public function index()
    {}

  /** 
  *This function generates the API key of AMS. 
  */
  public function category_api($key)
  {
     $db_key = $this->config->item('public_key');

     if($key == $db_key)
     {
     $category_id = $this->input->get('category',true);
     $office_id = $this->input->get('office',true);
     
     if($category_id <> "")
     {

      $total_category = 1;
      $rest = $this->apimodel->catname($category_id);
      if($rest<>false)
          {
             $category_name = $rest[0]->category_name; 
          }
          else { $category_name = "Invalid Category."; }
      }
     else
        {
          $category_name = "All";
          $rest = $this->apimodel->countcategory($office_id);
          $total_category = $rest[0]->cnt;
        }

     if($office_id <> "")
     {
        $rest1 = $this->apimodel->offname($office_id);
        if($rest1<>false)
          {
             $office_name = $rest1[0]->office_name; 
          }
          else
              {
                $office_name = "";
              }
     }


     if($category_id <> "")
     {
      /********* category Total in Store************/
      $status_id = 1; $asset_in_store = 0; $total_allocated=0;
      $total_maintenance = 0; $dead_state = 0;
      
        $rest22 = $this->apimodel->cattotal($office_id, $category_id);
        if($rest22 <> false)
          {
             $totalcat = $rest22[0]->cnt;
          }
        $result1 = $this->apimodel->category_instore($category_id,$office_id,$status_id);
          if($result1 <> false)
           {
              $asset_in_store = $result1[0]->total;
           }

        $result2 = $this->apimodel->category_instore($category_id,$office_id,2);
          if($result2 <> false)
           {
              $asset_in_transfer = $result2[0]->total;
           } 

        $result2 = $this->apimodel->category_instore($category_id,$office_id,3);
          if($result2 <> false)
           {
              $asset_in_dead = $result2[0]->total;
           }

         $result2 = $this->apimodel->category_instore($category_id,$office_id,4);
          if($result2 <> false)
           {
             $asset_in_maintenance = $result2[0]->total;
           }

         $result2 = $this->apimodel->category_instore($category_id,$office_id,5);
          if($result2 <> false)
           {
             $asset_in_allocated = $result2[0]->total;
           } 

         $result2 = $this->apimodel->category_instore($category_id,$office_id,6);
          if($result2 <> false)
           {
             $asset_disposed = $result2[0]->total;
           }                  

        $single_data[0] = array('Category Name'=>$category_name, 'Total'=>$totalcat,
          'total_in_store'=>$asset_in_store,'total_allocated'=>$asset_in_allocated,
          'total_in_maintainence'=>$asset_in_maintenance,'total_dead_state'=>$asset_in_dead,
          'total_disposed'=>$asset_disposed,'total_transfered'=>$asset_in_transfer);

        //var_dump($single_data);
        $acb = json_encode( $single_data);
        $acb = json_decode( $acb);
        $arr1 = array('Total Category' => $total_category, 'Office' => $office_name,
        'Category Name' => $category_name, 'Category' =>$acb,);
        $data['json1'] = json_encode( $arr1);
     }
     else /******* All Category of an office *************/
     {
        $i = 0;
        $all_res = $this->apimodel->DistinctCategory($office_id);
        //var_dump($all_res);
        $totalcat = sizeof($all_res);
        if($all_res <> false)
        {
           foreach($all_res as $res)
           {
            $category_id = $res->category_id;

             $c_name = $this->apimodel->catname($category_id);
             //var_dump($c_name);

            $internal_cat_name = $c_name[0]->category_name;

            $asset_in_store = 0; $total_allocated=0;
            $total_maintenance = 0; $dead_state = 0;
      
            $rest22 = $this->apimodel->cattotal($office_id, $category_id);
            if($rest22 <> false)
                {
                   $totalcat = $rest22[0]->cnt;
                }
            $result1 = $this->apimodel->category_instore($category_id,$office_id,1);
                if($result1 <> false)
                 {
                     $asset_in_store = $result1[0]->total;
                 }

            $result2 = $this->apimodel->category_instore($category_id,$office_id,2);
              if($result2 <> false)
               {
                  $asset_in_transfer = $result2[0]->total;
               } 

              $result2 = $this->apimodel->category_instore($category_id,$office_id,3);
                if($result2 <> false)
                 {
                    $asset_in_dead = $result2[0]->total;
                 }

              $result2 = $this->apimodel->category_instore($category_id,$office_id,4);
                if($result2 <> false)
                 {
                   $asset_in_maintenance = $result2[0]->total;
                 }

             $result2 = $this->apimodel->category_instore($category_id,$office_id,5);
              if($result2 <> false)
               {
                 $asset_in_allocated = $result2[0]->total;
               } 

             $result2 = $this->apimodel->category_instore($category_id,$office_id,6);
              if($result2 <> false)
               {
                 $asset_disposed = $result2[0]->total;
               }                  

              $single_data[$i] = array('Category Name'=>$internal_cat_name, 'Total'=>$totalcat,
              'total_in_store'=>$asset_in_store,'total_allocated'=>$asset_in_allocated,
              'total_in_maintainence'=>$asset_in_maintenance,'total_dead_state'=>$asset_in_dead,
              'total_disposed'=>$asset_disposed,'total_transfered'=>$asset_in_transfer);
            
              $i++;
          }
            $acb = json_encode( $single_data);
            $acb = json_decode( $acb);
            $arr1 = array('Total Category' => $total_category, 'Office' => $office_name,
            'Category Name' => $category_name, 'Category' =>$acb,);

            $data['json1'] = json_encode( $arr1);
             //header('Content-Type: application/json');
             //echo json_encode( $arr1);
          }

}
  echo $this->load->view('api/category_info',$data);
  
     /*
     $category_array = array("Category Name"=>$category_name,"Office Name"=>$office_name,);
     $data['json_cat'] = json_encode( $category_array);

     $status_res = $this->apimodel->statuslist();
     if($status_res <> false)
     {
      $i = 0; $j = 0;
      foreach($status_res as $res)
      {
        $status_id = $res->status_id;
        $status_name[$i][0] = "Total- ".$res->status_name;
        
        $result1 = $this->apimodel->category_instore($category_id,$office_id,$status_id);
        if($result1 <> false)
           {
             $asset_in_store = $result1[0]->total;
             $status_name[$i][1] = $asset_in_store;
           }
           $i++;
        }
     
     }
*/

  }
  else
     {
      exit('No direct script access allowed');
     }
  }

  /** 
  *This function counts different kinds of category total of AMS. 
  */
  public function asset_get($key, $search_type, $page_id = 1)
  {

     //$key = $this->input->get('key',true); 
     $category_id = $this->input->get('cat',true);
     $office_id = $this->input->get('office',true);
     $page_id = $this->input->get('page',true);

    //echo $page_id;

    /*
    if($page_id <> 1)
    {
      $paging = explode('page',$page_id);
      $page_id = $paging[1];
    }
    */

    $limit = 300;
    $limit_to = $limit * $page_id;

    $db_key = $this->config->item('public_key');

    if($key == $db_key)
    {
     switch($search_type)
     {

      case 'category':
       //echo "got it";
          if($category_id<>0)
              {
                $asset_count = $this->apimodel->assetlist_count($category_id);
                $asset = $this->apimodel->assetlist($category_id,$limit,$limit_to);
                $data['id']=$category_id;
                $data['url'] = base_url().'api/asset/asset_get/'.$db_key.'/'.$search_type;
              }
            else
              {
                 $asset_count = $this->apimodel->assetlimit_count($id = 0);
                 $asset = $this->apimodel->assetlimit($limit,$limit_to);
                 $data['id']=0;
                 $data['url'] = base_url().'api/asset/asset_get/'.$db_key.'/'.$search_type;
              }   
        break;

      case 'office':
            if($office_id<>"")
              {

               $asset_count = $this->apimodel->rtcassetlist_count($office_id); 
               $asset = $this->apimodel->rtcassetlist($office_id,$limit,$limit_to);
               $data['id']=$office_id;
               $data['url'] = base_url().'api/asset/asset_get/'.$db_key.'/'.$search_type;
              }
            else
              {
                $asset_count = $this->apimodel->rtcassetlist_count($office_id = 1); 
                $asset = $this->apimodel->rtcassetlist($office_id=1,$limit,$limit_to);
                $data['id']=1;
                $data['url'] = base_url().'api/asset/asset_get/'.$db_key.'/'.$search_type;
              }   
        break;

      case 'count':
                 if($id=="")
                   {
                    $id = 1;
                    $data['id']=$id;
                    $asset_count = $this->apimodel->groupasset_count($id); 
                    $asset = $this->apimodel->groupasset($id,$limit,$limit_to);
                    $data['url'] = base_url().'api/asset/asset_get/'.$db_key.'/'.$search_type;
                   }
                 else
                   {
                    $data['id']=$id;
                    $asset_count = $this->apimodel->groupasset_count($id); 
                    $asset = $this->apimodel->groupasset($id,$limit,$limit_to);
                    $data['url'] = base_url().'api/asset/asset_get/'.$db_key.'/'.$search_type;
                   }
            break;
          default:
       break; 
     }

     $count = sizeof($asset_count)-1;
     if($count >$limit)
       {
         $remain_limit = $count % $limit;
         $pages = round($count / $limit);
       }
     else
        {
          $pages=1;
          $remain_limit =0;
        }
    
    $data['pages'] = $pages;
    $data['remain_limit'] = $remain_limit;
    if($remain_limit>0)
    {
      $total_page = $pages + 1;
    }
    else
    {
      $total_page = $pages;
    }

    if($asset <> false)
      {
        $arr = array('count' => $count, 'Message' => 'Record Found', 
        'Total Page'=>$total_page, 'Current Page'=>$page_id, 'data' =>$asset);
        //header('Content-Type: application/json');
        //echo json_encode( $arr);
        $data['json'] = json_encode( $arr);
        echo $this->load->view('api/asset_info',$data);
      }
     else
        {
          //echo "No Record Found!";
          $arr = array('count' => $count, 'Message' => 'Record Not Found', 
          'Total Page'=>$total_page,'Current Page'=>$page_id,'data' => $asset);
          //header('Content-Type: application/json');
          //echo json_encode( $arr);
          $data['json'] = json_encode( $arr);
          echo $this->load->view('api/asset_info', $data);
        }
      
     }
      else
      {
        exit('No direct script access allowed');
      }
  }
 
  // show asset info in public
    public function aqi($id=""){
        
        $this->load->model('asset_model', '', TRUE);
        $data['asset_details'] = $this->asset_model->asset_details($id);
        $this->load->view('api/quick_info',$data);
    }
}
