<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: manufacture.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0

// Function list: add_manufacture,manufacture_list,manufacture_delete,manufacture_edit
//
/****************************************************/

/**
 * AMS manufacture Controller Class
 *
 * This method demonstrates the manufacture crud operation of AMS.
 */

class manufacture extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('user_model', '', TRUE);
        $this->load->model('manufacture_model', '', TRUE);
        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index()
    {
        $this->set_page_title('Manufacture');
        $this->set_page_sub_title('control panel');
    }

    /** 
    *This function creates a new manufacturer of AMS.
    */
    public function add_manufacture()
    {
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/setting_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ADD_NEW_MANUFACTURE'));
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/manufacture/manufacture_create');
    }

    /** 
    *This function retrieves the manufacturer list of AMS.
    */
    public function manufacture_list()
    {
        $this->set_page_title(load_message('MANUFACTURER_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/setting_script.js');
        $this->set_js('dist/js/global_script.js');
        $result = $this->manufacture_model->manufacturelist();
        //var_dump($result);
        $this->set_value('manufacture_list', $result);
        $this->load_view('admin_lte/manufacture/manufacture_list');
    }


    /** 
    *This function deletes an existing manufacturer from database.
    */

    public function manufacture_delete()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true));
            $result = sci_delete_db('manufacture', ['manufacture_id' => $jsondata->manufacture_id]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Manufacturer Delete";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/

                echo $this->lang->line('DELETE');
            }
        } else {
            exit('No direct script access allowed');
        }
    }

    /** 
    *This function updates an existing manufacturer details of AMS.
    */

    public function manufacture_edit()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true));
            $result = sci_update_db('manufacture', $jsondata, ['manufacture_id' => $jsondata->manufacture_id]);
            if ($result <> false) {
                /**** Audit Log ****/
                $action_name = "Manufacturer Update";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/
                echo $this->lang->line('UPDATE');
            }
        } else {
            exit('No direct script access allowed');
        }
    }

}
