<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: dashboard.php
// Created By:  Shahed
// Change history:
//
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: index,
//
/****************************************************/


class Dashboard extends SCI_Controller
{
    function __construct()
    {
        parent::__construct();


        $this->load->model('category_model', '', TRUE);
        $this->load->model('asset_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);
        $this->load->model('location_model', '', TRUE);

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }
    /**
     * Dynamic dashboard for different  role user
     */

    public function index()
    {
        $this->load->helper('chart');
        $this->set_page_title(load_message('DASHBOARD'));
        $this->set_js('dist/js/sci/project.js');
        $this->set_page_sub_title('');
        // $this->set_message('hello','danger',5);
        if (get_user_role_name() == "USER") {
            $this->load->model('asset_model', '', TRUE);
            $alloc_list = $this->asset_model->allocated_list_for_user();
            if ($alloc_list <> false) {
                $this->set_value('alloc_list', $alloc_list);
            } else {
                $this->set_value('alloc_list', null);
            }
        }

        $this->load_view('admin_lte/dashboard');
    }
}
