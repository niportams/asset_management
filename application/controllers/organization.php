<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: organization.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: add_organization,organization_list,organization_delete,organization_edit
//
/****************************************************/

 /**
 * AMS Asset organization Controller Class
 *
 * This method demonstrates the organization crud operation of AMS.
 */
class organization extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('user_model', '', TRUE);
        $this->load->model('organization_model', '', TRUE);
        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index()
    {
        $this->set_page_title('Manufacture');
        $this->set_page_sub_title('control panel');
    }

    /********Create new Organization**************/
    public function add_organization()
    {
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ADD_NEW_ORGANIZATION'));
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/organization/organization_create');
    }

    /**********Show all Organization as list***************/
    public function organization_list()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/setting_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ORGANIZATION_LIST'));
        $this->set_page_sub_title('');

        $result = $this->organization_model->organization_list();
        //var_dump($result);

        $this->set_value('organization_list', $result);

        $this->load_view('admin_lte/organization/organization_list');
    }

    /*********** Organization Delete ***************/
    public function organization_delete()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true));
            $result = sci_delete_db('organization', ['organization_id' => $jsondata->organization_id]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Organization Delete";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/

                echo $this->lang->line('DELETE');
            }
        } else {
            exit('No direct script access allowed');
        }
    }

    /*********** Organization Update ***************/
    public function organization_edit()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true), true);
            $organization_id = $jsondata["update_id"];
            unset($jsondata["update_id"]);
            $result = sci_update_db('organization', $jsondata, ['organization_id' => $organization_id]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Organization Update";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/

                echo '{"status": "success", "message": "' . $this->lang->line('ORGANIZATION_UPDATED') . '"}';
            }


        } else {
            exit('No direct script access allowed');
        }
    }

}
