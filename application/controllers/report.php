<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/****************************************************/
// Filename: report.php
// Created By:     Evana Yasmin 
// Change history: rtc_quick_report,rtc_room_wise_report,rtc_person_wise_report,category_wise_report,
// asset_life_expire,asset_value,get_asset_by_status,get_asset_by_category     
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: asset,allocation,asset_info,rtc_quick_report,rtc_room_wise_report,
// rtc_person_wise_report,category_wise_report,asset_life_expire,asset_value,get_asset_by_status,
// get_asset_by_category,
/****************************************************/

/**
 * AMS Asset report Controller Class
 *
 * This method demonstrates the asset reporting of AMS.
 */
class report extends SCI_Controller {

    function __construct() {
        parent::__construct();


        $this->load->model('user_model', '', TRUE);
        $this->load->model('manufacture_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('asset_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);
        $this->load->model('organization_model', '', TRUE);
        $this->load->model('location_model', '', TRUE);
        $this->load->model('report_model', '', TRUE);

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index() {
        $this->set_page_title('Report');
        $this->set_page_sub_title('control panel');
        //$this->set_value('test', 'sdfsdfsdfs');
        //$this->load_view('admin_lte/dashboard');
    }

    /******** Asset Store Reporting **************/

    public function asset() {
        $this->set_page_title(load_message('STORE_REPORTING'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/report_script.js');
        $this->set_js('dist/js/sci/project.js');

        $status_id = 1; //Registered Item
        $categories = $this->category_model->categorylist();
        $office = $this->office_model->officelist();
        $status = $this->asset_model->statuslist();
        $user_center = $this->session->userdata('user_center');
        $org_list = $this->organization_model->organization_list();
        $result = $this->report_model->filter_wise_store_asset_list('', '', $user_center, null, null);

        if ($result <> false) {
            $this->set_value('asset_list', $result);
        } else {
            $this->set_value('asset_list', "");
        }
        $this->set_value('user_center', $user_center);
        $this->set_value('category_list', $categories);
        $this->set_value('sts_list', $status);
        $this->set_value('office_list', $office);
        $this->set_value('org_list', $org_list);
        $this->load_view('admin_lte/report/asset_list');
    }

    /********Asset Allocation History Report************* */

    public function allocation() {
        $this->set_page_title(load_message('ASSET_ALLOCATION_REPORTING'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/report_script.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/asset_script.js');
        $status_id = 1; //Registered Item
        $categories = $this->category_model->categorylist();
        $office = $this->office_model->officelist();
        $status = $this->asset_model->statuslist();
        $user_center = $this->session->userdata('user_center');
        $result = $this->report_model->filter_wise_allocation_asset_list('', '', $user_center, null, null);

        if ($result <> false) {
            $this->set_value('allocation_list', $result);
        } else {
            $this->set_value('allocation_list', "");
        }
        $this->set_value('user_center', $user_center);
        $this->set_value('category_list', $categories);
        $this->set_value('sts_list', $status);
        $this->set_value('office_list', $office);
        $this->load_view('admin_lte/report/allocation_list');
    }

    /********All Asset Information Report************* */

    public function asset_info() {
        $this->set_page_title(load_message('ASSET_REPORTING'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/report_script.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/asset_script.js');

        $status_id = 1; //Registered Item
        $categories = $this->category_model->categorylist();
        $office = $this->office_model->officelist();
        $status = $this->asset_model->statuslist();
        $user_center = $this->session->userdata('user_center');
        $org_list = $this->organization_model->organization_list();
        $result = $this->report_model->office_wise_asset_info();

        if ($result <> false) {
            $this->set_value('asset_info', $result);
        } else {
            $this->set_value('asset_info', "");
        }

        $this->set_value('user_center', $user_center);
        $this->set_value('category_list', $categories);
        $this->set_value('sts_list', $status);
        $this->set_value('office_list', $office);
        $this->set_value('org_list', $org_list);
        $this->load_view('admin_lte/report/info_list');
    }

   /******** RTC Quick Report **************/
    public function rtc_quick_report() {
        $this->load->helper('asset_detail');
        
        $this->set_page_title(load_message('QUICK_REPORT_TITLE'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');

        if ($this->input->post()) {
            
            if($this->input->post('action')==='status'){
                
                $this->get_asset_by_status($this->input->post('action_status'),$this->input->post('rtc_id'));
                return;
            }
            
            if($this->input->post('action')==='cat_status'){
                
                $this->get_asset_by_category($this->input->post('action_status'),$this->input->post('rtc_id'));
                return;
            }
            
            //echo $this->input->post('rtc_id');
            $t = $this->report_model->category_wise_total($this->input->post('rtc_id'));
            $status_t = $this->report_model->status_wise_total($this->input->post('rtc_id'));

            $data['category'] = $t;
            $data['status'] = $status_t;

            $this->load->view('admin_lte/report/rtc_quick_report_ajax', $data);
        } else {
            $office = $this->office_model->officelist();
            $this->set_value('office_list', $office);

            $this->load_view('admin_lte/report/rtc_quick_report');
        }
    }

   /******** RTC Room Wise Asset Report **************/
    public function rtc_room_wise_report() {

        $this->set_page_title('Room Wise Report');
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/asset_script.js');
        if ($this->input->post()) {
            $rtc_id = $this->input->post('rtc_id');
            $room_id = $this->input->post('rtc_room_id');


            $rooms = $this->report_model->room_wise_asset_detail($room_id, $rtc_id);

            $data['rooms'] = $rooms;

            $this->load->view('admin_lte/report/rtc_room_wise_report_ajax', $data);
        } else {
            $office = $this->office_model->officelist();
            $this->set_value('office_list', $office);

            $this->load_view('admin_lte/report/rtc_room_wise_report');
        }
    }

    /******** RTC Person Wise Asset Report **************/
    public function rtc_person_wise_report() {

        $this->set_page_title('Person Wise Report');
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/asset_script.js');
        if ($this->input->post()) {
            $rtc_id = $this->input->post('rtc_id');
            $person_id = $this->input->post('rtc_room_id');


            $persons = $this->report_model->person_wise_asset_detail($person_id, $rtc_id);

            $data['persons'] = $persons;

            $this->load->view('admin_lte/report/rtc_person_wise_report_ajax', $data);
        } else {
            $office = $this->office_model->officelist();
            $this->set_value('office_list', $office);

            $this->load_view('admin_lte/report/rtc_person_wise_report');
        }
    }

 /******** Office and Category Wise Asset Report **************/
    public function category_wise_report() {

        $this->set_page_title('Item Wise Report');
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/asset_script.js');
        if ($this->input->post()) {
            $cat_id = $this->input->post('cat_id');

            $office = $this->report_model->category_wise_asset_detail($cat_id);
            $data['office'] = $office;


            $this->load->view('admin_lte/report/category_wise_report_ajax', $data);
        } else {
            $office = $this->office_model->officelist();
            $this->set_value('office_list', $office);

            $this->load_view('admin_lte/report/category_wise_report');
        }
    }

    /******** Asset Life-time Report **************/
    public function asset_life_expire() {
        $this->set_page_title('Asset Life Time Expires');
        $this->set_page_sub_title('');
        //$this->set_js('dist/js/sci/project.js');
        //$this->set_js('dist/js/asset_script.js');

        if($this->input->post('exp_year')!=NULL && $this->input->post('exp_year')!=-1)
        {
            $exp_year = $this->input->post('exp_year');
        }
        else{
            $exp_year=NULL;
        }
        
        if($this->input->post('exp_month')!=NULL && $this->input->post('exp_month')!=0)
        {
            $exp_month = $this->input->post('exp_month');
        }
        else{
            $exp_month=NULL;
        }

        $data['asset_exp'] = $this->report_model->asset_life_expire($exp_year,$exp_month);

        $this->set_value('asset_list', $data['asset_exp']);
        $this->load_view('admin_lte/report/asset_life_expire');
    }
    
    /******** Purchase Price Wise Asset Report **************/
    public function asset_value(){
        $this->set_page_title('Asset Value Wise Report');
        $this->set_page_sub_title('');
       
        //$data['asset_exp'] = $this->report_model->asset_life_expire($exp_year,$exp_month);
        
        $this->db->select('count(*) AS total_asset');
        $this->db->select('office.office_name');
        $this->db->select('SUM(`purchase_price`) as asset_value');
        $this->db->from('asset');
        $this->db->join('fams_office office', 'office.office_id = asset.office_id');
        
        $this->db->where('asset.purchase_price > 0');
        $this->db->group_by('asset.office_id');
        
        $query = $this->db->get();
        
        //echo $this->db->last_query();
        $result = $query->result();

        $this->set_value('asset_value', $result);
        $this->load_view('admin_lte/report/asset_value_report');
    }
    
    /******** Status Wise Asset Report **************/
    private function get_asset_by_status($status=null, $rtc_id=null){
        $this->db->select('asset.asset_id');
        $this->db->select('asset.asset_name');
        $this->db->select('asset.label_id');
        $this->db->select('asset.sci_id ');
        $this->db->select('asset.serial_no ');
        $this->db->select('user.fullname');
        $this->db->select('category.category_name');
        
        $this->db->from('asset');
        $this->db->join('status', 'asset.asset_status_id=status.status_id');
        $this->db->join('user', 'user.userid=asset.assigned_to','left');
        $this->db->join('category', 'category.category_id=asset.category_id','left');
        $this->db->where('status.status_name',$status);
        $this->db->where('asset.office_id',$rtc_id);
        
        $data['result'] = $this->db->get()->result();
        
        //echo $this->db->last_query();
       //var_dump($query->result());
        
       $this->load->view('admin_lte/report/partial/status',$data);
        
       
    }
    
    /******** Category Wise Asset Report **************/
    private function get_asset_by_category($cat_id=null, $rtc_id=null){
        $this->db->select('asset.asset_id');
        $this->db->select('asset.asset_name');
        $this->db->select('asset.label_id');
        $this->db->select('asset.sci_id ');
        $this->db->select('asset.serial_no ');
        $this->db->select('user.fullname');
        $this->db->select('category.category_name');
        
        $this->db->from('asset');
        $this->db->join('status', 'asset.asset_status_id=status.status_id');
        $this->db->join('user', 'user.userid=asset.assigned_to','left');
        $this->db->join('category', 'category.category_id=asset.category_id','left');
        $this->db->where('category.category_id',$cat_id);
        $this->db->where('asset.office_id',$rtc_id);
        
        $data['result'] = $this->db->get()->result();
        
        //echo $this->db->last_query();
       // var_dump($query->result());
        
       $this->load->view('admin_lte/report/partial/status',$data);
        
       
    }

}
