<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: department.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0

// Function list: add_department,department_list,department_delete,department_edit
//
/****************************************************/

/**
 * AMS department Controller Class
 *
 * This method demonstrates the department crud process of AMS.
 */

class department extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('user_model', '', TRUE);
        $this->load->model('department_model', '', TRUE);
        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index()
    {
        $this->set_page_title('Department');
        $this->set_page_sub_title('control panel');
    }

    /** 
    *This function creates new department into the system. 
    */
    
    public function add_department()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ADD_NEW_DEPARTMENT'));
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/department/department_create');
    }

    /** 
    *This function retrieves the department list from the database. 
    */
    public function department_list()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/setting_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('DEPARTMENT_LIST'));
        $this->set_page_sub_title('');

        $result = $this->department_model->department_list();
        //var_dump($result);

        $this->set_value('department_list', $result);

        $this->load_view('admin_lte/department/department_list');
    }

    /** 
    *This function deletes existing department from the database.
    */

    public function department_delete()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true));
            $result = sci_delete_db('department', ['department_id' => $jsondata->department_id]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Department Delete";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/

                echo $this->lang->line('DELETE');
            }
        } else {
            exit('No direct script access allowed');
        }

    }

    /** 
    *This function updates existing department information.
    */
    
    public function department_edit()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true), true);
            $department_id = $jsondata["update_id"];
            unset($jsondata["update_id"]);
            $result = sci_update_db('department', $jsondata, ['department_id' => $department_id]);
            if ($result <> false) {
                /**** Audit Log ****/
                $action_name = "Department Update";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/
                echo '{"status": "success", "message": "' . $this->lang->line('DEPARTMENT_UPDATED') . '"}';
            }


        } else {
            exit('No direct script access allowed');
        }
    }


}
