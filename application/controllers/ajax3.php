<?php

/************************************************************************************/
// Filename: ajax3.php
// Created By:     Evana Yasmin 
// Change history:
//         / Hasibul Haque / pass_change
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0

// Function list:- report_allocation_asset,report_asset_info,profile_update,pass_change,
// request_availability,allocation_revert_all,request_submit,asset_availability,temp_delete,
// allocation_submit,allocation_submit_all,upload,revert_upload,dispose_upload,allocation_upload,
// alloc_file_delete,file_delete,disposed_file_delete,revert_file_delete,get_files,get_revert_files,
// get_disposed_files,add_files,add_to_allocation,room_submit,room_update,allocation_edit,
// allocation_details,reference_details,assetsearch,pr_submit,pr_submit_all,pr_list,
// direct_allocation_submit,singleallocate,request_available_check,organization_submit,
// allocation_revert_form,organization_update
/****************************************************************************************/

/**
 * AMS Asset ajax3 Controller Class
 *
 * This method gets the request from ajax and complete the actions.
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');   
class ajax3 extends CI_Controller   
{
   
    
     function __construct()
    {
      parent::__construct();
      $this->load->library('encrypt');
      $this->load->helper('form');
      $this->load->library('session'); 
      $this->load->helper('url');
      $this->load->model('user_model','',TRUE);
      //$this->lang->load('message','english');      
    }
  
   
    /** 
    *This function retrieves the allocated asset list for reporting purpose
    *from database based on office id and display the results into a HTML page.
    */
    public function report_allocation_asset()
    {
     if($this->session->userdata('user_logged_in'))
      {   
       if ($this->input->is_ajax_request())
        {
           $this->load->model('report_model','',TRUE);   
           $filter =  $this->input->post('filter',true);
           $office_id = $this->session->userdata('user_center');
           if(isset($filter['office_id'])){
            if($filter['office_id'] != ''){
              $office_id = $filter['office_id'];
            }
           }
           
          $result = $this->report_model->filter_wise_allocation_asset_list($filter['asset_name'], $filter['category_id'], $filter['asset_label'],$filter['user_name'],$office_id, $filter['to'], $filter['from']);

          if($result<>false)
           {
            $data['allocation_list'] = $result;    
           }
           else
                {
                  $data['allocation_list'] = "";
                }  
          $this->load->view('admin_lte/report/filter_wise_allocation_list',$data);
        }
        else
         {
         exit('No direct script access allowed');
         }
       }
       else 
           {
               redirect('login');
           }   
    }

  /** 
   *This function retrieves the asset information for reporting purpose
   *from database based on office id and display the result sets into a HTML page.
  */
  public function report_asset_info()
    {
     if($this->session->userdata('user_logged_in'))
       {   
       if ($this->input->is_ajax_request())
        {
           $this->load->model('report_model','',TRUE);   
           $filter =  $this->input->post('filter',true);
           $office_id = $this->session->userdata('user_center');
           
           if(isset($filter['office_id']))
           {
            if($filter['office_id'] != ''){
               $office_id = $filter['office_id'];
            }
           }
           
          $result = $this->report_model->filter_wise_asset_info($filter['asset_name'], $filter['category_id'], $filter['asset_label'],$filter['user_name'],$office_id, $filter['to'], $filter['from'], $filter['status_id'],$filter['funded_by'],$filter['lifetime']);
          if($result<>false)
               {
                $data['asset_info'] = $result;    
               }
           else
                {
                  $data['asset_info'] = "";
                }  
          $this->load->view('admin_lte/report/filter_wise_asset_info',$data);
        }
        else
         {
         exit('No direct script access allowed');
         }
       }
       else 
           {
               redirect('login');
           }   
    }

   /** 
   *This function retrieves the user details 
   *from database based on user id and display the information on a HTML page.
   */
    public function profile_update()
    {

        //$this->set_js('dist/js/office.js');

        if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {

                $this->load->model('user_model','',TRUE);

                $db_userid =  $this->input->post('userid',true);
                $result = $this->user_model->user_details($db_userid);

                $data['profile_details'] = $result;

                $this->load->view('admin_lte/user/user_detail_edit',$data);

            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
    }

   /** 
   *This function demonstrates the change password form of a user.
   */
    public function pass_change()
    {

        //$this->set_js('dist/js/office.js');

        if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
                $this->load->view('admin_lte/user/change_pass');
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
    }

   /** 
   *This function checks the availability of a requested category item of a office.
   */
    public function request_availability()
    {

      if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $category_id =  $this->input->post('category_id',true);  
               $quantity =  $this->input->post('quantity',true);

               $result = $this->asset_model->requested_category_available($category_id,$quantity);
               if($result==false)
               {
                 //echo $this->lang->line('NOT_AVAILABLE');
                 $data['category_id'] = $category_id;
                 $data['quantity'] = $quantity; 

                 $status = "Not Available";
                 $res12 = $this->asset_model->temp_request($category_id,$quantity,$status);

                 $res3 = $this->asset_model->get_temp_allocation();
                    if($res3<>false)
                        {
                          $data['tmp_list'] = $res3;
                        }
                        else
                            {
                              $data['tmp_list'] = "";
                            }

                 //$msg = 0;
                    
                 echo $this->load->view('admin_lte/allocation/selected_request',$data);

               }
               else
               {
                  //$msg = "";
                  $data['category_id'] = $category_id;
                  $data['quantity'] = $quantity;
                  $status = "Available";

                  $res12 = $this->asset_model->temp_request($category_id,$quantity,$status);
                  if($res12<>false)
                  {
                   /****** Here i have to check the quantity availability of a category: pending ***/
                    
                    $res3 = $this->asset_model->get_temp_allocation();
                    if($res3<>false)
                        {
                          $data['tmp_list'] = $res3;
                        }
                        else
                            {
                              $data['tmp_list'] = "";
                            }
                        //var_dump($res3);
                  }
                  echo $this->load->view('admin_lte/allocation/selected_request',$data);
               }
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }

    }

 /** 
 *This function reverts the allocated list and retrieves the reverted allocated asset list 
 */
public function allocation_revert_all()
{

    if($this->session->userdata('user_logged_in'))
        {
          if ($this->input->is_ajax_request())
          {
            
           $this->load->model('asset_model','',TRUE);
           $this->load->model('allocation_model','',TRUE);

            $request_id =  $this->input->post('request_id',true);
            $reference_no =  $this->input->post('reference_no',true);
            $category_id =  $this->input->post('category_id',true);

            $res1 = $this->allocation_model->revert_request($request_id);
            if($res1<>false)
            {
              $rest2 = $this->allocation_model->allocation_reference($reference_no,$category_id);
              if($rest2 <> false)
              {
                foreach($rest2 as $re)
                {

                  $allocate_id = $re->allocate_id;
                  $asset_id = $re->asset_id;
                  $rest3 = $this->allocation_model->revert_allocation($allocate_id);
                  if($rest3 <> false)
                    {
                        $rest4 = $this->allocation_model->revert_asset($asset_id); 
                        if($rest4==false)
                        {
                          echo "Failed to update.";
                        }
                        else
                        {
                           $res6 = sci_select_db('fams_allocation_file',['requisition_id'=>$reference_no]);
                           if($res6<>false)
                              {
                                foreach($res6 as $ree)
                                {
                                 $file_id = $ree->file_id; 
                                 $file_name = $ree->file_name;
                                 $res77 = $this->allocation_model->delete_attach_file($file_id);
                                 if($res77 <> false)
                                    {
                                      $img_url = './uploads/' . $file_name;
                                      @unlink($img_url);
                                    }
                                }
                              }
                          //$res5 = $this->allocation_model->delete_allocated_files($reference_no);
                        }
                    }
                }
              }
            }
            $result = sci_select_db('allocation_request',['ref_no'=>$reference_no]);
           //var_dump($result);
         
           if($result==true)
           {
            $data['reference_details'] = $result;   
           }
           else
            {
              $data['reference_details'] = ""; 
            }

            echo $this->load->view('admin_lte/allocation/filter_list',$data);
          }
          else
          {
              exit('No direct script access allowed');
          }
        }
        else
        {
            redirect('login');
        }
}

 /** 
 *This function demonstrates the item request from a form for allocation process 
 */
    public function request_submit()
    {
      if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $jsondata =  $this->input->post('jsondata',true);

               $jsondata = json_decode( $jsondata,true);

                $res3 = $this->asset_model->get_temp_allocation();
                if($res3<>false)
                  {
                    //$data['tmp_list'] = $res3;
                   foreach($res3 as $list1)
                    {
                      $cat_id = $list1->category_id;
                      $cat_qty = $list1->qty;
                      $req_status = $list1->item_status;

                      $jsondata['office_id'] = $this->session->userdata('user_center');
                      $jsondata['category_id'] = $cat_id;
                      $jsondata['quantity'] = $cat_qty;
                      $jsondata['req_status'] = $req_status;
                      $request_json = json_encode($jsondata,true);

                      $req = $res = sci_insert_db('allocation_request',$request_json);
                      if($req==false)
                            {
                              //echo"Failed to insert data";
                            }
                    }

                  $restfull =  $this->asset_model->temp_delete_all();
                  /**** Audit Log ****/
                  $action_name = "Allocation Request Submit";
                  log_create($action_name,$request_json);
                  /**** Audit log end here *****/

                  echo '{"status": "success", "message": "'.$this->lang->line('ALLOCATION_REQUEST_SUCCESS').'"}';
   
                  }
                  else
                  {
                   echo '{"status": "error", "message": "'."You have not selected any item.".'"}';
                  }
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
    }

  /** 
  *This function chacks the availability of a requested quantity of a category for allocation/transfer process. 
  */
    public function asset_availability()
    {

      if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $category_id =  $this->input->post('category_id',true);  
               $quantity =  $this->input->post('quantity',true);

               $result = $this->asset_model->requested_category_available($category_id,$quantity);
               if($result==false)
               {
                 //echo $this->lang->line('NOT_AVAILABLE'); 
                 $msg = 0;
               }
               else
               {
                  //$msg = "";
                  $data['category_id'] = $category_id;
                  $data['quantity'] = $quantity;

                  $res12 = $this->asset_model->temp_allocate($category_id,$quantity);
                  if($res12<>false)
                  {

                   /****** Here i have to check the quantity availability of a category: pending ***/
                    $res3 = $this->asset_model->get_temp_allocation();
                    if($res3<>false)
                        {
                           $data['tmp_list'] = $res3;
                        }
                        else
                            {
                              $data['tmp_list'] = "";
                            }
                        //var_dump($res3);
                  }
                  echo $this->load->view('admin_lte/asset/selected_asset',$data);
               }
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }

    }

/** 
*This function deletes the temporary selected items for allocation/transfer process. 
*/ 
 public function temp_delete()
 {
   if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $item_id =  $this->input->post('item_id',true);  

              $rest12 = $this->asset_model->temp_delete($item_id);
              $res3 = $this->asset_model->get_temp_allocation();
              if($res3<>false)
                {
                  $data['tmp_list'] = $res3;
                }
                else
                {
                  $data['tmp_list'] = "";
                }
                        
              $this->load->view('admin_lte/asset/selected_asset',$data);
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
 }  

/** 
*This function deletes the temporary selected items for allocation process. 
*/ 

 public function allocation_submit()
 {
  if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $jsondata =  $this->input->post('jsondata',true);

               $jsondata = json_decode( $jsondata,true);

               $res3 = $this->asset_model->get_temp_allocation();
               if($res3<>false)
                  {
                    //$data['tmp_list'] = $res3;
                    foreach($res3 as $list1)
                    {
                      $cat_id = $list1->category_id;
                      $cat_qty = $list1->qty;
                    /********************  Allocation starts from here  **************************/

                    $result2 = $this->asset_model->requested_category_available($cat_id,$cat_qty);
                   // var_dump($result2);
                    if($result2<>false)
                    {
                    /************* Add assets to the allocation list *****************/
                    $result1 = $this->asset_model->cat_wise_asset_list($cat_id,$cat_qty);
                    //var_dump($result1);
                    if($result1<>false)
                        {
                            $requisition_no=date("Y-m-d H:i:s");
                            $requisition_no = str_replace('-','', $requisition_no); 
                            $requisition_no = str_replace(':','', $requisition_no);
                            $requisition_no = str_replace(' ','', $requisition_no);

                            foreach($result1 as $rest12)
                            {
                            $allocate_asset_id = $rest12->asset_id;
                            $allocate_category_id = $rest12->category_id;
                            $asset_label_id = $rest12->asset_readable_id;

                            $jsondata['asset_id'] = $allocate_asset_id;
                            $jsondata['category_id'] = $allocate_category_id;
                            $jsondata['asset_label_id'] = $asset_label_id;
                            $jsondata['requisition_no'] = $requisition_no;
                            $jsondata['allocate_state'] = "Delivered";
                            $jsondata['allocated_by'] = $this->session->userdata('user_db_id');
                            $jsondata['allocate_office'] = $this->session->userdata('user_center');

                            $allocation_json = json_encode($jsondata,true);

                           $req = sci_insert_db('allocation',$allocation_json);
                           if($req==false)
                                {
                                  //echo"Failed to insert data";
                                }
                            else
                                { /*********** Update Base Asset Table **********/
                                  //echo"succesfully allocated!";

                                  /**** Audit Log ****/
                                  $action_name = "Asset Allocation";
                                  log_create($action_name,$allocation_json);
                                  /**** Audit log end here *****/

                                  $req1 = $this->asset_model->update_asset_status($allocate_asset_id,$jsondata['allocate_to']);
                                }     
                            }
                           //echo $this->lang->line('ALLOCATION_SUCCESS'); 
                          }
 
                        }
                    else
                        {
                          echo '{"status": "error", "message": "'."Item is not available; Allocation is halted.".'"}';
                          break;
                        }
                       
                    }
                    $restfull =  $this->asset_model->temp_delete_all();
                    $restfile2 = $this->asset_model->get_file_include();

                    /* ##### Attachment of this allocation will be added here  #### */
                    
                    if($restfile2<>false)
                    {
                      foreach($restfile2 as $abc)
                      {
                        $attachment_name = $abc->file_name;  
                        //copy('temp/'.$attachment_name, 'uploads/');

                          $location = './uploads/' . $attachment_name;  
                          
                          copy('temp/'.$attachment_name, $location);

                        $this->asset_model->allocation_attachment($requisition_no,$attachment_name);
                      }
                    }
                   /********  ###### File Attachment End Here ######  ****************/ 
                  echo '{"status": "success", "message": "'.$this->lang->line('ALLOCATION_SUCCESS').'"}';

                  //echo $this->lang->line('ALLOCATION_SUCCESS'); 

                  }
                  else
                  {
                   echo '{"status": "error", "message": "'."You have not selected any item.".'"}';
                  }
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
 }

/** 
*This function completes the allocation process of multiple selected items at once.
*/ 
 public function allocation_submit_all()
 {
    if($this->session->userdata('user_logged_in'))
        {
          if ($this->input->is_ajax_request())
            {
              $this->load->model('asset_model','',TRUE);
              $this->load->model('allocation_model','',TRUE);
             
               $reference_no =  $this->input->post('reference_no',true);
               $requested_user =  $this->input->post('requested_user',true);
               $on_behalf =  $this->input->post('on_behalf',true);
               $asset_location =  $this->input->post('asset_location',true);
               $approved_by =  $this->input->post('approved_by',true);
               $value_context =  $this->input->post('value_context',true);
               $remarks =  $this->input->post('remarks',true);
               
               $jsondata['approved_by'] = $approved_by;
               $jsondata['reference_no'] = $reference_no;
               $jsondata['allocate_to'] = $requested_user;
               $jsondata['on_behalf'] = $on_behalf;
               if($asset_location == "")
               {
                $jsondata['asset_location'] = NULL;
               }else { $jsondata['asset_location'] = $asset_location; }
               
               $jsondata['value_context'] = $value_context;
               $jsondata['remarks'] = $remarks;


              //$check_string =  $this->input->post('check_string',true);
              //$check_item = explode(",", $check_string);

              //var_dump($check_item);

              //$len = sizeof($check_item);

               $result = $this->allocation_model->get_available_item($reference_no);
               //var_dump($result);
               $msg = 1;
               if($result <> false)
               {
                foreach($result as $res12)
                {
                $requested_id = $res12->arq_id; 
                $res1 = $this->allocation_model->item_details($requested_id);
                if($res1 <> false)
                 {
                    $j=0;
                    $request_id = $res1[0]->arq_id; 
                    $category_id = $res1[0]->category_id; 
                    $quantity = $res1[0]->quantity;
                    $allocate_to = $res1[0]->req_user_id;  
                    $on_behalf1 = $res1[0]->on_behalf; 

                    $result2 = $this->asset_model->requested_category_available($category_id,$quantity);
                    if($result2<>false)
                    {
                    /************* Add assets to the allocation list *****************/
                    $result1 = $this->asset_model->cat_wise_asset_list($category_id,$quantity);
                    //var_dump($result1);
                    if($result1<>false)
                        {
                            $requisition_no=date("Y-m-d H:i:s");
                            $requisition_no = str_replace('-','', $requisition_no); 
                            $requisition_no = str_replace(':','', $requisition_no);
                            $requisition_no = str_replace(' ','', $requisition_no);

                            foreach($result1 as $rest12)
                            {
                            $allocate_asset_id = $rest12->asset_id;
                            $allocate_category_id = $rest12->category_id;
                            $asset_label_id = $rest12->asset_readable_id;

                            $jsondata['asset_id'] = $allocate_asset_id;
                            $jsondata['category_id'] = $allocate_category_id;
                            $jsondata['asset_label_id'] = $asset_label_id;
                            $jsondata['requisition_no'] = $requisition_no;
                            $jsondata['allocate_state'] = "Delivered";
                            $jsondata['allocated_by'] = $this->session->userdata('user_db_id');
                            $jsondata['allocate_office'] = $this->session->userdata('user_center');

                            $allocation_json = json_encode($jsondata,true);

                          $req = $res = sci_insert_db('allocation',$allocation_json);
                          if($req==false)
                              {
                              //echo"Failed to insert data";
                              }
                            else
                              { /*********** Update Base Asset Table **************/
                              //echo"succesfully allocated!";

                                /***** Audit Log For Allocation *****/
                                $action_name = "Asset Allocation";
                                log_create($action_name,$allocation_json);
                                /***** Audit Log *****/

                              $req1 = $this->asset_model->update_asset_status($allocate_asset_id,$requested_user);
                              if($req1<>false)
                                {
                                  /***** Audit Log Asset Update After Allocation *****/
                                  $action_name = "Asset update after allocation";
                                  $logdata["allocate_id"] = $allocate_asset_id;
                                  $logdata["requested_user"] = $requested_user;
                                  $logdata =json_encode($logdata,true);

                                  log_create($action_name,$logdata);
                                  /***** Audit Log ********/
                                }
                              $this->allocation_model->update_request_status($request_id);
                              }        
                            }
                           //echo $this->lang->line('ALLOCATION_SUCCESS'); 
                          }

                    }
                    else
                        {

                          $unavailable[$j][0] = $category_id;
                          $unavailable[$j][1] = $quantity;

                          //echo"Sorry Item Not Available! Failed to allocate.";
                          $j++;
                        }
                 }
                }
                $msg = 1;
              }
              else
                    {
                      //echo"Item is not available to allocate";
                      $msg = 0; //Item not available 
                    } 
              // exit();
             // echo $this->lang->line('ALLOCATION_SUCCESS');  

              $restfull =  $this->asset_model->temp_delete_all();
              $restfile2 = $this->asset_model->get_file_include();

                /* ##### Attachment of this allocation will be added here  #### */
                  
                if($restfile2<>false)
                {
                  foreach($restfile2 as $abc)
                  {
                    $attachment_name = $abc->file_name;  
                    //copy('temp/'.$attachment_name, 'uploads/');

                      $location = './uploads/' . $attachment_name;  
                      
                      copy('temp/'.$attachment_name, $location);

                    $this->asset_model->allocation_attachment($reference_no,$attachment_name);
                  }
                }
          /********  ###### File Attachment End Here ######  ****************/ 

          //echo $jsondata->reference_no;
          $result = sci_select_db('allocation_request',['ref_no'=>$reference_no]);
           //var_dump($result);
         
           if($result==true)
           {
            $data['reference_details'] = $result;   
           }
           else
            {
              $data['reference_details'] = ""; 
            }

             if($msg <> 0)
             {
              echo $this->load->view('admin_lte/allocation/filter_list',$data);
             } 
             
          }
        else
            {
                exit('No direct script access allowed.');
            }
        }
        else
        {
            redirect('login');
        }
 }

/** 
*This function uploads the temporary file for allocation process.
*/ 
 public function upload()
 {
    if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
              // $jsondata =  $this->input->post('jsondata',true);
              
              if($_FILES["file"]["name"] != '')
              {
                $test = explode('.', $_FILES["file"]["name"]);
                $ext = end($test);
                $name = rand(1000, 9999) . '.' . $ext;
                $location = './temp/' . $name;  
                $img_url = base_url().'temp/'. $name;  
                move_uploaded_file($_FILES["file"]["tmp_name"], $location);

              //echo '<img src="'.$img_url.'" height="150" width="225" class="img-thumbnail" />';
               $result = $this->asset_model->tmp_file_include($name);
               if($result<>false)
                  {
                    $result3 = $this->asset_model->get_file_include();
                    if($result3<>false)
                        {
                          $data['file_list'] =$result3;
                        }
                      else
                        {
                          $data['file_list'] = "";
                        }
                      echo $this->load->view('admin_lte/asset/selected_files',$data);
                  }
              }
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
 }


/** 
* This function reverts/deletes the temporary uploaded file for allocation process.
*/ 

 public function revert_upload()
 {
    if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
              $this->load->model('allocation_model','',TRUE);
              $reference_no =  $this->input->post('reference_no',true);
              $revert_id =  $this->input->post('revert_id',true);
              $purpose =  $this->input->post('revert_purpose',true);

              if($_FILES["file"]["name"] != '')
              {
                $test = explode('.', $_FILES["file"]["name"]);
                $ext = end($test);
                $name = rand(1000, 9999) . '.' . $ext;
                $location = './uploads/' . $name;  
                $img_url = base_url().'uploads/'. $name;  
                move_uploaded_file($_FILES["file"]["tmp_name"], $location);

               //echo '<img src="'.$img_url.'" height="150" width="225" class="img-thumbnail" />';
               //$purpose = "revert_allocation";
               //$reference_no = "123456";

               $result = $this->allocation_model->revert_file_include($revert_id,$name,$reference_no,$purpose);
               if($result<>false)
                  {

                    $result3 = $this->allocation_model->get_revert_file($revert_id,$reference_no,$purpose);
                    //var_dump($result3);

                    if($result3<>false)
                    {
                      $data['file_list'] =$result3;
                    }
                    else
                        {
                          $data['file_list'] = "";
                        }
                      echo $this->load->view('admin_lte/allocation/reverted_files',$data);
                  }
              }
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
 }

/** 
* This function uploads files for disposing process.
*/ 

 public function dispose_upload()
 {
    if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
              $this->load->model('allocation_model','',TRUE);
              $this->load->model('asset_model','',TRUE);
            
              $asset_id =  $this->input->post('asset_id',true);

              if($_FILES["file"]["name"] != '')
              {
                $test = explode('.', $_FILES["file"]["name"]);
                $ext = end($test);
                $name = rand(1000, 9999) . '.' . $ext;
                $location = './uploads/' . $name;  
                $img_url = base_url().'uploads/'. $name;  
                move_uploaded_file($_FILES["file"]["tmp_name"], $location);

               //echo '<img src="'.$img_url.'" height="150" width="225" class="img-thumbnail" />';
               //$purpose = "revert_allocation";
               //$reference_no = "123456";

               $result = $this->asset_model->dispose_file_include($asset_id,$name);
               if($result<>false)
                  {

                    $result3 = $this->asset_model->disposed_filelist($asset_id); 
                    //var_dump($result3);

                    if($result3<>false)
                    {
                        $data['file_list'] =$result3;
                    }
                    else
                        {
                          $data['file_list'] = "";
                        }
                      echo $this->load->view('admin_lte/asset/disposed_files',$data);
                  }
              }
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
 } 

/** 
* This function uploads files and allocate single item for allocation process.
*/ 

 public function allocation_upload()
 {
    if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
              $this->load->model('allocation_model','',TRUE);
              $this->load->model('asset_model','',TRUE);
            
              $reference_no =  $this->input->post('reference_no',true);

              if($_FILES["allo_file"]["name"] != '')
              {
                $test = explode('.', $_FILES["allo_file"]["name"]);
                $ext = end($test);
                $name = rand(1000, 9999) . '.' . $ext;
                $location = './uploads/' . $name;  
                $img_url = base_url().'uploads/'. $name;  
                move_uploaded_file($_FILES["allo_file"]["tmp_name"], $location);

               
               $result = $this->allocation_model->allo_file_include($reference_no,$name);
               if($result<>false)
                  {

                    $result3 = $this->allocation_model->allo_filelist($reference_no); 
                    //var_dump($result3);

                    if($result3<>false)
                    {
                        $data['file_list'] =$result3;
                    }
                    else
                        {
                          $data['file_list'] = "";
                        }
                      echo $this->load->view('admin_lte/allocation/allocated_files',$data);
                  }
              }
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
 } 

/** 
* This function deletes the allocated file from allocation process.
*/ 
function alloc_file_delete()
{

if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('allocation_model','',TRUE);
               $file_id =  $this->input->post('file_id',true);  
               $file_name =  $this->input->post('file_name',true);
               $reference_no =  $this->input->post('reference_no',true);  


               $rest12 = $this->allocation_model->delete_alloc_files($file_id);
               if( $rest12<>false)
               {
                 //echo"File Deleted!";
                $img_url = './uploads/'.$file_name;
                @unlink($img_url);
                //echo $this->lang->line('DELETE');
               }
               $result3 = $this->allocation_model->allo_filelist($reference_no);
                if($result3<>false)
                {
                 $data['file_list'] =$result3;
                }
                else
                    {
                      $data['file_list'] = "";
                    }
              echo $this->load->view('admin_lte/allocation/allocated_files',$data);
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }

}

/** 
* This function deletes the allocated file from allocation process.
*/ 
function file_delete()
{
if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $file_id =  $this->input->post('file_id',true);  
               $file_name =  $this->input->post('file_name',true);  

               $rest12 = $this->asset_model->delete_tmp_files($file_id);
               if( $rest12<>false)
               {
                 //echo"File Deleted!";
                $img_url = './temp/'.$file_name;
                @unlink($img_url);
                //echo $this->lang->line('DELETE');
               }
               $result3 = $this->asset_model->get_file_include();
                if($result3<>false)
                {
                 $data['file_list'] =$result3;
                }
                else
                    {
                      $data['file_list'] = "";
                    }
              echo $this->load->view('admin_lte/asset/selected_files',$data);
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }

}

/** 
* This function deletes the attached files from disposal process.
*/ 
public function disposed_file_delete()
{

  if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);

                $file_id =  $this->input->post('file_id',true);  
                $file_name =  $this->input->post('file_name',true);  
                $asset_id =  $this->input->post('asset_id',true);  
                
               $rest12 = $this->asset_model->delete_disposed_files($file_id);
               if( $rest12<>false)
               {
                 //echo"File Deleted!";
                $img_url = './uploads/'.$file_name;
                @unlink($img_url);
                //echo $this->lang->line('DELETE');

                /***** Audit Log For Disposed File Delete *****/
                $action_name = "Disposed File Deletion";
                log_create($action_name,$file_name);
                /***** Audit Log *****/
               }

               $result3 = $this->asset_model->disposed_filelist($asset_id);
               //var_dump($result3);
               if($result3<>false)
                {
                 $data['file_list'] =$result3;
                }
                else
                {
                  $data['file_list'] = "";
                }
               echo $this->load->view('admin_lte/asset/disposed_files',$data);
            }
            else
              {
                  exit('No direct script access allowed');
              }
        }
        else
        {
            redirect('login');
        }
}

/** 
* This function deletes the attached files from allocation revert process.
*/
public function revert_file_delete()
{
  if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
              $this->load->model('allocation_model','',TRUE);

              $file_id =  $this->input->post('file_id',true);  
              $file_name =  $this->input->post('file_name',true);  
              $reference_no =  $this->input->post('reference_number',true); 
              $allocate_id =  $this->input->post('allocate_id',true); 
              $purpose =  $this->input->post('purpose',true); 

               $rest12 = $this->allocation_model->delete_revert_files($file_id);
               if( $rest12<>false)
               {
                  //echo"File Deleted!";
                  $img_url = './uploads/'.$file_name;
                  @unlink($img_url);
                  //echo $this->lang->line('DELETE');

                  /***** Audit Log For Disposed File Delete *****/
                  $action_name = "Reverted File Deletion";

                  log_create($action_name,$file_name);
                  /***** Audit Log *****/
               }

               $result3 = $this->allocation_model->get_revert_file($allocate_id,$reference_no,$purpose);
               //var_dump($result3);
               if($result3<>false)
                {
                 $data['file_list'] =$result3;
                }
                else
                {
                  $data['file_list'] = "";
                }
               echo $this->load->view('admin_lte/allocation/reverted_files',$data);
            }
            else
              {
                  exit('No direct script access allowed');
              }
        }
        else
        {
            redirect('login');
        }

}

/** 
* This function retrieves the attached files of a requisition.
*/
public function get_files()
{
if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $reference_no =  $this->input->post('reference_no',true);  
               $requisition_no =  $this->input->post('requisition_no',true);  

               $rest12 = $this->asset_model->get_files($reference_no,$requisition_no);
              // var_dump($rest12);
               if($rest12<>false)
                  {
                    $data['file_list'] = $rest12;
                  }
                  else
                  {
                    $data['file_list'] = "";
                  }
                  
                 echo $this->load->view('admin_lte/asset/attached_files',$data);
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }

}

/** 
* This function retrieves the attached files of allocation revert.
*/
public function get_revert_files()
{
if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
             $this->load->model('allocation_model','',TRUE);
             $allocate_id =  $this->input->post('allocate_id',true);  

             $rest12 = $this->allocation_model->get_files($allocate_id);
            // var_dump($rest12);
             if($rest12<>false)
                {
                  $data['file_list'] = $rest12;
                }
                else
                {
                  $data['file_list'] = "";
                }
                
               echo $this->load->view('admin_lte/allocation/attached_files',$data);
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }

}

/** 
* This function retrieves the attached files of disposed assets.
*/
public function get_disposed_files()
{
if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
             $this->load->model('asset_model','',TRUE);
             $asset_id =  $this->input->post('asset_id',true);  

             $decrypt_asset = base64_decode(urldecode($asset_id));
             $rest12 = $this->asset_model->disposed_filelist($decrypt_asset);
            // var_dump($rest12);
             if($rest12<>false)
                {
                  $data['file_list'] = $rest12;
                }
                else
                {
                  $data['file_list'] = "";
                }
                
               echo $this->load->view('admin_lte/asset/disposal_files',$data);
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }

}

/** 
* This function retrieves the attached files of a requisition/reference number.
*/

public function add_files()
{
if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $requisition_no =  $this->input->post('requisition_no',true);  
               $reference_no = "";
               $rest12 = $this->asset_model->get_files($reference_no,$requisition_no);
               // var_dump($rest12);
               if($rest12<>false)
                  {
                    $data['file_list'] = $rest12;
                  }
                  else
                  {
                    $data['file_list'] = "";
                  } 

                $data['requisition_no'] = $requisition_no;
                echo $this->load->view('admin_lte/asset/add_attachment',$data);
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }

}

/** 
* This function uploads the file for allocation process.
*/
public function add_to_allocation()
{
  if($this->session->userdata('user_logged_in'))
        {
            if ($this->input->is_ajax_request())
            {
               $this->load->model('asset_model','',TRUE);
               $requisition_no =  $this->input->post('requisition_no',true);  

                $restfile2 = $this->asset_model->get_file_include();

                  /* ##### Attachment of this allocation will be added here  #### */
                    
                    if($restfile2<>false)
                    {
                      foreach($restfile2 as $abc)
                      {
                        $file_id = $abc->file_id;
                        $attachment_name = $abc->file_name;    
                        //copy('temp/'.$attachment_name, 'uploads/');

                        $location = './uploads/' . $attachment_name;  
                          
                        copy('temp/'.$attachment_name, $location);

                        $res = $this->asset_model->allocation_attachment($requisition_no,$attachment_name);

                        if($res==false)
                        {
                         echo $this->lang->line('ATTACHMENT_FAILED');
                        }
                        else
                        {

                          /***** Audit Log For allocated file Attachment *****/
                          $action_name = "Allocated File attachment";
                          $log_json["requisition_no"] = $requisition_no;
                          $log_json["attachment_name"] = $attachment_name;
                          json_encode($log_json,true);
                          log_create($action_name,json_encode($log_json,true));
                          /***** Audit Log *****/


                          $rest12 = $this->asset_model->delete_tmp_files($file_id);
                          if($rest12<>false)
                            {
                              $img_url = './temp/'.$attachment_name;
                              @unlink($img_url);
                            }
                      
                        }
                  
                      }
                      //echo $this->lang->line('ATTACHMENT_ADD');
                      $reference_no = "";
                      $rest12 = $this->asset_model->get_files($reference_no,$requisition_no);
                      // var_dump($rest12);
                       if($rest12<>false)
                          {
                            $data['file_list'] = $rest12;
                          }
                          else
                          {
                            $data['file_list'] = "";
                          } 
                      echo $this->load->view('admin_lte/asset/attached_files',$data);
                    }
                    else
                    {
                      //echo"Please Upload files first before add!";
                      echo 3;
                    }
                   /********  ###### File Attachment End Here ######  ****************/ 
            
            }
            else
            {
                exit('No direct script access allowed');
            }
        }
        else
        {
            redirect('login');
        }
}

/** 
* This function creates a new room of an office.
*/
public function room_submit()
{

 if($this->session->userdata('user_logged_in'))
       {   
       $this->load->model('office_model','',TRUE);
       
       if ($this->input->is_ajax_request())
        {
         $jsondata =  $this->input->post('jsondata',true); 
         //var_dump($jsondata);
         $result = sci_insert_db('office_room',$jsondata);
         if($result<>false)
           {

              $action_name = "Office Room Create"; //** log file create**/
              log_create($action_name,$jsondata);

              echo '{"status": "success", "message": "'.$this->lang->line('ROOM_CREATED').'"}';

           }
        }
        else
         {
         exit('No direct script access allowed');
         }
       }
       else
        {
            redirect('login');
        } 
}
/** 
* This function updates a existing room of an office.
*/
public function room_update()
{
if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('office_model','',TRUE);
        if($this->input->is_ajax_request())
        {
           $jsondata =  json_decode($this->input->post('jsondata',true));
           $result = sci_select_db('office_room',['room_id'=>$jsondata->room_id]);
           //var_dump($result);
            $data['office_list'] = $this->office_model->officelist();
           if($result==true)
           {
            $data['room_details'] = $result;   
           }else{$data['room_details'] = ""; }
            
          $this->load->view('admin_lte/office/room_update',$data);
        }
        else
         {
         exit('No direct script access allowed');
         } 
       }
       else
            {
                redirect('login');
            }          

}

/** 
* This function updates the room information of allocated asset.
*/
public function allocation_edit()
{
    if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('allocation_model','',TRUE);
        $this->load->model('office_model','',TRUE);
        if($this->input->is_ajax_request())
        {
           $jsondata =  json_decode($this->input->post('jsondata',true));
           $result = sci_select_db('allocation',['allocate_id'=>$jsondata->allocate_id]);
           //var_dump($result);
           $data['room_list'] = $this->office_model->office_room_list();
           if($result==true)
           {
            $data['allocation_details'] = $result;   
           }else {$data['allocation_details'] = ""; }
            
          $this->load->view('admin_lte/allocation/allocation_update',$data);
        }
        else
         {
         exit('No direct script access allowed');
         } 
       }
   else
        {
            redirect('login');
        }          

}

/** 
* This function retrieves the detail information of an allocated asset.
*/
public function allocation_details()
{
  if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('allocation_model','',TRUE);
        if($this->input->is_ajax_request())
        {
           $jsondata =  json_decode($this->input->post('jsondata',true));
           //$requisition_no = $jsondata->requisition_no;
           $result = $this->allocation_model->get_allocated_file($jsondata->requisition_no);
           //$result = sci_select_db('allocation',['requisition_no'=>$jsondata->requisition_no]);
           //var_dump($result);
         
           if($result==true)
           {
            $data['allocation_details'] = $result;   
           }else{
            $data['allocation_details'] = ""; 
            }
         
         $result1 = sci_select_db('allocation_file',['requisition_id'=>$jsondata->requisition_no]);
         if($result1==true)
           {
            $data['allocation_files'] = $result1;   
           }else
           {
            //$result2 = sci_select_db('allocation_file',['requisition_id'=>$jsondata->reference_no]);
            $data['allocation_files'] = "";
           }

          $this->load->view('admin_lte/asset/allocation_details',$data);
        }
        else
         {
         exit('No direct script access allowed');
         } 
       }
       else
            {
                redirect('login');
            }          
}

/** 
* This function retrieves the reference/approval detail information of an allocation request.
*/
public function reference_details()
{
  if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('user_model','',TRUE);
        $this->load->model('office_model','',TRUE);
        $this->load->model('allocation_model','',TRUE);

        if($this->input->is_ajax_request())
        {
           $jsondata =  json_decode($this->input->post('jsondata',true));
           $result = sci_select_db('allocation_request',['ref_no'=>$jsondata->reference_no]);
           //var_dump($result);

           if($result==true)
           {
            $data['reference_details'] = $result;   
           }else{$data['reference_details'] = ""; 
        }
        
        $res = $this->allocation_model->delete_tmp_image(); 
        $result1 = $this->user_model->approval_list();
        //var_dump($result1);
        $data['approver_list'] =  $result1;
        
        $result2 = $this->office_model->office_room_list();
        //var_dump($result1);
        $data['room_list'] =  $result2;
        
        $this->load->view('admin_lte/allocation/reference_details',$data);
        }
        else
         {
         exit('No direct script access allowed');
         } 
       }
    else
        {
            redirect('login');
        }          
}

/** 
* This function retrieves the asset list from a search string.
*/    
  public function assetsearch()
  {
    if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('asset_model','',TRUE);
        if($this->input->is_ajax_request())
        {
           $querydata =  $this->input->post('query',true);
           
           $rest12 = $this->asset_model->get_asset($querydata);
           //var_dump($rest12);
           
           if($rest12==true)
           {
            $data['asset_list'] = $rest12;   
           }else{$data['asset_list'] = ""; }
            
          echo $this->load->view('admin_lte/asset/search_result',$data);
        }
        else
         {
         exit('No direct script access allowed');
         } 
       }
       else
            {
                redirect('login');
            }          

  }

/** 
* This function creates a procurement request of unavailable item during allocation
* /transfer request.
*/ 
  public function pr_submit()
  {
    if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('asset_model','',TRUE);
        if($this->input->is_ajax_request())
        {
            $ref_no =  $this->input->post('ref_no',true);
            $category_name =  $this->input->post('category_name',true);
            $quantity =  $this->input->post('quantity',true);

            $jsondata['reference_no'] = $ref_no;
            $jsondata['title'] = $category_name;
            $jsondata['quantity'] = $quantity;
            $jsondata['status'] = "Raised";
            $jsondata['remark'] = "";
            $jsondata['userid'] = $this->session->userdata('user_db_id');
            $jsondata['office_id'] = $this->session->userdata('user_center');

            $pr_json = json_encode($jsondata,true);

            $req = $res = sci_insert_db('pr_request',$pr_json);
            if($req==false)
                {
                  echo "Failed to insert data!";
                }
                else 
                 { 
                  $action_name = "PR Generate"; //** log file create**/
                  log_create($action_name,$pr_json);
                  echo $this->lang->line('PR_INSERT'); 
                }
            }
          else
           {
           exit('No direct script access allowed');
           } 
       }
   else
        {
            redirect('login');
        }          
  }

/********* End of PR Request***********/

/** 
* This function creates a procurement request of all unavailable item during allocation
* /transfer request.
*/ 

  public function pr_submit_all()
  {
    if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('allocation_model','',TRUE);
        if($this->input->is_ajax_request())
        {
            $ref_no =  $this->input->post('ref_no',true);
            
            $result = $this->allocation_model->get_unavailable($ref_no);
            if($result <> false)
            {
            foreach($result as $res)
              {
                $jsondata['reference_no'] = $ref_no;
                //$category_name = $res->category_name;
                //$catname = get_category_name($res->category_id);
                $jsondata['title'] = $res->category_name;
                $jsondata['category_id'] = $res->category_id;
                $jsondata['quantity'] = $res->qty;
                $jsondata['status'] = "Raised";
                $jsondata['remark'] = "";
                $jsondata['userid'] = $this->session->userdata('user_db_id');
                $jsondata['office_id'] = $this->session->userdata('user_center');
                
                $pr_json = json_encode($jsondata,true);
                $req = sci_insert_db('pr_request',$pr_json);

                $action_name = "PR Generate"; //** log file create**/
                log_create($action_name,$pr_json);
              }
              echo $this->lang->line('PR_INSERT');
            }
            else
              {
                $msg = 0;
              }

          }
        else
           {
           exit('No direct script access allowed');
           } 
       }
       else
            {
                redirect('login');
            }          
  }

  /********* End of all PR Request***********/

/** 
* This function retrieves all procurement request of unavailable item during allocation
* /transfer request.
*/ 
  
  public function pr_list()
  {
   if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('allocation_model','',TRUE);
        if($this->input->is_ajax_request())
        {
            $ref_no =  $this->input->post('ref_no',true);
            
            $result = $this->allocation_model->get_unavailable($ref_no);
            //var_dump($result);

            if($result<>false)
            {
              
              $result1 = $this->allocation_model->get_pr($ref_no);
              if($result1 <>false)
              {
                echo $msg = 0;
              }
              else
                  {
                    echo $msg = 1;
                  }
            }
            else
            {
              echo $msg = 0;
            }
            /*
            if($result <> false)
            {
              $data['pr_list'] = $result;
              $data['reference_no'] = $ref_no;
            }
            else
              {
                $data['pr_list'] = "";
                $data['reference_no'] = $ref_no;
              }
             echo $this->load->view('admin_lte/allocation/pr_list',$data);
             */ 
          }

          else
           {
           exit('No direct script access allowed');
           } 
       }
       else
            {
                redirect('login');
            }  
  }

/** 
* This function completes the single direct allocation from asset list.
*/ 

  public function direct_allocation_submit()
  {
    if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('allocation_model','',TRUE);
        $this->load->model('asset_model','',TRUE);
        if($this->input->is_ajax_request())
        {
           $asset_id =  $this->input->post('asset_id',true);
           $asset_label_id =  $this->input->post('asset_label_id',true);
           $category_id =  $this->input->post('category_id',true);
           
           $reference_no =  $this->input->post('reference_no',true);
           $requested_user =  $this->input->post('requested_user',true);
           $on_behalf =  $this->input->post('on_behalf',true);
           $asset_location =  $this->input->post('asset_location',true);
           $approved_by =  $this->input->post('approved_by',true);
           $value_context =  $this->input->post('value_context',true);
           $remarks =  $this->input->post('remarks',true);
           
           $jsondata['approved_by'] = $approved_by;
           $jsondata['reference_no'] = $reference_no;
           $jsondata['allocate_to'] = $requested_user;
           $jsondata['on_behalf'] = $on_behalf;
           if($asset_location == "")
           {
            $jsondata['asset_location'] = NULL;
           }else { $jsondata['asset_location'] = $asset_location; }
           
           $jsondata['value_context'] = $value_context;
           $jsondata['remarks'] = $remarks;

           //$jsondata = json_decode($jsondata,true);
          /********************  Allocation starts from here  **************************/

              $requisition_no=date("Y-m-d H:i:s");
              $requisition_no = str_replace('-','', $requisition_no); 
              $requisition_no = str_replace(':','', $requisition_no);
              $requisition_no = str_replace(' ','', $requisition_no);

              $jsondata['asset_id'] = $asset_id;
              $jsondata['category_id'] = $category_id;
              $jsondata['asset_label_id'] = $asset_label_id;
              $jsondata['requisition_no'] = $requisition_no;
              $jsondata['allocate_state'] = "Delivered";
              $jsondata['allocated_by'] = $this->session->userdata('user_db_id');
              $jsondata['allocate_office'] = $this->session->userdata('user_center');

              $allocation_json = json_encode($jsondata,true);

              $req = $res = sci_insert_db('allocation',$allocation_json);
              if($req==false)
                      {
                        //echo"Failed to insert data";
                      }
                    else
                      { /*********** Update Base Asset Table **************/

                        $action_name = "Asset Allocation"; //** log file create**/
                        log_create($action_name,$allocation_json);

                        //echo"Succesfully allocated!";
                        $req1 = $this->asset_model->update_asset_status($asset_id,$requested_user);
                        //$this->allocation_model->update_request_status($request_id);
                      }     
                 
                 //echo $this->lang->line('ALLOCATION_SUCCESS'); 

                  $restfile2 = $this->asset_model->get_files($reference_no,$requisition_no);

                  /* ##### Attachment of this allocation will be added here  #### */
                  
                  if($restfile2<>false)
                  {
                    foreach($restfile2 as $abc)
                    {
                      $attachment_name = $abc->file_name;  
                      //copy('temp/'.$attachment_name, 'uploads/');

                        $location = './uploads/' . $attachment_name;  
                        
                        copy('temp/'.$attachment_name, $location);

                      $this->asset_model->allocation_attachment($reference_no,$attachment_name);
                      $action_name = "Allocation File Attachment"; //** log file create**/
                      log_create($action_name,$reference_no);
                    }
                  }
                /********  ###### File Attachment End Here ###### ****************/ 
                echo $this->lang->line('ALLOCATION_SUCCESS'); 
          }
        else
         {
         exit('No direct script access allowed');
         } 
       }
   else
        {
            redirect('login');
        }  

  }

/** 
* This function completes the single direct allocation from asset list.
*/ 
  public function singleallocate()
  {
    if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('allocation_model','',TRUE);
        $this->load->model('asset_model','',TRUE);
        if($this->input->is_ajax_request())
        {
           $request_id =  $this->input->post('request_id',true);
           $category_id =  $this->input->post('category_id',true);
           $quantity =  $this->input->post('quantity',true);
           $reference_no =  $this->input->post('reference_no',true);
           $requested_user =  $this->input->post('requested_user',true);
           $on_behalf =  $this->input->post('on_behalf',true);
           $asset_location =  $this->input->post('asset_location',true);
           $approved_by =  $this->input->post('approved_by',true);
           $value_context =  $this->input->post('value_context',true);
           $remarks =  $this->input->post('remarks',true);
           
           $jsondata['approved_by'] = $approved_by;
           $jsondata['reference_no'] = $reference_no;
           $jsondata['allocate_to'] = $requested_user;
           $jsondata['on_behalf'] = $on_behalf;
           if($asset_location == "")
           {
            $jsondata['asset_location'] = NULL;
           }else { $jsondata['asset_location'] = $asset_location; }
           
           $jsondata['value_context'] = $value_context;
           $jsondata['remarks'] = $remarks;

           //$jsondata = json_decode($jsondata,true);
          /********************  Allocation starts from here  **************************/

          $result2 = $this->asset_model->requested_category_available($category_id,$quantity);
         // var_dump($result2);
          if($result2<>false)
          {
          /************* Add assets to the allocation list *****************/
          $result1 = $this->asset_model->cat_wise_asset_list($category_id,$quantity);
          //var_dump($result1);
          if($result1<>false)
              {
                  $requisition_no=date("Y-m-d H:i:s");
                  $requisition_no = str_replace('-','', $requisition_no); 
                  $requisition_no = str_replace(':','', $requisition_no);
                  $requisition_no = str_replace(' ','', $requisition_no);

                  foreach($result1 as $rest12)
                  {
                  $allocate_asset_id = $rest12->asset_id;
                  $allocate_category_id = $rest12->category_id;
                  $asset_label_id = $rest12->asset_readable_id;

                  $jsondata['asset_id'] = $allocate_asset_id;
                  $jsondata['category_id'] = $allocate_category_id;
                  $jsondata['asset_label_id'] = $asset_label_id;
                  $jsondata['requisition_no'] = $requisition_no;
                  $jsondata['allocate_state'] = "Delivered";
                  $jsondata['allocated_by'] = $this->session->userdata('user_db_id');
                  $jsondata['allocate_office'] = $this->session->userdata('user_center');

                  $allocation_json = json_encode($jsondata,true);

                 $req = $res = sci_insert_db('allocation',$allocation_json);
                 if($req==false)
                      {
                        //echo"Failed to insert data";
                      }
                  else
                      { /*********** Update Base Asset Table **************/

                        $action_name = "Asset Allocation"; //** log file create**/
                        log_create($action_name,$jsondata);

                        //echo"Succesfully allocated!";
                        $req1 = $this->asset_model->update_asset_status($allocate_asset_id,$requested_user);
                        $this->allocation_model->update_request_status($request_id);
                      }     
                  }
                 //echo $this->lang->line('ALLOCATION_SUCCESS'); 

                  $restfull =  $this->asset_model->temp_delete_all();
                  $restfile2 = $this->asset_model->get_file_include();

                  /* ##### Attachment of this allocation will be added here  #### */
                  
                  if($restfile2<>false)
                  {
                    foreach($restfile2 as $abc)
                    {
                      $attachment_name = $abc->file_name;  
                      //copy('temp/'.$attachment_name, 'uploads/');

                        $location = './uploads/' . $attachment_name;  
                        
                        copy('temp/'.$attachment_name, $location);

                      $this->asset_model->allocation_attachment($reference_no,$attachment_name);
                      $action_name = "Allocation File Attachment"; //** log file create**/
                      log_create($action_name,$reference_no);
                    }
                  }
                   /********  ###### File Attachment End Here ######  ****************/ 
                  echo $this->lang->line('ALLOCATION_SUCCESS'); 
                }
                  
              }
              else
                  {
                    echo "Sorry! Item not available now.";
                    //break;
                  }          
          }
        else
         {
         exit('No direct script access allowed');
         } 
       }
   else
        {
            redirect('login');
        }          

  }

/** 
* This function checks the availability of requested reference nummber of PR.
*/ 
 
  public function request_available_check()
  {
    if($this->session->userdata('user_logged_in'))
       {
        $this->load->model('allocation_model','',TRUE);
        if($this->input->is_ajax_request())
        {
            $reference_no =  $this->input->post('reference_no',true);
            $category_id =  $this->input->post('category_id',true);
            $quantity =  $this->input->post('quantity',true);

            $result2 = $this->allocation_model->requested_pr_available($reference_no,$category_id,$quantity);
            if($result2<>false)
              {
                //echo $this->lang->line('ITEM_AVAILABLE'); 
                $msg = 0;
              }
             else
              {  
                echo $this->lang->line('ITEM_NOT_AVAILABLE'); 
              } 
            }
          else
           {
           exit('No direct script access allowed');
           } 
       }
       else
            {
                redirect('login');
            }          
  }

/** 
* This function creates a new organization for asset capitalization.
*/ 
 public function organization_submit()
    {
        if ($this->session->userdata('user_logged_in')) {

            $this->load->model('department_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $deptdata = $this->input->post('jsondata', true);
                //$obj = json_decode($deptdata);
                $result = sci_insert_db('organization', $deptdata);
                if ($result <> false) {

                  $action_name = "Organization Create"; //** log file create**/
                  log_create($action_name,$deptdata);

                  echo '{"status": "success", "message": "' . $this->lang->line('ORGANIZATION_CREATED') . '"}';

                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

/** 
* This function generates allocation revert form of an allocation id.
*/ 
  public function allocation_revert_form()
  {
    if ($this->session->userdata('user_logged_in')) 
      {
        $this->load->model('allocation_model', '', TRUE);
        if ($this->input->is_ajax_request()) 
          {
            $allo_id = $this->input->post('allo_id', true);
            $asset_id = $this->input->post('revert_id', true);
            $revert_type = $this->input->post('revert_type', true);
            $reference_no = $this->input->post('reference_no', true);
            $jsondata['allocate_id'] = $allo_id;
            //$jsondata = json_encode($jsondata);

            if($revert_type == "allocation")
            {
             $result = sci_select_db('allocation',['allocate_id'=>$allo_id]);
             //var_dump($result);

               if($result==true)
               {
                 $data['allocation_details'] = $result;  
                 $data['reference_no'] = $reference_no; 
                 $data['asset_id'] = $asset_id;
                 $data['revert_type'] = $revert_type; 
               }
               else
                {
                  $data['allocation_details'] = ""; 
                } 

                $purpose = "revert_allocation";
                $result3 = $this->allocation_model->get_revert_file($allo_id,$reference_no,$purpose);
                if($result3<>false)
                {
                 $data['file_list'] =$result3;
                }
                else
                {
                  $data['file_list'] = "";
                }    
               echo $this->load->view('admin_lte/allocation/allocation_revert',$data);
           }
           else //*********** Transfer revert ***********/
              {

                  $result = sci_select_db('transfer',['transfer_id'=>$allo_id]);
                  //var_dump($result);

                 if($result==true)
                 {
                   $data['transfer_details'] = $result;  
                   $data['reference_no'] = $reference_no; 
                   $data['asset_id'] = $asset_id;
                   $data['revert_type'] = $revert_type; 
                 }
                 else
                  {
                    $data['transfer_details'] = ""; 
                  } 

                  $purpose = "revert_transfer";
                  $result3 = $this->allocation_model->get_revert_file($allo_id,$reference_no,$purpose);
                  if($result3<>false)
                  {
                   $data['file_list'] =$result3;
                  }
                  else
                  {
                    $data['file_list'] = "";
                  }    
                  echo $this->load->view('admin_lte/transfer/transfer_revert',$data);

              } 
            }
        else 
            {
              exit('No direct script access allowed');
            } 
          } 
        else {
            redirect('login');
        } 
  }  

/** 
* This function retrieves the detail information of existing organization for update purpose.
*/ 
public function organization_update()
    {

        if ($this->session->userdata('user_logged_in')) {

            $this->load->model('organization_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('organization', ['organization_id' => $jsondata->organization_id]);

                $data['organization_details'] = $result;
                $this->load->view('admin_lte/organization/organization_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }
}