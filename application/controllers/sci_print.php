<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/****************************************************/
// Filename: sci_print.php
// Created By:     Hasibul Huq
// Change history:
//
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: print_allocated_items,print_asset_detail,print_asset_list,
//
/****************************************************/

/**
 * Description of print
 *
 * @author shahed.chaklader
 */
class sci_print extends SCI_Controller {

    //put your code here


    public function __construct() {
        parent::__construct();
    }

    /**
     * @param null $requision_no
     * allocated item list print
     */
    public function print_allocated_items($requision_no = null) {

        if ($requision_no == null) {
            return;
        }
        
        $this->load->model('print_model');
        $result = $this->print_model->allocated_requisition_info($requision_no);
        
        //var_dump($result);
        $print['result'] = $result;
        $this->load->view('admin_lte/print/print_header');
        $this->load->view('admin_lte/print/allocated_item_print',$print);
        $this->load->view('admin_lte/print/print_footer');
    }

    /**
     * @param null $asset_id
     * Asset details print
     */
    public function print_asset_detail($asset_id = null) {

        if ($asset_id == null) {
            return;
        }
        
        $this->load->model('print_model');
        $result = $this->print_model->asset_detail($asset_id);
        
        //var_dump($result);
        $print['result'] = $result;
        $this->load->view('admin_lte/print/print_header');
        $this->load->view('admin_lte/print/asset_detail_print',$print);
        
        $this->load->view('admin_lte/print/print_footer');
    }

    /**
     * Print Asset list
     */
    public function print_asset_list() {

        $asset_id = $this->input->post('asset_id')?$this->input->post('asset_id'):null;
        $office_id = $this->input->post('office_id')?$this->input->post('office_id'):$this->session->userdata('user_center');
   
        
        $this->load->model('print_model');
        $result = $this->print_model->asset_list($asset_id,$office_id);
        
        //var_dump($result);
       
        $print['result'] = $result;
        $this->load->view('admin_lte/print/print_header');
        $this->load->view('admin_lte/print/asset_list_print',$print);
        
        $this->load->view('admin_lte/print/print_footer');
    }

}