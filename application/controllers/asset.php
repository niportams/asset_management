<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: asset.php
// Created By:     Evana Yasmin 
// Change history: Hasibul Haque:-- requisition,requisitions,approvedrequisitions
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0

// Function list:-- capitalize,capitallist,register,deadstate,capital_register,assetlist,deadstatelist,
// delete,update,capital_update,capital_delete,requisition,manual_allocation,requisitionlist,requisitions,
// approvedrequisitions,allocation_request,allocation_request,transfer_request,new_asset_register,
// allocate,allocated_list,transfered_list,revert,state_revert,transfer,state,disposedlist,maintenancelist,
// permission_button,capital_limit_update,

/****************************************************/

/**
 * AMS asset Controller Class
 * This method demonstrates the asset related operation of AMS.
 */

class asset extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();


        $this->load->model('user_model', '', TRUE);
        $this->load->model('manufacture_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('asset_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);
        $this->load->model('organization_model', '', TRUE);
        $this->load->model('location_model', '', TRUE);

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Asset');
        $this->set_page_sub_title('control panel');
    }

    /** 
    *This function demonstrates the asset capitalization form of asset.
    */
    
    public function capitalize()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/sci/project.js');
        $this->load->model('department_model', '', TRUE);

        $user_center = $this->session->userdata('user_center');
        //var_dump($user_center); 
        //echo $data['user_center'] = $session_data['user_name'];

        $this->set_page_title(load_message('CAPITALIZE_NEW_ASSET'));
        $this->set_page_sub_title('');

        $result = $this->category_model->categorylist();
        $result1 = $this->office_model->officelist();
        $result2 = $this->location_model->locationlist();
        $result3 = $this->supplier_model->supplierlist();
        $result4 = $this->user_model->userlist();
        $org_list = $this->organization_model->organization_list();
        $dept_list = $this->department_model->department_list();

        //var_dump($result); 
        $this->set_value('user_center', $user_center);
        $this->set_value('category_list', $result);
        $this->set_value('office_list', $result1);
        $this->set_value('supplier_list', $result3);
        $this->set_value('user_list',$result4);
        $this->set_value('org_list',$org_list);
        $this->set_value('dept_list',$dept_list);

        //var_dump($result4);

        //$token=uniqid();
        $token = date("Y-m-d H:i:s");
        $token = str_replace('-', '', $token);
        $token = str_replace(':', '', $token);
        $token = str_replace(' ', '', $token);
        $this->set_value('grn', $token);

        $this->load_view('admin_lte/asset/asset_capitalize');
    }

    /** 
    *This function retrieves the asset capitalization list form database.
    */

    public function capitallist()
    {
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/sci/project.js');
        $user_office = $this->session->userdata('user_center');
        $user_role = get_user_role_name($this->session->userdata('user_role_id'));

        $this->set_page_title(load_message('CAPITAL_LIST'));
        $this->set_page_sub_title('');

        if ($user_role == 'SUPER_ADMIN') /******* All capital list for NIPORT HQ **********/ 
        {
            $result = $this->asset_model->capitallist();
        } else {
            $result = $this->asset_model->rtccapitallist($user_office);
        }

        //var_dump($result);
        $this->set_value('capital_list', $result);
        $this->load_view('admin_lte/asset/capital_list');
    }


    /** 
    * This function generates the asset registration form of AMS.
    */
    
    public function register()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
        $this->set_js('dist/js/sci/date_picker.js');
        $this->set_js('dist/js/sci/project.js');

        $this->set_page_title(load_message('REGISTER_ASSET'));
        $this->set_page_sub_title('');

        // $result = $this->category_model->categorylist();
        // $result1 = $this->manufacture_model->manufacturelist();
        // $result2 = $this->supplier_model->supplierlist();
        //$this->set_value('category_list',$result);
        //$this->set_value('manufacture_list',$result1);
        // $this->set_value('supplier_list',$result2);

        $this->load_view('admin_lte/asset/asset_register');

    }

    /** 
    * This function generates the Deadstate form of AMS.
    */

    public function deadstate()
    {
        $this->set_page_title('Asset State Entry');
        $this->set_page_sub_title('status update');

        //$result3 = $this->asset_model->assetstatuslist(); 
        //$this->set_value('status_list',$result3);
        $this->load_view('admin_lte/asset/asset_deadstate_form');

    }
    
    /** 
    *This function demonstrates the asset capitalized registration form of asset.
    */
   
    public function capital_register()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
        $this->set_js('dist/js/sci/date_picker.js');
        $this->set_js('dist/js/sci/project.js');
        $grn_no = $this->uri->segment(3);
        //$capital_id = $this->uri->segment(3);

        $this->set_page_title(load_message('REGISTER_ASSET'));
        $this->set_page_sub_title('');

        $result = $this->category_model->categorylist();
        $result1 = $this->manufacture_model->manufacturelist();
        $result2 = $this->supplier_model->supplierlist();

        //var_dump($result);

        $this->set_value('grn_no', $grn_no);

        if ($grn_no <> "") {
            $result4 = $this->asset_model->grn_check_register($grn_no);
            if ($result4 <> 0) {
                $this->set_value('asset_capital', $result4);
            } else {
                $this->set_value('asset_capital', "");
            }
        }

        $this->set_value('category_list', $result);
        $this->set_value('manufacture_list', $result1);
        $this->set_value('supplier_list', $result2);
        $this->load_view('admin_lte/asset/capital_register');
    }

    /** 
    *This function retrieves all asset list(except disposed asset) from database.
    */
   
    public function assetlist()
    {
        $this->set_page_title(load_message('ASSET_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/jsonmap.js');

        $status_id = 1; //Registered Item
        $result = $this->asset_model->asset_list();
        $rests = $this->asset_model->statuslist();
        //var_dump($rests);

        if ($result <> false) {
            $this->set_value('asset_list', $result);
        } else {
            $this->set_value('asset_list', "");
        }

        $this->set_value('sts_list', $rests);
        $this->load_view('admin_lte/asset/asset_list');
    }

    /** 
    *This function retrieves all deadstate asset from database.
    */
   
    public function deadstatelist()
    {
        $this->set_page_title(load_message('DEADSTATE_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $status_id = 3; //Registered Item
        $result = $this->asset_model->status_wise_asset_list($status_id);
        $rests = $this->asset_model->statuslist();
        //var_dump($rests);

        if ($result <> false) {
            $this->set_value('asset_list', $result);
        } else {
            $this->set_value('asset_list', "");
        }

        $this->set_value('sts_list', $rests);
        $this->load_view('admin_lte/asset/dead_list');
    }

    /** 
    *This function deletes an existing asset from database.
    */
   
    public function delete()
    {
        if ($this->input->is_ajax_request()) {
            //$asset_id = $this->input->post('delete_id',true);
            //$decrypt_asset = base64_decode(urldecode($asset_id));
            $jsondata = $this->input->post('jsondata', true);
            $jsondata = json_decode($jsondata, true);

            $deleteid = base64_decode(urldecode($jsondata['delete_id']));
            $res = $this->asset_model->asset_delete($deleteid);
            if ($res <> false) {

            /***** Audit Log: Asset Delete*****/
                $action_name = "Asset Delete";
                log_create($action_name,json_encode($jsondata,true));
            /***** Audit Log*****/
            echo $this->lang->line('DELETE');
            }
        } else {
            exit('No direct script access allowed');
        }

    }

    /** 
    *This function updates an existing asset detail information of asset.
    */
    
    public function update()
    {
        if ($this->input->is_ajax_request()) {

            //echo"got it";
            $user_center = $this->session->userdata('user_center');
            $jsondata = $this->input->post('jsondata', true);
            $jsondata = json_decode($jsondata, true);

            $updateid = base64_decode(urldecode($jsondata['update_id']));
            //$status_id= $jsondata['asset_status_id'];
            //unset($json_output['updateid']);

            $purchase_date = null;
            $warrenty_date = null;
            $asset_lifetime = null;
            if ($jsondata['purchase_date'] != null) {
                $purchase_date = DateTime::createFromFormat('d/m/Y', $jsondata['purchase_date']);
                $purchase_date = $purchase_date->format('Y-m-d');
            }
            if ($jsondata['warrenty_date'] != null) {
                $warrenty_date = DateTime::createFromFormat('d/m/Y', $jsondata['warrenty_date']);
                $warrenty_date = $warrenty_date->format('Y-m-d');
            }
            if ($jsondata['asset_lifetime'] != null) {
                $asset_lifetime = DateTime::createFromFormat('d/m/Y', $jsondata['asset_lifetime']);
                $asset_lifetime = $asset_lifetime->format('Y-m-d');
            }
            /*************** SCI ID Uniqueness Checking *******************/

            $sci_id = $jsondata['sci_id'];

            if ($sci_id <> "") {
             $rest = $this->asset_model->sciid_update_check($updateid, $sci_id);

                if ($rest == false) {
                    //echo"SCI is unique!";
                    $sci_flag = 1;
                    
                    /****** SCI # is Not unique *********/
                } else {
                    $sci_flag = 2;
                    /****** SCI Not unique *********/
                }

                //var_dump($rest);
            } else {
                $sci_flag = 0; /* ** SCI ic empty ** */
            }

            if (($sci_flag == 1) or ($sci_flag == 0)) {

                $jsonarray = array("category_id" => $jsondata['category_id'],
                    'asset_name' => filter_var($jsondata['asset_name'], FILTER_SANITIZE_STRING),
                    'asset_description' => filter_var($jsondata['asset_description'], FILTER_SANITIZE_STRING),
                    'manufacture_id' => empty($jsondata['manufacture_id']) ? 0 : $jsondata['manufacture_id'],
                    'model_name' => filter_var($jsondata['model_name'], FILTER_SANITIZE_STRING),
                    'serial_no' => filter_var($jsondata['serial_no'], FILTER_SANITIZE_STRING),
                    'model_no' => filter_var($jsondata['model_no'], FILTER_SANITIZE_STRING),
                    'reference_no' => filter_var($jsondata['reference_no'], FILTER_SANITIZE_STRING),
                    'sci_id' => filter_var($jsondata['sci_id'], FILTER_SANITIZE_STRING),
                    'purchase_date' => filter_var($purchase_date, FILTER_SANITIZE_STRING),
                    'purchase_no' => filter_var($jsondata['purchase_no'], FILTER_SANITIZE_STRING),
                    'sku' => filter_var($jsondata['sku'], FILTER_SANITIZE_STRING),
                    //'purchase_price' => filter_var($jsondata['purchase_price'],FILTER_SANITIZE_STRING),
                    'purchase_price' => empty(filter_var($jsondata['purchase_price'], FILTER_SANITIZE_STRING)) ? 0 : filter_var($jsondata['purchase_price'], FILTER_SANITIZE_STRING),
                    'purchase_location' => filter_var($jsondata['purchase_location'], FILTER_SANITIZE_STRING),
                    'warrenty_date' => empty($warrenty_date) ? NULL : $warrenty_date,
                    'asset_lifetime' => empty($asset_lifetime)? NULL : $asset_lifetime,
                    'asset_remarks' => filter_var($jsondata['asset_remarks'], FILTER_SANITIZE_STRING),
                    'pr_reference_no' =>filter_var($jsondata['pr_reference_no'], FILTER_SANITIZE_STRING),
                );
                //echo $asset_json = json_encode($jsonarray,true);
                
                /*** Asset label change after category change ***/   
                $rest = $this->asset_model->get_asset_category($updateid);
                $ex_category_id = $rest[0]->category_id;
                
                if($ex_category_id<>$jsondata['category_id'])
                {
                  $max_category_id = $this->asset_model->get_max_office_category($user_center, $jsondata['category_id']);
                  //var_dump($max_category_id);

                  $max_id = $max_category_id[0]->max + 1;
                  $auto_asset_id = generate_assetid($user_center, $jsondata['category_id'], $updateid, $max_category_id[0]->max);
                  $result = $this->asset_model->registered_asset_update($updateid, $auto_asset_id, $max_id);  
                }
                /*** Asset label change after category change: End Here ***/   

                $result = sci_update_db('asset', $jsonarray, ['asset_id' => $updateid]);
                if ($result <> false) {

                    /***** Audit Log: Asset Update*****/
                    $action_name = "Asset Registration Update";
                    log_create($action_name,json_encode($jsonarray,true));
                    /***** Audit Log*****/

                echo '{"status": "success", "message": "' . $this->lang->line('ASSET_DETAILS_UPDATE') . '"}';
                }
            } else {
                echo '{"status": "error", "message": "' . $this->lang->line('SCI_DUPLICATE') . '"}';
            }

        } else {
            exit('No direct script access allowed');
        }
    }

/** 
*This function updates an existing asset capital information of asset.
*/

    public function capital_update()
    {
        if ($this->input->is_ajax_request()) {
            $db_capitalid = $this->input->post('capital_id', true);

            $capital_id = $this->input->post('capital_id', true);
            $asset_name = $this->input->post('asset_name', true);
            $category_id = $this->input->post('category_id', true);
            $office_id = $this->input->post('office_id', true);
            $supplier_id = $this->input->post('supplier_id', true);
            $voucher_no = $this->input->post('voucher_no', true);
            //$location_id =  $this->input->post('location_id',true);

            $total_received = $this->input->post('total_received', true);

            //$grn_no =  $this->input->post('grn_no',true);
            $gik_check = $this->input->post('gik_check', true);
            $funded_by = $this->input->post('funded_by', true);
            $received_by = $this->input->post('received_by', true);
            $own_funded = $this->input->post('own_funded', true);

            $department_id = $this->input->post('department_id', true);

            $this->asset_model->capital_update($capital_id, $asset_name, $category_id, $office_id, $total_received,
            $gik_check, $funded_by, $received_by, $own_funded, $supplier_id, $voucher_no,$department_id);

            /********* Notification set for capital edit***********/ 
            //set_notification(load_message('CAPITAL_UPDATE'), "", base_url() . "asset/capitallist", $next_approver_id, $capital_id, 'Capital Update');

            //echo"Saved Changes!";

            /***** Audit Log: Asset Update *****/
             $action_name = "Asset Capitalization Update";
             log_create($action_name,$capital_id);
            /***** Audit Log *****/

            echo $this->lang->line('UPDATE');
        } else {
            exit('No direct script access allowed');
        }
    }
    /*
     public function capital_update()
    {
       if ($this->input->is_ajax_request())
       {
         $json_output = json_decode($jsondata, true);

         $updateid= $json_output['updateid'];
         unset($json_output['updateid']);

         $result = sci_update_db('capitalize',$json_output,['capital_id'=>$updateid]);
         if($result<>false)
               {
                   echo $this->lang->line('UPDATE');
               }
         }
         else
          {
           exit('No direct script access allowed');
          }
    }
    */

    /** 
    *This function deletes an existing asset capital of AMS.
    */

    public function capital_delete()
    {
        if ($this->input->is_ajax_request()) {
            $capital_id = $this->input->post('capital_id', true);
            $decrypt_capital = base64_decode(urldecode($capital_id));
            $this->asset_model->capital_delete($decrypt_capital);

            /***** Audit Log: Asset Update*****/
             $action_name = "Asset Capitalization Delete";
             log_create($action_name,$decrypt_capital);

            //echo"Record Deleted Permanently!";
            echo $this->lang->line('DELETE');
        } else {
            exit('No direct script access allowed');
        }

    }

    /** 
    *This function demonstrates the asset requisition form of asset.
    */
    public function requisition()
    {

        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');

        $this->set_page_title(load_message('REQUISITION_REQUEST_LABEL'));
        $this->set_page_sub_title('');
        $this->load->model('category_model', '', TRUE);
        $this->load->model('user_model', '', TRUE);
        $result3 = $this->user_model->userlist();
        $this->set_value('user_list', $result3);
        $result = $this->category_model->categorylistwithoutparent();
        $this->load->model('office_model', '', TRUE);
        $result2 = $this->office_model->office_room_list();
        //var_dump($result2);
        $this->set_value('room_list', $result2);
        $this->set_js("dist/js/pages/add_new_field_script.js");
        $this->set_value('assets_name', $result);
        // $result = $this->asset_model->categorylist();

        $this->load_view('admin_lte/asset/asset_requisition');

    }

    /** 
    *This function demonstrates the manual allocation process of asset.
    */

    public function manual_allocation()
    {

        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_page_title(load_message('ASSET_ALLOCATION'));
        $this->set_page_sub_title('');
        $this->load->model('category_model', '', TRUE);
        $this->load->model('user_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);
        $result = $this->category_model->categorylistwithoutparent();

        $res = $this->asset_model->temp_delete_all();

        $result3 = $this->asset_model->get_file_include();
        $img_url = base_url() . 'temp/';
        if ($result3 <> false) {
            //$data['file_list'] =$result3;
            foreach ($result3 as $res3) {
                $file_id = $res3->file_id;
                $file_name = $res3->file_name;
                $rest1 = $this->asset_model->delete_tmp_files($file_id);
                if ($rest1 <> false) {
                    $img_url = './temp/' . $file_name;
                    @unlink($img_url);
                }
            }
        }

        $this->set_js("dist/js/pages/add_new_field_script.js");
        $this->set_value('assets_name', $result);

        $result3 = $this->user_model->userlist();
        $this->set_value('user_list', $result3);

        $result1 = $this->user_model->approval_list();
        //var_dump($result1);
        $this->set_value('approver_list', $result1);

        $result2 = $this->office_model->office_room_list();
        //var_dump($result1);
        $this->set_value('room_list', $result2);

        $this->load_view('admin_lte/asset/asset_allocation');

    }

    /** 
    *This function retrievs the requested asset list from requisition.
    */
   
    public function requisitionlist()
    {
        $this->set_js('dist/js/sci/project.js');
        $this->set_page_title(load_message('PENDING_REQUISITION_LIST'));
        $this->set_page_sub_title('');
        $result = $this->asset_model->requisitionlist();

        //var_dump($result); 
        $this->set_value('requisition_list', $result);
        $this->load_view('admin_lte/asset/requisition_list');
    }

    /** 
    *This function retrievs the request list of a user from database.
    */

    public function requisitions()
    {
        $this->set_js('dist/js/sci/project.js');
        $this->set_page_title(load_message('MY_REQUISITIONS'));
        $this->set_page_sub_title('');

        $result = $this->asset_model->requisitions();

        //var_dump($result); 
        $this->set_value('requisition_list', $result);
        $this->load_view('admin_lte/asset/show_requisition_list');
    }

    /** 
    *This function retrievs the approved request list of a user from database.
    */
   
    public function approvedrequisitions()
    {
        $this->set_js('dist/js/sci/project.js');
        $this->set_page_title(load_message('ACTIVITY_HISTORY'));
        $this->set_page_sub_title('');

        $result = $this->asset_model->approvedrequisitions();

        //var_dump($result); 
        $this->set_value('requisition_list', $result);
        $this->load_view('admin_lte/asset/show_requisition_list');
    }

    /** 
    *This function retrievs the requested allocation list from database.
    */
 
    public function allocation_request()
    {
        $user_center = $this->session->userdata('user_center');
        $this->set_js('dist/js/sci/project.js');
        $this->load->model('asset_model', '', TRUE);
        $this->set_page_title(load_message('REQUESTED_ALLOCATION_LIST'));
        $this->set_page_sub_title('');

        $result = $this->asset_model->allocation_approved_list();

        //var_dump($result);
        $this->set_value('allocation_approved_list', $result);

        $this->load_view('admin_lte/asset/allocation_approved_list');

    }

    /** 
    *This function retrievs the requested transfer list from database.
    */
   public function transfer_request()
    {
        $user_center = $this->session->userdata('user_center');
        $this->set_js('dist/js/sci/project.js');
        $this->load->model('asset_model', '', TRUE);
        $this->set_page_title(load_message('REQUESTED_TRANSFER_LIST'));
        $this->set_page_sub_title('');

        $result = $this->asset_model->transfer_approved_list();

        //var_dump($result);
        $this->set_value('transfer_approved_list', $result);
        $this->load_view('admin_lte/asset/transfer_approved_list');

    }

    /** 
    *This function demonstrates the new asset registration form to register new asset.
    */
   
    public function new_asset_register()
    {
        $this->set_page_title('Asset Registration');
        $this->set_page_sub_title('new asset');

        $result = $this->asset_model->available_grn_list();
        if($result<>false)
        {
            $data['grn_list'] = $result;
        }
        else { $data['grn_list'] = ""; }

        $data['registration_way'] = 1;

        $this->load->view('admin_lte/asset/new_asset_register', $data);
    }

    /** 
    *This function completes the asset allocation process of a requested user.
    */

    public function allocate()
    {
        $user_center = $this->session->userdata('user_center');
        if ($this->input->is_ajax_request()) 
        {
            $approved_by = $this->input->post('approved_by', true);
            $item_id = $this->input->post('item_id', true);
            $requisition_id = $this->input->post('requisition_id', true);
            $category_id = $this->input->post('category_id', true);
            $asset_qty = $this->input->post('asset_qty', true);
            $office_id = $this->input->post('office_id', true);
            $allocate_to = $this->input->post('allocate_to', true);
            $allocate_type = $this->input->post('allocate_type', true);

            $asset_location = $this->input->post('asset_location', true);
            $on_behalf = $this->input->post('on_behalf', true);
          
            $result2 = $this->asset_model->requested_category_available($category_id, $asset_qty);
            // var_dump($result2);
            if ($result2 <> false) {
                /************* Add assets to the allocation list *****************/
                $result1 = $this->asset_model->cat_wise_asset_list($category_id, $asset_qty);
                //var_dump($result1);
                if ($result1 <> false) {
                    foreach ($result1 as $rest12) 
                    {
                        $allocate_asset_id = $rest12->asset_id;
                        $allocate_category_id = $rest12->category_id;
                        $asset_label_id = $rest12->asset_readable_id;

                        $allocated_by = $this->session->userdata('user_db_id');
                        $allocate_to = $allocate_to;
                        $allocate_office = $office_id;
                        $remarks = "";

                        $req = $this->asset_model->allocate_asset($approved_by,$allocate_asset_id, $asset_label_id, $allocate_category_id, $requisition_id,
                            $allocated_by, $allocate_to, $allocate_office, $allocate_to, $remarks,$asset_location,$on_behalf);
                        if ($req == false) {
                            //echo"Failed to insert data";
                        } else {
                            // ********** Update Base Asset Table *********** //
                            //echo"succesfully allocated!";
                            $req1 = $this->asset_model->update_asset_status($allocate_asset_id, $allocate_to);
                            $req2 = $this->asset_model->update_requisition_asset($item_id, $requisition_id);
                            //var_dump($req2);

                            /**** Audit Log for Asset Allocation ****/
                          
                            $action_name = "Asset Allocation";
                            $allocate_json["asset_id"] = $allocate_asset_id;
                            $allocate_json["asset_label_id"] = $asset_label_id;
                            $allocate_json["asset_category_id"] = $allocate_category_id;
                            $allocate_json["asset_requisition_id"] = $requisition_id;
                            $allocate_json["allocated_by"] = $allocated_by;
                            $allocate_json["allocate_to"] = $allocate_to;
                            $allocate_json["allocate_office"] = $allocate_office;
                            $allocate_json["remarks"] = $remarks;
                            log_create($action_name,json_encode($allocate_json,true));
                   
                            /**** Audit log end here *****/
                        }
                    }
                    echo $this->lang->line('ALLOCATION_SUCCESS');

                    $rest3 = $this->asset_model->get_allocated_list($requisition_id);
                    if ($rest3 <> false) {
                        //$data['allocated_list']=$rest3;
                        $this->set_value('allocated_list', $rest3);
                        //$this->load_view('admin_lte/asset/requisition_allocated_list');
                    } else {
                        //$data['allocated_list']="";
                        $this->set_value('allocated_list', $rest3);
                        //$this->load_view('admin_lte/asset/requisition_allocated_list');
                    }
                }
            /************ Asset allocation end here *************/
            } else {
                echo "Failed to allocate!";
            }

        } else {
            exit('No direct script access allowed');
        }

    }

    /** 
    *This function retrieves the allocated asset list of an specific office.
    */
    public function allocated_list()
    {
        $this->set_page_title(load_message('ALLOCATED_ASSET_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $result = $this->asset_model->allocated_list();

        //var_dump($result); 
        if ($result <> false) {
            $this->set_value('alloc_list', $result);
        } else {
            $this->set_value('alloc_list', "");
        }
        $this->set_value('alloc_list', $result);
        $this->load_view('admin_lte/asset/allocated_list');
    }

    /** 
    *This function retrieves the transfereed asset list of an specific office.
    */
    
    public function transfered_list()
    {
        $this->set_page_title(load_message('TRANSFERED_ASSET_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $result = $this->asset_model->transfered_list();

        //var_dump($result); 
        if ($result <> false) {
            $this->set_value('transfer_list', $result);
        } else {
            $this->set_value('transfer_list', "");
        }

        $this->load_view('admin_lte/asset/transfered_list');
    }

    /** 
    *This function completes the asset reverting process of an asset.
    */
    public function revert()
    {
        $this->load->model('allocation_model', '', TRUE);

        if ($this->input->is_ajax_request()) 
        {
            $allo_id = $this->input->post('allo_id', true);
            $revert_id = $this->input->post('revert_id', true);
            $revert_type = $this->input->post('revert_type', true);
            $from_office = $this->input->post('from_office', true);
            $reference_no = $this->input->post('reference_no', true);

            //$decrypt_asset = base64_decode(urldecode($asset_id));
            if($revert_type == "allocation")
            {
                $purpose = "revert_allocation";
                $result3 = $this->allocation_model->get_revert_file($allo_id,$reference_no,$purpose);
                if($result3==false)
                {
                  //echo $this->lang->line('REVERT_DOCUMENT');  
                    $msg = 0;
                }
                else
                {
                    $res = $this->asset_model->asset_revert($allo_id, $revert_id, $revert_type, $from_office);
                    if ($res <> false) 
                    {
                        /**** Audit Log ****/
                        $action_name = "Asset Allocation Revert";
                        $allocate_json["asset_id"] = $allo_id;
                        $allocate_json["revert_id"] = $revert_id;
                        $allocate_json["revert_type"] = $revert_type;
                        $allocate_json["from_office"] = $from_office;

                        log_create($action_name,json_encode($allocate_json,true));
                        /**** Audit log end here *****/

                        echo $this->lang->line('REVERT');
                        //echo "Asset is reverted";
                    }  
                }
            }
            else
            {
                $purpose = "revert_transfer";

                $result3 = $this->allocation_model->get_revert_file($allo_id,$reference_no,$purpose);
                if($result3==false)
                {
                  //echo $this->lang->line('REVERT_DOCUMENT');  
                    $msg = 0;
                }
                else
                {
                    $res = $this->asset_model->asset_revert($allo_id, $revert_id, $revert_type, $from_office);
                    if ($res <> false) 
                    {

                       /**** Audit Log ****/
                        $action_name = "Asset Transfer Revert";
                        $allocate_json["asset_id"] = $allo_id;
                        $allocate_json["revert_id"] = $revert_id;
                        $allocate_json["revert_type"] = $revert_type;
                        $allocate_json["from_office"] = $from_office;

                        log_create($action_name,json_encode($allocate_json,true));
                        /**** Audit log end here *****/

                        echo $this->lang->line('TRANSFER_REVERTED');
                        //echo "Asset is reverted";
                    }  
                }

            }
            
        } else {
            exit('No direct script access allowed');
        }
    }

   /** 
    *This function completes the asset state reverting process.
    */
    
    public function state_revert()
    {
        if ($this->input->is_ajax_request()) 
        {
            $this->load->model('asset_model', '', TRUE);

            $state_id = $this->input->post('state_id', true);
            $asset_id = $this->input->post('asset_id', true);
            $state_type = $this->input->post('state_type', true);

            $decrypt_asset = base64_decode(urldecode($asset_id));

            $result = $this->asset_model->get_asset_history($decrypt_asset,$state_id);
            //var_dump($result);

            if($result <> false)
            {
               $revert_state = $result[0]->previous_state;
               $res = $this->asset_model->state_revert($decrypt_asset, $state_type,$revert_state); 
               if ($res <> false) 
                {
                    /**** Audit Log ****/
                    $action_name = "Asset State Revert";
                    log_create($action_name,$decrypt_asset);
                    /**** Audit log end here *****/

                    echo $this->lang->line('ASSET_REVERT');
                    // echo "Asset is reverted";
                } 
            }
            else
                {
                    echo"Failed to revert asset.";
                }
            
        } else {
            exit('No direct script access allowed.');
        }
    }

    /** 
    *This function completes the asset transfer process of an asset.
    */

    public function transfer()
    {
        $user_center = $this->session->userdata('user_center');
        if ($this->input->is_ajax_request()) {
            $item_id = $this->input->post('item_id', true);
            $requisition_id = $this->input->post('requisition_id', true);
            $category_id = $this->input->post('category_id', true);
            $asset_qty = $this->input->post('asset_qty', true);
            $office_id = $this->input->post('office_id', true);
            $allocate_to = $this->input->post('allocate_to', true);
            $allocate_type = $this->input->post('allocate_type', true);

            $result2 = $this->asset_model->requested_category_available($category_id, $asset_qty);
            // var_dump($result2);
            if ($result2 <> false) 
            {
                /************* Add assets to the allocation list *****************/
                $result1 = $this->asset_model->cat_wise_asset_list($category_id, $asset_qty);
                //var_dump($result1);
                if ($result1 <> false) 
                {
                    foreach ($result1 as $rest12) {
                        $transfer_asset_id = $rest12->asset_id;
                        $transfer_category_id = $rest12->category_id;
                        $asset_label_id = $rest12->asset_readable_id;

                        $transfer_by = $this->session->userdata('user_db_id');
                        $transfer_to = $allocate_to;
                        $transfer_office = $office_id;
                        $remarks = "";

                        $req = $this->asset_model->transfer_asset($transfer_asset_id, $asset_label_id, $transfer_category_id, $requisition_id,
                        $transfer_by, $transfer_to, $transfer_office, $remarks);
                        if ($req == false) {
                            //echo"Failed to insert data";
                        } else {
                            /*********** Update Base Asset Table **************/
                            
                            //echo"succesfully allocated!";
                            $req1 = $this->asset_model->transfer_asset_status($transfer_asset_id, $transfer_to, $transfer_office);
                            $req2 = $this->asset_model->update_requisition_asset($item_id, $requisition_id);
                            //var_dump($req2);
                        }
                    }

                    /**** Audit Log ****/
                        $action_name = "Asset Transfer";
                        $transfer_json["asset_id"] = $transfer_asset_id;
                        $transfer_json["asset_label_id"] = $asset_label_id;
                        $transfer_json["asset_category_id"] = $transfer_category_id;
                        $transfer_json["asset_requisition_id"] = $requisition_id;
                        $transfer_json["transfer_by"] = $transfer_by;
                        $transfer_json["transfer_to"] = $transfer_to;
                        $transfer_json["transfer_office"] = $transfer_office;
                        $transfer_json["remarks"] = $remarks;
                        log_create($action_name,json_encode($transfer_json,true));
                    /**** Audit log end here *****/

                    echo $this->lang->line('TRANSFER_SUCCESS');

                }
               
            } else {
                echo "Failed to Transfer!";
            }
        } else {
            exit('No direct script access allowed');
        }

    }

    /**************** Asset Deatils Information Showing  ********************/
    /*
    public function details()
    {
     if ($this->input->is_ajax_request())
          {
             $jsondata =  $this->input->post('jsondata',true);
             $jsondata = json_decode($jsondata, true);
             echo $detailid= base64_decode(urldecode($jsondata->asset_id));

            $res = $this->asset_model->asset_details($detailid);
            var_dump($res);

            if($res<>false)
            {
              $this->set_value('asset_details',$res);
            }
            else
            {
               $this->set_value('asset_details',"");
            }
            $this->load_view('admin_lte/asset/asset_details');
            }
        else
        {
        exit('No direct script access allowed');
        }
    }
  */


    /** 
    *This function generates the asset state change form of an asset.
    */
    public function state()
    {

        //$state_id = $this->uri->segment(3);
        $state_name = $this->uri->segment(3);
        $this->set_js('dist/js/sci/project.js');
        $this->set_page_title(load_message('ASSET_STATE_LABEL'));
        $this->set_page_sub_title($state_name . '&nbsp; state entry');

        //$this->set_value('state_id',$state_id);
        $this->set_value('chosen_state_name', $state_name);
        $this->load_view('admin_lte/asset/asset_search_form');
    }

    /** 
    *This function retrieves disposed asset list from database.
    */
    public function disposedlist()
    {
        $this->set_page_title(load_message('DISPOSED_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');

        $status_id = 6; //Disposed Item
        $result = $this->asset_model->status_wise_asset_list($status_id);
        $rests = $this->asset_model->statuslist();
        //var_dump($rests);

        if ($result <> false) {
            $this->set_value('asset_list', $result);
        } else {
            $this->set_value('asset_list', "");
        }

        $this->set_value('sts_list', $rests);
        $this->load_view('admin_lte/asset/disposed_list');
    }

    
    /** 
    *This function retrieves maintenance asset list from database.
    */
    public function maintenancelist()
    {
        $this->set_page_title(load_message('MAINTAINENCE_ASSET_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $status_id = 4; //Disposed Item
        $result = $this->asset_model->status_wise_asset_list($status_id);
        $rests = $this->asset_model->statuslist();
        //var_dump($rests);

        if ($result <> false) {
            $this->set_value('asset_list', $result);
        } else {
            $this->set_value('asset_list', "");
        }

        $this->set_value('sts_list', $rests);
        $this->load_view('admin_lte/asset/maintenance_list');
    }

    /** 
    *This function generates permission button of ams.
    */
    public function permission_button()
    {

        if ($this->input->is_ajax_request()) {
          
            $asset_id = $this->input->post('asset_id', true);
            $data['asset_id'] = $asset_id;

            $this->load->view('admin_lte/asset/permission_button', $data);
        } else {
            exit('No direct script access allowed');
        }
    }

    /** 
    *This function updates capital registration limit of asset capitalization.
    */
    public function capital_limit_update()
    {
        if ($this->input->is_ajax_request())
       {

         $jsondata = $this->input->post('jsondata', true);
         $json_output = json_decode($jsondata, true);

         $updateid= $json_output['updateid'];
         unset($json_output['updateid']);

         $result = sci_update_db('capitalize',$json_output,['capital_id'=>$updateid]);
         if($result<>false)
           {
               echo $this->lang->line('UPDATE');
           }
         }
         else
          {
           exit('No direct script access allowed');
          }
    }
    
    
    
}
