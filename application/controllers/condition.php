<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: condition.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: physical_check,check_list
//
/****************************************************/

/**
 * AMS Asset condition Controller Class
 *
 * This method demonstrates the physical checking process of AMS.
 */

class condition extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('physical_model', '', TRUE);
    }

    public function index()
    {
        $this->set_page_title('Physical Condition');
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/page/condition');
    }

    /********Create new asset physical condition**************/
    public function physical_check()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_page_title(load_message('ASSET_PHYSICAL_CHECK'));
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/general_script.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/physical/physical_check_form');
    }

    /********** Show all physical check list ***************/
    public function check_list()
    {

        $this->set_page_title(load_message('PHYSICAL_CHECK_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $result = $this->physical_model->check_list();
        //var_dump($result);

        $this->set_value('check_list', $result);

        $this->load_view('admin_lte/physical/physical_list');
    }


}
