<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: privilege.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: addprivilege,privilegelist,privilege_edit,privilege_delete
//
/****************************************************/

/**
 * AMS Asset privilege Controller Class
 *
 * This method demonstrates the role privilege process of AMS.
 */
class privilege extends SCI_Controller
{
    
    function __construct()
    {
        parent::__construct();

        $this->load->model('privilege_model', '', TRUE);
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Privilege');
        $this->set_page_sub_title('control panel');
    }

    /********Create new privilege**************/
    public function addprivilege()
    {
        $this->set_js('dist/js/jsonmap.js');

        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ADD_NEW_PRIVILEGE'));
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/privilege/privilege_create');
    }

    /**********Show all privilege***************/
    public function privilegelist()
    {
        $this->set_page_title(load_message('PRIVILEGE_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/jsonmap.js');

        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/general_script.js');
        $this->set_js('dist/js/global_script.js');
        $result = $this->privilege_model->privilegelist();
        //var_dump($result);
        $this->set_value('privilege_list', $result);
        $this->load_view('admin_lte/privilege/privilege_list');
    }

    /**************privilege Update**************/

    public function privilege_edit()
    {
        if ($this->input->is_ajax_request()) {

            $jsondata = $this->input->post('jsondata', true);

            $json_output = json_decode($jsondata, true);
            $updateid = $json_output['updateid'];
            unset($json_output['updateid']);

            $result = sci_update_db('privilege', $json_output, ['privilege_id' => $updateid]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Privilege Update";
                log_create($action_name,$jsondata);
                /**** Audit log end here *****/

                echo $this->lang->line('UPDATE');
            }

        } else {
            exit('No direct script access allowed');
        }

    }

    /************Privilege Delete**************/
    public function privilege_delete()
    {
        if ($this->input->is_ajax_request()) {
            $privilege_id = $this->input->post('privilege_id', true);
            $this->privilege_model->privilege_delete($privilege_id);

            //echo"Privilege Deleted!";
            /**** Audit Log ****/
            $action_name = "Privilege Delete";
            log_create($action_name,$privilege_id);
            /**** Audit log end here *****/

            echo $this->lang->line('DELETE');
        } else {
            exit('No direct script access allowed');
        }
    }


}
