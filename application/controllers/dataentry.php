<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: dataentry.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: asset_register
//
/****************************************************/

 /**
 * AMS Asset dataentry Controller Class
 * This method demonstrates the data entry process of AMS.
 */

class dataentry extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('user_model', '', TRUE);
        $this->load->model('manufacture_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('asset_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);
        $this->load->model('organization_model', '', TRUE);
        $this->load->model('location_model', '', TRUE);

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index()
    {
        $this->set_page_title('Data Collection Form');
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/page/allocation');
    }

    /** 
    *This function completes the old asset registration along with allocation process.
    */
    public function asset_register()
    {

        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
        $this->set_js('dist/js/sci/date_picker.js');
        $this->set_js('dist/js/sci/project.js');

        $this->set_page_title(load_message('DATA_ENTRY'));
        $this->set_page_sub_title('');

         $result = $this->category_model->categorylist();
         $result1 = $this->manufacture_model->manufacturelist();
         $result2 = $this->supplier_model->supplierlist();

         $result3 = $this->user_model->userlist();
         $result4 = $this->office_model->office_room_list();
        //var_dump($result1);
         $org_list = $this->organization_model->organization_list();
         $this->set_value('org_list',$org_list);

         $token = date("Y-m-d H:i:s");
         $token = str_replace('-', '', $token);
         $token = str_replace(':', '', $token);
         $token = str_replace(' ', '', $token);
         $this->set_value('grn', $token);
            
         $this->set_value('category_list',$result);
         $this->set_value('manufacture_list',$result1);
         $this->set_value('supplier_list',$result2);
         $this->set_value('user_list',$result3);
         $this->set_value('room_list', $result4);

        $this->load_view('admin_lte/operator/data_entry');  
    }

}
