<?php
/****************************************************/
// Filename: ajax4.php
// Created By:     Evana Yasmin 

// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0

// Function list: backupDatabaseTables,startbackup,db_delete
/****************************************************/

/**
 * AMS Asset Allocation Controller Class
 *
 * This method demonstrates the allocation process of AMS.
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class ajax4 extends CI_Controller
{


    function __construct()
    {
        parent::__construct();

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('user_model', '', TRUE);
       //$this->lang->load('message','english');
    }

/** 
*This function dumps the database into sql format 
*/
function backupDatabaseTables($dbHost,$dbUsername,$dbPassword,$dbName,$tables = '*')
{
    //connect & select the database
    $db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName); 

    //get all of the tables
    if($tables == '*'){
        $tables = array();
        $result = $db->query("SHOW TABLES");
        while($row = $result->fetch_row()){
            $tables[] = $row[0];
        }
    }else{
        $tables = is_array($tables)?$tables:explode(',',$tables);
    }
    
    //loop through the tables
    foreach($tables as $table){
        $result = $db->query("SELECT * FROM $table");
        $numColumns = $result->field_count;
        //$return = "";
        @$return .= "DROP TABLE IF EXISTS $table;";

        $result2 = $db->query("SHOW CREATE TABLE $table");
        $row2 = $result2->fetch_row();

        $return .= "\n\n".$row2[1].";\n\n";

        for($i = 0; $i < $numColumns; $i++){
            while($row = $result->fetch_row()){
                $return .= "INSERT INTO $table VALUES(";
                for($j=0; $j < $numColumns; $j++){
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = @ereg_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return .= '"'.$row[$j].'"' ; } else { $return .= '""'; }
                    if ($j < ($numColumns-1)) { $return.= ','; }
                }
                $return .= ");\n";
            }
        }

        $return .= "\n\n\n";
    }

    //save file
    $handle = fopen('db_backup/db-'.date('Y-m-d').'-'.time().'.sql','w+');
    fwrite($handle,$return);
    fclose($handle);
   
}

/** 
*This function calls  the function to dump the database into sql format 
*/
public function startbackup()
{

$db = $this->load->database('default', TRUE);
//var_dump($db);

 $host =  $db->hostname;
 $username =  $db->username;
 $user_pass =  $db->password;
 $database =  $db->database;

 $this->backupDatabaseTables($host,$username,$user_pass,$database);
 
  //echo"Database is exported successfully!";
  echo $this->lang->line('EXPORT_SUCCESS');
}

public function testing()
{
    $this->startbackup();
}


/** 
*This function deletes the existing database file
*/

public function db_delete()
{
     if ($this->session->userdata('user_logged_in')) 
     {
            if ($this->input->is_ajax_request()) 
            {
             
             $file_name = $this->input->post('file_name', true);
             $db_url = './db_backup/' . $file_name;
             @unlink($db_url);  
             echo $this->lang->line('DELETE');
            } 
            else 
            {
                exit('No direct script access allowed.');
            }
        } else {
            redirect('login');
        }
}


    
}