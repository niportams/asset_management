<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/****************************************************/
// Filename: page.php
// Created By:     Hasibul Huq
// Change history:
//
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: index,contact,
//
/****************************************************/
/**
 * Description of page
 *
 * @author hasibul.huq
 */
class page extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('office_model', '', TRUE);

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Page');
        $this->set_page_sub_title('control panel');
        //$this->set_value('test', 'sdfsdfsdfs');
        //$this->load_view('admin_lte/dashboard');
    }

    /**
     * contact page
     */
    public function contact()
    {

        $all_office = $this->office_model->all_office_details();
        $data['all_office']=$all_office;
        $this->load->view('admin_lte/page/contact',$data);
    }
}
