<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/****************************************************/
// Filename: notification.php
// Created By:     shahed.chaklader
// Change history:
//
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: index,get_notification,update_status,set_notification
//
/****************************************************/

/**
 * Description of notifications
 *
 * @author shahed.chaklader
 */
class notifications extends CI_Controller
{
    /**
     * Notification list
     */
    public function index()
    {

        $a = new SCI_Controller();
        $a->set_js('dist/js/sci/project.js');
        $this->db->select("*");
        $this->db->from('notifications');
        $this->db->order_by("arrival_date", "desc");
        $this->db->order_by("status", "desc");
        $this->db->where('user_id', $this->session->userdata['user_db_id']);
        $this->db->join('user', 'user.userid = notifications.user_form', 'left');
        $res = $this->db->get();


        $a->set_page_title('Notifications');
        $a->set_value('notifications', $res->result());

        $a->load_view('admin_lte/notification/all_notification');
    }

    /**
     * @param null $user_id
     * @param null $sender_id
     * @param int $status
     * @param null $arrival_date
     * @param null $read_date
     * @param null $return_type
     * Return notifications
     */
    public function get_notification($user_id = NULL, $sender_id = NULL, $status = 1, $arrival_date = NULL, $read_date = NULL, $return_type = NULL)
    {
        if ($this->session->userdata('user_logged_in')) {

            get_notification($user_id, $sender_id, $status, $arrival_date, $read_date, $return_type);

        } else {

            echo "-100"; //not logged in

        }
    }

    /**
     * @param $id
     * Update the notification status unread to read
     */
    public function update_status($id)
    {

        if ($this->session->userdata('user_logged_in'))

            update_status($id);

        else

            echo "-100"; //not logged in

    }

    /**
     * @param null $notice
     * @param null $title
     * @param null $action
     * @param null $user_id
     * @param null $sender_id
     * @param int $status
     * Set notification (inserting data in database)
     */
    public function set_notification($notice = NULL, $title = NULL, $action = NULL, $user_id = NULL, $sender_id = NULL, $status = 1)
    {

        if ($this->session->userdata('user_logged_in'))

            set_notification($notice, $title, $action, $user_id, $sender_id, $status);

        else

            echo "-100";//not logged in

    }

}
