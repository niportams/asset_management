<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/****************************************************/
// Filename: maintenance.php
// Created By:     Hasibul Huq
// Change history:
//
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: index,
//
/****************************************************/

/**
 * Description of maintenance
 *
 * @author hasibul.huq
 */

class maintenance extends CI_Controller
{
    /**
     * ridirects to the maintenance page
     */
    public function index()
    {
        if ($this->config->item('maintenance_mode') == TRUE && $this->session->userdata('user_maintain') != 1) {
            $this->load->view('admin_lte/maintenance_view');
        } else {
            redirect('login');
        }
    }
}
   