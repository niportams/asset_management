<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: dbback.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0

// Function list: dbexport,backuplist
//
/****************************************************/

/**
 * AMS Asset Database Backup Controller Class
 *
 * This method demonstrates the database backup process of AMS.
 */

class dbback extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();


        $this->load->model('user_model', '', TRUE);
        
        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Asset');
        $this->set_page_sub_title('control panel');
    }

    /** 
    *This function demonstrates the database export form of AMS.
    */

    public function dbexport()
    {
      $this->set_page_title('Database Backup');
      $this->set_page_sub_title('export');

      $this->load_view('admin_lte/dbbackup/export_form');   
    }

    /** 
    *This function retrieves the exported database list of AMS.
    */
    public function backuplist()
    {
        
        $this->set_page_title('Database Backup List');
        //$this->set_page_sub_title('export');

        $this->load->helper('directory');
        $map = directory_map('./db_backup/',1);
        //var_dump($map);
        $this->set_value('db_list', $map);
        $this->load_view('admin_lte/dbbackup/db_list');
    }
    
}
