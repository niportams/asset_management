<?php
/****************************************************/
// Filename: allocation.php
// Created By:     Evana Yasmin 16 Nov 2017
// Change history:
//      06.12.2018   / Evana Yasmin / newrequest

// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: allocation_list,newrequest,requestlist,reverted_allocation_list,
// reverted_transfer_list,direct_allocation,allocation_edit,choice_allocate,choice_transfer,
/****************************************************/

/**
 * AMS Asset Allocation Controller Class
 *
 * This method demonstrates the allocation process of AMS.
 */


if (!defined('BASEPATH')) exit('No direct script access allowed');

class allocation extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
    }

  
    public function index()
    {
        $this->set_page_title('Asset Allocation');
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/page/allocation');
    }

    /** 
    *This function retrieves the allocated asset list 
    *from database and display the items into a HTML page.
    */
    public function allocation_list()
    {
        $this->load->model('asset_model', '', TRUE);
        $this->set_page_title(load_message('ALLOCATION_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/allocation_script.js');
        
        $result = $this->asset_model->allocated_requisition_list();
        
        if ($result <> false) {
            $this->set_value('allocation_list', $result);
        } else {
            $this->set_value('allocation_list', "");
        }
        $this->set_value('allocation_list', $result);
        $this->load_view('admin_lte/asset/groupwise_allocated_list');
    }

    /** 
    *This function demostrates the manual allocation request of assets.
    */
    public function newrequest()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_js('dist/js/allocation_script.js');
        $this->set_page_title(load_message('ALLOCATION_REQUEST'));
        $this->set_page_sub_title('');
        
        $this->load->model('asset_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);
        $this->load->model('user_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);

        $reference_no = date("Y-m-d H:i:s");
        $reference_no = str_replace('-', '', $reference_no);
        $reference_no = str_replace(':', '', $reference_no);
        $reference_no = str_replace(' ', '', $reference_no);
        $reference_no = substr($reference_no, 6);

        $this->set_value('reference_no', $reference_no);

        $result = $this->category_model->categorylistwithoutparent();
        $res = $this->asset_model->temp_delete_all();

        $this->set_js("dist/js/pages/add_new_field_script.js");
        $this->set_value('assets_name', $result);

        $result3 = $this->user_model->userlist();
        $this->set_value('user_list', $result3);

        $result1 = $this->user_model->approval_list();
        //var_dump($result1);
        $this->set_value('approver_list', $result1);

        $result2 = $this->office_model->office_room_list();
        //var_dump($result1);
        $this->set_value('room_list', $result2);

        $this->load_view('admin_lte/allocation/allocation_request');
    }

/** 
*This function demostrates the requested allocation list of assets.
*/
public function requestlist()
{
    $this->load->model('allocation_model', '', TRUE);
    $this->load->model('user_model', '', TRUE);
    $this->load->model('office_model', '', TRUE);
    $this->set_js('dist/js/allocation_script.js');
    
    $this->set_page_title(load_message('REQUESTED_LIST'));
    $this->set_page_sub_title('');
  
    $result = $this->allocation_model->requested_list();
    
    if ($result <> false)
    {
        $this->set_value('request_list', $result);
    } else {
        $this->set_value('request_list', "");
    }
    
    $this->load_view('admin_lte/allocation/request_list'); 
}

/** 
* This function retrieves the reverted allocation list of assets from database and display
* them as list view.
*/
public function reverted_allocation_list()
{
    $this->load->model('allocation_model', '', TRUE);
    
    $this->set_js('dist/js/allocation_script.js');
    $this->set_page_title(load_message('REVERTED_LIST'));
    $this->set_page_sub_title('');
  
    $result = $this->allocation_model->reverted_allocation_list();
    
    if ($result <> false)
    {
        $this->set_value('alloc_list', $result);
    } else {
        $this->set_value('alloc_list', "");
    }
    
    $this->load_view('admin_lte/allocation/reverted_allocation_list'); 

}

/** 
*This function retrieves the reverted transfer list of assets from database and display
* them as list view.
*/
public function reverted_transfer_list()
{
    $this->load->model('allocation_model', '', TRUE);
    
    $this->set_js('dist/js/allocation_script.js');
    $this->set_page_title(load_message('REVERTED_TRANSFER_LIST'));
    $this->set_page_sub_title('');
  
    $result = $this->allocation_model->reverted_transfer_list();
    //var_dump($result);
    if ($result <> false)
    {
        $this->set_value('transfer_list', $result);
    } else {
        $this->set_value('transfer_list', "");
    }
    $this->load_view('admin_lte/transfer/reverted_transfer_list'); 
}

/** 
*This function demonstrates the allocation of a single item without the requisition process.
*/
public function direct_allocation()
{
    $this->set_js('dist/js/jsonmap.js');
    $this->set_page_title(load_message('SINGLE_ALLOCATION'));
    $this->set_js('dist/js/global_script.js');
    $this->set_js('dist/js/general_script.js');
    $this->set_js('dist/js/sci/project.js');
    $this->set_page_sub_title('');
    $this->load_view('admin_lte/allocation/single_allocation_form');
}

/** 
*This function allows the user to change the allocated location of an allocated asset.
*/
public function allocation_edit()
{
    $this->set_js('dist/js/allocation_script.js');
    $this->set_js('dist/js/general_script.js');
    $this->set_js('dist/js/sci/project.js');
    if($this->input->is_ajax_request())
        {
           $this->load->model('allocation_model','',TRUE);
           
           $allocate_id = $this->input->post('allocate_id', true);
           $location_id = $this->input->post('location_id', true);
                     
           $result = $this->allocation_model->allocation_update($allocate_id,$location_id); 
           if($result == true)
           {
                echo $this->lang->line('UPDATE');

                /***** Audit Log: Allocation Update *****/
                $action_name = "Asset Allocation Update";
                $allocate_json["allocate_id"] = $allocate_id;
                $allocate_json["location_id"] = $location_id;
                log_create($action_name,json_encode($allocate_json,true));
                /***** Audit Log *****/
           } 
        }
    else
         {
         exit('No direct script access allowed');
         } 
       
}

/** 
*This function demonstrates the dynamic allocation process where user can choose the item from
* the list at the time of allocation.
*/

public function choice_allocate()
{
    $this->set_js('dist/js/allocation_script.js');
    $this->set_js('dist/js/general_script.js');
    $this->set_js('dist/js/sci/project.js');
    
    if($this->input->is_ajax_request())
    {
            $this->load->model('allocation_model','',TRUE);
            $this->load->model('asset_model','',TRUE);

            $approved_by = $this->input->post('approved_by', true);
            $check_string = $this->input->post('check_string', true);
            $requisition_id = $this->input->post('requisition_id', true);
            $category_id = $this->input->post('category_id', true);
            $asset_qty = $this->input->post('asset_qty', true);
            $office_id = $this->input->post('office_id', true);
            $allocate_to = $this->input->post('allocate_to', true);
            $asset_location = $this->input->post('asset_location', true);
            $on_behalf = $this->input->post('on_behalf', true);
            $requisition_pkid = $this->input->post('requisition_pkid',true);
            $allocate_type = "Delivered";

            //var_dump($check_string);
            $asset_array = explode(',',$check_string);
            //var_dump($asset_array);

            $length = sizeof($asset_array);
            for($i = 0; $i<$length-1; $i++)
            {
               $asset_id =  $asset_array[$i];  
               $result = $this->asset_model->asset_details($asset_id);
               if($result <> false)
               {
                $asset_label_id = $result[0]->asset_readable_id;
                $allocated_by = $this->session->userdata('user_db_id');

                $allocate_office = $office_id;
                $remarks = "";

                $req = $this->asset_model->allocate_asset($approved_by,$asset_id, $asset_label_id, $category_id, $requisition_id,
                $allocated_by, $allocate_to, $allocate_office, $allocate_to, $remarks,$asset_location,$on_behalf);

                    if ($req == false) {
                            //echo"Failed to insert data";
                        } 
                        else 
                        {
                            // ********** Update Base Asset Table *********** //
                            //echo"succesfully allocated!";
                            $req1 = $this->asset_model->update_asset_status($asset_id, $allocate_to);
                            $req2 = $this->asset_model->update_requisition_asset($requisition_pkid, $requisition_id);
                            //var_dump($req2);

                            /**** Audit Log for Asset Allocation ****/
                          
                            $action_name = "Asset Allocation";
                            $allocate_json["asset_id"] = $asset_id;
                            $allocate_json["asset_label_id"] = $asset_label_id;
                            $allocate_json["asset_category_id"] = $category_id;
                            $allocate_json["asset_requisition_id"] = $requisition_id;
                            $allocate_json["allocated_by"] = $allocated_by;
                            $allocate_json["allocate_to"] = $allocate_to;
                            $allocate_json["allocate_office"] = $allocate_office;
                            $allocate_json["asset_location"] = $asset_location;
                            $allocate_json["on_behalf"] = $on_behalf;
                            $allocate_json["remarks"] = $remarks;
                            log_create($action_name,json_encode($allocate_json,true));
                   
                            /**** Audit log end here *****/
                        }
                }
            } 
            echo $this->lang->line('ALLOCATION_SUCCESS');
        }
    else
         {
         exit('No direct script access allowed');
         } 
         
}

/** 
*This function demonstrates the dynamic transfer process where user can choose the item from
* the list during transfer.
*/
public function choice_transfer()
{
    
    $this->set_js('dist/js/allocation_script.js');
    $this->set_js('dist/js/general_script.js');
    $this->set_js('dist/js/sci/project.js');
    
    if($this->input->is_ajax_request())
        {
            $this->load->model('allocation_model','',TRUE);
            $this->load->model('asset_model','',TRUE);

            $check_string = $this->input->post('check_string', true);
            $requisition_id = $this->input->post('requisition_id', true);
            $category_id = $this->input->post('category_id', true);
            $asset_qty = $this->input->post('asset_qty', true);
            $transfer_office = $this->input->post('office_id', true);
            $transfer_to = $this->input->post('allocate_to', true);
            
            $requisition_pkid = $this->input->post('requisition_pkid',true);
            $allocate_type = "Delivered";

            $transfer_by = $this->session->userdata('user_db_id');
            //var_dump($check_string);
            $asset_array = explode(',',$check_string);
            //var_dump($asset_array);

            $length = sizeof($asset_array);
            for($i = 0; $i<$length-1; $i++)
            {
               $transfer_asset_id =  $asset_array[$i];  
               $result = $this->asset_model->asset_details($transfer_asset_id);
               if($result <> false)
               {
                $asset_label_id = $result[0]->asset_readable_id;
                $transfer_by = $this->session->userdata('user_db_id');

                $transfer_office = $transfer_office;
                $remarks = "";

                $req = $this->asset_model->transfer_asset($transfer_asset_id, $asset_label_id, $category_id, $requisition_id,
                $transfer_by, $transfer_to, $transfer_office, $remarks);
                if ($req == false) 
                {
                    //echo"Failed to insert data";
                } else 
                {
                    /*********** Update Base Asset Table **************/
                    $req1 = $this->asset_model->transfer_asset_status($transfer_asset_id, $transfer_to, $transfer_office);
                    $req2 = $this->asset_model->update_requisition_asset($requisition_pkid, $requisition_id);
                    //var_dump($req2);
                }
                        
               }
            }

            /**** Audit Log ****/
            $action_name = "Asset Transfer";
            $transfer_json["asset_id"] = $transfer_asset_id;
            $transfer_json["asset_label_id"] = $asset_label_id;
            $transfer_json["asset_category_id"] = $category_id;
            $transfer_json["asset_requisition_id"] = $requisition_id;
            $transfer_json["transfer_by"] = $transfer_by;
            $transfer_json["transfer_to"] = $transfer_to;
            $transfer_json["transfer_office"] = $transfer_office;
            $transfer_json["remarks"] = $remarks;
            log_create($action_name,json_encode($transfer_json,true));

            /**** Audit log end here *****/

            echo $this->lang->line('TRANSFER_SUCCESS'); 
         }
    else
         {
         exit('No direct script access allowed');
         } 
           
}


}
