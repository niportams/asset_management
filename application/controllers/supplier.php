<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: supplier.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: add_supplier,supplier_list,supplier_delete,supplier_edit
//
/****************************************************/

 /**
 * AMS Asset supplier Controller Class
 *
 * This method demonstrates the supplier CRUD operation of AMS.
 */
class supplier extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('user_model', '', TRUE);
        $this->load->model('supplier_model', '', TRUE);
        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        //$this->lang->load('message','english');
    }

    public function index()
    {
        $this->set_page_title('Supplier');
        $this->set_page_sub_title('control panel');
    }

    /********Create new Supplier**************/
    public function add_supplier()
    {
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/setting_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ADD_NEW_SUPPLIER'));
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/supplier/supplier_create');
    }

    /**********Show All Supplier***************/
    public function supplier_list()
    {
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/setting_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('SUPPLIER_LIST'));
        $this->set_page_sub_title('');

        $result = $this->supplier_model->supplierlist();
        //var_dump($result);

        $this->set_value('supplier_list', $result);
        $this->load_view('admin_lte/supplier/supplier_list');
    }

    /***********Supplier Delete***************/

    public function supplier_delete()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true));
            $result = sci_delete_db('supplier', ['supplier_id' => $jsondata->supplier_id]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Supplier Delete";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/
                echo $this->lang->line('DELETE');
            }
        } else {
            exit('No direct script access allowed');
        }
    }

    /***********Supplier Update***************/
    public function supplier_edit()
    {
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true));
            $result = sci_update_db('supplier', $jsondata, ['supplier_id' => $jsondata->supplier_id]);
            if ($result <> false) {

                 /**** Audit Log ****/
                $action_name = "Supplier Edit";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/

                echo $this->lang->line('UPDATE');
            }
        } else {
            exit('No direct script access allowed');
        }
    }


}
