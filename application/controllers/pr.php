<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: pr.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: request,prlist
/****************************************************/

 /**
 * AMS pr Controller Class
 *
 * This method demonstrates the Procurement Request(pr) crud operation of AMS.
 */

class pr extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('pr_model', '', TRUE);  

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Report');
        $this->set_page_sub_title('control panel');
        //$this->set_value('test', 'sdfsdfsdfs');
        //$this->load_view('admin_lte/dashboard');
    }

    /******** Asset PR  Request Form Generation ************/
    public function request()
    {

        $this->set_js('dist/js/jsonmap.js');
        $this->set_page_title('PR Request');
        $this->set_page_sub_title('Request');
        $this->set_js("dist/js/pages/add_new_field_script.js");

        $this->load_view('admin_lte/pr/request');

    }

    /******** Asset PR requested list retrieve from database **************/
    public function prlist()
    {

        //$this->set_js('dist/js/jsonmap.js');
        $this->set_page_title('PR List');
        $this->set_page_sub_title('Pr');
        
        $result = $this->pr_model->prlist();

        if ($result <> false) {
            $this->set_value('pr_list', $result);
        } else {
            $this->set_value('pr_list', "");
        }
        $this->load_view('admin_lte/pr/pr_list');

    }

}
