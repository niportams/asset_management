<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: category.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: addcategory,categorylist,category_edit,category_delete
//
/****************************************************/

 /**
 * AMS Category Controller Class
 *
 */


class category extends SCI_Controller
{
    //put your code here

    function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', '', TRUE);
        $this->load->helper('url');
    }

    public function index()
    {
        $this->set_page_title('Asset Category');
        $this->set_page_sub_title('control panel');
    }

    /** 
    *This function generates a new category.
    */
    public function addcategory()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ADD_NEW_CATEGORY'));
        $this->set_page_sub_title('');

        $result = $this->category_model->categorylist();
        //var_dump($result);

        $this->set_value('category_list', $result);
        $this->load_view('admin_lte/category/category_create');
    }

    /** 
    * This function retrieves the category list from database.
    */
    public function categorylist()
    {
        $this->set_page_title('Category List');
        $this->set_page_sub_title('');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/asset_script.js');
        $this->set_js('dist/js/global_script.js');

        $result = $this->category_model->categorylist();
        //var_dump($result); 

        $result1 = $this->category_model->categoryParentlist();
        //var_dump($result1); 

        $result2 = $this->category_model->categoryChildist();
        //var_dump($result2);

        $this->set_value('category_list', $result);
        $this->set_value('parent_list', $result1);
        $this->set_value('child_list', $result2);

        //$this->load_view('admin_lte/category/category_list_recursive');
        $this->load_view('admin_lte/category/category_list');
    }

    /** 
    * This function demonstrates the category edit form to edit category from a category id.
    */
    public function category_edit()
    {
      if ($this->input->is_ajax_request()) {
            $jsondata = $this->input->post('jsondata', true);
            $jsondata = json_decode($jsondata, true);
            $updateid = $jsondata['update_id'];
            if($jsondata['parent_id']==0){
                $jsondata['parent_id']= null;
            }

            if($jsondata['lifetime']==0){
                    $jsondata['lifetime']= null;
                }

            unset($jsondata['update_id']);

            //$updateid= base64_decode(urldecode($jsondata['update_id'])); 
            $result = sci_update_db('category', $jsondata, ['category_id' => $updateid]);
            if ($result <> false) {

                /**** Audit Log for category update****/
                $action_name = "Category Update";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/

                echo '{"status": "success", "message": "' . $this->lang->line('CATEGORY_UPDATED') . '"}';
               // log_message('error', 'Some variable did not contain a value.');
            }
            //$this->load_log("category_edit");
        } else {
            exit('No direct script access allowed');
        }
    }

    /** 
    * This function deletes a category from a category id.
    */
    public function category_delete()
    {
        if ($this->input->is_ajax_request()) {
            $category_id = $this->input->post('category_id', true);
            $this->category_model->category_delete($category_id);

            //echo"Category Deleted!";

            /**** Audit Log for category update****/
            $action_name = "Category Delete";
            log_create($action_name,$category_id);
            /**** Audit log end here *****/

            echo $this->lang->line('DELETE');
        } else {
            exit('No direct script access allowed');
        }
    }


}
