<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: ajax2.php
// Created By:     Evana Yasmin 
// Change history:
//      Hasibul Huq : update_requisition_asset_status

// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: status_wise_asset,asset_edit_form,registration_way,grn_search,
// asset_allocate,asset_transfer,cat_wise_asset,cat_asset_count,addto_allocation_list,
// report_store_asset,asset_details,pr_request_submit,asset_search,allocation_asset_search,
// single_allocation,allocate,asset_status_update,asset_physical_search,physical_check_submit,
// update_requisition_asset_status,category_assetlist
// 
/****************************************************/

/**
 * AMS ajax2 controller for ajax response of different functionality.
 */
class ajax2 extends CI_Controller
{


 /**
 * This constructor loads the different global library and helper file.
 */
    function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('user_model', '', TRUE);
        //$this->lang->load('message','english');
    }

    /** 
    *This function retrieves the asset list based on asset states/status
    *from database and display the items into a HTML page.
    */
    
    public function status_wise_asset()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $status_id = $this->input->post('asset_status', true);

                $result = $this->asset_model->status_wise_asset_list($status_id);
                //var_dump($result);

                if ($result <> false) {
                    $data['asset_list'] = $result;
                } else {
                    $data['asset_list'] = "";
                }
                $this->load->view('admin_lte/asset/status_wise_asset_list', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    *This function demonnstrates the asset edit form.
    */
    public function asset_edit_form()
    {
        //$this->set_js('dist/js/jsonmap.js');
        if ($this->session->userdata('user_logged_in')) {

            $this->load->model('asset_model', '', TRUE);
            $this->load->model('category_model', '', TRUE);
            $this->load->model('manufacture_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));

                //base64_decode(urldecode($jsondata->asset_id))

                $result = sci_select_db('asset', ['asset_id' => base64_decode(urldecode($jsondata->asset_id))]);
                if ($result == true) {
                    $data['asset_details'] = $result;
                }
                $result1 = $this->category_model->categorylist();
                $result2 = $this->manufacture_model->manufacturelist();
                $result3 = $this->asset_model->assetstatuslist();


                $data['category_list'] = $result1;
                $data['manufacture_list'] = $result2;
                $data['status_list'] = $result3;

                $this->load->view('admin_lte/asset/asset_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }

    }

    /** 
    ** This function demonstrates the asset registration form to register assets.
    */
    public function registration_way()
    {
        if ($this->session->userdata('user_logged_in')) {
            $user_center = $this->session->userdata('user_center');
            if ($this->input->is_ajax_request()) {

                //$data['read_mode']=1;
                $registration_way = $this->input->post('registration_way', true);
                if ($registration_way == 1) /******* New Product Registration *********/ {
                    $data['registration_way'] = 1;
                    $this->load->model('asset_model', '', TRUE);
                    $result = $this->asset_model->available_grn_list();
                    //var_dump($result);

                    if($result<>false)
                    {
                    $data['grn_list'] = $result;
                    }
                    else { $data['grn_list'] = ""; }

                    $this->load->view('admin_lte/asset/asset_registration_form', $data);
                } else {
                    /******* Old product Registration *********/
                    $data['registration_way'] = 2;

                    $token = date("Y-m-d H:i:s");
                    $token = str_replace('-', '', $token);
                    $token = str_replace(':', '', $token);
                    $token = str_replace(' ', '', $token);

                    //$token=uniqid();
                    $data['grn_no'] = $token;
                    $this->load->model('category_model', '', TRUE);
                    $this->load->model('manufacture_model', '', TRUE);

                    $result = $this->category_model->categorylist();
                    $result1 = $this->manufacture_model->manufacturelist();
                    $data['category_list'] = $result;
                    $data['manufacture_list'] = $result1;
                    $data['office_id'] = $user_center;
                    $this->load->view('admin_lte/asset/asset_registration_form', $data);
                }


            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

   /** 
    ** This function finds the GRN# to register new asset in AMS.
    */
    public function grn_search()
    {

       if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $grn_no_search = $this->input->post('grn_no_search', true);

                $grn_result = $this->asset_model->grn_check_register($grn_no_search);
                //var_dump($result);

                if ($grn_result <> false) {
                    $data['read_mode'] = 2;
                    $this->load->model('asset_model', '', TRUE);
                    $this->load->model('category_model', '', TRUE);
                    $this->load->model('manufacture_model', '', TRUE);

                    $result = $this->category_model->categorylist();
                    $result1 = $this->manufacture_model->manufacturelist();
                    $data['category_list'] = $result;
                    $data['manufacture_list'] = $result1;
                    $data['asset_capital'] = $grn_result;
                } else {
                    $data['read_mode'] = 1;
                    $data['asset_capital'] = "";
                    $data['received_item'] = "";
                    $data['pending_item'] = "";
                    $data['registered_item'] = "";
                }
                $this->load->view('admin_lte/asset/new_asset_registration', $data);

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }

    }

    /** 
    ** This function demonstrates the allocation form of requested  items to allocate asset.
    */

    public function asset_allocate()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) 
            {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('category_model', '', TRUE);
                $this->load->model('pr_model', '', TRUE);
                $this->load->model('requisition_model', '', TRUE);
                $requested_id = base64_decode(urldecode($this->input->post('requested_id', true)));

                $resul11 = $this->pr_model->raised_pr($requested_id);
                if($resul11<>false)
                {
                  $data['pr_status'] = 1;  //already raised
                }else
                {
                 $data['pr_status'] = 0;   //Not raised yet
                }

                $result = $this->asset_model->approved_requisition_details($requested_id);
                $approval_list = $this->requisition_model->approver_List($requested_id);
                //var_dump($result);

                if ($result <> false) {
                    $data['requisition_details'] = $result;
                    $cat_result = $this->category_model->categorylist();
                    $data['category_list'] = $cat_result;
                    $data['approval_list']=$approval_list;
                    $req_result = $this->asset_model->request_list($requested_id);
                    $data['req_list'] = $req_result;

                    // var_dump($req_result);
                    //$deel = $this->asset_model->delete_temp_allocation();
                } else {
                    $data['requisition_details'] = "";
                }

                /*******Previous Allocated ITEM IF available*******/

                $rest3 = $this->asset_model->get_allocated_list($requested_id);
                //var_dump($rest3);

                if ($rest3 <> false) {
                    $data['allocated_list'] = $rest3;

                } else {
                    $data['allocated_list'] = "";

                }

                $this->load->view('admin_lte/asset/allocate_asset', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    ** This function demonstrates the Transfer form of requested  items to transfer asset.
    */
    public function asset_transfer()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('category_model', '', TRUE);
                $this->load->model('pr_model', '', TRUE);
                $this->load->model('requisition_model', '', TRUE);

                $requested_id = base64_decode(urldecode($this->input->post('requested_id', true)));

                $result = $this->asset_model->approved_requisition_transfer($requested_id);
                $approval_list = $this->requisition_model->approver_List($requested_id);
                //var_dump($result);

                $resul11 = $this->pr_model->raised_pr($requested_id);
                if($resul11<>false)
                {
                  $data['pr_status'] = 1;  //already raised
                }else
                {
                 $data['pr_status'] = 0;   //Not raised yet
                }


                if ($result <> false) {
                    $data['requisition_details'] = $result;
                    $cat_result = $this->category_model->categorylist();
                    $data['category_list'] = $cat_result;
                    $data['approval_list']=$approval_list;
                    $req_result = $this->asset_model->request_list($requested_id);
                    $data['req_list'] = $req_result;

                    // var_dump($req_result);
                    //$deel = $this->asset_model->delete_temp_allocation();
                } else {
                    $data['requisition_details'] = "";
                }

                /*******Previous Allocated ITEM IF available*******/
                /*
               $rest3 = $this->asset_model->get_allocated_list($requested_id);
               //var_dump($rest3);

                   if($rest3<>false)
                   {
                    $data['allocated_list']=$rest3;

                   }
                   else
                      {
                       $data['allocated_list']="";

                      }
                      */

                $this->load->view('admin_lte/asset/transfer_asset', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    ** This function retrieves the category wise asset list based on specific category.
    */

    /********Category wise Asset Retrieve*************/
    public function cat_wise_asset()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('category_model', '', TRUE);

                $cat_id = $this->input->post('cat_id', true);

                $result = $this->asset_model->cat_wise_asset($cat_id);
                //var_dump($result);

                if ($result <> false) {
                    $data['asset_list'] = $result;

                } else {
                    $data['asset_list'] = "";
                }
                $this->load->view('admin_lte/asset/cat_wise_asset', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }


    /** 
    ** This function checks/counts the availability of a category in a store.
    */

    /******************  Category Wise Asset Available **************************/
    public function cat_asset_count()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('category_model', '', TRUE);

                //echo" Got it";

                $cat_asset_id = $this->input->post('cat_asset_id', true);

                $cat_asset_array = explode('-', $cat_asset_id);

                $cat_id = $cat_asset_array[0];
                //$cat_item = preg_replace('/\s+/', '', $cat_asset_array[1]);
                $cat_item = strtolower($cat_asset_array[1]);


                $result = $this->asset_model->cat_wise_asset_count($cat_id, $cat_item);
                //var_dump($result);

                if ($result <> false) {
                    $data['asset_available'] = $result;

                } else {
                    $data['asset_available'] = 0;
                }
                $this->load->view('admin_lte/cat_asset_available', $data);

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

   /** 
    ** This function adds the items into allocation list for allocation process.
    */

   public function addto_allocation_list()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {

                $this->load->model('asset_model', '', TRUE);
                $this->load->model('category_model', '', TRUE);

                $cat_asset_id = $this->input->post('cat_asset_id', true);
                $total_qty = $this->input->post('total_qty', true);
                $request_id = $this->input->post('request_id', true);

                $cat_asset_array = explode('-', $cat_asset_id);

                $cat_id = $cat_asset_array[0];
                //$cat_item = preg_replace('/\s+/', '', $cat_asset_array[1]);
                $cat_item = strtolower($cat_asset_array[1]);

                $result = $this->asset_model->cat_wise_asset_count($cat_id, $cat_item);
                if ($result <> false) {
                    if ($result < $total_qty) {
                        //echo"Your expected quantity exceeds the limit.Please re-type quantity!";
                        echo "1";

                    } else {
                        /********** First check asset quantity matching *************/

                        $result6 = $this->asset_model->cat_wise_asset_qty_check($cat_id, $request_id, $total_qty);
                        if ($result6 == false) /********* Quantity exceed the limit  *********/ {

                            // var_dump($result6);
                            echo "<span class='text-danger'>Please input the quantity based on requisition!</span>";
                        } else {
                            /************* Add assets to the allocation list *****************/
                            $result1 = $this->asset_model->cat_wise_asset_list($cat_id, $cat_item, $total_qty);
                            //var_dump($result1);
                            if ($result1 <> false) {
                                //echo"sfsd";
                                foreach ($result1 as $record) {
                                    $asset_id = $record->asset_id;
                                    $asset_name = $record->asset_name;
                                    $category_id = $record->category_id;
                                    $label_id = $record->asset_readable_id;
                                    $qty = 1;
                                    $req = $this->asset_model->temp_allocate($asset_id, $asset_name, $category_id, $qty, $label_id, $request_id);
                                    if ($req == false) {
                                        echo "Failed to insert data";
                                    }
                                }
                            }
                        }
                        /************ Temp allocation end here*************/

                        /************Show the temporary allocation data to the list******************/
                        $result4 = $this->asset_model->temp_allocation_list();
                        //var_dump($result4);

                        if ($result4 <> false) {
                            $data['temp_asset'] = $result4;
                            $this->load->view('admin_lte/asset/temp_allocation_list', $data);
                        } else {

                            $data['temp_asset'] = "";
                            $this->load->view('admin_lte/asset/temp_allocation_list', $data);
                        }

                    }
                }
                //}

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

   /** 
    ** This function retrieves the asset list from database for reporting purpose.
    */

    /*******************Store Asset List for ********************/
    public function report_store_asset()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('report_model', '', TRUE);
                $filter = $this->input->post('filter', true);
                $office_id = $this->session->userdata('user_center');
                if (isset($filter['office_id'])) {
                    if ($filter['office_id'] != '') {
                        $office_id = $filter['office_id'];
                    }
                }

                $result = $this->report_model->filter_wise_store_asset_list($filter['asset_name'], $filter['category_id'], $office_id, $filter['to'], $filter['from'],$filter['funded_by'],$filter['lifetime']);


                if ($result <> false) {
                    $data['asset_list'] = $result;
                } else {
                    $data['asset_list'] = "";
                }
                $this->load->view('admin_lte/report/filter_wise_store_asset_list', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    **This function retrieves the detail information of an asset and display 
    * on a modal form.
    */
    public function asset_details()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $jsondata = $this->input->post('jsondata', true);

                $jsondata = json_decode($jsondata, true);
                $detailid = base64_decode(urldecode($jsondata['asset_id']));

                $res = $this->asset_model->asset_details($detailid);
                //var_dump($res);
                if ($res <> false) {
                    $data['asset_details'] = $res;
                } else {
                    $data['asset_details'] = "";
                }

                $this->load->view('admin_lte/asset/asset_details', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    **This function creates a procurement request for unavailable assets.
    */
   
    public function pr_request_submit()
    {
        if ($this->session->userdata('user_logged_in')) {

            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                $pr_requsition = [];
                $assets = [];
                $index = 0;
                for ($i = 1; $i < sizeof($jsondata); $i++) {
                    if ($jsondata[$i]["name"] != "asset_name" && $jsondata[$i]["name"] != "asset_quantity") {
                        $pr_requsition[$jsondata[$i]["name"]] = $jsondata[$i]["value"];
                    } else {
                        $assets[$index]["name"] = $jsondata[$i]["value"];
                        $i = $i + 1;
                        $assets[$index]["quantity"] = $jsondata[$i]["value"];
                        $index = $index + 1;
                    }

                }
                $pr_requsition["status"] = "REQUESTED";
                $pr_requsition["userid"] = $this->session->userdata('user_db_id');
                $pr_requsition["office_id"] = $this->session->userdata('user_center');
                $pr_requsition["approve_of"] = 1;
                // print_r($requsition);
                // print_r($assets);


                $result = sci_insert_db('pr_request', json_encode($pr_requsition));

                if ($result <> false) {
                    for ($x = 0; $x < sizeof($assets); $x++) {
                        $assets[$x]["request_id"] = $result;
                        $result_assets = sci_insert_db('pr_request_assets', json_encode($assets[$x]));
                    
                    }

                    $action_name = "Asset Requisition"; //** log file create**/
                    log_create($action_name,json_encode($pr_requsition));

                    //*********** set notification ********************
                    set_notification(load_message('PR_REQUEST'), "", base_url() . "pr/requestlist", $pr_requsition["approve_of"], $result, 'pr_request');
                    echo $this->lang->line('INSERT');
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    **This function retrieves the asset details based on searching given asset ID.
    */

    public function asset_search()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);

                $asset_search = $this->input->post('asset_search', true);
                $chosen_state_name = $this->input->post('chosen_state_name', true);

                $result = $this->asset_model->asset_search($asset_search);
                //var_dump($result);

                if ($result <> false) {
                    $asset_id = $result[0]->asset_id;
                    $data['asset_details'] = $result;
                    $result1 = $this->asset_model->assetstatuslist();
                    //var_dump($result1);
                    $data['status_list'] = $result1;
                    $data['status_desired_state'] = $chosen_state_name;

                    $result3 = $this->asset_model->disposed_filelist($asset_id);
                    //var_dump($result3);
                    if($result3<>false)
                    {
                        $data['file_list'] =$result3;
                    }
                    else
                    {
                        $data['file_list'] = "";
                    }

                    $this->load->view('admin_lte/asset/status_update_form', $data);
                } else {
                    //$data['status_list']="";
                    //echo "<font color='red'>".$this->lang->line('ASSET_ID')."</font>";
                    echo 1;
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    *This function retrieves the allocation details based on searching given asset ID.
    */
    
    public function allocation_asset_search()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model','', TRUE);
                $this->load->model('user_model','', TRUE);
                $this->load->model('office_model','', TRUE);

                $asset_search = $this->input->post('asset_search', true);
                
                $result = $this->asset_model->asset_search($asset_search);
                //var_dump($result);

                if ($result <> false) {
                    $asset_id = $result[0]->asset_id;
                    $data['asset_details'] = $result;
                                        
                    $reference_no = date("Y-m-d H:i:s");
                    $reference_no = str_replace('-', '', $reference_no);
                    $reference_no = str_replace(':', '', $reference_no);
                    $reference_no = str_replace(' ', '', $reference_no);
                    $reference_no = substr($reference_no, 6);

                    $data['reference_no'] = $reference_no;

                    $result3 = $this->user_model->userlist();
                    $data['user_list'] = $result3;
                    
                    $result1 = $this->user_model->approval_list();
                    //var_dump($result1);
                    $data['approver_list'] = $result1;
                    
                    $result2 = $this->office_model->office_room_list();
                    //var_dump($result1);
                    $data['room_list'] = $result2;

                    $this->load->view('admin_lte/allocation/allocation_form', $data);
                } else {
                    //$data['status_list']="";
                    //echo "<font color='red'>".$this->lang->line('ASSET_ID')."</font>";
                    echo 1;
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function allocates a single item from asset list.
    */

   public function single_allocation()
    {
      if ($this->session->userdata('user_logged_in')) 
        {
            if ($this->input->is_ajax_request()) 
            {
                $this->load->model('asset_model','', TRUE);
                $this->load->model('user_model','', TRUE);
                $this->load->model('office_model','', TRUE);

                $asset_encrypt = $this->input->post('asset_encrypt', true);
                $label_id = $this->input->post('label_id', true);
                $category_id = $this->input->post('category_id', true);
                                   
                $reference_no = date("Y-m-d H:i:s");
                $reference_no = str_replace('-', '', $reference_no);
                $reference_no = str_replace(':', '', $reference_no);
                $reference_no = str_replace(' ', '', $reference_no);
                $reference_no = substr($reference_no, 6);

               
                $data['asset_id'] = base64_decode(urldecode($asset_encrypt));
                $data['asset_label_id'] = $label_id;
                $data['category_id'] = $category_id;
                $data['reference_no'] = $reference_no;

                $result3 = $this->user_model->userlist();
                $data['user_list'] = $result3;
                
                $result1 = $this->user_model->approval_list();
                //var_dump($result1);
                $data['approver_list'] = $result1;
                
                $result2 = $this->office_model->office_room_list();
                //var_dump($result1);
                $data['room_list'] = $result2;

                $this->load->view('admin_lte/allocation/person_allocation_form', $data);
                
              }
            
            else 
                { exit('No direct script access allowed'); }
        }    
        else 
        {
            redirect('login');
        }
    }
    
    /** 
    * This function completes the allocation process of an asset.
    */
    
    public function allocate()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);

                $item_id = $this->input->post('item_id', true);
                $requisition_id = $this->input->post('requisition_id', true);
                $category_id = $this->input->post('category_id', true);
                $asset_qty = $this->input->post('asset_qty', true);
                $office_id = $this->input->post('office_id', true);
                $allocate_to = $this->input->post('allocate_to', true);
                $allocate_type = $this->input->post('allocate_type', true);

                $result2 = $this->asset_model->requested_category_available($category_id, $asset_qty);
                // var_dump($result2);
                if ($result2 <> false) 
                {
                    /************* Add assets to the allocation list *****************/
                    $result1 = $this->asset_model->cat_wise_asset_list($category_id, $asset_qty);
                    //var_dump($result1);
                    if ($result1 <> false) {
                        foreach ($result1 as $rest12) {
                            $allocate_asset_id = $rest12->asset_id;
                            $allocate_category_id = $rest12->category_id;
                            $asset_label_id = $rest12->asset_readable_id;

                            $allocated_by = $this->session->userdata('user_db_id');
                            $allocate_to = $allocate_to;
                            $allocate_office = $office_id;
                            $remarks = "";

                            $req = $this->asset_model->allocate_asset($allocate_asset_id, $asset_label_id, $allocate_category_id, $requisition_id,
                                $allocated_by, $allocate_to, $allocate_office, $allocate_to, $remarks);
                            if ($req == false) {
                                //echo"Failed to insert data";
                            } else {
                                /*********** Update Base Asset Table **************/
                                //echo"succesfully allocated!";
                                $req1 = $this->asset_model->update_asset_status($allocate_asset_id, $allocate_to);
                                $req2 = $this->asset_model->update_requisition_asset($item_id, $requisition_id);
                                //var_dump($req2);
                            }
                        }

                        $req_result = $this->asset_model->request_list($requisition_id);
                        $data['req_list'] = $req_result;

                        echo $this->lang->line('ALLOCATION_SUCCESS');
                        //$this->load->view('admin_lte/asset/requested_requisition_list',$data);

                    }
                    /************ Asset allocation end here*************/
                } else {
                    echo "Failed to allocate!";
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the asset status from asset ID.
    */

   public function asset_status_update()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);

                $jsondata = json_decode($this->input->post('jsondata', true));

                $update_id = base64_decode(urldecode($jsondata->asset_id));
                $status_id = $jsondata->status_id;
                $state_name = $jsondata->state_name;

                $result_details = sci_select_db('asset', ['asset_id' => $update_id]);

                //var_dump($result_details);

                if ($result_details<>false) 
                 {
                    $current_state = $result_details[0]->asset_status_id;
                    $current_office = $result_details[0]->office_id;
                    $assigned_user = $result_details[0]->assigned_to;
                 } 

                if($status_id == 6) /**** Status disposed*****/
                {
                
                  $res2 = $this->asset_model->disposed_file($update_id);  
                  //var_dump($res2);
                  $create_date = $res2[0]->create_date;

                 if($create_date <> NULL)
                 {
                    $result1 = $this->asset_model->history_add($update_id,$status_id,$current_state,$current_office,$assigned_user);
                    if($result1 <> false)
                         {
                            $this->asset_model->change_asset_status($update_id, $status_id);
                            /**** Audit Log ****/
                            $action_name = "Asset State Change";
                            log_create($action_name,json_encode($jsondata,true));
                            /**** Audit log end here *****/
                            echo $this->lang->line('UPDATE');
                         }
                      else 
                        {
                            echo $this->lang->line('FAILED_UPDATE');
                       }
                }
                else
                    {
                      //echo $this->lang->line('FAILED_DISPOSED'); 
                       $msg = 0;
                       //echo $msg; 
                    }

                }
                else /********** Maintenance or deadstate ********/
                {
               
                //var_dump($result);

                if($current_state <> $status_id)
                {
                       $result1 = $this->asset_model->history_add($update_id,$status_id,$current_state,$current_office,$assigned_user);
                       $result = $this->asset_model->change_asset_status($update_id, $status_id); 

                       if($result <> false)
                       {

                      /**** Audit Log ****/
                        $action_name = "Asset State Change";
                        log_create($action_name,json_encode($jsondata,true));
                        /**** Audit log end here *****/

                        echo $this->lang->line('UPDATE');
                       }
                       else
                       {
                        echo $this->lang->line('FAILED_UPDATE');
                       }

                }  
                else
                    {
                        /**** Audit Log ****/
                        $action_name = "Asset State Change";
                        log_create($action_name,json_encode($jsondata,true));
                        /**** Audit log end here *****/
                             
                       echo $this->lang->line('UPDATE'); 
                    }  
                    
                 
                 
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function demonstrates the asset physical form from searching Asset ID.
    */
   public function asset_physical_search()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $asset_search = $this->input->post('asset_search', true);

                $result = $this->asset_model->asset_search($asset_search);
                //var_dump($result);

                if ($result <> false) {
                    $data['asset_details'] = $result;
                    $result1 = $this->asset_model->assetstatuslist();
                    //var_dump($result1);
                    $data['checked_by'] = $this->session->userdata('user_db_id');

                    $this->load->view('admin_lte/physical/comment_form', $data);
                } else {
                    //$data['status_list']="";
                    echo 1;
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function completes the physiacal checking of an asset.
    */
    public function physical_check_submit()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('physical_model', '', TRUE);
                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);

                $jsondata = json_decode($jsondata, true);

                unset($jsondata['asset_search']);
                $jsondata = json_encode($jsondata, true);

                $result = sci_insert_db('physical_check', $jsondata);
                if ($result <> false) {

                    $action_name = "Asset Physical Check"; //** log file create**/
                    log_create($action_name,$jsondata);

                    echo '{"status": "success", "message": "' . $this->lang->line('PHYSICAL_CHECK_SUBMITTED') . '"}';

                } else {
                    echo '{"status": "error", "message": "' . $this->lang->line('PHYSICAL_CHECK_SUBMITTED_FAILED') . '"}';

                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function changes the requisition state of an asset.
    */
    public function update_requisition_asset_status()
    {
        $this->load->model('asset_model', '', TRUE);
        $asset_id = $this->input->post('asset_id', true);
        $status = $this->input->post('status', true);
        $result = sci_update_db('requisitions_assets', ['requisition_state' => $status], ['id' => $asset_id]);
        if ($status == 'Processed') {
            echo "This item is accepted";
        } else {
            echo "This item is declined";
        }

    }

    /** 
    * This function retrieves the available asset list from a category.
    */
    public function category_assetlist()
    {
      if ($this->session->userdata('user_logged_in')) 
      {
            if ($this->input->is_ajax_request()) 
            {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('requisition_model', '', TRUE);

                $requisition_id = $this->input->post('requisition_id', true);
                $category_id = $this->input->post('category_id', true);
                $quantity = $this->input->post('quantity', true);
                $requisition_pkid = $this->input->post('requisition_pkid', true);
                $index = $this->input->post('index', true);
                $process = $this->input->post('process', true);

                $result = $this->asset_model->catasset_list($category_id);
                //var_dump($result);

                if($process == 'allocate')
                {
                 $requisition_result = $this->asset_model->approved_requisition_details($requisition_id);
                }
                else //transfer
                {
                $requisition_result = $this->asset_model->approved_requisition_transfer($requisition_id);
                }
                                
                if ($requisition_result <> false) {
                    $data['requisition_details'] = $requisition_result;
                } else {
                    $data['requisition_details'] = "";
                }


                if ($result <> false) 
                {
                    $data['asset_list'] = $result;
                    $data['category_id'] = $category_id;
                    $data['quantity'] = $quantity;
                    $data['requisition_pkid'] = $requisition_pkid;
                    $data['index'] = $index;

                     if($process == 'allocate')
                     {
                       $this->load->view('admin_lte/allocation/category_assetlist', $data); 
                     }
                     else
                     {
                       $this->load->view('admin_lte/transfer/category_assetlist', $data);  
                     }
                    
                } else {
                    
                    echo 1;
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }  
    }
}
