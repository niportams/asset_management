<?php
/****************************************************/
// Filename: ajax.php
// Created By:     Evana Yasmin 16 oct 2017
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: generate_assetid,user_registration,userrole_submit,privilege_submit,
// privilege_details,privilege_update,profile_details,profile_update,role_update,category_submit,
// category_update,asset_capital,capital_update,office_submit,office_update,department_submit,department_update,
// supplier_create,supplier_update,manufacture_create,manufacture_update,asset_register,requisition_request_submit,
// requisition_list,requisition_list_only,update_requisition_status,update_transfer_status,get_category_by_parent_id,
// google_map,get_room_by_office_id,get_person_by_office_id,flow_submit,transfer_flow_submit,flow_update,transfer_flow_update,
// requested_pr_submit,entry_submit,registration_limit
//
/****************************************************/

/**
 * AMS Asset Allocation Controller Class
 *
 * This method demonstrates the allocation process of AMS.
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class ajax extends CI_Controller
{


    function __construct()
    {
        parent::__construct();


        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('user_model', '', TRUE);
       //$this->lang->load('message','english');
    }


    /** 
    *This function returns the system generated asset id. 
    */
    public function generate_assetid($office_id, $category_id, $asset_id, $max_cat_id)
    {

        //$this->load->model('office_model','',TRUE);
        $office_prefix = get_office_prefix($office_id);

        $category_prefix = get_category_prefix($category_id);

        /*
        if((strlen($office_id)<3) and (strlen($office_id)>0))
          {
              $length = 3-strlen($office_id);
              $catlength = 3-strlen($category_id);

              $nw_office_id = '0';
              $nw_cat_id = '0';

              for($i=0; $i<$length; $i++)
              {
                 $nw_office_id .= '0';
              }
              $nw_office_id = $nw_office_id.$office_id;

              for($j=0; $j<$catlength; $j++)
              {
                 $nw_cat_id .= '0';
              }
              $nw_cat_id = $nw_cat_id.$category_id;

              //$asset_id = '0'.$asset_id;
              $asset_id = $max_cat_id + 1;
              //$asset_id = '0'.$asset_id;
              $formated_id = $nw_office_id.$nw_cat_id.$asset_id;
          }
        */
       // $asset_id = $max_cat_id + 1;
        $formated_id = $office_prefix . $category_prefix . $max_cat_id;
        return $formated_id;
    }

    /** 
    *This function demostrates the user registration process of AMS. 
    */
    public function user_registration()
    {
        $this->load->model('user_model', '', TRUE);
        $this->load->model('role_model', '', TRUE);

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                //echo"Got it!";
                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);

                $json_output = json_decode($jsondata, true);

                //var_dump($json_output);

                $result = $this->user_model->get_username($json_output['user_name']);
                if ($result == true) /*Username already exist*/ {
                    echo $this->lang->line('USER_NAME_ALREADY_TAKEN');
                } else /********Username unique: register user************/ {
                    $a = array("employee_id" => empty($json_output['employee_id']) ? null : $json_output['employee_id'],
                        'user_name' => filter_var($json_output['user_name'], FILTER_SANITIZE_STRING),
                        'user_pass' => md5($json_output['user_pass']),
                        'user_email' => filter_var($json_output['user_email'], FILTER_SANITIZE_STRING),
                        'user_role' => filter_var($json_output['user_role'], FILTER_SANITIZE_STRING),
                        'user_nicename' => filter_var($json_output['user_nicename'], FILTER_SANITIZE_STRING),
                        'posting_center' => filter_var($json_output['posting_center'], FILTER_SANITIZE_STRING)
                    );

                    $json = json_encode($a, true);

                    //var_dump($json);
                    $current_id = sci_insert_db('login', $json);
                    if ($current_id <> false) {
                        $b = array('userid' => $current_id,
                            'fullname' => filter_var($json_output['fullname'], FILTER_SANITIZE_STRING),
                            'designation' => filter_var($json_output['designation'], FILTER_SANITIZE_STRING),
                            'department' => filter_var($json_output['department'], FILTER_SANITIZE_STRING),
                            'phone' => filter_var($json_output['phone'], FILTER_SANITIZE_STRING),
                            'mobile' => filter_var($json_output['mobile'], FILTER_SANITIZE_STRING),
                            'posting_district' => filter_var($json_output['posting_district'], FILTER_SANITIZE_STRING),
                            'posting_upazella' => filter_var($json_output['posting_upazella'], FILTER_SANITIZE_STRING),
                            'posting_village' => filter_var($json_output['posting_village'], FILTER_SANITIZE_STRING),
                            'posting_postcode' => filter_var($json_output['posting_postcode'], FILTER_SANITIZE_STRING),
                            'hr_id' => filter_var($json_output['hr_id'], FILTER_SANITIZE_STRING),
                            'national_id' => filter_var($json_output['national_id'], FILTER_SANITIZE_STRING),
                            'user_status' => 1,
                        );

                        $profile_json = json_encode($b, true);
                        $res = sci_insert_db('user', $profile_json);
                        if ($res <> false) {

                            $action_name = "User Create"; //** log file create**/
                            log_create($action_name,$profile_json);

                            echo $this->lang->line('INSERT');
                        }
                    }
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function inserts new user role process of AMS. 
    */
    public function userrole_submit()
    {
        $this->load->model('role_model', '', TRUE);
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {

                $role_name = $this->input->post('role_name', true);
                $role_rank = $this->input->post('role_rank', true);
                $check_string = $this->input->post('check_string', true);

                $len = strlen($check_string);
                $check_string[$len - 1] = '';
                $privilege = trim($check_string);
                //echo $check_string;
                $result = $this->role_model->role_create($role_name,$role_rank, $privilege);
                //echo"Successfully inserted";
                if($result <> false)
                 {
                   $action_name = "User Role Create"; //** log file create**/
                   $log_json["role_name"] = $role_name;
                   $log_json["role_rank"] = $role_rank;
                   $log_json["privilege"] = $privilege;

                   log_create($action_name,json_encode($log_json,true));
                   echo $this->lang->line('INSERT');
                 }   
               
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function inserts new privilege of AMS. 
    */
    public function privilege_submit()
    {

        if ($this->session->userdata('user_logged_in')) {

            if ($this->input->is_ajax_request()) {

                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);
                $result = sci_insert_db('privilege', $jsondata);
                if ($result <> false) {

                   $action_name = "Role Privilege Create"; //** log file create **/
                   log_create($action_name,$jsondata);

                echo '{"status": "success", "message": "' . $this->lang->line('PRIVILEGE_CREATED') . '"}';
                } else {
                    echo '{"status": "error", "message": "' . $this->lang->line('PRIVILEGE_CREATE_FAILED') . '"}';
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

/** 
* This function retrieves the privilege details from privilege_id. 
*/
    public function privilege_details()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('privilege_model', '', TRUE);
                $privilege_id = $this->input->post('privilege_id', true);
                //$jsondata =  json_decode($this->input->post('jsondata',true),true);

                //var_dump($jsondata);
                $result = $this->privilege_model->privilege_details($privilege_id);
                if ($result <> false) {
                    $data['privilege_name'] = $result;
                }
                $this->load->view('admin_lte/privilege/privilege_name', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the privilege details from privilege_id. 
    */
    public function privilege_update()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('privilege_model', '', TRUE);
                $privilege_id = $this->input->post('privilege_id', true);

                $result = $this->privilege_model->privilege_details($privilege_id);

                //$this->set_value('privilege_name',$result);
                $data['privilege_details'] = $result;
                $this->load->view('admin_lte/privilege/privilege_update', $data);

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function retrieves the profile details from userid. 
    */
    public function profile_details()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('user_model', '', TRUE);
                $db_userid = $this->input->post('userid', true);

                $result = $this->user_model->user_details($db_userid);

                //var_dump($result);
                $data['profile_details'] = $result;
                $this->load->view('admin_lte/user/user_details', $data);

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function retrieves the profile details from userid. 
    */
    public function profile_update()
    {

        //$this->set_js('dist/js/office.js');

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {

                $this->load->model('user_model', '', TRUE);
                $this->load->model('role_model', '', TRUE);
                $this->load->model('office_model', '', TRUE);
                $this->load->model('department_model', '', TRUE);
                $dept_list = $this->department_model->department_list();

                $db_userid = $this->input->post('userid', true);
                $result = $this->user_model->user_details($db_userid);

                //var_dump($result);
                $result1 = $this->role_model->rolelists();
                $result2 = $this->office_model->officelist();

                $data['profile_details'] = $result;
                $data['role_list'] = $result1;
                $data['office_list'] = $result2;
                $data['dept_list']=$dept_list;

                $this->load->view('admin_lte/user/user_update', $data);

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the role details. 
    */
    public function role_update()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('role_model', '', TRUE);
                $role_id = $this->input->post('role_id', true);

                $result = $this->role_model->role_details($role_id);
                //var_dump($result);
                $data['role_details'] = $result;
                $rolelist = $this->role_model->rolelist();
                $data['role_list'] = $rolelist;
                /*********All Privilege List***********/
                $result1 = $this->role_model->privilegelist();
                $data['privilege_list'] = $result1;
                $this->load->view('admin_lte/role/role_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function completes the category creation process. 
    */
    public function category_submit()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                $jsondata = json_decode($jsondata, true);
                if($jsondata['parent_id']==0){
                    $jsondata['parent_id']= null;
                }
                if($jsondata['lifetime']==0){
                    $jsondata['lifetime']= null;
                }
                $jsondata = json_encode($jsondata, true);
                $result = sci_insert_db('category', $jsondata);
                if ($result <> false) {

                   $action_name = "Category Create"; //** log file create**/
                   log_create($action_name,$jsondata);

                    echo '{"status": "success", "message": "' . $this->lang->line('CATEGORY_CREATED') . '"}';

                } else {
                    echo '{"status": "error", "message": "' . $this->lang->line('CATEGORY_CREATE_FAILED') . '"}';

                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the category details. 
    */
    public function category_update()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('category_model', '', TRUE);
                $category_id = $this->input->post('category_id', true);
                $result = $this->category_model->category_details($category_id);
                //var_dump($result);
                $data['category_details'] = $result;

                /*********All Privilege List***********/
                $result1 = $this->category_model->categorylist();
                $data['category_list'] = $result1;

                $this->load->view('admin_lte/category/category_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function completes the asset capitalization of AMS.
    */
    public function asset_capital()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->model('asset_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                // var_dump($jsondata);

                $decoded_json = json_decode($jsondata, true);
                //echo $decoded_json['grn_no'];
                //$decoded_json['registered_item']=0;
                //$grn_no = '1aad';

                $res = $this->asset_model->grn_check($decoded_json['grn_no']);
                if ($res == 0) {

                    $result = sci_insert_db('capitalize', $jsondata);
                    if ($result <> false) {

                        $action_name = "Asset Capitalization"; //** log file create**/
                        log_create($action_name,$jsondata);

                        echo $this->lang->line('INSERT');
                    } else {
                        echo $this->lang->line('FAILED_INSERT');
                    }

                } else {
                    echo $this->lang->line('GRN_DUPLICATE');
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the asset capitalization of AMS.
    */
    public function capital_update()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->model('category_model', '', TRUE);
            $this->load->model('office_model', '', TRUE);
            $this->load->model('asset_model', '', TRUE);
            $this->load->model('supplier_model', '', TRUE);
            $this->load->model('user_model','',TRUE);
            $this->load->model('organization_model', '', TRUE);
            $this->load->model('department_model', '', TRUE);

            if ($this->input->is_ajax_request()) {

                $user_center = $this->session->userdata('user_center');

                $capital_id = $this->input->post('capital_id', true);
                $decrypt_capital = base64_decode(urldecode($capital_id));

                $result_details = $this->asset_model->capital_details($decrypt_capital);
                //var_dump($result);
                $data['capital_details'] = $result_details;

                $result = $this->category_model->categorylist();
                $result1 = $this->office_model->officelist();
                $result3 = $this->supplier_model->supplierlist();
                $result4 = $this->user_model->userlist();
                $org_list = $this->organization_model->organization_list();
                $dept_list = $this->department_model->department_list();

                $data['category_list'] = $result;
                $data['office_list'] = $result1;
                $data['supplier_list'] = $result3;
                $data['user_center'] = $user_center;
                $data['user_list'] = $result4;
                $data['org_list'] = $org_list;
                $data['dept_list'] = $dept_list;

                $this->load->view('admin_lte/asset/capital_update', $data);
            } else {
                exit('No direct script access allowed');
            }

        } else {
            redirect('login');
        }
    }

    /** 
    * This function completes the office creation of AMS.
    */
    public function office_submit()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->model('office_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);
                 $jsondata = json_decode($jsondata, true);
                
                $latitude = $jsondata['latitude'];
                $longitude = $jsondata['longitude'];

                $geo_loc = $latitude.','.$longitude;
                $jsondata["geo_loc"] = $geo_loc;
                
                $jsondata = json_encode($jsondata, true);

                $result = sci_insert_db('office', $jsondata);
                if ($result <> false) {

                    $action_name = "Office Create"; //** log file create**/
                    log_create($action_name,$jsondata);

                    echo '{"status": "success", "message": "' . $this->lang->line('OFFICE_CREATED') . '"}';

                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the office details of AMS.
    */
    public function office_update()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->model('office_model', '', TRUE);
            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('office', ['office_id' => $jsondata->office_id]);
                //var_dump($result);

                if ($result == true) {
                    $data['office_details'] = $result;
                } else {
                    $data['office_details'] = "";
                }

                $this->load->view('admin_lte/office/office_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function creates a new department.
    */
    public function department_submit()
    {
        if ($this->session->userdata('user_logged_in')) {

            $this->load->model('department_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $deptdata = $this->input->post('jsondata', true);
                //$obj = json_decode($deptdata);

                $result = sci_insert_db('department', $deptdata);
                if ($result <> false) {

                    /***** Audit Log*****/
                    $action_name = "Department Create";
                    log_create($action_name,$deptdata);
                    /***** Audit Log*****/
                    echo '{"status": "success", "message": "' . $this->lang->line('DEPARTMENT_CREATED') . '"}';
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function retrieves the detail information of a department for editing purpose.
    */
    public function department_update()
    {

        if ($this->session->userdata('user_logged_in')) {

            $this->load->model('department_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('department', ['department_id' => $jsondata->department_id]);

                $data['department_details'] = $result;
                $this->load->view('admin_lte/department/department_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function creates the new supplier.
    */
    public function supplier_create()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->model('supplier_model', '', TRUE);
            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);

                $result = sci_insert_db('supplier', $jsondata);
                if ($result <> false) {

                /***** Audit Log *****/
                $action_name = "Supplier Create";
                log_create($action_name,$jsondata);
                /***** Audit Log *****/

                echo $this->lang->line('INSERT');
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the supplier information.
    */
    public function supplier_update()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->model('supplier_model', '', TRUE);
            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('supplier', ['supplier_id' => $jsondata->supplier_id]);

                if ($result == true) {
                    $data['supplier_details'] = $result;
                }
                $this->load->view('admin_lte/supplier/supplier_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function creates the new manufacturer.
    */
    public function manufacture_create()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->model('manufacture_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                $result = sci_insert_db('manufacture', $jsondata);
                if ($result <> false) {

                /***** Audit Log *****/
                $action_name = "Manufacturer Create";
                log_create($action_name,$jsondata);
                /***** Audit Log *****/
                echo $this->lang->line('INSERT');
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the manufacturer information.
    */
    public function manufacture_update()
    {
        if ($this->session->userdata('user_logged_in')) {

            $this->load->model('manufacture_model', '', TRUE);
            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('manufacture', ['manufacture_id' => $jsondata->manufacture_id]);
                if ($result == true) {
                    $data['manufacture_details'] = $result;
                }
                $this->load->view('admin_lte/manufacture/manufacture_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function register the new asset of AMS.
    */
    public function asset_register()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->model('asset_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                //$office_id = $this->session->userdata('posting_center');
                // $user_center = $this->session->userdata('user_center');
                $jsondata = $this->input->post('jsondata', true);

                $jsondata = json_decode($jsondata, true);
                //var_dump($jsondata);

                $registration_type = $jsondata['registration_type'];
                $purchase_date = null;
                $warrenty_date = null;
                $asset_lifetime = null;

                if ($jsondata['purchase_date'] != null) {
                    $purchase_date = DateTime::createFromFormat('d/m/Y', $jsondata['purchase_date']);
                    $purchase_date = $purchase_date->format('Y-m-d');
                }
                if ($jsondata['warrenty_date'] != null) {
                    $warrenty_date = DateTime::createFromFormat('d/m/Y', $jsondata['warrenty_date']);
                    $warrenty_date = $warrenty_date->format('Y-m-d');
                }
                if ($jsondata['asset_lifetime'] != null) {
                    $asset_lifetime = DateTime::createFromFormat('d/m/Y', $jsondata['asset_lifetime']);
                    $asset_lifetime = $asset_lifetime->format('Y-m-d');
                }
                /*************** SCI ID Uniqueness Checking *******************/

                $sci_id = $jsondata['sci_id'];

                if ($sci_id <> "") {
                    $rest = $this->asset_model->sciid_check($sci_id);

                    if ($rest == false) {
                        //echo"SCI is unique!";
                        $sci_flag = 1;
                        /****** SCI # is Not unique *********/
                    } else {
                        $sci_flag = 2;
                        /****** SCI Not unique *********/
                    }

                    //var_dump($rest);
                } else {
                    $sci_flag = 0; /* ** SCI ic empty ** */
                }

                if (($sci_flag == 1) or ($sci_flag == 0)) {
                    $jsonarray = array("category_id" => $jsondata['category_id'],
                        'capital_id' => $jsondata['capital_id'],
                        'asset_name' => filter_var($jsondata['asset_name'], FILTER_SANITIZE_STRING),
                        'asset_description' => filter_var($jsondata['asset_description'], FILTER_SANITIZE_STRING),
                        'manufacture_id' => empty($jsondata['manufacture_id']) ? 0 : $jsondata['manufacture_id'],
                        'model_name' => filter_var($jsondata['model_name'], FILTER_SANITIZE_STRING),
                        'serial_no' => filter_var($jsondata['serial_no'], FILTER_SANITIZE_STRING),
                        'model_no' => filter_var($jsondata['model_no'], FILTER_SANITIZE_STRING),
                        'reference_no' => filter_var($jsondata['reference_no'], FILTER_SANITIZE_STRING),
                        'label_id' => "",
                        'sci_id' => $jsondata['sci_id'],
                        'purchase_date' => filter_var($purchase_date, FILTER_SANITIZE_STRING),
                        'purchase_no' => filter_var($jsondata['purchase_no'], FILTER_SANITIZE_STRING),
                        'grn_no' => filter_var($jsondata['grn_no'], FILTER_SANITIZE_STRING),
                        'sku' => filter_var($jsondata['sku'], FILTER_SANITIZE_STRING),
                        'purchase_price' => empty(filter_var($jsondata['purchase_price'], FILTER_SANITIZE_STRING)) ? 0 : filter_var($jsondata['purchase_price'], FILTER_SANITIZE_STRING),
                        'purchase_location' => filter_var($jsondata['purchase_location'], FILTER_SANITIZE_STRING),
                        'warrenty_date' => empty($warrenty_date) ? NULL : $warrenty_date,
                        'asset_lifetime' => empty($asset_lifetime)? NULL : $asset_lifetime,
                        'assigned_to' => 0,
                        'registration_type' => $jsondata['registration_type'],
                        'asset_remarks' => filter_var($jsondata['asset_remarks'], FILTER_SANITIZE_STRING),
                        'asset_status_id' => 1,
                        'office_id' => $jsondata['office_id'],
                        'asset_readable_id' => '',
                        'pr_reference_no' => $jsondata['pr_reference_no'],
                    );

                    if ($registration_type == 'bulk') {
                        //echo "bulk registration";
                        $length = sizeof($jsondata);

                        $asset_json = json_encode($jsonarray, true);
                        $received_item = $jsondata['received_item'];
                        $item_registered = $jsondata['registered_item_no'];


                        $item_pending = $jsondata['pending_item_no'];

                        $res123 = $this->asset_model->capital_register_status($jsondata['capital_id']);
                       
                        $db_pending_item = $res123[0]->pending_item;
                        $db_registered_item = $res123[0]->registered_item_no;
                        $required_item = $received_item - $db_registered_item;
                        
                        if ($required_item > 0) {
                            for ($i = 0; $i < $db_pending_item; $i++) {
                                $res = sci_insert_db('asset', $asset_json);
                                if ($res <> false) {
                                    $db_registered_item = $db_registered_item + 1;
                                    $update_status = $this->asset_model->register_capital_update($jsondata['capital_id'], $db_registered_item);


                                    $max_category_id = $this->asset_model->get_max_office_category($jsondata['office_id'], $jsondata['category_id']);
                                    //var_dump($max_category_id);
                                    $max_id = $max_category_id[0]->max + 1;

                                    $auto_asset_id = $this->generate_assetid($jsondata['office_id'], $jsondata['category_id'], $res, $max_category_id[0]->max);

                                    $result = $this->asset_model->registered_asset_update($res, $auto_asset_id, $max_category_id[0]->max);
                                }
                            }
                            if ($res <> false) {

                                /***** Audit Log*****/
                                $action_name = "Asset Registration";
                                log_create($action_name,$asset_json);
                                /***** Audit Log*****/

                                echo '{"status": "success", "message": "' . $this->lang->line('ASSET_REGISTRATION') . '"}';
                            }
                        } else {
                            echo '{"status": "error", "message": "' . $this->lang->line('ASSET_ALREADY_REGISTRATION') . '"}';
                        }
                    } else {
                        //echo"single registration";
                        //print_r($a);
                        $asset_json = json_encode($jsonarray, true);
                        $res = sci_insert_db('asset', $asset_json);
                        if ($res <> false) {

                            if($jsondata['capital_id'] <> 99999) /*** Old Asset Registration ****/
                            {
                                $res123 = $this->asset_model->capital_register_status($jsondata['capital_id']);
                                //var_dump($res123);

                                $db_pending_item = $res123[0]->pending_item;
                                $db_registered_item = $res123[0]->registered_item_no;

                                //$required_item = $received_item - $db_registered_item;

                                //$item_registered = $jsondata['registered_item_no'] + 1;
                                $item_registered = $db_registered_item + 1;

                                $update_status = $this->asset_model->register_capital_update($jsondata['capital_id'], $item_registered);
                                }

                            $max_category_id = $this->asset_model->get_max_office_category($jsondata['office_id'], $jsondata['category_id']);
                            $max_id = $max_category_id[0]->max + 1;

                            $auto_asset_id = $this->generate_assetid($jsondata['office_id'], $jsondata['category_id'], $res, $max_category_id[0]->max);
                            // echo $auto_asset_id;

                            $result = $this->asset_model->registered_asset_update($res, $auto_asset_id, $max_id);

                            //echo '{"status": "success", "message": "' . $this->lang->line('ASSET_REGISTRATION') . '"}';
                            
                            /***** Audit Log*****/
                            $action_name = "Asset Registration";
                            log_create($action_name,$asset_json);
                            /***** Audit Log*****/

                            echo '{"status": "success", "message": "' . $this->lang->line('ASSET_REGISTRATION') . '"}';

                        }

                    }

                /********* Notification Set for procurement request***********/
                if($jsondata['pr_reference_no']<>"")
                {
                  //set_notification(load_message('PR_REQUEST_REGISTER'), "", base_url() . "allocation/requestlist", $this->session->userdata('user_db_id'), $jsondata['pr_reference_no'], 'PR Available');
                  $result33 = $this->asset_model->pr_asset_check($jsondata['pr_reference_no'],$jsondata['category_id']);
                  if($result33 <> false)
                    {
                      $count = $result33[0]->cnt; 
                      $result34 = $this->asset_model->request_asset_check($jsondata['pr_reference_no'],$jsondata['category_id']);
                      if($result34 <> false)
                      { 
                        $req_count = $result34[0]->quantity;
                        if($req_count<=$count) 
                        {
                           set_notification(load_message('PR_REQUEST_REGISTER'), "", base_url() . "allocation/requestlist", $this->session->userdata('user_db_id'), $jsondata['pr_reference_no'], 'PR Available'); 
                           $result35 = $this->asset_model->request_asset_update($jsondata['pr_reference_no'],$jsondata['category_id']);
                        }
                      }  
                    }  
                }
                   
                } else {
                    /********** SCI is not unique  ************/
                    echo '{"status": "error", "message": "' . $this->lang->line('SCI_DUPLICATE') . '"}';
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }


    /** 
    * This function completes the requisition request from the system.
    */
    public function requisition_request_submit()
    {
        if ($this->session->userdata('user_logged_in')) {

            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);
                $requsition = [];
                $assets = [];
                $index = 0;
                for ($i = 0; $i < sizeof($jsondata); $i++) {
                    if ($jsondata[$i]["name"] != "asset_name" && $jsondata[$i]["name"] != "asset_quantity" && $jsondata[$i]["name"] != "category_id") {
                        if ($jsondata[$i]["value"] == '') {
                            $jsondata[$i]["value"] = null;
                        }
                        $requsition[$jsondata[$i]["name"]] = $jsondata[$i]["value"];
                    } else {
                        $assets[$index]["name"] = $jsondata[$i]["value"];
                        $i = $i + 1;
                        if ($jsondata[$i]["value"] == '') {
                            $jsondata[$i]["value"] = null;
                        }
                        $assets[$index]["category_id"] = $jsondata[$i]["value"];
                        $i = $i + 1;
                        $assets[$index]["quantity"] = $jsondata[$i]["value"];
                        $assets[$index]["requisition_state"] = "REQUESTED";
                        $index = $index + 1;
                    }

                }
                if ($requsition["type"] == "INTERNAL") {
                    //$department_id = $this->user_model->user_department($this->session->userdata('user_db_id'));
                    $department_id = $this->user_model->user_department($requsition['on_behalf']);

                    $requsition["department_id"]= $department_id[0]->department;
                    $requsition["step"]= 1;
                    $administritive_id = $this->user_model->requisition_flow_next($this->session->userdata('user_center'),$requsition["department_id"],1);
                    if ($administritive_id) {
                        if($this->session->userdata('user_center')==1){
                            $requsition["approve_of"] = $administritive_id[0]->next_flow_id;
                        }else{
                            $requsition["approve_of"] = $administritive_id[0]->id;
                        }
                    }
                } else if ($requsition["type"] == "RTC") {
                    $department_id = $this->user_model->user_department($this->session->userdata('user_db_id'));

                    $requsition["department_id"]= $department_id[0]->department;
                    $requsition["step"]= 1;
                    $administritive_id = $this->user_model->transfer_flow_next($requsition["step"]);
                    $requsition["approve_of"] = $administritive_id[0]->user_id;
                }
                $requsition["status"] = "REQUESTED";
                $requsition["userid"] = $requsition['on_behalf'];
                $requsition['on_behalf'] = $this->session->userdata('user_db_id');
                $requsition["office_id"] = $this->session->userdata('user_center');
                // print_r($requsition);
                // print_r($assets);

                if($administritive_id) {
                    $result = sci_insert_db('requisitions', json_encode($requsition));

                    if ($result <> false) {
                        for ($x = 0; $x < sizeof($assets); $x++) {
                            $assets[$x]["requisition_id"] = $result;
                            $result_assets = sci_insert_db('requisitions_assets', json_encode($assets[$x]));
                        }

                        $action_name = "Asset Requisition"; //** log file create**/
                        log_create($action_name, json_encode($requsition));

                        //*********** set notification ********************
                        set_notification(load_message('REQUISITION_REQUEST'), "", base_url() . "asset/requisitionlist", $requsition["approve_of"], $result, 'requisition');
                        echo '{"status": "success", "message": "' . $this->lang->line('REQUISITION_APPLICATION_SUBMIT') . '"}';


                    }
                }else{
                    echo '{"status": "error", "message": "Please Contact With Admin to Set Requisition Flow"}';
                }
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function retrieves the requested requisitions for approval.
    */
    public function requisition_list()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('user_model', '', TRUE);
                $this->load->model('requisition_model','',TRUE);
                $requisition_id = $this->input->post('requisition_id', true);
                $result = $this->asset_model->requisition_details($requisition_id);
                $approval_list = $this->requisition_model->approver_List($requisition_id);
                $office_id_temp = $this->session->userdata('user_center');
                if ($result) {
                    $office_id_temp = $result[0]->office_id;
                }
                $employee_user_id = $this->user_model->requisition_flow_next($office_id_temp, $result[0]->department_id,$result[0]->step);
                $transfer_flow_next = $this->user_model->transfer_flow_next($result[0]->step);

                //var_dump($result);
                $data['requisition_details'] = $result;
                $data['user_details'] = $employee_user_id;
                $data['transfer_flow_next'] = $transfer_flow_next;
                $data['office_id_temp'] = $office_id_temp;
                $data['approval_list']=$approval_list;
                $this->load->view('admin_lte/asset/requisition_details', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function retrieves the user specific requisitions for view.
    */

    public function requisition_list_only()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('requisition_model','',TRUE);
                $requisition_id = $this->input->post('requisition_id', true);
                $result = $this->asset_model->requisition_details($requisition_id);
                $approval_list = $this->requisition_model->approver_List($requisition_id);
                $data['requisition_details'] = $result;
                $data['approval_list'] = $approval_list;
                $this->load->view('admin_lte/asset/show_requisition_details', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the allocation requisition process by respected approver.
    */

    public function update_requisition_status()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('user_model', '', TRUE);
                $requisition_id = $this->input->post('requisition_id', true);
                $status = $this->input->post('status', true);
                $remarks = $this->input->post('remarks', true);
                $result = $this->asset_model->requisition_details($requisition_id);
                if ($result) {
                    $current_approver_id = $this->user_model->requisition_flow_next($result[0]->office_id,$result[0]->department_id,$result[0]->step);
                    $data['requisition_id'] = $requisition_id;
                    $data['approved_by'] = $this->session->userdata('user_db_id');
                    $data['create_date '] = date("Y-m-d H:i:s");
                    if($result[0]->office_id==1 && $status!= "declined"){
                        $data['authorization'] = $current_approver_id[0]->access_type;
                    }elseif ($status== "declined"){
                        $data['authorization'] = "DECLINED";
                    }else{
                        $data['authorization'] = "PROCEED";
                    }

                    $data['remarks'] = $remarks;
                    $next_approver_id = $this->user_model->requisition_flow_next($result[0]->office_id,$result[0]->department_id,$result[0]->step+1);
                    $data = json_encode($data, true);
                    if ($result[0]->office_id==1 && $result[0]->approve_of == $this->session->userdata('user_db_id')&& $status!= "declined" ) {

                        $change_data['status'] = $current_approver_id[0]->access_type;
                        $change_data['approve_of'] = $next_approver_id[0]->next_flow_id;
                        $change_data['step'] = $next_approver_id[0]->step;
                        sci_update_db('requisitions', $change_data, ['id' => $requisition_id]);
                        sci_insert_db('requisitions_approvals', $data);

                        $action_name = "Requisition Approval"; //** log file create**/
                        log_create($action_name,$data);

                        //*********** find notification id *****************
                        $n_id = sci_select_db('notification_map', ['r_id' => $requisition_id, 'refference' => 'requisition'], 'n_id');

                        //*********** notification status change **********
                        update_status($n_id[0]->n_id);
                        //*********** set notification ********************
                        if($current_approver_id[0]->access_type=="PROCEED"){
                            set_notification(load_message('REQUISITION_PROCEED_USER'), $remarks, '', $result[0]->userid, $requisition_id, 'requisition');
                            set_notification(load_message('REQUISITION_REQUEST'), "", base_url() . "asset/allocation_request", $next_approver_id[0]->next_flow_id, $requisition_id, 'requisition');

                        }else{
                            set_notification('Your Requisition has been '.$current_approver_id[0]->access_type, $remarks, base_url() .'asset/requisitions', $result[0]->userid, $requisition_id, 'requisition');
                            set_notification(load_message('REQUISITION_REQUEST'), "", base_url() . "asset/requisitionlist", $next_approver_id[0]->next_flow_id, $requisition_id, 'requisition');

                        }
                        if($result[0]->step == 1){
                            sci_update_db('requisitions_assets', ['requisition_state' => $current_approver_id[0]->access_type], ['requisition_id' => $requisition_id,'requisition_state' => "REQUESTED"]);
                        }else{
                            $previous_approver_id = $this->user_model->requisition_flow_next($result[0]->office_id,$result[0]->department_id,$result[0]->step-1);
                            sci_update_db('requisitions_assets', ['requisition_state' => $current_approver_id[0]->access_type], ['requisition_id' => $requisition_id,'requisition_state' => $previous_approver_id[0]->access_type]);
                        }
                    echo "Requisition has been Processed";
                    }elseif ($result[0]->office_id!=1 &&$result[0]->approve_of == $this->session->userdata('user_db_id')&& $status!= "declined"){
                        $change_data['status'] = "PROCEED";
                        $change_data['approve_of'] = $next_approver_id[0]->id;
                        $change_data['step'] = $result[0]->step+1;
                        sci_update_db('requisitions', $change_data, ['id' => $requisition_id]);
                        sci_insert_db('requisitions_approvals', $data);

                        $action_name = "Requisition Approvals"; //** log file create**/
                        log_create($action_name,$data);

                        sci_update_db('requisitions_assets', ['requisition_state' => "PROCEED"], ['requisition_id' => $requisition_id,'requisition_state' => "REQUESTED"]);

                        //*********** find notification id *****************
                        $n_id = sci_select_db('notification_map', ['r_id' => $requisition_id, 'refference' => 'requisition'], 'n_id');

                        //*********** notification status change **********
                        update_status($n_id[0]->n_id);
                        //*********** set notification ********************
                        set_notification(load_message('REQUISITION_PROCEED_USER'), $remarks, base_url() .'asset/requisitions', $result[0]->userid, $requisition_id, 'requisition');
                        set_notification(load_message('REQUISITION_REQUEST'), "", base_url() . "asset/allocation_request", $next_approver_id[0]->id, $requisition_id, 'requisition');
                        echo "Requisition has been Processed";
                    }else{
                        $change_data['status'] = $status;
                        $change_data['approve_of'] = 0;
                        sci_update_db('requisitions', $change_data, ['id' => $requisition_id]);
                        sci_insert_db('requisitions_approvals', $data);

                        //*********** find notification id *****************
                        $n_id = sci_select_db('notification_map', ['r_id' => $requisition_id, 'refference' => 'requisition'], 'n_id');

                        //*********** notification status change **********
                        update_status($n_id[0]->n_id);
                        //*********** set notification ********************

                        set_notification('Your Requisition has been declined', $remarks, base_url() .'asset/requisitions', $result[0]->userid, $requisition_id, 'requisition');
                        sci_update_db('requisitions_assets', ['requisition_state' => "DECLINED"], ['requisition_id' => $requisition_id]);
                        echo "Requisition has been Declined";
                    }
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the transfer requisition process by respected approver.
    */
    public function update_transfer_status()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('asset_model', '', TRUE);
                $this->load->model('user_model', '', TRUE);
                $requisition_id = $this->input->post('requisition_id', true);
                $status = $this->input->post('status', true);
                $remarks = $this->input->post('remarks', true);
                $result = $this->asset_model->requisition_details($requisition_id);
                if ($result) {
                    $transfer_flow_next = $this->user_model->transfer_flow_next($result[0]->step+1);
                    $current_flow_next = $this->user_model->transfer_flow_next($result[0]->step);
                    $data['requisition_id'] = $requisition_id;
                    $data['approved_by'] = $this->session->userdata('user_db_id');
                    $data['create_date '] = date("Y-m-d H:i:s");
                    $data['authorization'] = $current_flow_next[0]->access_type;
                    $next_approver_id = $transfer_flow_next[0]->user_id;
                    $data['remarks'] = $remarks;
                    $data = json_encode($data, true);

                    if ($status == 'proceed') {
                        $change_data['status'] = $current_flow_next[0]->access_type;
                        $change_data['approve_of'] = $transfer_flow_next[0]->user_id;
                        $change_data['step'] = $transfer_flow_next[0]->step;
                        sci_update_db('requisitions', $change_data, ['id' => $requisition_id]);
                        sci_insert_db('requisitions_approvals', $data);
                        
                        $action_name = "Requisition Transfer Approval"; //** log file create**/
                        log_create($action_name,$data);

                        if($result[0]->step == 1){
                            sci_update_db('requisitions_assets', ['requisition_state' => $current_flow_next[0]->access_type], ['requisition_id' => $requisition_id,'requisition_state' => "REQUESTED"]);
                        }else{
                            $previous_approver_id = $this->user_model->transfer_flow_next($result[0]->step-1);
                            sci_update_db('requisitions_assets', ['requisition_state' => $current_flow_next[0]->access_type], ['requisition_id' => $requisition_id,'requisition_state' => $previous_approver_id[0]->access_type]);
                        }

                        //*********** find notification id *****************
                        $n_id = sci_select_db('notification_map', ['r_id' => $requisition_id, 'refference' => 'requisition'], 'n_id');

                        //*********** notification status change **********
                        update_status($n_id[0]->n_id);
                        
                        //*********** set notification ********************
                        set_notification("Transfer request has been ".$transfer_flow_next[0]->access_type, $remarks, '', $result[0]->userid, $requisition_id, 'requisition');
                        if($current_flow_next[0]->access_type=="PROCEED"){
                            set_notification(load_message('REQUISITION_REQUEST'), "", base_url() . "asset/transfer_request", $next_approver_id, $requisition_id, 'requisition');
                        }else {
                            set_notification(load_message('REQUISITION_REQUEST'), "", base_url() . "asset/requisitionlist", $next_approver_id, $requisition_id, 'requisition');
                        }
                        echo "Transfer request has been ".$current_flow_next[0]->access_type;
                    }elseif ($status == 'declined') {
                        $change_data['status'] = $status;
                        $change_data['approve_of'] = 0;
                        sci_update_db('requisitions', $change_data, ['id' => $requisition_id]);
                        sci_insert_db('requisitions_approvals', $data);

                        $action_name = "Requisition Transfer Approval"; //** log file create**/
                        log_create($action_name,$data);

                        //*********** find notification id *****************
                        $n_id = sci_select_db('notification_map', ['r_id' => $requisition_id, 'refference' => 'requisition'], 'n_id');

                        //*********** notification status change **********
                        update_status($n_id[0]->n_id);

                        //*********** set notification ********************
                        set_notification("Transfer request has been declined", $remarks, '', $result[0]->userid, $requisition_id, 'requisition');

                        echo "Transfer request has been declined";
                    }
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }
    
    /** 
    * This function retrieves the category from a parent id.
    */
    public function get_category_by_parent_id($parent_id=null){
        echo trim(get_category_by_parent_id($parent_id));
    }

    /** 
    * This function demonstrates the google map from latitute and longitude.
    */
    public function google_map($lat,$lng,$type = 'roadmap', $zoom = 10){
        generate_map($lat,$lng);
       //load_google_map(5, $type = 'roadmap', $zoom = 10);
    }
    
    /** 
    * This function retrieves the list of rooms from a specific office id.
    */
    public function get_room_by_office_id(){
        $this->load->model('office_model');
        $rooms=$this->office_model->get_room_by_office_id($this->input->post('rtc_id'));
        $opt='<option value="-1">Please select room</option>';
        if($rooms && is_array($rooms)){
 
            foreach($rooms as $room){
                $opt .= '<option value="'.$room->room_id.'">'.$room->room_name.'</option>';
            }
            
        }
        echo $opt;
    }

    /** 
    * This function retrieves the person/employee information from a specific office id.
    */
    public function get_person_by_office_id(){
        $this->load->model('user_model');
        $persons=$this->user_model->get_persons_by_office_id($this->input->post('rtc_id'));
        $opt='<option value="-1">Please select Person</option>';
        if($persons && is_array($persons)){

            foreach($persons as $person){
                $opt .= '<option value="'.$person->userid.'">'.$person->fullname.'-'.$person->designation.'</option>';
            }

        }
        echo $opt;
    }

/** 
* This function creates the requisition flow of an allocation.
*/

 public function flow_submit()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                //$jsondata = json_decode($jsondata, true);
               
                //$jsondata = json_encode($jsondata, true);


                $result = sci_insert_db('fams_requisition_flow', $jsondata);
                if ($result <> false) {

                    $action_name = "Allocation Flow Create"; //** log file create**/
                    log_create($action_name,$jsondata);

                    echo '{"status": "success", "message": "' . $this->lang->line('FLOW_CREATED') . '"}';

                } else {
                    echo '{"status": "error", "message": "' . $this->lang->line('FLOW_CREATE_FAILED') . '"}';

                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

/** 
* This function creates the requisition flow of asset transfer.
*/
   function transfer_flow_submit()
   {

    if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                
                $result = sci_insert_db('fams_transfer_flow', $jsondata);
                if ($result <> false) {

                    $action_name = "Transfer Flow Create"; //** log file create**/
                    log_create($action_name,$jsondata);

                    echo '{"status": "success", "message": "' . $this->lang->line('TRANSFER_FLOW_CREATED') . '"}';

                } else {
                    echo '{"status": "error", "message": "' . $this->lang->line('FLOW_CREATE_FAILED') . '"}';

                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
   } 

/** 
* This function updates the requisition flow of asset allocation.
*/
 
    public function flow_update()
    {

        if ($this->session->userdata('user_logged_in')) {

            $this->load->model('department_model', '', TRUE);
            $this->load->model('requisition_model', '', TRUE);
            $this->load->model('user_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('fams_requisition_flow', ['id' => $jsondata->id]);
                $data['flow_details'] = $result;
                

                $result = $this->requisition_model->departmentlist();
                if($result <> false)
                {
                    $data['department_list'] = $result;
                }
                else
                {
                    $data['department_list'] = "";
                }
                $result4 = $this->user_model->userlist();
                //var_dump($result); 
                $data['user_list'] = $result4;
                
                $this->load->view('admin_lte/requisition/flow_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

/** 
* This function updates the requisition flow of asset transfer.
*/
 
    function transfer_flow_update()
    {
        if ($this->session->userdata('user_logged_in')) {

           
            $this->load->model('requisition_model', '', TRUE);
            $this->load->model('user_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('fams_transfer_flow', ['id' => $jsondata->id]);
                $data['transfer_flow_details'] = $result;
                
                $result4 = $this->user_model->userlist();
                //var_dump($result); 
                $data['user_list'] = $result4;
                
                $this->load->view('admin_lte/requisition/transfer_flow_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }

    }

/** 
* This function creates the procurement request for allocation/transfer.
*/
    public function requested_pr_submit()
    {

     if ($this->session->userdata('user_logged_in')) {
   
            $this->load->model('requisition_model', '', TRUE);
            $this->load->model('user_model', '', TRUE);

            if ($this->input->is_ajax_request()) 
            {
                $reference_no = $this->input->post('ref_no', true);
                $request_id = $this->input->post('request_id', true);
                $pr_list = $this->input->post('pr_list', true);
                
                $jsonarray = json_decode($pr_list,true);
                //var_dump($jsonarray);
                foreach($jsonarray as $item)
                {
                    $category_id = $item["category_id"];
                    $asset_name = ltrim($item["asset_name"]);
                    $quantity = $item["quantity"];  

                    $jsondata['reference_no'] = $reference_no;
                    $jsondata['requisition_id'] = $request_id;
                    $jsondata['title'] = $asset_name;
                    $jsondata['category_id'] = $category_id;
                    $jsondata['quantity'] = $quantity;
                    $jsondata['status'] = "Raised";
                    $jsondata['remark'] = "";
                    $jsondata['userid'] = $this->session->userdata('user_db_id');
                    $jsondata['office_id'] = $this->session->userdata('user_center');
                    $pr_json = json_encode($jsondata,true);
                    $req = sci_insert_db('pr_request',$pr_json);

                    $action_name = "PR Generate"; //** log file create**/
                    log_create($action_name,$pr_json);
                }
                echo $this->lang->line('PR_INSERT');

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
   
}

/** 
* This function demostrates for data entry operator for registering old/existing assets.
*/
  public function entry_submit()
    {

       if ($this->session->userdata('user_logged_in')) {
            $this->load->model('asset_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                //$office_id = $this->session->userdata('posting_center');
                $user_center = $this->session->userdata('user_center');
                $jsondata = $this->input->post('jsondata', true);

                $jsondata = json_decode($jsondata, true);
                //var_dump($jsondata);

                $purchase_date = null;
                $warrenty_date = null;
                $asset_lifetime = null;

                if ($jsondata['purchase_date'] != null) {
                    $purchase_date = DateTime::createFromFormat('d/m/Y', $jsondata['purchase_date']);
                    $purchase_date = $purchase_date->format('Y-m-d');
                }
                if ($jsondata['warrenty_date'] != null) {
                    $warrenty_date = DateTime::createFromFormat('d/m/Y', $jsondata['warrenty_date']);
                    $warrenty_date = $warrenty_date->format('Y-m-d');
                }
                if ($jsondata['asset_lifetime'] != null) {
                    $asset_lifetime = DateTime::createFromFormat('d/m/Y', $jsondata['asset_lifetime']);
                    $asset_lifetime = $asset_lifetime->format('Y-m-d');
                }

               
                /*************** SCI ID Uniqueness Checking *******************/

                $sci_id = $jsondata['sci_id'];

                if ($sci_id <> "") {
                    $rest = $this->asset_model->sciid_check($sci_id);

                    if ($rest == false) {
                        //echo"SCI is unique!";
                        $sci_flag = 1;
                        /****** SCI # is Not unique *********/
                    } else {
                        $sci_flag = 2;
                        /****** SCI Not unique *********/
                    }
                    //var_dump($rest);
                } else {
                    $sci_flag = 0; /* ** SCI ic empty ** */
                }

                if (($sci_flag == 1) or ($sci_flag == 0)) {
                    $jsonarray = array("category_id" => $jsondata['category_id'],
                        'capital_id' => $jsondata['capital_id'],
                        'asset_name' => filter_var($jsondata['asset_name'], FILTER_SANITIZE_STRING),
                        'asset_description' => filter_var($jsondata['asset_description'], FILTER_SANITIZE_STRING),
                        'manufacture_id' => empty($jsondata['manufacture_id']) ? 0 : $jsondata['manufacture_id'],
                        'model_name' => filter_var($jsondata['model_name'], FILTER_SANITIZE_STRING),
                        'serial_no' => filter_var($jsondata['serial_no'], FILTER_SANITIZE_STRING),
                        'model_no' => filter_var($jsondata['model_no'], FILTER_SANITIZE_STRING),
                        'reference_no' => filter_var($jsondata['reference_no'], FILTER_SANITIZE_STRING),
                        'label_id' => "",
                        'sci_id' => $jsondata['sci_id'],
                        'purchase_date' => empty($purchase_date) ? NULL : $purchase_date,
                        'purchase_no' => filter_var($jsondata['purchase_no'], FILTER_SANITIZE_STRING),
                        'grn_no' => filter_var($jsondata['grn_no'], FILTER_SANITIZE_STRING),
                        'sku' => filter_var($jsondata['sku'], FILTER_SANITIZE_STRING),
                        'purchase_price' => empty(filter_var($jsondata['purchase_price'], FILTER_SANITIZE_STRING)) ? 0 : filter_var($jsondata['purchase_price'], FILTER_SANITIZE_STRING),
                        'purchase_location' => filter_var($jsondata['purchase_location'], FILTER_SANITIZE_STRING),
                        'warrenty_date' => empty($warrenty_date) ? NULL : $warrenty_date,
                        'asset_lifetime' => empty($asset_lifetime)? NULL : $asset_lifetime,
                        'assigned_to' => empty($jsondata['assigned_to'])? NULL : $jsondata['assigned_to'], 
                        'registration_type' => '',
                        'asset_remarks' => filter_var($jsondata['asset_remarks'], FILTER_SANITIZE_STRING),
                        'asset_status_id' => $jsondata['asset_status_id'],
                        'office_id' => $user_center,
                        'asset_readable_id' => '',
                        'funded_by' => empty($jsondata['funded_by'])? $jsondata['own_funded'] : $jsondata['funded_by'],
                        'pr_reference_no' => $jsondata['pr_reference_no'],
                    );

                        //echo"single registration";
                        //print_r($a);
                        $asset_json = json_encode($jsonarray, true);
                        $res = sci_insert_db('asset', $asset_json);
                        if ($res <> false) 
                        {
  
                            $max_category_id = $this->asset_model->get_max_office_category($user_center, $jsondata['category_id']);
                            $max_id = $max_category_id[0]->max + 1;

                            $auto_asset_id = $this->generate_assetid($user_center, $jsondata['category_id'], $res, $max_category_id[0]->max);
                            // echo $auto_asset_id;

                            $result = $this->asset_model->registered_asset_update($res, $auto_asset_id, $max_id);

                            //echo '{"status": "success", "message": "' . $this->lang->line('ASSET_REGISTRATION') . '"}';
                            
                            /***** Audit Log *****/
                            $action_name = "Asset Registration";
                            log_create($action_name,$asset_json);
                            /***** Audit Log *****/

                            /*********** Manual Old allocation starts************/
                            if($jsondata['asset_status_id'] == 5)
                            {
                                //echo $jsondata['asset_location'];
                                if ($jsondata['allocation_date'] != null) {
                                    $allocation_date = DateTime::createFromFormat('d/m/Y', $jsondata['allocation_date']);
                                    $allocation_date = $allocation_date->format('Y-m-d');
                                    }
                                    else
                                    {
                                        $allocation_date = date('Y-m-d');
                                    }

                                if($jsondata['asset_location']=="")
                                {
                                   $allocate_location = NULL; 
                                }
                                else
                                {
                                  $allocate_location = $jsondata['asset_location'];  
                                }
                                //$allocate_location = empty($jsondata['assigned_to']) ? NULL : $jsondata['asset_location'];

                                $requisition_no = date("Y-m-d H:i:s");
                                $requisition_no = str_replace('-', '', $requisition_no);
                                $requisition_no = str_replace(':', '', $requisition_no);
                                $requisition_no = str_replace(' ', '', $requisition_no);
                                
                                $res22 = $this->asset_model->auto_asset_allocate($res, $auto_asset_id, $requisition_no,$jsondata['category_id'],
                                $jsondata['assigned_to'],$user_center,$allocate_location,$allocation_date);
                                
                                /***Manual old allocation log***/
                                $action_name = "Manual Old Alloction";
                                $allo_json["requisition_no"] = $requisition_no;
                                $allo_json["assetid"] = $res;
                                $allo_json["label_id"] = $auto_asset_id;
                                $allo_json["assigned_to"] = $jsondata['assigned_to'];
                                $allo_json["office_to"] = $user_center;
                                $allo_json["asset_location"] = $allocate_location;
                                $allo_json["allocation_date"] = $allocation_date;
                                log_create($action_name,json_encode($allo_json,true));
                            }
                            /***** Allocation end here******/
                            echo '{"status": "success", "message": "' . $this->lang->line('ASSET_REGISTRATION') . '"}';
                        }
                   
                } else {
                    /********** SCI is not unique  *   ***********/
                    echo '{"status": "error", "message": "' . $this->lang->line('SCI_DUPLICATE') . '"}';
                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }   
    }

/** 
* This function updates the capital registration limit of asset capitalization.
*/
    public function registration_limit()
    {
        if ($this->session->userdata('user_logged_in')) 
        {
          
            $this->load->model('asset_model', '', TRUE);

            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('fams_capitalize', ['capital_id' => $jsondata->capital_id]);
                
                //var_dump($result);
                $data['capital_details'] = $result;
                
                $this->load->view('admin_lte/asset/capital_limit_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }

    }
   
}