<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/****************************************************/
// Filename: office.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: addoffice,newroom,roomlist,officelist,office_delete,office_edit
// updateroom,deleteroom,
/****************************************************/

 /**
 * AMS Asset office Controller Class
 *
 * This method demonstrates the office and room crud operation of AMS.
 */
class office extends SCI_Controller
{

    function __construct()
    {
        parent::__construct();


        $this->load->model('user_model', '', TRUE);
        $this->load->model('role_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);
        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Office');
        $this->set_page_sub_title('control panel');
    }

    /********Create new Office**************/
    public function addoffice()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/general_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ADD_NEW_OFFICE'));
        $this->set_page_sub_title('');
        $this->load_view('admin_lte/office/office_create');
    }

    /*************** Create New Room for an Office  ******************/
    public function newroom()
    {
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/general_script.js');
        $this->set_js('dist/js/global_script.js');
        $this->set_page_title(load_message('ADD_NEW_ROOM'));
        $this->set_page_sub_title('');
        $office = $this->office_model->officelist();
        $this->set_value('office_list', $office);
        $this->load_view('admin_lte/office/room_create');
    }

    /**********Show all Room***************/
    public function roomlist()
    {
        $this->set_page_title(load_message('ROOM_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/setting_script.js');
        $this->set_js('dist/js/global_script.js');
        $result = $this->office_model->office_room_list();
        //var_dump($result);

        $this->set_value('room_list', $result);
        $this->load_view('admin_lte/office/room_list');
    }

    /**********Show all Office***************/
    public function officelist()
    {
        $this->set_page_title(load_message('OFFICE_LIST'));
        $this->set_page_sub_title('');
        $this->set_js('dist/js/jsonmap.js');
        $this->set_js('dist/js/sci/project.js');
        $this->set_js('dist/js/setting_script.js');
        $this->set_js('dist/js/global_script.js');
        $result = $this->office_model->officelist();
        //var_dump($result);

        $this->set_value('office_list', $result);

        $this->load_view('admin_lte/office/office_list');
    }

    /***********Office Delete***************/

    public function office_delete()
    {
        $this->set_js('dist/js/jsonmap.js');
        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true));
            $result = sci_delete_db('office', ['office_id' => $jsondata->office_id]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Office Delete";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/

                echo $this->lang->line('DELETE');
            }
        } else {
            exit('No direct script access allowed');
        }

    }

    /***********Office Update***************/
    public function office_edit()
    {
        $this->set_js('dist/js/jsonmap.js');
        if ($this->input->is_ajax_request()) {

            $jsondata = $this->input->post('jsondata', true);
            $json_output = json_decode($jsondata, true);

            $updateid = $json_output['update_id'];

            $latitude = $json_output['latitude'];
            $longitude = $json_output['longitude'];

            $json_output['geo_loc'] = $latitude.','.$longitude;

            unset($json_output['update_id']);
            $latitude = $json_output['latitude'];
            $longitude = $json_output['longitude'];

            $json_output['geo_loc'] = $latitude.','.$longitude;

            $result = sci_update_db('office', $json_output, ['office_id' => $updateid]);
            if ($result <> false) {
                /**** Audit Log ****/
                $action_name = "Office Update";
                log_create($action_name,$jsondata);
                /**** Audit log end here *****/
                echo '{"status": "success", "message": "' . $this->lang->line('OFFICE_UPDATED') . '"}';

            }

        } else {
            exit('No direct script access allowed');
        }
    }

    /************ Room updateroom  ***************/
    public function updateroom()
    {

        $this->set_js('dist/js/jsonmap.js');
        if ($this->input->is_ajax_request()) {

            $jsondata = $this->input->post('jsondata', true);
            $json_output = json_decode($jsondata, true);

            $updateid = $json_output['update_id'];
            unset($json_output['update_id']);

            $result = sci_update_db('office_room', $json_output, ['room_id' => $updateid]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Room Update";
                log_create($action_name,$jsondata);
                /**** Audit log end here *****/

                echo '{"status": "success", "message": "' . $this->lang->line('ROOM_UPDATED') . '"}';

            }

        } else {
            exit('No direct script access allowed');
        }

    }

    /************  Delete room ***************/
    public function deleteroom()
    {

        if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true));
            $result = sci_delete_db('office_room', ['room_id' => $jsondata->room_id]);
            if ($result <> false) {

                /**** Audit Log ****/
                $action_name = "Room Delete";
                log_create($action_name,json_encode($jsondata,true));
                /**** Audit log end here *****/

                echo $this->lang->line('DELETE');
            }
        } else {
            exit('No direct script access allowed');
        }
    }

}
