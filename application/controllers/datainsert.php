<?php
/****************************************************/
// Filename: datainsert.php
// Created By:  Shahed
// Change history:
//
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: index,get_GRN,get_capital_id,get_sku,get_asset_description,set_empty,get_asset_readable_id,
//get_asset_remarks,get_asset_create_date,get_asset_grn,get_asset_label_id,get_asset_manufacturer_id,
//get_asset_model_name,get_asset_model_number,get_asset_office_id,get_asset_purchase_location,
//get_asset_purchase_order_no,get_asset_ref_no,get_asset_reg_type,get_asset_serial_no,get_asset_sci_id,
//get_asset_purchase_price,get_asset_warranty_date,get_asset_purchase_date,get_asset_life_time,get_asset_assigned_to,
//get_asset_update_date,get_asset_status_id,get_asset_PR,get_asset_category,
//
/****************************************************/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class datainsert extends SCI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        
        $this->set_page_title('Import Asset');
    }

    public function index() {
        

        $this->load_view('admin_lte/import/excel_import');
        //$this->read_excel();
    }

    /**
     * An data import system from formated xlsx file
     */
    public function import_data() {
        
        $temp_file_name = date("ymdhis").'.xlsx';

        $config = array(
            'upload_path' => 'temp/',
            'allowed_types' => 'xlsx|xls',
            'file_name' => $temp_file_name,
        );
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file')) {
            $data = $this->upload->data();

            $this->read_excel($temp_file_name);

        }
    }

    /**
     * @param $file
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     * Read the imported file and insert data into the database
     */
    public function read_excel($file) {
        $this->load->library('EXcel', 'excel');

        $objPHPExcel = PHPExcel_IOFactory::load('temp/'.$file);
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        $asset = array();

        //get fields name from array first row
        $fields = $allDataInSheet[1];

        //Remove first row
        unset($allDataInSheet[1]);


        foreach ($allDataInSheet as $d) {

            $new_asset = array(
                'category_id' => $this->get_asset_category($d['A']),
                'capital_id ' => $this->get_capital_id($d['B']),
                'asset_name' => $this->set_empty($d['C']),
                'asset_description' => $this->get_asset_description( $d['D']),
                'manufacture_id' => $this->get_asset_manufacturer_id($d['E']),
                'model_name' => $this->get_asset_model_name($d['F']),
                'serial_no' => $this->get_asset_serial_no($d['G']),
                'model_no' => $this->get_asset_model_number($d['H']),
                'reference_no' => $this->get_asset_ref_no($d['I']),
                'label_id' => $this->get_asset_label_id($d['J']),
                'purchase_date' => $this->get_asset_purchase_date($d['K']),
                'purchase_no' => $this->get_asset_purchase_order_no($d['L']),
                'grn_no' => $this->get_GRN($d['M']),
                'sci_id' => $this->get_asset_sci_id($d['N']),
                'sku' => $this->get_sku($d['O']),
                'purchase_price' => $this->get_asset_purchase_price($d['P']),
                'purchase_location' => $this->get_asset_purchase_location($d['Q']),
                'warrenty_date' => $this->get_asset_warranty_date($d['R']),
                'asset_lifetime' => $this->get_asset_life_time($d['S']),
                'assigned_to' => $this->get_asset_assigned_to($d['T']),
                'registration_type' => $this->get_asset_reg_type($d['U']),
                'asset_remarks' => $this->get_asset_remarks($d['V']),
                'asset_status_id' => $this->get_asset_status_id($d['W']),
                'office_id' => $this->get_asset_office_id($d['X']),
                'asset_readable_id' => $this->get_asset_readable_id($d['Y']),
                'pr_reference_no' => $this->get_asset_PR($d['Z']),
                'create_date' => $this->get_asset_create_date($d['AA']),
                'update_date' => $this->get_asset_update_date($d['AB'])
            );

            array_push($asset, $new_asset);
            unset($new_asset);
        }

        //var_dump($asset);
        $this->db->insert_batch('asset_temp', $asset);
        $total_recored_added= $this->db->affected_rows();
        
        $get_all_new_asset = $this->db->get('asset_temp')->result();
        $this->set_value('total_added', $total_recored_added);
        $this->set_value('get_all_new_asset', $get_all_new_asset);
        $this->load_view('admin_lte/import/result');
        
    }

    /**
     * @param null $val
     * @return false|mixed|null|string
     * A function get system generated GRN no
     */
    private function get_GRN($val=null) {
        if ($val === NULL) {
            $token = date("Y-m-d H:i:s");
            $token = str_replace('-', '', $token);
            $token = str_replace(':', '', $token);
            $token = str_replace(' ', '', $token);

            return $token;
        }else{
            return $val;
        }
    }

    /**
     * @param null $id
     * @return null|string
     * A function to get capital ID
     */
    private function get_capital_id($id = NULL) {
        if ($id === NULL) {
            return '99999';
        } else {
            return $id;
        }
    }

    /**
     * @param null $sku
     * @return null|string
     * A function that return the sku
     */
    private function get_sku($sku = NULL) {
        if ($sku === NULL) {
            return '';
        } else {
            return $sku;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns asset description
     */
    private function get_asset_description($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that return an empty value or the the value
     */
    private function set_empty($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns asset readable id
     */
    private function get_asset_readable_id($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns the asset remarks
     */
    private function get_asset_remarks($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return false|null|string
     * A function that returns the created date
     */
    private function get_asset_create_date($val = NULL) {
        if ($val === NULL) {
            return date("Y-m-d H:i:s");
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return false|null|string
     * A function that return the grn
     */
    private function get_asset_grn($val = NULL) {
        if ($val === NULL) {
            return date("Y-m-d H:i:s");
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns the asset label id
     */
    private function get_asset_label_id($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return int|null
     * A function that returns the asset manufecture id
     */
    private function get_asset_manufacturer_id($val = NULL) {
        if ($val === NULL) {
            return 0;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|stringa
     * A function that returns the asset model name
     */
    private function get_asset_model_name($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns the asset model number
     */
    private function get_asset_model_number($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return int|null
     * A function that returns the asset office  ID
     */
    private function get_asset_office_id($val = NULL) {
        if ($val === NULL) {
            return 0;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns the purchase location
     */
    private function get_asset_purchase_location($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns purchase order number
     */
    private function get_asset_purchase_order_no($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns the reference no
     */
    private function get_asset_ref_no($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns registration type (BULK/ SINGLE)
     */
    private function get_asset_reg_type($val = NULL) {
        if ($val === NULL) {
            return 'single';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns Asset serial NO
     */
    private function get_asset_serial_no($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns asset SCI id
     */
    private function get_asset_sci_id($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return int|null
     * A function that returns purchase price
     */
    private function get_asset_purchase_price($val = NULL) {
        if ($val === NULL) {
            return 0;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null
     * A function that returns warranty date
     */
    private function get_asset_warranty_date($val = NULL) {
        if ($val === NULL) {
            return NULL;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null
     * A function that returns purchase date
     */
    private function get_asset_purchase_date($val = NULL) {
        if ($val === NULL) {
            return NULL;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null
     * A function that returns Life time .
     */
    private function get_asset_life_time($val = NULL) {
        if ($val === NULL) {
            return NULL;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null
     * A function that returns Assigned to (User ID)
     */
    private function get_asset_assigned_to($val = NULL) {
        if ($val === NULL) {
            return NULL;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null
     * A function that returns update date
     */
    private function get_asset_update_date($val = NULL) {
        if ($val === NULL) {
            return NULL;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return int|null
     * A function that returns status id
     */
    private function get_asset_status_id($val = NULL) {
        if ($val === NULL) {
            return 1;
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function that returns PR number
     */
     private function get_asset_PR($val = NULL) {
        if ($val === NULL) {
            return '';
        } else {
            return $val;
        }
    }

    /**
     * @param null $val
     * @return null|string
     * A function return category id
     */
    private function get_asset_category($val = NULL) {
        if ($val === NULL) {
            return '0';
        } else {
            return $val;
        }
    }
}
