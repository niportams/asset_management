<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/****************************************************/
// Filename: admin.php
// Created By:     Hasibul Huq
// Change history:
//
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, National Institute of Population Research and Training (NIPORT)
// @license An open source application
// @Version     1.0
// Function list: index,loggedinuser,loggedout,
//
/****************************************************/

/**
 * Description of user
 *
 * @author hasibul.huq
 */
class admin extends SCI_Controller
{
    //put your code here

    function __construct()
    {
        parent::__construct();


        $this->load->model('user_model', '', TRUE);
        $this->load->model('role_model', '', TRUE);
        $this->load->model('office_model', '', TRUE);
        $this->load->model('department_model', '', TRUE);

        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        //$this->set_js('dist/js/office.js');
    }

    public function index()
    {
        $this->set_page_title('User');
        $this->set_page_sub_title('control panel');
        //$this->set_value('test', 'sdfsdfsdfs');
        //$this->load_view('admin_lte/dashboard');
    }

    
    /********Show the logged in user list to the system admin (Developer)**************/
    public function loggedinuser()
    {
        $this->set_page_title('Logged in User List');
        $this->set_page_sub_title('');
        $data=[];
        $query = sci_select_db('ci_sessions',"user_data !=''",'*');

        for ($i=0; $i < sizeof($query);$i++){
            $data[$i] = unserialize($query[$i]->user_data);
            $data[$i]['session_id'] = $query[$i]->session_id;
            $data[$i]['ip_address'] = $query[$i]->ip_address;
            $data[$i]['user_agent'] = $query[$i]->user_agent;
            //var_dump(unserialize($query[$i]->user_data));
        }
        $this->set_value('user_list', $data);
        $this->load_view('admin_lte/admin/logged_in_user_list');
    }
    /********System Admin(Developer) can logged the user out from the system **************/
    public function loggedout(){
        if ($this->input->is_ajax_request()) {
            $key = $this->input->post('key', true);

            sci_update_db('fams_ci_sessions',['user_data'=>''],['session_id'=>$key]);
            echo "user Logged out";
        }
    }

}
