<?php
if (!function_exists('set_data_table')) {
    
    function set_data_table($table_option=null){
        $ci = & get_instance();
        
       echo 
        '<table class="table table-bordered sci-table sci-table-blue dataTable-full-functional display" id="asset-list" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>#SN.</th>
                <th></th>
                <th></th>
                <th>'.load_message('ASSET_TITLE').'</th>
                <th>'.load_message('CATEGORY').'</th>
                <th>'.load_message('MODEL_NAME').'</th>
                <th>'.load_message('MODEL_NO').'</th>
                <th>'.load_message('ASSET_LABEL_ID').'</th>
                <th>'.load_message('ASSET_ID_L').'</th>
                <th>'.load_message('PURCHASE_DATE').'</th>
                <th>'.load_message('REGISTRATION_DATE').'</th>
                <th>'.load_message('STATUS').'</th>
                <th></th>
            </tr>
        </thead>
       
    </table>';
        
    }
}

