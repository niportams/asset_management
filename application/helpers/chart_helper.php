<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<?php if (!function_exists('Capital_V_Reg')) { ?>

    <?php function Capital_V_Reg()
    { ?>
        <?php
        $chart = sci_asset_count_categoriwise();
        
        foreach ($chart as $c) {
            $label[] = '"' . $c->label . '"';
            $data[] = $c->value;
        }
        
        if(!isset($label))
            $label = array();
        
        if(!isset($data))
            $data = array();
        
        $cat_label = '[' . implode(",", $label) . ']';
        $data = '[' . implode(",", $data) . ']';

        //var_dump((object)$cat_label);
        ?>
        <!-- BAR CHART -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Category wise Registered Asset</h3>
            </div>
            <div class="box-body">
                <div class="chart">
                    <canvas id="capitalVsRegiistration"></canvas>
                    <script>
                        lbl = '<?php $label; ?>';

                        //console.log(lbl);
                        chart = {
                            labels:<?php echo $cat_label; ?>,
                            dataset: [
                                {
                                    label: '',
                                    data:<?php echo $data; ?>,
                                    backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de', '#dd4b39', '#0073b7', '#001F3F', '#39CCCC', '#3D9970', '#01FF70', '#FF851B', '#F012BE', '#605ca8', '#D81B60', '#111', '#d2d6de','#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de', '#dd4b39', '#0073b7', '#001F3F', '#39CCCC', '#3D9970', '#01FF70', '#FF851B', '#F012BE', '#605ca8', '#D81B60', '#111', '#d2d6de','#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de', '#dd4b39', '#0073b7', '#001F3F', '#39CCCC', '#3D9970', '#01FF70', '#FF851B', '#F012BE', '#605ca8', '#D81B60', '#111', '#d2d6de','#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de', '#dd4b39', '#0073b7', '#001F3F', '#39CCCC', '#3D9970', '#01FF70', '#FF851B', '#F012BE', '#605ca8', '#D81B60', '#111', '#d2d6de']
                                },
                            ]

                        };

                        get_bar_chart('capitalVsRegiistration', chart);
                    </script>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    <?php } ?>
    <?php
} ?>