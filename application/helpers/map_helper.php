<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<?php if (!function_exists('load_google_map')) { ?>
    <?php

    function load_google_map($loc, $type = 'roadmap', $zoom = 10)
    {

//        $src = "http://www.google.com/maps/embed/v1/place?key=";
//        $src .= $CI->config->item('map_key');
//        $src .= "&q=$cord&zoom=10&maptype=roadmap";
        ?>
        <style>
            .map {
                border: solid 1px #CCC;
                height: 450px;
            }
        </style>
        <div class="map" align="center" style="margin-bottom:20px;">
            <div id="map"></div>
           
            <?php 
                $lat = substr($loc, 0, strpos($loc, ","));
                $lng = substr($loc, strpos($loc, ",")+1 );

            ?>
            <iframe style="width:100%; height:100%" frameBorder="0" style="border:0" src="<?= base_url(); ?>ajax/google_map/<?=$lat;?>/<?=$lng;?>"
                    allowfullscreen></iframe>
        </div><!-- map -->
    <?php } ?>
<?php } 

    function generate_map($lat,$lng,$type = 'roadmap', $zoom = 18){
        $CI = &get_instance();
        
        $data['lat']=$lat;
        $data['lng']=$lng;
        $data['type']=$type;
        $data['zoom']=$zoom;
        
        $CI->load->view('admin_lte/map/rtc_marker',$data);
        return;
    }

?>