<?php

function permission_check($url) {
    $ci = & get_instance();
//    $ci->db->select('privilege_id');
//    $ci->db->from('privilege');
//    $ci->db->where('privilege', $url);
//
//    $query = $ci->db->get();
//    $data = $query->result();
//    $permission = get_user_permission();
//    if (in_array($data[0]->privilege_id, $permission)) {
//        return true;
//    } else
//        return false;

    $permission_array = explode(",", $ci->session->userdata('access_permission'));
    //var_dump($permission_array);
    if (in_array($url, $permission_array)) {
         return true;
    }
    
    return false;
}

function permitted_button($permission, $btn_icon = null, $btn_class = null, $btn_id = "", $data = "", $on_click = "", $btn_title = "") {

    if (permission_check($permission)) {
        $btn = '<button title="' . $btn_title . '" class="' . $btn_class . '" data-' . $data . ' onclick="' . $on_click . '">';
        $btn .= '<span class="' . $btn_icon . '" aria-hidden="true"></span>';
        $btn .= '</button>';

        echo $btn;
    }
}
