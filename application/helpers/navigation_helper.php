<?php

function get_nav() {
    $navigation = array(
        array(
            'title' => 'Dashboard',
            'permission' => '22',
            'uri' => 'dashboard',
            'icon' => 'fa fa-dashboard',
            'has_child' => false,
            'child' => ''
        ),
        array(
            'title' => 'Asset Capital',
            'permission' => '119',
            'uri' => '#',
            'icon' => 'fa fa-cart-arrow-down',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Capitalize Asset',
                    'permission' => '7',
                    'uri' => 'asset/capitalize',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Capital List',
                    'permission' => '82',
                    'uri' => 'asset/capitallist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Asset Information',
            'permission' => '120',
            'uri' => '#',
            'icon' => 'fa fa-cubes',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Asset Registration',
                    'permission' => '8',
                    'uri' => 'asset/register',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Asset List',
                    'permission' => '11',
                    'uri' => 'asset/assetlist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Requisition',
            'permission' => '121',
            'uri' => '#',
            'icon' => 'fa fa-list-alt',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Requisition Request',
                    'permission' => '16',
                    'uri' => 'asset/requisition',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Pending Requested list',
                    'permission' => '110',
                    'uri' => 'asset/requisitionlist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'My Requisitions',
                    'permission' => '115',
                    'uri' => 'asset/requisitions',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Activity History',
                    'permission' => '133',
                    'uri' => 'asset/approvedrequisitions',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                )
            )
        ),
        array(
            'title' => 'Asset Allocation',
            'permission' => '122',
            'uri' => '#',
            'icon' => 'fa fa-check',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Generate Request List (Manual)',
                    'permission' => '164',
                    'uri' => 'allocation/newrequest',
                    'icon' => 'fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Allocation Request List',
                    'permission' => '165',
                    'uri' => 'allocation/requestlist',
                    'icon' => 'fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Requested Allocation List',
                    'permission' => '112',
                    //'uri' => 'asset/asset_allocate',
                    'uri' => 'asset/allocation_request',
                    'icon' => 'fa fa-angle-right',
                    'is_label' => false,
                ),
                /* array(
                  'title' => 'Asset Allocation',
                  'permission' => '154',
                  'uri' => 'asset/manual_allocation',
                  'icon' => 'fa fa-angle-right',
                  'is_label' => false,
                  ), */
                array(
                    'title' => 'Allocated Asset List',
                    'permission' => '123',
                    'uri' => 'asset/allocated_list',
                    'icon' => 'fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Allocation List',
                    'permission' => '160',
                    'uri' => 'allocation/allocation_list',
                    'icon' => 'fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Reverted Allocation List',
                    'permission' => '179',
                    'uri' => 'allocation/reverted_allocation_list',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Procurement Request List',
                    'permission' => '167',
                    'uri' => 'pr/prlist',
                    'icon' => 'fa fa-angle-right',
                    'is_label' => false,
                ),/*
                array(
                    'title' => 'Single Item Allocation',
                    'permission' => '197',
                    'uri' => 'allocation/direct_allocation',
                    'icon' => 'fa fa-angle-right',
                    'is_label' => false,
                ),*/
            )
        ),
        array(
            'title' => 'Asset Transfer',
            'permission' => '140',
            'uri' => '#',
            'icon' => 'fa fa-paper-plane',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Requested Transfer List',
                    'permission' => '135',
                    'uri' => 'asset/transfer_request',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Transfered Asset List',
                    'permission' => '136',
                    'uri' => 'asset/transfered_list',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Reverted Transfered List',
                    'permission' => '180',
                    'uri' => 'allocation/reverted_transfer_list',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Asset Disposal',
            'permission' => '141',
            'uri' => '#',
            'icon' => 'fa fa-external-link-square ',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Asset Dispose',
                    'permission' => '129',
                    'uri' => 'asset/state/Disposed',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Asset Disposed List',
                    'permission' => '134',
                    'uri' => 'asset/disposedlist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Asset DeadState',
            'permission' => '142',
            'uri' => '#',
            'icon' => 'fa fa-trash',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Dead State',
                    'permission' => '129',
                    'uri' => 'asset/state/Deadstate',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Asset Deadstate List',
                    'permission' => '127',
                    'uri' => 'asset/deadstatelist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Asset Maintenance',
            'permission' => '144',
            'uri' => '#',
            'icon' => 'fa fa-wrench',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Maintenance State',
                    'permission' => '129',
                    'uri' => 'asset/state/Maintenance',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Asset Maintenance List',
                    'permission' => '131',
                    'uri' => 'asset/maintenancelist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Physical Condition',
            'permission' => '143',
            'uri' => '#',
            'icon' => 'fa fa-pencil-square-o',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Physical Check',
                    'permission' => '138',
                    'uri' => 'condition/physical_check',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Physicaly Checked List',
                    'permission' => '139',
                    'uri' => 'condition/check_list',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Reports',
            'permission' => '116',
            'uri' => '#',
            'icon' => 'fa  fa-bar-chart',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Institute wise reporting',
                    'permission' => '168',
                    'uri' => 'report/rtc_quick_report',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Room wise reporting',
                    'permission' => '169',
                    'uri' => 'report/rtc_room_wise_report',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Person wise reporting',
                    'permission' => '170',
                    'uri' => 'report/rtc_person_wise_report',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Item wise reporting',
                    'permission' => '176',
                    'uri' => 'report/category_wise_report',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Store Reporting',
                    'permission' => '111',
                    'uri' => 'report/asset',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Allocation Reporting',
                    'permission' => '148',
                    'uri' => 'report/allocation',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Asset Reporting',
                    'permission' => '149',
                    'uri' => 'report/asset_info',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Asset Lifetime',
                    'permission' => '193',
                    'uri' => 'report/asset_life_expire',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Data Entry',
            'permission' => '198',
            'uri' => '#',
            'icon' => 'fa fa-keyboard-o',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Asset Registration',
                    'permission' => '199',
                    'uri' => 'dataentry/asset_register',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                )
            ),
        ),
        array(
            'title' => 'Settings',
            'permission' => '117',
            'uri' => '#',
            'icon' => 'fa fa-cogs',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'USER',
                    'permission' => '25',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New User',
                    'permission' => '1',
                    'uri' => 'user/adduser',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'User List',
                    'permission' => '29',
                    'uri' => 'user/userlist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'User Password Reset',
                    'permission' => '205',
                    'uri' => 'user/change_password',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'USER ROLE',
                    'permission' => '38',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Role',
                    'permission' => '24',
                    'uri' => 'role/addrole',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Role List',
                    'permission' => '27',
                    'uri' => 'role/rolelist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'PRIVILEGE',
                    'permission' => '118',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Privilege',
                    'permission' => '26',
                    'uri' => 'privilege/addprivilege',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Privilege List',
                    'permission' => '28',
                    'uri' => 'privilege/privilegelist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'CATEGORY',
                    'permission' => '76',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Category',
                    'permission' => '77',
                    'uri' => 'category/addcategory',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Category List',
                    'permission' => '78',
                    'uri' => 'category/categorylist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'OFFICE',
                    'permission' => '145',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Office',
                    'permission' => '86',
                    'uri' => 'office/addoffice',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Office List',
                    'permission' => '85',
                    'uri' => 'office/officelist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'ROOM',
                    'permission' => '196',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Room',
                    'permission' => '155',
                    'uri' => 'office/newroom',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Room List',
                    'permission' => '156',
                    'uri' => 'office/roomlist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'UNIT',
                    'permission' => '146',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Unit',
                    'permission' => '88',
                    'uri' => 'department/add_department',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Unit List',
                    'permission' => '87',
                    'uri' => 'department/department_list',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'SUPPLIER',
                    'permission' => '147',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Supplier',
                    'permission' => '93',
                    'uri' => 'supplier/add_supplier',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Supplier List',
                    'permission' => '94',
                    'uri' => 'supplier/supplier_list',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'MANUFACTURER',
                    'permission' => '97',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Manufacturer',
                    'permission' => '98',
                    'uri' => 'manufacture/add_manufacture',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Manufacture List',
                    'permission' => '99',
                    'uri' => 'manufacture/manufacture_list',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'ORGANIZATION',
                    'permission' => '172',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Organization',
                    'permission' => '171',
                    'uri' => 'organization/add_organization',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Organization List',
                    'permission' => '173',
                    'uri' => 'organization/organization_list',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'REQUISITION FLOW',
                    'permission' => '189',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Flow',
                    'permission' => '181',
                    'uri' => 'requisition/add_flow',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Flow List',
                    'permission' => '182',
                    'uri' => 'requisition/flowlist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'TRANSFER FLOW',
                    'permission' => '190',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Add New Transfer Flow',
                    'permission' => '185',
                    'uri' => 'requisition/add_transfer_flow',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Transfer Flow List',
                    'permission' => '186',
                    'uri' => 'requisition/transfer_flowlist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Developer Info',
                    'permission' => '200',
                    'uri' => '#',
                    'icon' => '',
                    'is_label' => true,
                ),
                array(
                    'title' => 'Logged in User List',
                    'permission' => '201',
                    'uri' => 'admin/loggedinuser',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
            )
        ),
        array(
            'title' => 'Database Backup',
            'permission' => '202',
            'uri' => '#',
            'icon' => 'fa fa-download',
            'has_child' => true,
            'child' => array(
                array(
                    'title' => 'Export Database',
                    'permission' => '203',
                    'uri' => 'dbback/dbexport',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                array(
                    'title' => 'Database List',
                    'permission' => '203',
                    'uri' => 'dbback/backuplist',
                    'icon' => 'fa fa fa-angle-right',
                    'is_label' => false,
                ),
                
            )
        ),
    );

    //all menu permisssino of current user
    $permission = get_user_permission();

    foreach ($navigation as $nav) {

        $ci = &get_instance();

        $parent_class = '';
        $child_class = '';

        if ($nav['has_child']) {
            $parent_class = 'treeview';
            $child_class = 'treeview-menu';
        }


        if (in_array($nav['permission'], $permission)) {


            #Activate menu in left panel
            $active_parent = ($nav['uri'] == get_current_uri() || $nav['uri'] == get_current_uri() . '/index') ? " active " : "";


            //find the child is active, if so, this will activate the parent also
            if ($nav['has_child']) {
                foreach ($nav['child'] as $child) {
                    $active_parent = ($child['uri'] == get_current_uri() || $child['uri'] == get_current_uri() . '/index') ? " active " : "";

                    if ($active_parent != "")
                        break;
                }
            }

            // echo $active_parent;
            //exit;
            $menu = '
                <li class="' . $active_parent . $parent_class . ' ">
                    <a href="' . base_url() . $nav['uri'] . '">
                        <i class="' . $nav['icon'] . '"></i>
                        <span>' . $nav['title'] . '</span>
                        <!--<span class="pull-right-container">
                            <span class="label label-primary pull-right">4</span>
                        </span>-->
                    </a>';

            //If there is any sub menu
            if ($nav['has_child']) {
                $menu .= '<ul class="' . $child_class . '">';

                foreach ($nav['child'] as $child) {
                    $active = ($child['uri'] == get_current_uri() || $child['uri'] == get_current_uri() . '/index') ? " active " : "";

                    if (in_array($child['permission'], $permission))
                        if ($child['is_label']) {
                            $menu .= '<li class="header">' . $child['title'] . '</li>';
                        } else {
                            $menu .= '<li class="' . $active . '"><a href="' . base_url() . $child['uri'] . '"><i class="' . $child['icon'] . '"></i>' . $child['title'] . '</a></li>';
                        }
                    //Child Navigation if have the permission
                }

                $menu .= '</ul>';
            }

            $menu .= '</li>';

            echo $menu;
        }
    }
    echo '<br><br><br>';
}

function get_current_uri() {
    $ci = &get_instance();
    $uri_segment = $ci->uri->segment(1);
    $uri_segment .= ($ci->uri->segment(2)) ? '/' . $ci->uri->segment(2) : '';

    return $uri_segment;
}