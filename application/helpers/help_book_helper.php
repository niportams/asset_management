<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php


if (!function_exists('moduler_helper')) {
    
    function moduler_helper($file_name=null){
        $CI = & get_instance();
       
        //Getting the last uri string and search it in root/help folder as html file
        $help_file = 'help/'.end($CI->uri->segments).".pdf";
        if (file_exists($help_file)){
            echo '<h1><a href="'.base_url().$help_file.'" target="_blank" title="Help"><img width="32px;" src="'. base_url().'theme/dist/img/help_book.png"></a></h1>';
        }
           
    }
}
?>