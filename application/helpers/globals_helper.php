<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Application specific global variables
class Globals
{
    private static $category_list = null;
    
    private static function init(){
        $ci = & get_instance();
        $ci->load->model('category_model');
        self::$category_list=$ci->category_model->get_all_categories();
    }


    public static function get_category_array(){
        
        self::init();
        return self::$category_list;
    }

    


//    private static function initialize()
//    {
//        if (self::$initialized)
//            return;
//
//        self::$category_list = null;
//        self::$initialized = true;
//    }
//
//    public static function category_list($memberId)
//    {
//        self::initialize();
//        self::$category_list = $memberId;
//    }
//
//
//    public static function authenticatedMemeberId()
//    {
//        self::initialize();
//        return self::$authenticatedMemberId;
//    }
}