<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function get_category_by_parent_id($parent_id = null) {
    $ci = & get_instance();
    $ci->load->model('category_model');

    $parent_cat = $ci->category_model->get_category_by_id($parent_id);

    return json_encode($parent_cat);
}

if (!function_exists('get_category_list')) :

    function get_category_list($name = "", $id = "", $class = "", $first_child = "", $selected = "", $type = "select", $parent_disabled = true,$disabled_element = false) {

        //$type = select/value
        $ci = & get_instance();
        $ci->load->model('category_model');

        $parent_cat = (array) $ci->category_model->get_category_by_id();
        
        
        //exit();
        $disabled_element = ($disabled_element)?'disabled="disabled"':'';
        
        

        //Insert first child
        foreach ($parent_cat as $index => $val) {

            $child_cat = $ci->category_model->get_category_by_id($parent_cat[$index]->category_id);

            $parent_cat[$index]->{'child'} = $child_cat;
        }

        //Insert second child
        foreach ($parent_cat as $index => $val) {

            if ($parent_cat[$index]->child) {

                foreach ($parent_cat[$index]->child as $i => $v) {

                    $child_cat = $ci->category_model->get_category_by_id($parent_cat[$index]->child[$i]->category_id);

                    $parent_cat[$index]->child[$i]->{'child'} = $child_cat;
                }
            }

            //$child_cat = $ci->category_model->get_category_by_id($parent_cat[$index]->child->category_id);
            //$parent_cat[$index]->child->{'child'} = $child_cat;
        }

        //var_dump($parent_cat);

        if ($type == "value") {
            return $parent_cat;
        } else {
            
            $select = '<select name="' . $name . '" id="' . $id . '" class="' . $class . '" '.$disabled_element.'>';
            
            $select .= '<option value="" selected="selected">Please select</option>';
//            if ($selected != "") {
//                $select .= '<option value="0" selected="selected">' . $first_child . '</option>';
//            } else {
//                $select .= '<option value="0">' . $first_child . '</option>';
//            }
            //$select .= '<option value="0">' . $first_child . '</option>';
            //Grand parent
            foreach ($parent_cat as $cat) {

                if ($cat->child) {
                    //$select .= '<optgroup label="' . $cat->category_name . '">';
                    //Creating grand parent with child

                    $disabled = ($parent_disabled) ? 'disabled="disabled"' : '';

                    //selected
//                    if ($selected != "") {
//                        $select .= '<option value="0" selected="selected">' . $first_child . '</option>';
//                    } else {
//                        $select .= '<option value="0">' . $first_child . '</option>';
//                    }

                    $sel =($cat->category_id==$selected)?'selected="selected"':'';
                    
                    $select .= '<option value="' . $cat->category_id .'" '.$sel. ' class="select-parent" ' . $disabled . ' >' . $cat->category_name . '</option>';

                    foreach ($cat->child as $cat_child) {
                        //echo $cat_child->category_id. ',';
                       
                        $sel =$cat_child->category_id==$selected?'selected="selected"':'';

                        //echo $sel;
                        //find child of child
                        if ($cat_child->child) {
                            
                            $select .= '<option value="' . $cat_child->category_id . '" '.$sel.' class="select-parent" >&nbsp;&nbsp;&nbsp;' . $cat_child->category_name . '</option>';

                            //Adding sub child
                            foreach ($cat_child->child as $cat_sub_sub) {
                                $sel =($cat_sub_sub->category_id==$selected)?'selected="selected"':'';
                                $select .= '<option value="' . $cat_sub_sub->category_id . '" '.$sel.'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $cat_sub_sub->category_name . '</option>';
                            }
                        } 
                        else {
                           // if ($selected == $cat_child->category_id) {
                            
                                $select .= '<option value="' . $cat_child->category_id . '" '.$sel.' >&nbsp;&nbsp;&nbsp;' . $cat_child->category_name . '</option>';

                        }
                    }

                    //$select .= '</optgroup>';
                } else {
                   // if ($selected == $cat->category_id) {
                       // $select .= '<option value="' . $cat->category_id . '" selected="selected">' . $cat->category_name . '</option>';
                    //} else {
                        $sel =($cat->category_id==$selected)?'selected="selected"':'';
                        $select .= '<option value="' . $cat->category_id . '" '.$sel.' >' . $cat->category_name . '</option>';
                   // }
                }
            }

            $select .= '<select>';

            echo $select;
        }
    }

endif;


if (!function_exists('select_chain')) :

    function select_chain() {
        $ci = & get_instance();
        $ci->load->model('category_model');

        $parent_cat = $ci->category_model->get_category_by_id();

        draw_select($parent_cat);
        //var_dump($parent_cat);
    }

    function draw_select($options = null, $name = "", $id = "item", $class = " form-control", $selcted = "") {

        $select_box = '<select  name="' . $name . '" id="' . $id . '" class="select-chain parent ' . $class . '" >';
        $select_box .= '<option value="" selected="selected">Please Select an Item</option>';
        foreach ($options as $cat) {
            $select_box .= '<option value="' . $cat->category_id . '">' . $cat->category_name . '</option>';
        }
        $select_box .= '</select>';

        echo $select_box;
    }





endif;

if (!function_exists('get_districts_list')) :

    function get_districts_list($name = "", $id = "", $class = "",$header="", $selected = "") {
        $select = '<select name="' . $name . '" id="' . $id . '" class="' . $class .'">';
        $districts = [' Bhola','Bagerhat','Bandarban','Barguna','Barishal','Bogura','Brahmanbaria','Chandpur','Chapai Nawabganj','Chattogram','Chuadanga','Cox\'s Bazar','Cumilla','Dhaka','Dinajpur','Faridpur','Feni','Gaibandha','Gazipur','Gopalganj','Habiganj','Jamalpur','Jashore','Jhalokati','Jhenaidah','Joypurhat','Khagrachhari','Khulna','Kishoreganj','Kurigram','Kushtia','Lakshmipur','Lalmonirhat','Madaripur','Magura','Manikganj','Meherpur','Moulvibazar','Munshiganj','Mymensingh','Naogaon','Narail','Narayanganj','Narsingdi','Natore','Netrokona','Nilphamari','Noakhali','Pabna','Panchagarh','Patuakhali','Pirojpur','Rajbari','Rajshahi','Rangamati','Rangpur','Satkhira','Shariatpur','Sherpur','Sirajganj','Sunamganj','Sylhet','Tangail','Thakurgaon'];
        $select .= '<option value="">'.$header.'</option>';
        foreach ($districts as $d){
            if($d==$selected){
                $select .= '<option value="' . $d . '" selected="selected">' . $d . '</option>';
            }else{
                $select .= '<option value="' . $d . '">' . $d . '</option>';
            }

        }
        $select .= '</select>';
        echo $select;
    }
endif;