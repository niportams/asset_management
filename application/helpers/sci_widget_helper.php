<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('sci_total_capitalize_asset')) {

    function sci_total_capitalize_asset() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select_sum('received_item');
        $ci->db->from('fams_capitalize');
        $ci->db->where(['receiving_office' => $ci->session->userdata('user_center')]);

        $query = $ci->db->get();
        $data = $query->result();
        if ($data[0]->received_item) {
            echo $data[0]->received_item;
        } else {
            echo 0;
        }
    }

}

if (!function_exists('sci_total_store_asset')) {

    function sci_total_store_asset() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select('*');
        $ci->db->from('fams_asset');
        $ci->db->where(['asset_status_id' => 1, 'office_id' => $ci->session->userdata('user_center')]);

        $query = $ci->db->get();
        $data = $query->num_rows();
        echo $data;
    }

}


if (!function_exists('sci_total_dead_asset')) {

    function sci_total_dead_asset() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select('*');
        $ci->db->from('fams_asset');
        $ci->db->where('office_id', $ci->session->userdata('user_center'));
        $ci->db->where_in('asset_status_id', [3, 4, 6]);

        $query = $ci->db->get();
        $data = $query->num_rows();
        echo $data;
    }

}

if (!function_exists('sci_total_allocated_asset')) {

    function sci_total_allocated_asset() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select('*');
        $ci->db->from('asset');
        $ci->db->where('asset_status_id', 5);
        $ci->db->where('office_id', $ci->session->userdata('user_center'));

        $query = $ci->db->get();
        $data = $query->num_rows();
        echo $data;
    }

}

if (!function_exists('sci_total_category')) {

    function sci_total_category() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select('*');
        $ci->db->from('fams_category');
        $ci->db->where('category_status', 1);

        $query = $ci->db->get();
        $data = $query->num_rows();
        echo $data;
    }

}

if (!function_exists('sci_asset_count_categoriwise')) {

    function sci_asset_count_categoriwise() {
        $ci = & get_instance();
        // $ci->load->database();
        $ci->db->select('fams_category.category_name as label, COUNT(fams_asset.asset_id) as value');
        $ci->db->from('fams_asset');
        $ci->db->join('fams_category', 'fams_category.category_id = fams_asset.category_id', 'left');
        $ci->db->where('fams_asset.office_id', $ci->session->userdata('user_center'));
        $ci->db->group_by('fams_asset.category_id');


        $query = $ci->db->get();
        $data = $query->result();

        return $data;
    }

}

if (!function_exists('sci_color_set')) {

    function sci_color_set() {

        $color = ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de', '#dd4b39', '#0073b7', '#001F3F', '#39CCCC', '#3D9970', '#01FF70', '#FF851B', '#F012BE', '#605ca8', '#D81B60', '#111', '#d2d6de'];

        return $color;
    }

}

if (!function_exists('sci_total_capitalize_by_month')) {

    function sci_total_capitalize_by_month() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select_sum('received_item');
        $ci->db->select('MONTH(create_date) as month');
        $ci->db->from('fams_capitalize');
        $ci->db->where(['receiving_office' => $ci->session->userdata('user_center')]);
        $ci->db->where('YEAR(create_date)', date('Y'));
        $ci->db->group_by('MONTH(create_date)');

        $query = $ci->db->get();
        $data = $query->result();
        $asset = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($data as &$value) {
            $asset[$value->month - 1] = $value->received_item;
        }
        return $asset;
    }

}

if (!function_exists('sci_total_register_by_month')) {

    function sci_total_register_by_month() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select_sum('registered_item_no');
        $ci->db->select('MONTH(create_date) as month');
        $ci->db->from('fams_capitalize');
        $ci->db->where(['receiving_office' => $ci->session->userdata('user_center')]);
        $ci->db->where('YEAR(create_date)', date('Y'));
        $ci->db->group_by('MONTH(create_date)');

        $query = $ci->db->get();
        $data = $query->result();
        $asset = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($data as &$value) {
            $asset[$value->month - 1] = $value->registered_item_no;
        }
        return $asset;
    }

}

if (!function_exists('sci_total_requisition')) {

    function sci_total_requisition() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select('COUNT(id) as no, MONTH(created_date) as month');
        $ci->db->from('fams_requisitions');
        $ci->db->where(['office_id' => $ci->session->userdata('user_center')]);
        $ci->db->group_by('MONTH(created_date)');

        $query = $ci->db->get();
        $data = $query->result();
        $requisition = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($data as &$value) {
            $requisition[$value->month - 1] = $value->no;
        }
        return $requisition;
    }

}

if (!function_exists('sci_total_delivered')) {

    function sci_total_delivered() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select('COUNT(id) as no, MONTH(created_date) as month');
        $ci->db->from('fams_requisitions');
        $ci->db->where(['office_id' => $ci->session->userdata('user_center')]);
        $ci->db->where('status', 'DELIVERED');
        $ci->db->group_by('MONTH(created_date)');

        $query = $ci->db->get();
        $data = $query->result();
        $requisition = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($data as &$value) {
            $requisition[$value->month - 1] = $value->no;
        }
        return $requisition;
    }

}

if (!function_exists('sci_total_requested_requisition')) {

    function sci_total_requested_requisition() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select_sum('quantity');
        $ci->db->from('requisitions_assets');
        $ci->db->join('requisitions', 'requisitions_assets.requisition_id = requisitions.id', 'LEFT');
        $ci->db->where('requisitions.userid', $ci->session->userdata('user_db_id'));

        $query = $ci->db->get();
        $data = $query->result();
        if ($data[0]->quantity) {
            echo $data[0]->quantity;
        } else {
            echo 0;
        }
    }

}

if (!function_exists('sci_total_request_approve')) {

    function sci_total_request_approve() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select_sum('quantity');
        $ci->db->from('requisitions_assets');
        $ci->db->join('requisitions', 'requisitions_assets.requisition_id = requisitions.id', 'LEFT');
        $ci->db->where('requisitions.userid', $ci->session->userdata('user_db_id'));
        $ci->db->where_in('requisitions_assets.requisition_state', ['Delivered', 'Processed']);


        $query = $ci->db->get();
        $data = $query->result();
        if ($data[0]->quantity) {
            echo $data[0]->quantity;
        } else {
            echo 0;
        }
    }

}

if (!function_exists('sci_total_request_declined')) {

    function sci_total_request_declined() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select_sum('quantity');
        $ci->db->from('requisitions_assets');
        $ci->db->join('requisitions', 'requisitions_assets.requisition_id = requisitions.id', 'LEFT');
        $ci->db->where('requisitions.userid', $ci->session->userdata('user_db_id'));
        $ci->db->where('requisitions_assets.requisition_state', 'Declined');

        $query = $ci->db->get();
        $data = $query->result();
        if ($data[0]->quantity) {
            echo $data[0]->quantity;
        } else {
            echo 0;
        }
    }

}

if (!function_exists('sci_total_personal_allocate_count')) {

    function sci_total_personal_allocate_count() {
        $ci = & get_instance();
        // $ci->load->database();

        $ci->db->select('*');
        $ci->db->from('allocation');
        $ci->db->where('allocate_to', $ci->session->userdata('user_db_id'));
        $ci->db->where('allocate_state', 'Delivered');

        $query = $ci->db->get();
        $data = $query->num_rows();
        echo $data;
    }

}

if (!function_exists('sci_asset_value')) {

    function sci_asset_value() {
        $ci = & get_instance();
        // $ci->load->database();
        
        $office_id=$ci->session->userdata('user_center');

        $ci->db->select_sum('purchase_price', 'total_val');
        $ci->db->from('asset');
        $ci->db->where('office_id',$office_id);
        $query = $ci->db->get();
        $result = $query->result();
        $data['total_val']= $result[0]->total_val;
        
        $ci->db->select('count(*) as total_asset');
        $ci->db->from('asset');
        $ci->db->where('office_id',$office_id);
        $query = $ci->db->get();
        $result = $query->result();
        $data['total_asset']= $result[0]->total_asset;
        
        $ci->db->select('count(*) as total_asset');
        $ci->db->from('asset');
        $ci->db->where('office_id',$office_id);
        //$ci->db->where('purchase_price>0');
        $ci->db->where(array('purchase_price >'=>'0'));
        $query = $ci->db->get();
        $result = $query->result();
        $data['total_asset_with_price']= $result[0]->total_asset;
        
        $ci->db->select('count(*) as total_asset');
        $ci->db->from('asset');
        $ci->db->where('office_id',$office_id);
        //$ci->db->where('purchase_price>0');
        $ci->db->where(array('purchase_price'=>'0'));
        $query = $ci->db->get();
        $result = $query->result();
        $data['total_asset_no_price']= $result[0]->total_asset;
        
        return $data;
        
    }

}

if (!function_exists('sci_pending_reg_asset')) {

    function sci_pending_reg_asset() {
        $ci = & get_instance();

        // $ci->load->database();
        $ci->db->select('count(*) AS total');
        $ci->db->select_sum('received_item', 'received');
        $ci->db->select_sum('registered_item_no', 'registered');
        
        $ci->db->from('capitalize');
        $ci->db->where('received_item !=','registered_item_no',FALSE);
        $ci->db->where('receiving_office',$ci->session->userdata('user_center'));
        $query = $ci->db->get();
        
        //echo $ci->db->last_query();
        $result = $query->result();
        
        return $result;
        
    }

}


if (!function_exists('sci_asset_life_expire')) {

    function sci_asset_life_expire() {
        $ci = & get_instance();
        $ci->db->select('count(*) AS total_life_expire');
        $ci->db->from('asset');
        $ci->db->like('asset_lifetime', date('Y'));
        $ci->db->where('office_id',$ci->session->userdata('user_center'));
        $query = $ci->db->get();
        
        //echo $ci->db->last_query();
        $result = $query->result();
        
        return $result;
        
    }

}


if (!function_exists('sci_asset_reg_expire')) {

    function sci_asset_reg_expire() {
        $ci = & get_instance();
        $ci->db->select('count(*) AS exp_item');
        $ci->db->from('capitalize');
        
        $ci->db->where('DATE_ADD(create_date, INTERVAL 10 DAY)>=now()');
        $ci->db->where('DATE_ADD(create_date, INTERVAL 10 DAY) <=DATE_ADD(now(), INTERVAL 10 DAY)');
        $ci->db->where('receiving_office',$ci->session->userdata('user_center'));
        $query = $ci->db->get();
        
        //echo $ci->db->last_query();
        $result = $query->result();
        
        return $result;
        
    }

}
function week_range(){
     $today = getdate();
   print_r($today);
   echo "<br/>";

   $weekStartDate = $today['mday'] - $today['wday'];
   $weekEndDate = $today['mday'] - $today['wday']+6;
   echo "<br/>";
   echo "<br/>";
   echo "week start date:".$weekStartDate;
   echo "<br/>";
   echo "week end date:".$weekEndDate;
}


//Select asset_name, now() as today, DATE_ADD(`create_date`, INTERVAL 10 DAY) as exp_date from fams_capitalize WHERE DATE_ADD(`create_date`, INTERVAL 10 DAY)>= now() and DATE_ADD(`create_date`, INTERVAL 10 DAY) <=DATE_ADD(now(), INTERVAL 10 DAY)
