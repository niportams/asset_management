<?php function asset_detail_modal(){?>    
<!-- /.modal -->
<div class="modal fade" id="asset-detail-modal" >
            <div class="modal-dialog" style="width:850px; z-index: 10060" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo load_message('ASSET_DETAIL_INFO'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div id="asset_details"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-asset_id="" class="btn btn-success pull-left single-asset-detail-print-modal"><span class="fa fa-print"></span> <?php echo load_message('PRINT');?></button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo load_message('CLOSE'); ?></button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!--    Modal for Capital Edit end here -->
<?php }?>