<?php


if (!function_exists('get_category_name')) {

    function get_category_name($id)
    {
       $all_cats = Globals::get_category_array();
        foreach ($all_cats as $cat){
           if($cat['category_id']== $id)
           {    
               echo $cat['category_name'];
               return;
               
           }
        }
    }

}

if (!function_exists('get_parent_category_name')) {

    function get_parent_category_name($child_id)
    {
         $all_cats = Globals::get_category_array();
        
        foreach ($all_cats as $cat){
           if($cat['category_id']== $child_id)
           {    
               $parent_id= $cat['parent_id'];
               get_category_name($parent_id);
               return;
               
           }
        }
    }

}
