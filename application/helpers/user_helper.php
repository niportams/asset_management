<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


 /***********Asset auto id generate**************/
  function generate_assetid($office_id, $category_id, $asset_id, $max_cat_id)
    {

        //$this->load->model('office_model','',TRUE);
        $office_prefix = get_office_prefix($office_id);

        $category_prefix = get_category_prefix($category_id);

        $asset_id = $max_cat_id + 1;
        $formated_id = $office_prefix . $category_prefix . $asset_id;
        return $formated_id;
    }
    
if (!function_exists('get_user_nice_name')) {

    function get_user_nice_name()
    {
        $ci = &get_instance();
        echo $ci->session->userdata('user_nice_name');
    }

}

if (!function_exists('get_user_email')) {

    function get_user_user_email()
    {
        $ci = &get_instance();
        echo $ci->session->userdata('user_email');
    }

}

if (!function_exists('get_user_phone')) {

    function get_user_user_phone($id)
    {
        $result = sci_select_db('fams_user', ['userid' => $id], 'phone');
        echo $result[0]->phone;
    }

}

if (!function_exists('get_user_mobile')) {

    function get_user_user_mobile($id)
    {
        $result = sci_select_db('fams_user', ['userid' => $id], 'mobile');
        echo $result[0]->mobile;
    }

}

if (!function_exists('get_user_fullname')) {

    function get_user_fullname($id)
    {
        $result = sci_select_db('fams_user', ['userid' => $id], 'fullname');
        if ($result <> false) {
            echo $result[0]->fullname;
        } else {
            $result1 = sci_select_db('fams_login', ['id' => $id], 'user_nicename');
            if ($result1 <> false) {
                echo $result1[0]->user_nicename;
            } else {
                echo "";
            }

        }

    }

}

if (!function_exists('get_user_designation')) {

    function get_user_designation($id)
    {
        $result = sci_select_db('fams_user', ['userid' => $id], 'designation');
        if ($result <> false) {
            echo $result[0]->designation;
        } else {
            echo "";
        }

    }

}

/*******  Updated By : EVANA YASMIN  *************/

if (!function_exists('get_category_lifeime')) {

    function get_category_lifeime($id)
    {
        $result = sci_select_db('fams_category', ['category_id' => $id], 'lifetime');
        if ($result <> false) {
            return $result[0]->lifetime;
        } else {
            echo "";
        }

    }

}

if (!function_exists('get_department_name')) {

    function get_department_name($id)
    {
        $result = sci_select_db('fams_department', ['department_id' => $id], 'department_name');
        if ($result <> false) {
            return $result[0]->department_name;
        } else {
            echo "";
        }

    }

}

if (!function_exists('get_format_date')) {

    function get_format_date($date)
    {
        //$create_m = date("M", strtotime($date));
        //$create_d = date("d", strtotime($date));
       // $create_y = date("Y", strtotime($date));
        
        echo $newDate = date("d-M-Y", strtotime($date));

        //echo $create_d . '-' . $create_m . '-' . $create_y;
    }

}

if (!function_exists('get_child_name')) {

    function get_child_name($category_id)
    {

        $CI = &get_instance();
        $CI->load->model('category_model');
        $result = $CI->category_model->getparent($category_id);
        return $result;
    }

}

if (!function_exists('get_office_name')) {

    function get_office_name($id)
    {
        $result = sci_select_db('fams_office', ['office_id' => $id], 'office_name');
        echo $result[0]->office_name;
    }

}

if (!function_exists('get_office_room')) {

    function get_office_room($id)
    {
        $result = sci_select_db('fams_office_room', ['room_id' => $id], 'room_name,room_location');
        if ($result <> false) {
            echo $result[0]->room_name .'('. $result[0]->room_location.')';
        } else {
            echo "";
        }

    }

}

if (!function_exists('get_office_address')) {

    function get_office_address($id)
    {
        $result = sci_select_db('fams_office', ['office_id' => $id], 'office_address');
        return $result[0]->office_address;
    }
}
if (!function_exists('get_office_prefix')) {

    function get_office_prefix($id)
    {
        $result = sci_select_db('fams_office', ['office_id' => $id], 'office_prefix');
        return $result[0]->office_prefix;
    }

}

if (!function_exists('get_user_location')) {

    function get_user_location($id)
    {
        $result = sci_select_db('fams_user', ['userid' => $id], 'posting_village, posting_upazella, posting_district,posting_postcode');
        echo $result[0]->posting_village . ',' . $result[0]->posting_upazella . ',' . $result[0]->posting_district . ',' . $result[0]->posting_postcode . ', Bangladesh';
    }

}

if (!function_exists('get_category_name')) {

    function get_category_name($id)
    {
        $result = sci_select_db('fams_category', ['category_id' => $id], 'category_name');
        if($result<>false)
        {
          echo $result[0]->category_name;  
        }
        else {
             echo "";  
        }
    }

}

if (!function_exists('get_parent_category_name')) {

    function get_parent_category_name($id)
    {
        $result = sci_select_db('fams_category', ['category_id' => $id], 'parent_id');
        get_category_name($result[0]->parent_id);
    }

}

if (!function_exists('get_asset_name')) {

    function get_asset_name($id)
    {
        $result = sci_select_db('fams_asset', ['asset_id' => $id], 'asset_name');
        echo $result[0]->asset_name;
    }
}

if (!function_exists('get_asset_status')) {

    function get_asset_status($id)
    {
        $result = sci_select_db('fams_status', ['status_id' => $id], 'status_name');
        if ($result <> false) {
            echo $result[0]->status_name;
        } else {
            echo "-";
        }
        
    }

}

if (!function_exists('get_manufacture_name')) {

    function get_manufacture_name($id)
    {
        $result = sci_select_db('fams_manufacture', ['manufacture_id' => $id], 'manufacture_name');
        if ($result <> false) {
            echo $result[0]->manufacture_name;
        } else {
            echo "-";
        }
    }
}


if (!function_exists('get_category_available')) {

    function get_category_available($category_id)
    {
        $CI = &get_instance();
        $CI->load->model('asset_model');

        $result = $CI->asset_model->category_available($category_id);

        return $result[0]->available;
    }
}

if (!function_exists('get_category_prefix')) {

    function get_category_prefix($category_id)
    {
        $CI = &get_instance();
        $CI->load->model('category_model');

        $result = $CI->category_model->category_prefix($category_id);

        return $result[0]->category_prefix;
    }

}

/**********  EVANA: End Here *************/
function get_user_db_id()
{
    $ci = &get_instance();
    echo $ci->session->userdata('user_db_id');
}

function get_user_id()
{
    $ci = &get_instance();
    echo $ci->session->userdata('user_id');
}

function get_user_login_status()
{
    $ci = &get_instance();
    return $ci->session->userdata('user_logged_in');
}

function get_user_permission()
{
    $ci = &get_instance();
    return explode(",", $ci->session->userdata('user_role'));
}

if (!function_exists('get_user_role_name')) {

    function get_user_role_name()
    {
        $ci = &get_instance();
        $result = sci_select_db('fams_role', ['role_id' => $ci->session->userdata('user_role_id')], 'role_name');
        return $result[0]->role_name;
    }

}

if (!function_exists('get_user_role_name_by_id')) {

    function get_user_role_name_by_id($id)
    {
        $ci = &get_instance();
        $result = sci_select_db('fams_role', ['role_id' => $id], 'role_name');
        return $result[0]->role_name;
    }

}