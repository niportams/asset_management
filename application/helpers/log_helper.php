<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('log_create')) 
{

    function log_create($action_name,$params)
    {
        
        $ci = &get_instance();
        date_default_timezone_set('Asia/Dhaka');
        
        $userid = $ci->session->userdata('user_db_id');
        $username = $ci->session->userdata('user_name');

		$file = FCPATH . 'application/logs/' . 'auditlog-'.date('Y-m-d').'.php';

		if (file_exists($file)) 
		{
			//echo "The file $file exists";
			file_put_contents($file, " $action_name ;" .date("Y-m-d H:i:s") . ";" . "$params ;" ."$username($userid)\n", FILE_APPEND | LOCK_EX);
		} 
		else 
		{
			//echo "The file $file does not exist";

			$file = FCPATH . 'application/logs/' . 'auditlog-'.date('Y-m-d').'.php';
			file_put_contents($file, " $action_name ;" .date("Y-m-d H:i:s") . ";" . "$params ;" ."$username($userid)\n", FILE_APPEND | LOCK_EX);
		}
		
    }

}

